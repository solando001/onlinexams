<!doctype html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>First Touch Exam Portal</title>
        <meta name="description" content="First Touch">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600" rel="stylesheet">

        <!-- Favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

        <!-- Stylesheet -->
        <link rel="stylesheet" href="css/main.min.css">

        <style type="text/css">
            #bg-picture{
                background-image: url('');
            }
        </style>
    </head>
    <body>
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        
        <header class="c-navbar">
            <a class="c-navbar__brand" href="#!">
                <img src="img/logo.png" alt="Dashboard's Logo">
            </a>
           
           <!-- Navigation items that will be collapes and toggle in small viewports -->
            <nav class="c-nav collapse" id="main-nav">
                <ul class="c-nav__list">
                    <!--<li class="c-nav__item">
                        <a class="c-nav__link" href="#!">Events</a>
                    </li>
                    <li class="c-nav__item">
                        <a class="c-nav__link" href="#!">Browse</a>
                    </li>
                    <li class="c-nav__item">
                        <a class="c-nav__link" href="#!">Your Ticket</a>
                    </li>
                    <li class="c-nav__item">
                        <div class="c-field c-field--inline has-icon-right u-hidden-up@tablet">
                            <span class="c-field__icon">
                                <i class="fa fa-search"></i> 
                            </span>
                            
                            <label class="u-hidden-visually" for="navbar-search-small">Seach</label>
                            <input class="c-input" id="navbar-search-small" type="text" placeholder="Search">
                        </div>
                    </li>-->
                </ul>
            </nav>
            <!-- // Navigation items  -->

           <!-- <div class="c-field has-icon-right c-navbar__search u-hidden-down@tablet u-ml-auto u-mr-small">
                <span class="c-field__icon">
                    <i class="fa fa-search"></i> 
                </span>
                
                <label class="u-hidden-visually" for="navbar-search">Search</label>
                <input class="c-input" id="navbar-search" type="text" placeholder="Search">
            </div> 
            
            
            <div class="c-dropdown dropdown">
                <a  class="c-avatar c-avatar--xsmall has-dropdown dropdown-toggle" href="#" id="dropdwonMenuAvatar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="c-avatar__img" src="img/avatar-72.jpg" alt="User's Profile Picture">
                </a>

                <div class="c-dropdown__menu dropdown-menu dropdown-menu-right" aria-labelledby="dropdwonMenuAvatar">
                    <a class="c-dropdown__item dropdown-item" href="#">Edit Profile</a>
                    <a class="c-dropdown__item dropdown-item" href="#">View Activity</a>
                    <a class="c-dropdown__item dropdown-item" href="#">Manage Roles</a>
                </div>
            </div>-->

            <button class="c-nav-toggle" type="button" data-toggle="collapse" data-target="#main-nav">
                <span class="c-nav-toggle__bar"></span>
                <span class="c-nav-toggle__bar"></span>
                <span class="c-nav-toggle__bar"></span>
            </button><!-- // .c-nav-toggle -->
        </header>

        <div class="container" id="bg-picture">
            <div class="row">
                <div class="col-sm-12">
                    <div class="u-mv-large u-text-center">
                        <h2 class="u-mb-xsmall">Welcome Jessica! Welcome back to the Dashboard.</h2>
                        <p class="u-text-mute u-h6">Check out your past searches and the content you’ve browsed in. <a href="#">View last results</a></p>
                    </div>
                </div>
            </div>

            <div class="row">
                 <div class="col-md-5 col-xl-3 u-mb-medium u-hidden-down@tablet">
                    <div class="c-profile-card">
                        <div class="c-profile-card__cover">
                            <img src="img/profile-card-cover.jpg" alt="Adam's profile cover">
                        </div>

                        <div class="c-profile-card__user">
                            <div class="c-profile-card__avatar">
                                <img src="img/avatar-150.jpg" alt="Adam's image">
                            </div>

                            <h4 class="c-profile-card__name">Adebayo Oladele 
                                
                            </h4>
                        </div>

                        

                        <ul class="c-profile-card__meta">
                            <li class="c-profile-card__meta-item">
                                <i class="fa fa-map-marker"></i>JSS 1
                            </li>

                            <li class="c-profile-card__meta-item">
                                <i class="fa fa-bullhorn"></i>Ilorin
                            </li>

                            <li class="c-profile-card__meta-item">
                                <i class="fa fa-inbox"></i>11 years
                            </li>
                        </ul>
                    </div><!--// .c-profile -->

                    <!-- FAQ --
                    <h4 class="u-h6 u-text-bold u-mb-xsmall">FAQ</h4>

                    <!-- DEVELOPER NOTE 
                        Modify this component to `c-list`
                    -->
                   <!-- <ul>
                        <li class="u-mb-xsmall">
                            <a class="u-text-dark u-text-small" href="#">How can I connect my bank account?</a>
                        </li>

                        <li class="u-mb-xsmall">
                            <a class="u-text-dark u-text-small" href="#">Why Dashboard doesn’t show any data?</a>
                        </li>
                        <li class="u-mb-xsmall">
                            <a class="u-text-dark u-text-small" href="#">If I change my avatar in one version will it appears in next version?</a>
                        </li>
                    </ul>
                    <a class="u-text-small u-color-blue" href="#">Visit FAQ Page</a>
-->
                </div>



                <div class="col-sm-12 col-lg-9">
                    
                                <div class="c-table-responsive@tablet">
                                    <table class="c-table u-mb-large">
                                        <caption class="c-table__title">
                                            List of Exams 
                                        </caption>
                                        <thead class="c-table__head c-table__head--slim">
                                            <tr>
                                                <th class="c-table__cell c-table__cell--head">Exam Name</th>
                                                <th class="c-table__cell c-table__cell--head">Time Allowed</th>
                                                <th class="c-table__cell c-table__cell--head">Number of Question</th>
                                                <th class="c-table__cell c-table__cell--head">
                                                    <span class="u-hidden-visually">Actions</span>
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr class="c-table__row">
                                                <td class="c-table__cell">Mathematics
                                                    <span class="u-block u-text-mute u-text-xsmall">
                                                      Teacher: Mr Morenike Amoe
                                                    </span>
                                                </td>

                                                <td class="c-table__cell">1hr 30min</td>
                                                <td class="c-table__cell">40 Questions
                                                    <span class="u-block u-text-xsmall u-text-mute">
                                                        32 Tickets remaining
                                                    </span>
                                                </td>
                                                <td class="c-table__cell u-text-right">
                                                    <a class="c-btn c-btn--success" href="#">Start Exam</a>
                                                </td>
                                            </tr>

                                            <tr class="c-table__row">
                                                <td class="c-table__cell">English Language
                                                    <span class="u-block u-text-mute u-text-xsmall">
                                                        Teacher: Mrs Amoko Alice
                                                    </span>
                                                </td>
                                                <td class="c-table__cell">1hr 20min</td>
                                                <td class="c-table__cell">
                                                    <del class="u-text-danger">Completed</del>
                                                </td>
                                                <td class="c-table__cell u-text-right">
                                                    <a class="c-btn is-disabled" href="#">Start Exam</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                </div>
            </div>                
            </div>
        </div>

        <!-- Main javascsript -->
        <script src="js/main.min.js"></script>
    </body>
</html>