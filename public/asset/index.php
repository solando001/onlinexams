<!doctype html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>First Touch Exam Portal</title>
        <meta name="description" content="First Touch">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600" rel="stylesheet">

        <!-- Favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

        <!-- Stylesheet -->
        <link rel="stylesheet" href="css/main.min.css">

        <style type="text/css">
            #bg-picture{
                background-image: url('');
            }
        </style>
    </head>
    <body>
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        
        <header class="c-navbar">
            <a class="c-navbar__brand" href="#!">
                <img src="img/logo.png" alt="Dashboard's Logo">
            </a>
           
           <!-- Navigation items that will be collapes and toggle in small viewports -->
            <nav class="c-nav collapse" id="main-nav">
                <ul class="c-nav__list">
                    <!--<li class="c-nav__item">
                        <a class="c-nav__link" href="#!">Events</a>
                    </li>
                    <li class="c-nav__item">
                        <a class="c-nav__link" href="#!">Browse</a>
                    </li>
                    <li class="c-nav__item">
                        <a class="c-nav__link" href="#!">Your Ticket</a>
                    </li>
                    <li class="c-nav__item">
                        <div class="c-field c-field--inline has-icon-right u-hidden-up@tablet">
                            <span class="c-field__icon">
                                <i class="fa fa-search"></i> 
                            </span>
                            
                            <label class="u-hidden-visually" for="navbar-search-small">Seach</label>
                            <input class="c-input" id="navbar-search-small" type="text" placeholder="Search">
                        </div>
                    </li>-->
                </ul>
            </nav>
            <!-- // Navigation items  -->

           <!-- <div class="c-field has-icon-right c-navbar__search u-hidden-down@tablet u-ml-auto u-mr-small">
                <span class="c-field__icon">
                    <i class="fa fa-search"></i> 
                </span>
                
                <label class="u-hidden-visually" for="navbar-search">Search</label>
                <input class="c-input" id="navbar-search" type="text" placeholder="Search">
            </div> 
            
            
            <div class="c-dropdown dropdown">
                <a  class="c-avatar c-avatar--xsmall has-dropdown dropdown-toggle" href="#" id="dropdwonMenuAvatar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="c-avatar__img" src="img/avatar-72.jpg" alt="User's Profile Picture">
                </a>

                <div class="c-dropdown__menu dropdown-menu dropdown-menu-right" aria-labelledby="dropdwonMenuAvatar">
                    <a class="c-dropdown__item dropdown-item" href="#">Edit Profile</a>
                    <a class="c-dropdown__item dropdown-item" href="#">View Activity</a>
                    <a class="c-dropdown__item dropdown-item" href="#">Manage Roles</a>
                </div>
            </div>-->

            <button class="c-nav-toggle" type="button" data-toggle="collapse" data-target="#main-nav">
                <span class="c-nav-toggle__bar"></span>
                <span class="c-nav-toggle__bar"></span>
                <span class="c-nav-toggle__bar"></span>
            </button><!-- // .c-nav-toggle -->
        </header>

        <div class="container" id="bg-picture">
            <div class="row">
                <div class="col-sm-12">
                    <div class="u-mv-large u-text-center">
                        <h2 class="u-mb-xsmall">Hi Jessica! Welcome back to the Dashboard.</h2>
                        <p class="u-text-mute u-h6">Check out your past searches and the content you’ve browsed in. <a href="#">View last results</a></p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-lg-4">
                    <div class="c-card u-p-medium u-text-center u-mb-medium" data-mh="landing-cards">

                        <img class="u-mb-small" src="img/icon-intro1.svg" alt="iPhone icon">

                        <h4 class="u-h6 u-text-bold u-mb-small">
                            Check your performance. See the results of all your active campaings.
                        </h4>
                        <a class="c-btn c-btn--info" href="register.html">Student Registration</a>
                    </div>
                </div>

                <div class="col-sm-12 col-lg-4">
                    <div class="c-card u-p-medium u-text-center u-mb-medium" data-mh="landing-cards">

                        <img class="u-mb-small" src="img/icon-intro2.svg" alt="iPhone icon">

                        <h4 class="u-h6 u-text-bold u-mb-small">
                            Start console and prepare new stuff for your customers or community!
                        </h4>
                        <a class="c-btn c-btn--info" href="#">View Results</a>
                    </div>
                </div>

                <div class="col-sm-12 col-lg-4">
                    <div class="c-card u-p-medium u-text-center u-mb-medium" data-mh="landing-cards">
                        <img class="u-mb-small" src="img/icon-intro3.svg" alt="iPhone icon">

                        <h4 class="u-h6 u-text-bold u-mb-small">
                            All Files ready? <br>Start promoting your apps today.
                        </h4>
                        <a class="c-btn c-btn--info" href="login.html">Login To Account</a>
                    </div>
                </div>
            </div>                
            </div>
        </div>

        <!-- Main javascsript -->
        <script src="js/main.min.js"></script>
    </body>
</html>