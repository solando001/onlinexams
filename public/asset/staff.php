<!doctype html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>User Profile | First Touch Exam Portal</title>
        <meta name="description" content="Dashboard UI Kit">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,600" rel="stylesheet">

        <!-- Favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

        <!-- Stylesheet -->
        <link rel="stylesheet" href="css/main.min.css">
    </head>
    <body class="o-page">
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <div class="o-page__sidebar js-page-sidebar">
            <div class="c-sidebar">
                <a class="c-sidebar__brand" href="#">
                    <img class="c-sidebar__brand-img" src="img/logo.png" alt="Logo"> Dashboard
                </a>
                
                <h4 class="c-sidebar__title">Dashboards</h4>
                <ul class="c-sidebar__list">

                    <li class="c-sidebar__item">
                        <a class="c-sidebar__link" href="home-overview.html">
                            <i class="fa fa-home u-mr-xsmall"></i>Overview
                        </a>
                    </li>

                    <li class="c-sidebar__item">
                        <a class="c-sidebar__link" href="performance.html">
                            <i class="fa fa-tachometer u-mr-xsmall"></i>Performance <span class="c-badge c-badge--success c-badge--xsmall u-ml-xsmall">New</span>
                        </a>
                    </li>

                    <li class="c-sidebar__item">
                        <a class="c-sidebar__link" href="analytics.html">
                            <i class="fa fa-line-chart u-mr-xsmall"></i>Analytics
                        </a>
                    </li>

                    <li class="c-sidebar__item">
                        <a class="c-sidebar__link" target="_blank" href="index.html">
                            <i class="fa fa-rocket u-mr-xsmall"></i>Landing
                        </a>
                    </li>

                    <li class="c-sidebar__item">
                        <a class="c-sidebar__link" target="_blank" href="projects.html">
                            <i class="fa fa-table u-mr-xsmall"></i>Projects
                        </a>
                    </li>
                </ul>

            </div><!-- // .c-sidebar -->
        </div><!-- // .o-page__sidebar -->

        <main class="o-page__content">
            <header class="c-navbar u-mb-medium">
                <button class="c-sidebar-toggle u-mr-small">
                    <span class="c-sidebar-toggle__bar"></span>
                    <span class="c-sidebar-toggle__bar"></span>
                    <span class="c-sidebar-toggle__bar"></span>
                </button><!-- // .c-sidebar-toggle -->

                <h2 class="c-navbar__title u-mr-auto">Your Profile</h2>
            


                <div class="c-dropdown dropdown">
                    <a  class="c-avatar c-avatar--xsmall has-dropdown dropdown-toggle" href="#" id="dropdwonMenuAvatar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="c-avatar__img" src="img/avatar-72.jpg" alt="User's Profile Picture">
                    </a>

                    <div class="c-dropdown__menu dropdown-menu dropdown-menu-right" aria-labelledby="dropdwonMenuAvatar">
                        <a class="c-dropdown__item dropdown-item" href="#">Edit Profile</a>
                        <a class="c-dropdown__item dropdown-item" href="#">View Profile</a>
                        <a class="c-dropdown__item dropdown-item" href="#">Manage Roles</a>
                         <a class="c-dropdown__item dropdown-item" href="#">Log Out</a>
                    </div>
                </div>
            </header>

            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-8 c-card u-p-medium u-mb-medium">
                          <div class="row">
                                        
                                        <div class="col-lg-6">
                                            <div class="c-field u-mb-small">
                                                <label class="c-field__label" for="firstName">First Name</label> 
                                                <input class="c-input" type="text" id="firstName" placeholder="Jason"> 
                                            </div>

                                            <div class="c-field u-mb-small">
                                                <label class="c-field__label" for="lastName">Last Name</label> 
                                                <input class="c-input" type="text" id="lastName" placeholder="Clark"> 
                                            </div>

                                            <div class="c-field u-mb-small">
                                                <label class="c-field__label" for="bio">Bio</label>
                                                <textarea class="c-input" id="bio">Jason is a tech savy Architect and Entrepreneur, maintaining a keen interest in software and design tools, such as parametric modelling (Rhino/Grasshopper) and BIM. </textarea>
                                            </div>

                                            <div class="c-field u-mb-small">
                                                <label class="c-field__label" for="email">E-mail Address</label>
                                                <input class="c-input" id="email" type="email" placeholder="jason@clark.com">
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="c-field u-mb-small">
                                                <label class="c-field__label" for="companyName">Company Name</label>
                                                <input class="c-input" id="companyName" type="text" placeholder="Dashboard Ltd.">
                                            </div>

                                            <div class="c-field u-mb-small">
                                                <label class="c-field__label" for="website">Website</label>
                                                <input class="c-input" id="website" type="text" placeholder="zawiastudio.com">
                                            </div>  

                                            <label class="c-field__label" for="socialProfile">Social Profiles</label>

                                            <div class="c-field has-addon-left u-mb-small">
                                                <span class="c-field__addon u-bg-twitter">
                                                    <i class="fa fa-twitter u-color-white"></i>
                                                </span>
                                                <input class="c-input" id="socialProfile" type="text" placeholder="Clark">
                                            </div>

                                            <div class="c-field has-addon-left">
                                                <span class="c-field__addon u-bg-facebook">
                                                    <i class="fa fa-facebook u-color-white"></i>
                                                </span>
                                                <input class="c-input" type="text" placeholder="Clark">
                                            </div>
                                        </div>
                                    </div>
                    </div>

                    <div class="col-xl-4">
                        <div class="c-card u-p-medium u-mb-medium">

                            <div class="u-text-center">
                                <div class="c-avatar c-avatar--large u-mb-small u-inline-flex">
                                    <img class="c-avatar__img" src="img/avatar-150.jpg" alt="Adam's Face">
                                </div>

                                <h3 class="u-h5">Mrs Adejoke Alao</h3>
                                <p>Freelance Designer, Previously TapQ</p>
                                <span class="u-text-mute u-text-small">London, United Kingdom</span>
                            </div>

                            <div class="u-flex u-mt-medium">
                                <a class="c-btn c-btn--info c-btn--fullwidth u-mr-xsmall" href="#">Create Exams</a>
                                <a class="c-btn c-btn--secondary c-btn--fullwidth" href="#">View Questions</a>
                            </div>

                            <table class="c-table u-text-center u-pv-small u-mt-medium u-border-right-zero u-border-left-zero">
                                <thead>
                                    <tr>
                                        <th class="u-pt-small">

                                            <div class="c-rating">
                                                <i class="c-rating__icon is-active fa fa-star"></i>
                                                <i class="c-rating__icon is-active fa fa-star"></i>
                                                <i class="c-rating__icon is-active fa fa-star"></i>
                                                <i class="c-rating__icon is-active fa fa-star"></i>
                                                <i class="c-rating__icon fa fa-star"></i>
                                            </div>
                                            
                                        </th>
                                        <th class="u-pt-small u-color-primary">38</th>
                                        <th class="u-pt-small u-color-primary">54</th>
                                        <th class="u-pt-small u-color-primary">186</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="u-text-mute u-text-xsmall u-pb-small u-text-uppercase">Rating</td>
                                        <td class="u-text-mute u-text-xsmall u-pb-small u-text-uppercase">Review</td>
                                        <td class="u-text-mute u-text-xsmall u-pb-small u-text-uppercase">Clients</td>
                                        <td class="u-text-mute u-text-xsmall u-pb-small u-text-uppercase">Finished Gigs</td>
                                    </tr>
                                </tbody>
                            </table>

                            <div class="c-feed has-icons u-mt-medium">
                                <div class="c-feed__item has-icon">
                                    <i class="c-feed__item-icon u-bg-info fa fa-tumblr"></i>
                                    <p>Product Designer</p>
                                    <span class="c-feed__meta">Tumblr- London, United Kingdom</span>
                                </div>

                                <div class="c-feed__item has-icon">
                                    <i class="c-feed__item-icon u-bg-fancy fa fa-dropbox"></i>
                                    <p>Intern</p>
                                    <p class="c-feed__meta">Dropbox - Berlin, Germany</p>
                                </div>
                            </div>

                           
                        </div>

                       
                    </div>

                </div>

            </div><!-- // .container-fluid -->
            
        </main><!-- // .o-page__content -->
        
        <script src="js/main.min.js"></script>
    </body>
</html>