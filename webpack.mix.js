let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.js('resources/assets/js/create_exam.js', 'public/js/exam');
//   .sass('resources/assets/sass/app.scss', 'public/css');

 mix.js('resources/assets/js/take_exam.js', 'public/js/exam');
