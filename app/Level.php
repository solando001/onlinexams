<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model

{
    //
    protected $table = 'levels';

    protected $fillable = [
        'name','display_name', 'description'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function students(){
        return $this->belongsToMany('App\User', 'student_level', 'level_id', 'user_id');
    }

}
