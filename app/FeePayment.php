<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeePayment extends Model
{
    protected $table = "fee_payments";

    protected $fillable = ['student_id', 'fee_id', 'amount', 'balance', 'payment_type', 'payment_mode', 'invoice_id', 'description'];

    public function fee()
    {
        return $this->belongsTo(Fee::class, 'fee_id');
    }
}
