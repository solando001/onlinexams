<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model


{
    protected $table = 'subjects';

    protected $fillable = [
        'name','display_name', 'description'
    ];

    public function students(){
        return $this->belongsToMany('App\User', 'user_subject', 'subject_id', 'user_id');
    }
}
