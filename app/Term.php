<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $table = 'term';
    protected $casts=[
        'active'=>'boolean'
    ];

    protected $fillable = ['name', 'active'];

    public static  function current() : Term{
        $term=Term::where('active',true)->first();
        if($term==null){
            $term=Term::find(1);
        }
        return $term;
    }
}
