<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassPosition extends Model
{
    protected $table = "class_positions";

    protected $fillable = ['user_id', 'student_id', 'level_id', 'session_id', 'term_id', 'total', 'position'];

    public function student()
    {
        return $this->belongsTo(User::class, 'student_id');
    }
}
