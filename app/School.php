<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $table = 'school';

    protected $fillable = ['name','value','description'];

    /**
     * @return array
     */
    public static function details()
    {
        $data = [];
        $school = School::all();
        foreach ($school as $s) {
            $data[$s->name] = $s->value;
        }
        return $data;
}

}