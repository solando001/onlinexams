<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LessonUpload extends Model
{
   protected $table = "lesson_uploads";
   protected $fillable = ['lesson_id', 'name', 'type', 'title', 'url', 'description'];

   public function lesson()
   {
       return $this->belongsTo(LessonNotes::class, 'lesson_id');
   }
}
