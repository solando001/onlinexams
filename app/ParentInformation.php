<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParentInformation extends Model
{
    protected  $table = "parent_informations";

    protected $fillable = ['user_id', 'name', 'phone', 'email', 'location', 'address', 'relationship'];

    public function children()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
