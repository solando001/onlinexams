<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected  $table = "prices";

    protected $fillable = ['user_id', 'price', 'description', 'active'];

    public function user()
    {
        return $this->hasOne('App/User', 'user_id');
    }
}
