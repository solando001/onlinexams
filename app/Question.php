<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model

{
    /**
     * @var array
     *
     */

    protected $table='questions';
    protected $fillable = ['user_id','name','ref_id','image'];

    public function user(){
        return $this->belongsTo('App\User');
    }
    public function options()
    {
        return $this->hasMany('App\QuestionOption', 'question_id');
    }
    public function userAnswers(){
        return $this->hasMany('App\ExamUserAnswer','exam_question_id');
    }
    public  function exam(){
        return $this->belongsToMany('App\Exam','exam_question','question_id','exam_id');
    }
}
