<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = "transactions";

    protected $fillable = ['amount', 'transaction_ref', 'user_id', 'successful', 'status', 'email', 'gateway', 'qty'];

//    protected $casts = [
//        'narration'=> 'array'
//    ];
}
