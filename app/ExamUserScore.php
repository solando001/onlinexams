<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamUserScore extends Model

{
    protected $table="exam_user_score";

    protected $fillable = ['user_id','exam_id','score','attempt','total_question_passed','total_question','time_completed','total_question_failed','exam_log_ref', 'term_id', 'session_id'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public  function user(){
        return $this->belongsTo('App\User');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public  function exam(){
        return $this->belongsTo('App\Exam','exam_id');
    }

    public  static  function logs($exam_log_ref){
        return ExamLog::where('ref_id',$exam_log_ref)->first();
    }
}