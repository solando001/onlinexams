<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolManagementSetting extends Model
{
    protected $table = "school_management_settings";

    protected $fillable = ['user_id', 'type', 'name', 'max_score', 'session_id', 'term_id', 'school_name'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function session()
    {
        return $this->belongsTo(Session::class, 'session_id');
    }

    public function term()
    {
        return $this->belongsTo(Term::class, 'term_id');
    }
}
