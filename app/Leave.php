<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    protected $table = 'leaves';

    protected  $fillable = ['user_id', 'uuid', 'depart', 'return', 'purpose', 'is_granted', 'who_granted', 'reason', 'status'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
