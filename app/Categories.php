<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'category';

    protected $fillable = ['name'];

    public function jobs()
    {
        return $this->belongsToMany('App\Jobs','jobs_category','category_id','job_id');
    }
}
