<?php
declare(strict_types = 1);

namespace App\Services\Grade;

use App\User;
use App\Exam;

final class CalculateScore
{
    private $exam;
    private $user;
    public  $score=0;
    public $failed;
    public $passed;
    private $attempt;
    public function __construct(Exam $exam, User $user, $attempt)
    {
        $this->exam =$exam; //Set exam variable
        $this->user=$user; //Set Score variable
        $this->attempt =$attempt;
        $this->calculate_score();

    }
    ///Exam Total Question;;
    private  function get_total_score(){
        return $this->exam->mark;
    }
    private  function get_total_question(){
        return $this->exam->total_question;
    }
    private  function mark_per_question(){
        return $this->get_total_score()/$this->get_total_question();
    }
    protected function calculate_score(){
        $user_answers =$this->exam->userAnswers()->where('user_id',$this->user->id)->where('attempt',$this->attempt)->get();
        $score=0;
        $passed=0;
        $failed=0;
        if($user_answers->count()>0){ //if user has answers
            $checker = new ExamAssessor(); //
            foreach ($user_answers as $answer){
                if($checker->assess($answer)){
                    $score +=$this->mark_per_question();
                    $passed +=1;
                }else{
                    $failed +=1;
                }
            }
        }
        $this->score =$score;
        $this->passed=$passed;
        $this->failed =$failed;
        // Update Score for this exam;
    }

}
