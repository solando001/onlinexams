<?php

namespace App\Services\Grade;

use App\Question;
use App\ExamUserAnswer;

class ExamAssessor
{

    public function assess(ExamUserAnswer $answer)
    {
        if($answer!=null){
            //no answerType
            return $this->mark_MCQ($answer); //Use Default for now
        }
        return false;
    }
    private  function mark_MCQ(ExamUserAnswer $answer){
        $question =$answer->question; //get Question
        $correct_options = $question->options->where('correct_option', true)->pluck('id')->toArray(); //get ID of correct options in an array
        $user_options = $answer->exam_answer_id; //an array of IDs

        if($user_options==$correct_options){
            return true;
        }
        return false;

    }
    private function mark_short_answer(ExamUserAnswer $answer){
    //TODO
    }

}
