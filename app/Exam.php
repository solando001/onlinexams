<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model

{
    /**
     * @var array
     *
     */
    protected $table='exams';
    protected $fillable = ['name','mark','user_id','level_id','subject_id','ref_id','time','random_question','attempt_allowed','active','total_question','is_timed', 'term_id','session_id',
    ];
    protected $casts=[
        'is_timed'=>'boolean',
        'active'=>'boolean',
        'random_question'=>'boolean',
    ];
    public function setTimeAttribute($value)
    {
        if ($value==null){
            $this->attributes['time'] = '00:00';
        }
        else{
            $this->attributes['time'] = $value;
        }

    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function scores(){
        return $this->hasMany('App\ExamUserScore','exam_id');
    }

    public function overrides(){
        return $this->hasMany('App\ExamOverride','exam_id');
    }

    public  function userAnswers(){
        return $this->hasMany('App\ExamUserAnswer','exam_id');
    }

    public  function logs(){
        return $this->hasMany('App\ExamLog','exam_id');
    }

    public  function questions(){
        return $this->belongsToMany('App\Question','exam_question','exam_id','question_id');
    }

    public  function subject(){
        return $this->hasOne('App\Subject','subject_id');
    }

    public function quizSubject()
    {
        return $this->belongsTo('App\Subject', 'subject_id');
    }

    public  function subjects(){
        return $this->belongsTo('App\Subject');
    }

    public  function level(){
        return $this->belongsTo('App\Level','level_id');
    }
}
