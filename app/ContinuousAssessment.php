<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContinuousAssessment extends Model
{
    protected $fillable = "continuous_assessment";

    protected $table = ['user_id', 'session_id', 'term_id', 'level_id', 'subject_id', 'name', 'mark'];
}
