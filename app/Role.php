<?php

namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole

{

    protected $table="roles";

    protected $fillable = [
        'name', 'display_name', 'description',
    ];

    public  function users(){
        return $this->belongsToMany('App\User','role_user','role_id','user_id');
    }



    /**public static function boot()
    {
    parent::boot();

    static::addGlobalScope('name', function (Builder $builder) {
    $builder->where('name', '!=', 'system'); ///hide system role from prying eyes
    });
    }
    public function users()
    {
        return $this->belongsToMany(Config::get('auth.providers.users.model'), Config::get('entrust.role_user_table'),Config::get('entrust.role_foreign_key'),Config::get('entrust.user_foreign_key'));
        // return $this->belongsToMany(Config::get('auth.model'), Config::get('entrust.role_user_table'));
    }**/
}
