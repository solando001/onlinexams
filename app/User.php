<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable, EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'surname', 'other_names', 'gender', 'username', 'email', 'password','active','paid'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function student_level()
    {
        return $this->belongsToMany('App\Level', 'student_level', 'user_id', 'level_id');
    }

    public function teacher_level()
    {
        return $this->belongsToMany('App\Level', 'teacher_level', 'user_id', 'level_id');
    }

    public function subjects()
    {
        return $this->belongsToMany('App\Subject', 'user_subject', 'user_id', 'subject_id')->withPivot('session_id');
    }

    public function parent()
    {
        return $this->hasOne('App\ParentInformation', 'user_id', 'id');
    }

    public function score()
    {
        return $this->hasMany(ScoreSheet::class, 'student_id', 'id');
    }

}
