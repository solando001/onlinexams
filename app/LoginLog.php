<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginLog extends Model
{
    protected $table = "login_logs";

    protected $fillable =['user_id', 'type', 'description', 'ip', 'today'];

    public function student()
    {
        return $this->belongsTo('App\User', 'user_id');
    }


}
