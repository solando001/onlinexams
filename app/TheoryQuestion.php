<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TheoryQuestion extends Model
{
    protected $table = "theory_questions";

    protected $fillable = ['ref_id', 'name', 'total_questions', 'time', 'user_id', 'level_id', 'lesson_id', 'subject_id', 'session_id', 'term_id', 'active'];

    public function questionList()
    {
        return $this->hasMany('App\TheoryQuestionDetail', 'theory_question_id');
    }

    public function examSubject()
    {
        return $this->belongsTo('App\Subject', 'subject_id');
    }

    public function teacherInfo()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function questionLevel()
    {
        return $this->belongsTo('App\Level', 'level_id');
    }

    public function lesson()
    {
        return $this->belongsTo(LessonNotes::class);
    }
}
