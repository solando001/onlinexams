<?php
namespace App\Traits;

use App\Role;
use App\User;

trait UserTrait
{
    /**
     * Create user on the database
     * @param array $data
     * @return bool
     */
    public function saveUser(array $data)
    {
        $user = User::create([
            'email' => $data['email'],
            'surname' => $data['surname'],
            'other_names' => $data['other_names'],
            'gender' => $data['gender'],
            'password' => bcrypt($data['surname']),
            'username'=> $data['username'],
            'active' => 1
        ]);
        if ($user) {
            if (isset($data['role_id'])) {
                $role = Role::find($data['role_id']);
                $role_id = $role != null && $role->name != 'system' ? $role->id : Role::where('name', 'student')->first()->id;
            } else {
                $role_id = Role::where('name', 'student')->first()->id;
            }
            $user->roles()->attach($role_id);
            return $user;
        }
        return false;
    }
}
