<?php

namespace App\Traits;


use App\Services\UploadDir;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Question;
use App\QuestionOption;
use Ramsey\Uuid\Uuid;

trait ExamQuestionTrait
{
    /**
     * @param $questions
     * @param $exam_id
     * @return bool
     */
    public function saveQuestions($questions, $exam_id)
    {
        $user_id = Auth::user()->id;
        foreach ($questions as $ques) {
            $image_path = '';
            if ($ques['image']) {
                $image_path = $this->UploadQuestionImage($ques['image']);
            }

            $question = Question::create(['name' => $ques['name'], 'user_id' => $user_id, 'ref_id' => Uuid::uuid4(), 'image' => $image_path]);

            if (count($ques['options']) > 0) {
                foreach ($ques['options'] as $option) {
                    QuestionOption::create(['option' => $option['option'], 'user_id' => $user_id, 'correct_option' => $option['correct_option'], 'question_id' => $question->id]);
                }
                $question->exam()->attach($exam_id);
            }
        }
        session(['question_count' => count($questions)]);
        return true;
    }

    /**
     * @param $imageBase64
     * @return string
     */
    public function UploadQuestionImage($imageBase64)
    {
        $image = Image::make($imageBase64)->encode('jpg');
        $image = $image->stream();
        $file_name = Uuid::uuid4() . '.' . 'jpg';
        $path = $this->getExamQuestionPath();
        $ques_path = 'public/' . $path . DIRECTORY_SEPARATOR . $file_name;
        Storage::disk('local')->put($ques_path, $image->__toString());
        return $ques_path;
    }

    /**
     * @param $image_path
     */
    private function deleteUploadedImage($image_path)
    {
        if (Storage::exists($image_path)) {
            Storage::delete($image_path);
        }

    }

    public function updateQuestion(Request $request)
    {
        if(Auth::user()->hasRole(['admin','system'])){
            $user_id = Auth::id();
            $options = $request->options;
            $question = Question::where('ref_id', $request->ref_id)->first();
            if ($question != null) {
                $question->name = $request->name; //set question's value
                $question->update();
                $newOptions = [];
                if (!empty($options) && is_array($options)) {
                    foreach ($options as $opt) {
                        //dd($opt['correct']);
                        $newOptions[] = new QuestionOption(['option' => $opt['option'], 'correct_option' => isset($opt['correct_option']) && $opt['correct_option'] == true ? true : false, 'user_id' => $user_id]);
                    }
                    $question->options()->delete(); ///Purge old options
                    $question->options()->saveMany($newOptions);
                    session('success', 'Question was updated successfully');
                    return response(['status' => true, 'msg' => 'Question was updated successfully']);
                }
            }
            return response(['status' => false, 'msg' => 'Question not found']);
        }
        return response(['status' => false, 'msg' => 'Permission Denied']);
    }

    public function deleteQuestion(Request $request)
    {
        if(Auth::user()->hasRole(['admin','system'])){
            $question = Question::where('ref_id', $request->ref_id)->first();
            if ($question != null) {
                $this->deleteUploadedImage($question->image); //purge questions
                $question->options()->delete(); ///Purge options
                $question->delete();
                session('success', 'Question was deleted successfully');
                return response(['status' => true, 'msg' => 'Question was deleted successfully']);
            }
            return response(['status' => false, 'msg' => 'Question not found']);
        }
        return response(['status' => false, 'msg' => 'Permission Denied']);
    }

    public function getExamQuestionPath()
    {
        $path = 'exam';
        if (!Storage::exists($path)) Storage::makeDirectory($path, 775);
        return $path;
    }

    public function addQuestion(Request $request)
    {
        if(Auth::user()->hasRole(['admin','system'])){
            $image_path = '';
            if ($request->image) {
                $image_path = $this->UploadQuestionImage($request->image);
            }

            $question = Question::create(['name' => $request->name, 'user_id' => Auth::id(), 'ref_id' => Uuid::uuid4(), 'image' => $image_path]);

            if (count($request->options) > 0) {
                foreach ($request->options as $option) {
                    QuestionOption::create(['option' => $option['option'], 'user_id' => Auth::id(), 'correct_option' => $option['correct_option'], 'question_id' => $question->id]);
                }
                $question->exam()->attach($request->exam_id);
            }
            return response(['status' => true, 'data' => $question->toArray(), 'msg' => 'Question was added successfully']);
        }
        return response(['status' => false, 'msg' => 'Permission Denied']);
    }
}