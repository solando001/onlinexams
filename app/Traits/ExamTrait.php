<?php
namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Exam;

trait ExamTrait
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function publish_exam(Request $request)
    {
        if (Auth::user()->hasRole(['admin', 'system', 'teacher'])) {
            $exam = Exam::where('ref_id', $request->ref_id)->first();
            if($exam){
                if(!$exam->active){
                    $exam->update(['active' => true]);
                }
                return response()->json(['status' => true, 'exam' => $exam]);
            }
            return response()->json(['status' => false, 'msg' => 'Exam not found']);
        }
        return response()->json(['status' => false, 'msg' => 'Permission denied']);
    }

    public function publish($ref_id)
    {
        if (Auth::user()->hasRole(['admin', 'system', 'teacher'])) {
            $exam = Exam::where('ref_id', $ref_id)->first();
            if($exam){
                if(!$exam->active) {
                    $exam->update(['active' => true]);
                }
                return redirect('exam')->with('success','Exam "'.$exam->name.'" was published');
            }
            return redirect('exam')->with('error','Exam not found');
        }
        return redirect('exam')->with('error','Permission Denied');
    }
}
