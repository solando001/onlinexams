<?php
/**
 * Created by PhpStorm.
 * User: Femz12
 * Date: 1/24/2017
 * Time: 9:55 PM
 */

namespace App\Traits;

use Illuminate\Http\Request;
use App\School;

trait SchoolSettingsTrait
{
    /**
     * @var array
     */
    public $updateAbleTextFields =['name','email','phone','address'];

    public function store(Request $request)
    {
        foreach ($this->updateAbleTextFields as $field){
            if($request->has($field)){
                $this->createOrUpdate(strtoupper($field), $request->$field);
            }
        }
        return back()->with('message', 'Settings were saved');
    }

    /**
     * @param $key
     * @param null $value
     * @return School
     */
    public function createOrUpdate($key, $value = null)
    {
        $setting = School::where('name', $key)->first();

        if ($setting) {  //found
            $setting->value = $value;
            $setting->update();
            return $setting;
        }
        $setting = School::create(['name' => $key, 'value' => $value]);
        return $setting;

    }
}