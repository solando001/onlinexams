<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ExamLog extends Model

{
    protected $table='exam_log';
    protected $fillable = ['user_id','exam_id','time_remaining','started','finished','ip','additional_info','ref_id','session_id'];

    protected $casts=[
        'additional_info'=>'array',
    ];
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function exam(){
        return $this->belongsTo('App\Exam','exam_id');
    }
    public static  function userAttempts(Exam $exam, $userid=null){
        if($userid==null){
            $userid =Auth::id();
        }
        $attempts =ExamLog::where('user_id',$userid)->where('exam_id',$exam->id)->get(); //Get existing attempts
        return $attempts->count();

    }

    public  static  function score($ref_id){
        return ExamUserScore::where('exam_log_ref',$ref_id)->first();
    }



}
