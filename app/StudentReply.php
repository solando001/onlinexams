<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentReply extends Model
{
    protected $table = "student_replies";

    protected $fillable = ['reply', 'lesson_id', 'user_id', 'teacher_id', 'session_id', 'term_id', 'url', 'status'];


    public function student()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function lesson()
    {
        return $this->belongsTo(LessonNotes::class);
    }
}
