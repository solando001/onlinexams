<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherReply extends Model
{
    protected $table = "teacher_replies";

    protected $fillable = ['student_id', 'teacher_id', 'lesson_id', 'reply_id', 'text', 'url', 'status'];

    public function teacher()
    {
        return $this->belongsTo(User::class, 'teacher_id');
    }

    public function lesson()
    {
        return $this->belongsTo(LessonNotes::class);
    }
}
