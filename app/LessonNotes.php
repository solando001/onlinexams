<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LessonNotes extends Model
{
    protected $table = "lesson_notes";

    protected $fillable = ['user_id', 'subject_id', 'level_id', 'session_id', 'term_id', 'title', 'lesson_note', 'active', 'count'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function level()
    {
        return $this->belongsTo(Level::class);
    }
}
