<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentScore extends Model
{
    protected $table = "student_scores";

    protected $fillable = ['user_id', 'student_id', 'level_id', 'session_id', 'term_id', 'total', 'average', 'position'];

    public function student()
    {
        return $this->belongsTo(User::class, 'student_id');
    }
}
