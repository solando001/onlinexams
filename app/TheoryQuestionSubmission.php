<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TheoryQuestionSubmission extends Model
{
    protected $table = "theory_question_submissions";

    protected $fillable = ['user_id', 'question_theory_id', 'question_detail_id', 'answer_detail','score','total'];

    public function studentInfo()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function QuestionInfo()
    {
        return $this->belongsTo('App\TheoryQuestion', 'question_theory_id');
    }
}
