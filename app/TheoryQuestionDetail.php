<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TheoryQuestionDetail extends Model
{
    protected $table = "theory_question_details";

    protected $fillable = ['theory_question_id', 'question_detail', 'mark', 'url'];
}
