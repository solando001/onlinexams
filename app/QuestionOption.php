<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionOption extends Model
{
    /**
     * @var array
     *
     */
    protected $table='question_option';
    protected $fillable = ['user_id','question_id','option','correct_option'];

    public  function user(){
        return $this->belongsTo('App\User');
    }
    public function question(){
        return $this->belongsTo('App\Question','question_id');
    }

}
