<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    //
    protected $table = 'sessions';
    protected $casts=[
        'active'=>'boolean'
    ];

     protected $fillable = ['name','active','close_date'];

    public static  function current() : Session{
        $session=Session::where('active',true)->first();
        if($session==null){
            $session=Session::find(1);
        }
        return $session;
    }

}
