<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScoreSheet extends Model
{
    protected $table = "score_sheets";

    protected $fillable = ['user_id', 'student_id', 'subject_id', 'level_id', 'session_id', 'term_id', 'ca1', 'ca2', 'prt1', 'prt2', 'exam', 'total'];

    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id');
    }

    public function student()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
