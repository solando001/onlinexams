<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamUserAnswer extends Model

{
    protected $casts = [
        'exam_answer_id' => 'array',
    ];
    protected $table="exam_user_answer";
    protected $fillable = ['exam_id','question_id','user_id','attempt','exam_answer_id','exam_answer_text','exam_log_ref'];

    public function user(){
        return $this->belongsTo('App\User');
    }
    public function exam(){
        return $this->belongsTo('App\Exam','exam_id');
    }
    public function question(){
        return $this->belongsTo('App\Question','question_id');
    }
}
