<?php
namespace App\Helpers;

use App\Services\SuperUser;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Modules\Quiz\Entities\Quiz;
use Modules\Quiz\Entities\QuizLog;
use Modules\Quiz\Entities\QuizUserScore;

class QuizValidationHelper{

    public  static  function exceededAttempt(Quiz $quiz){
        $user=Auth::user();
       if($user->hasRole(SuperUser::roles())){
            return false; //override this for super users
        }
       $override =$quiz->overrides()->where('user_id',Auth::id())->where('isUsed',0)->first();
        $attempts =QuizLog::userAttempts($quiz);
        if( $attempts > $quiz->attempt_allowed && $override==null){
            return true;
        }
        return false;
    }

    public  static function can_take_quiz(Quiz $quiz){
     $user=Auth::user();
     if($user->hasRole(SuperUser::roles())){
         true; //override this for super users
     }
     //TODO Implement later
     return true;
    }
    public static function hasExpired(Quiz $quiz,$override=null){
        $now =Carbon::now();
        $end =Carbon::parse($quiz->end);
        if($override!=null){
            if($override->has_deadline==0){ //deadline not set
                return false;
            }
           if($override->has_deadline==1){
                $end =Carbon::parse($override->expiry_date);
           }
        }
        if($end->format('Y-m-d')!=$now->format('Y-m-d') && $now->gt($end)){ //deadline exceeded
           return true;
        }
        return false;
    }
    public  static function has_questions(Quiz $quiz)
    {
        if ($quiz->questions()->count()>0) {
            return true;
        }
        elseif ($quiz->has_custom_question) {
            $course = $quiz->courses->first();
            if($course->questions()->count()>0){
                return true;
            }
            return false;
        }
        return false;
    }

    public static function calculateAverageScoreForEachSubject($subject_id, $level_id,  $term_id, $session_id){
        $no = \App\Subject::findOrFail($subject_id);
        $all_sum = \App\ScoreSheet::where('subject_id', $subject_id)->where('level_id', $level_id)
                    ->where('session_id', $session_id)->where('term_id', $term_id)->sum('total');
        return number_format($avg = $all_sum/count($no->students),1);

    }

    public static function calculateHighestScoreInSubject($subject_id, $level_id, $session_id, $term_id){
        return  \App\ScoreSheet::where('subject_id', $subject_id)->where('level_id', $level_id)
                                                ->where('session_id', $session_id)->where('term_id', $term_id)->max('total');
    }
    public static function calculateLowestScoreInSubject($subject_id, $level_id, $session_id, $term_id){
        return \App\ScoreSheet::where('subject_id', $subject_id)->where('level_id', $level_id)
                                                ->where('session_id', $session_id)->where('term_id', $term_id)->min('total');
    }

}

?>
