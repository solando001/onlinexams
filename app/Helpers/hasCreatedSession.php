<?php
declare(strict_types = 1);
if (!function_exists('hasCreatedSession')) {


    function hasCreatedSession()
    {
        $session = \App\Session::all();
        $count = count($session);
        if($count){
            return true;
        }
        return false;
    }
}