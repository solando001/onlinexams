<?php
if (! function_exists('current_session_id')) {
    /**
     * allows you to easily apply a runtime timezone value to all of your datetimes
     *
     * @param \Datetime $value
     * @param string $format
     * @param string $timezone
     * @return string
     */

    function current_session_id()
    {
        return \App\Session::current()->id;
    }

    function current_term_id()
    {
        return \App\Term::current()->id;
    }
}
if (! function_exists('selected_session_id')) {

    function selected_session_id()
    {
        if (session('selected_school_session_id')) {
            return session('selected_school_session_id');
        }

        return current_session_id();
    }
}

//Current Session
if (! function_exists('current_session')) {
    function current_session()
    {
        return \App\Session::find(selected_session_id());
    }
}



