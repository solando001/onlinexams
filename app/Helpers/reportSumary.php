<?php 

namespace App\Helpers;

class ResportSummary{
    public static function calculateAverageScoreForEachSubject($subject_id, $level_id,  $term_id, $session_id){
        $no = \App\Subject::findOrFail($subject_id);
        $all_sum = \App\ScoreSheet::where('subject_id', $subject_id)->where('level_id', $level_id)
                    ->where('session_id', $session_id)->where('term_id', $term_id)->sum('total');
        return number_format($all_sum/count($no->students),1);

    }

    public static function calculateHighestScoreInSubject($subject_id, $level_id, $session_id, $term_id){
        return  \App\ScoreSheet::where('subject_id', $subject_id)->where('level_id', $level_id)
                                                ->where('session_id', $session_id)->where('term_id', $term_id)->max('total');
    }
    public static function calculateLowestScoreInSubject($subject_id, $level_id, $session_id, $term_id){
        return \App\ScoreSheet::where('subject_id', $subject_id)->where('level_id', $level_id)
                                                ->where('session_id', $session_id)->where('term_id', $term_id)->min('total');
    }

    public static function teachersRemark($total){
        if($total >= "0"  && $total<= "30"){
            $comment = "This is a poor result, sit up next term.";
        }elseif($total >= "31"  && $total<= "50"){
            $comment = "This is a fair result, You can do well.";
        }elseif($total >= "51"  && $total<= "70"){
            $comment = "This is a good result. There is room for improvement.";
        }elseif($total >= "71"  && $total <= "100"){
            $comment = "This is a very good performance. You can do better.";
        }else{
            $comment = "This is an Excellent result, Keep it up";
        }
        return $comment;
    }
}