<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TheoryQuestionGrade extends Model
{
    protected $table = "theory_question_grades";

    protected $fillable = ['student_id', 'teacher_id', 'theory_question_id', 'total', 'grade'];

    public function student()
    {
        return $this->belongsTo('App\User', 'student_id');
    }

    public function gradeQuestion()
    {
        return $this->belongsTo('App\TheoryQuestion', 'theory_question_id');
    }
}
