<?php

namespace App\Http\Controllers;

use App\Leave;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class LeaveController extends Controller
{
    public function index()
    {

        return view('leave.index');
    }

    public function store(Request $request)
    {
        //dd($request);
       $resume = date('d-m-Y h:i:s', strtotime($request->resume));
       $depart = date('d-m-Y h:i:s', strtotime($request->depart));
        if($depart >= $resume)
        {
            return back()->with('error', 'Resumption date cannot be equal or greater than departure date!');
        }
        $uid = Uuid::uuid4();
        $leave = Leave::create([
            'uuid' => $uid,
            'user_id' => Auth::user()->id,
            'depart' => $request->depart,
            'return' => $request->resume,
            'purpose' => $request->purpose,
            'status' => 'pending',
        ]);
        return back()->with('message', 'Leave request successfully sent!');
    }

    public function all()
    {
        $leaves = Leave::where('status', 'pending')->orderBy('created_at', 'desc')->paginate(20);
        return view('leave.all', ['leaves' => $leaves]);
    }

    public function grant($id)
    {
        $find = Leave::where('uuid', $id)->first();
        $find->update(['is_granted' => true, 'status' => 'Granted']);
        return back()->with('message', 'Leave request successfully granted!');
    }

    public function deny($id)
    {
        $find = Leave::where('uuid', $id)->first();
        $find->update(['is_granted' => false, 'status' => 'Denied']);
        return back()->with('message', 'Leave request Denied!');
    }

    public function leave_denied()
    {
        $leaves = Leave::where('status', 'denied')->orderBy('created_at', 'desc')->paginate(20);
        return view('leave.denied', ['leaves' => $leaves]);
    }


    public function leave_granted()
    {
        $leaves = Leave::where('status', 'granted')->orderBy('created_at', 'desc')->paginate(20);
        return view('leave.granted', ['leaves' => $leaves]);
    }
}
