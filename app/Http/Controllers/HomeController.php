<?php

namespace App\Http\Controllers;

use App\Exam;
use App\ExamUserScore;
use App\Level;
use App\Payment;
use App\Role;
use App\School;
use App\Setting;
use App\Subject;
use App\TheoryQuestion;
use App\TheoryQuestionGrade;
use App\TheoryQuestionSubmission;
use App\Traits\SchoolSettingsTrait;
use App\User;
use App\Term;
use App\Session;
use App\Price;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    use SchoolSettingsTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        if (Auth::user()->hasRole(['admin', 'system'])) {
            return redirect()->route('admin.home');
        } elseif (Auth::user()->hasRole(['teacher'])) {
            return redirect()->route('teacher.home');
        }
        //check for institution
        $school = School::details();
        $school_name = $school['NAME'];
        if($school_name == "GIVENBIZ VARSITY"){
            return view('institution.student_home');
        }
        return redirect()->route('student.home');
    }

    public function create_student()
    {
        $levels = Level::all();
        $subjects = Subject::all();
        return view('user.create_student', ['levels' => $levels, 'subjects' => $subjects]);
    }

    public function create_class()
    {
        $classes = Level::all();
        return view('user.create_class', ['classes'=> $classes]);
    }

    public function edit_class(Request $request)
    {
        $id = $request->id;

        $edit_Level = Level::where('id', $id)->first();

        $edit_Level->update(['name' => $request->name,'display_name' => $request->display_name]);

        return back()->with(['message' => 'Class Edited Successfully']);

    }

    public function store_class(Request $request)
    {
        $des = "This is ".$request->name;
        $class = Level::create([
            'name' => $request->name,
            'display_name' => $request->display_name,
            'description' => $des
        ]);

        return back()->with(['message' => 'Class Created Successfully']);

    }

    public function admin_home()
    {
        $levels = Level::all();
        $subjects = Subject::all();

        $users = User::all();
        $role = Role::where('name','student')->first();
        $role_teacher = Role::where('name', 'teacher')->first();
        $users_count = $role->users()->count();
        $teacher_count = $role_teacher->users()->count();
        $exams = Exam::all();
        $exam_count = $exams->count();
        return view('admin.home', ['levels' => $levels, 'subjects' => $subjects,
            'users_count' => $users_count, 'teacher_count' => $teacher_count, 'exam_count' => $exam_count]);
    }

    public function teacher_home()
    {
        $levels = Level::all();
        $subjects = Subject::all();
        $users = User::all();
        return view('user.teacher_home', ['levels' => $levels, 'subjects' => $subjects, 'users' => $users]);
    }

    public function student_home()
    {
        return view('user.student_home');
    }

    public function edit_student($id)
    {
        $levels = Level::all();
        $subjects = Subject::all();
        $student = User::findOrFail($id);

        $reg_subject = $student->subjects;
        $unreg_subject = $subjects->diff($reg_subject);

        $cur_student_level = $student->student_level;

        return view('user.edit_student', [
            'subjects'=>$subjects,
            'levels'=>$levels,
            'student' => $student,
            'cur_student_level'=>$cur_student_level,
            'reg_subject' => $reg_subject,
            'unreg_subject' => $unreg_subject
            ]);
    }

    public function edit_student_detail(Request  $request)
    {
        if (Auth::user()->hasRole(['admin', 'system'])) {

            $StudentId = $request->student_id;

            $UpdateStudent = User::findOrFail($StudentId);
            $UpdateStudent->update([
                'surname' => $request->surname,
                'other_names' => $request->other_names,
                'gender' => $request->gender,
                'email' => $request->email,
            ]);

            if($UpdateStudent)
            {
                if($request->has('level_id') && !empty($request->level_id)){
                    $UpdateStudent->student_level()->attach($request->level_id);
                }

                if($request->has('subject_ids') && !empty($request->subject_ids)){
                    $UpdateStudent->subjects()->attach($request->subject_ids, array('session_id' => current_session_id()));
                }
            }

            return back()->with(['message' => 'Student Details Updated Successfully']);

        }else{
            return back()->with(['error' => 'Permission Error Occurred!']);
        }
    }

    public function edit_student_class(Request  $request)
    {
        if(empty($request->subject)){
            return back()->with(['error' => 'Please select a subject!']);
        }
        $StudentId = $request->student_id;

        $UpdateStudent = User::findOrFail($StudentId);
        if($UpdateStudent){
            if($request->has('subject') && !empty($request->subject)){
                $UpdateStudent->subjects()->detach($request->subject, array('session_id' => current_session_id()));
            }
        }
        return back()->with(['message' => 'Student Class Updated Successfully']);
        

    }

    public function delete_student($id)
    {
        $student = User::findOrFail($id);
        $student->delete();

        return back()->with(['messages' => 'Students Details Deleted Successfully']);
    }

    public function users()
    {
        $users = User::paginate(10);
        return view('user.list', ['users' => $users]);
    }

    public function create_session(){
        $termlist = Term::all();
        $sessionlist = Session::all();
        $cur_session = Session::where('active', true)->first();
        $cur_term = Term::where('active', true)->first();
        return view('user.create_session', ['termlist' => $termlist, 'sessionlist' => $sessionlist, 'cur_term' => $cur_term, 'cur_session' => $cur_session]);
    }

    public function change_term($id) {

         Term::query()->update(['active' => false]);

        $term = Term::where('id', $id)->first();
        $term->active = true;
        $term->save();

        return back()->with(['messages' => 'Term Chnaged Successfully!!']);
        
    }

    public function student_list()
    {
        if (Auth::user()->hasRole(['admin' | 'system'])) {

        }
        $users = User::paginate(10);
        //$role = Role::where('name','student');
        //$users = $role->users()->get();
        return view('user.studentlist', ['users' => $users]);
    }


    ///list of student in a specific class
    public function student_class_list()
    {

        $users = User::paginate(10);
        //$role = Role::where('name','student');
        //$users = $role->users()->get();
        return view('user.studentlist_class', ['users' => $users]);
    }

    public function exstudent(Request $request)
    {
        $create = Level::create([
            'name' => $request->name,
            'display_name' => 'This is '.$request->name,
            'description' => 'This is Class of '.$request->name
        ]);

        return back()->with(['messages' => 'Action Successful!!']);
    }

    public function level()
    {
        //return view('user.student_class');
        $levels = Level::paginate(10);
        return view('user.student_class', ['levels' => $levels]);
    }

    ///function to display student list in a class
    public function list_student($id)
    {
        $class = Level::findOrFail($id);
        $class_list = Level::all();//where('id', '>', $class->id)->get();
        $students = $class->students;
        return view('user.list_of_student', ['students'=>$students, 'class'=>$class, 'class_list' => $class_list]);
    }

    public  function  fix_class(Request $request)
    {
        if(empty($request->studentId))
        {
            return back()->with(['error' => 'Select at least One student']);
        }

        if($request->new_class_id == $request->current_class)
        {
            return back()->with(['error' => 'You cannot promote student into the same class, please select another class']);
        }

        $studentIds = $request->studentId;

        foreach ($studentIds as $std)
        {
             DB::table('student_level')->where('user_id', $std)->delete();
             DB::table('student_level')->insert(['user_id' => $std, 'level_id' => $request->new_class_id]);
        }
        $nextlevelName = Level::where('id', $request->new_class_id)->first();
        return back()->with(['success' => 'Selected Students Has Been Promoted to'. ' '.$nextlevelName->name]);

    }

    public function remove_student($id)
    {
        $student = User::findOrFail($id)->delete();
        return back()->with(['success' => 'Student Removed!!']);
    }

    //function to promote Student
    public function promote_student($id)
    {

        $level = Level::where('id', $id)->first();

        $current_level = $level->id;

        $nextLevel = $current_level + 1;

        $nextlevelName = Level::where('id', $nextLevel)->first();

        if($nextLevel!=null){
            $studentsInClass = $level->students;

            foreach ($studentsInClass as $students)
            {

                $currentLevel = DB::table('student_level')->where('user_id', $students->id)->first();
                if($currentLevel)
                {
                    $promote = DB::table('student_level')->where('user_id', $students->id)->delete();
                    DB::table('student_level')->insert([
                        'user_id' => $students->id,
                        'level_id' => $nextLevel
                    ]);
                }
            }

            return back()->with(['messages' => 'All Students Has Been Promoted to'. ' '.$nextlevelName->name]);
        }else{

            return back()->with(['error' => 'Sorry, No class to promote student to again']);
        }
        //return('Action Successful');
    }

    //function to display current student result in a class
    public function current_student_result($id)
    {
        $student = User::findOrFail($id);
        $termID = Term::where('active', '1')->first();
        $sessionID = Session::where('active', '1')->first();
        $results = ExamUserScore::where('user_id', $student->id)->where('session_id', $sessionID->id)->where('term_id', $termID->id)->get();
        return view('user.current_result',['results'=>$results, 'student'=>$student]);
    }

    public function subject()
    {
        //return view('user.student_class');
        $subjects = Subject::paginate(10);
        return view('user.student_subject', ['subjects' => $subjects]);
    }

    public function result()
    {
        $levels = Level::all();
        $subjects = Subject::all();
        $terms = Term::all();
        $sessions = Session::all();
        return view('user.result_search', ['subjects' => $subjects, 'levels' => $levels, 'terms' => $terms, 'sessions' => $sessions]);
    }

    public function search_result(Request $request)
    {
       if($request->exam_type == "obj") {
           $exam_ids = Exam::where(['session_id' => $request->session_id, 'term_id' => $request->term_id, 'subject_id' => $request->subject_id, 'level_id' => $request->level_id])->pluck('id')->toArray();
           $results = ExamUserScore::whereIn('exam_id', $exam_ids)->get();
           return view('user.results', ['scores'=>$results]);
       }else{
          $theory = TheoryQuestion::where(['session_id' => $request->session_id, 'term_id' => $request->term_id, 'subject_id' => $request->subject_id, 'level_id' => $request->level_id])
              ->pluck('id')->toArray();
          $results = TheoryQuestionGrade::whereIn('theory_question_id', $theory)->get();
       }
    }

    public function store_session(Request $request)
    {
        $user=Auth::user();
        //close current session
        $active_sessions =Session::where('active',true)->get();
        foreach ($active_sessions as $sess){
            $sess->active=false;
            $sess->close_date =Carbon::now();
            $sess->save();
        }
        $request->request->add(['active'=>true]);
        $session =Session::create($request->all());
        if($session){
            return back()->with('message','Session created successfully');
        }
    }

    public function school_setup()
    {
        return view('user.school_setup');
    }

    public function save_school_setup(Request $request)
    {
        $active_terms =Term::where('active',true)->get();
        foreach ($active_terms as $term){
            $term->active=false;
            $term->save();
        }
        $new_term = Term::find($request->term);
        if($new_term){
            $new_term->active=true;
            $new_term->save();
        }



       $session = Session::create([
           'name'=>$request->school_session,
           'active'=> true
       ]);
        foreach ($this->updateAbleTextFields as $field){
            if($request->has($field)){
                $this->createOrUpdate(strtoupper($field), $request->$field);
            }
        }
        if($session){
            return redirect('home');
        }
    }


    public function index()
    {
        $fields = $this->updateAbleTextFields;
        $data = [];
        foreach ($fields as $field) {
            $row = School::where('name', strtoupper($field))->first();
            if ($row) {
                $data[$field] = $row->value;
            }
        }

        return view('school::settings.index',$data);
    }

    public function create_price()
    {
        $price = Price::where('active', 1)->first();
        $role = Role::where('name', 'student')->first();
        $student = $role->users()->orderBy('created_at', 'desc')->get();
        return view('admin.create_price', ['price' => $price, 'student' => $student]);
    }

    public function activate()
    {
        $set = Setting::all();
        return view('admin.manage_setting', ['set' => $set]);
    }

    public function postSettings(Request $request)
    {
        $settings = Setting::create($request->all());
        $settings->save();
        return back()->with('message','Settings Added successfully');
    }

    public function del($id)
    {
        $del = Setting::findOrFail($id);
        $del->delete();
        return back()->with('message','Delete successful');
    }

    public function activate_payment($id)
    {
       $getSettings = Setting::where('id', $id)->first();

       $act = $getSettings->active;

       if($act == '1')
       {
           $new = '0';
       }else{
           $new = '1';
       }

       $getSettings->update(['active' => $new]);

        return back()->with('message','Action successfully');
    }

    public function store_price(Request $request)
    {
       $price = Price::create([
           'user_id' => Auth::user()->id,
           'price' => $request->price,
           'description' => 'Payment Price',
           'active' => 1
       ]);

       if($price)
       {
           return back()->with('message', 'Payment Price Created Successfully');
       }
    }

    public function payment()
    {
       $role = Role::where('name', 'student')->first();
       $student = $role->users()->orderBy('created_at', 'desc')->get();
       $current_session = Session::where('active', true)->first();
       $current_term = Term::where('active', true)->first();
       $payment = Payment::where('session', $current_session->name)->where('term', $current_term->name)->first();
       $price = Setting::where('active', true)->sum('amount');
        return view('admin.payment', ['price' => $price, 'student' => $student, 'current_session' => $current_session, 'current_term' => $current_term, 'payment'
        => $payment]);
    }

    public function paymentHistory()
    {
        $paymentHistory = Payment::all();
        return view('admin.payment_history', ['paymentHistory' => $paymentHistory]);
    }

    public function activateStudent($id)
    {
        $User_status = User::findOrFail($id);
        if($User_status->paid == '0')
        {
            $User_status->update(['paid' => true]);
            return back()->with('message', 'Student Login Activated');
        }else{
            $User_status->update(['paid' => false]);
            return back()->with('message', 'Student Login Deactivated');
        }
    }
}
