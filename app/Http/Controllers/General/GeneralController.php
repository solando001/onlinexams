<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GeneralController extends Controller
{
    public function landingPage()
    {
        return view('general.landing_page');
    }

    public function contactUs()
    {
        return view('general.contact_us');
    }

    public function genPass(){
        return view('auth.gen_pass');
    }

    public function getPass(Request $request){
        return back()->with(['message' => $request->surname]);
    }
}