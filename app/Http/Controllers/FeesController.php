<?php

namespace App\Http\Controllers;

use App\Fee;
use App\FeePayment;
use App\Session;
use App\Term;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FeesController extends Controller
{
    public function index()
    {
        $fees = Fee::orderBy('created_at', 'desc')->get();
        return view('fees.index', ['fees' => $fees]);
    }

    public function create()
    {
        $term = Term::all();
        $session = Session::all();
        return view('fees.create', ['terms' => $term, 'sessions' => $session]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'amount' => ['required', 'numeric']
        ]);
        $check = Fee::where('session_id', $request->session_id)->where('term_id', $request->term_id)->first();
        if($check == null)
        {
            $save = Fee::create($request->all());
            $save->save();
            return redirect('fees')->with('message', 'Fees Created Successfully');
        }
    }

    public function pay_fees()
    {
        $student = Auth::user();
        $sessionId = Session::where('active', true)->first();
        $termId = Term::where('active', true)->first();
        $current_fees = Fee::where('session_id', $sessionId->id)->first();
        $check = FeePayment::where('student_id', $student->id)->where('fee_id', $current_fees->id)->orderBy('created_at', 'desc')->first();
//        dd($current_fees);
        return view('fees.student.pay_fees', ['currentFee' => $current_fees, 'check' => $check]);
    }

    public function process_fees(Request $request)
    {
        $student = Auth::user();
        $payment_type = $request->payment_type;
        if($payment_type == "Part Payment"){
            $amount = $request->amount/2;
        }else{
            $amount = $request->amount;
        }
        $initial_amount = $request->amount;
        $balance = $initial_amount - $amount;
        $check = FeePayment::where('student_id', $student->id)->where('fee_id', $request->fee_id)->where('payment_type', 'Full Payment')->first();
        if($check == null)
        {
            FeePayment::create([
                'student_id' => $student->id,
                'fee_id' => $request->fee_id,
                'amount' => $amount,
                'balance' => $balance,
                'payment_type' => $payment_type,
                'payment_mode' => 'Online',
                'invoice_id' => str_random(7),
                'description' => $payment_type,
            ]);
        }else{
            dd('payment made before');
        }
        return back();
    }

    public function fees_list()
    {
        $feelist = FeePayment::where('student_id', Auth::user()->id)->latest()->get();
        return view('fees.student.fees_list', ['feelist' => $feelist]);
    }


}
