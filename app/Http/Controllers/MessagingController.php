<?php

namespace App\Http\Controllers;

use App\ParentInformation;
use App\Role;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class MessagingController extends Controller
{
    public function index()
    {
        return view('user.messaging.index');
    }

    public function sendMessage(Request $request)
    {
        //dd($request);
        $recipient = $request->recipient;
        if($recipient == "Parent" )
        {
            $parent = ParentInformation::all()->pluck('phone')->toArray();

            $rec = implode(',', $parent);

            ///dd($rec);

            $client = new Client();

            $res = $client->request('POST', env('TERMI_BASE_URL').'/send',
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json'
                    ],
                    'json' => [
                       "to" => $rec,
                       "from" => "IvyHill",
                       "sms" => $request->message,
                       "type" => "plain",
                       "channel" => "generic",
                       "api_key" => env('TERMI_API_KEY'),
                    ]
                ]
            );

            if($res){
                return back()->with(['message' => 'Message Sent to Parents Successfully']);
            }else{

                return back()->with(['error' => 'Error Sending Message']);
            }


        }else{

            $role_teacher = Role::where('name', 'teacher')->first();
            //$users_count = $role->users()->count();
            $teacher = $role_teacher->users()->pluck('email');
            dd($teacher);
        }
    }
}
