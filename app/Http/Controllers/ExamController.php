<?php

namespace App\Http\Controllers;

use App\Http\Requests\ExamRequest;
use App\Level;
use App\Question;
use App\QuestionOption;
use App\Exam;
use App\ExamLog;
use App\ExamUserAnswer;
use App\ExamUserScore;
use App\Services\Grade\CalculateScore;
use App\Session;
use App\Setting;
use App\Subject;
use App\Term;
use App\TheoryQuestion;
use App\TheoryQuestionDetail;
use App\TheoryQuestionGrade;
use App\TheoryQuestionSubmission;
use App\User_Subject; //Recently addes to display subject assigned to a teacher only
use App\Traits\ExamQuestionTrait;
use App\Traits\ExamTrait;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Psy\Exception\RuntimeException;
use Ramsey\Uuid\Uuid;

class ExamController extends Controller
{
    use ExamTrait, ExamQuestionTrait;

    public function index()
    {
        $settings = Setting::where('name', 'Online Exams')->where('active', true)->first();
        if($settings == null){
            return view('user.theory.settings');
        }

        if (Auth::user()->hasRole(['system', 'admin'])) {
            $session = Session::where('active', '1')->first();
            $term = Term::where('active', '1')->first();
//            $exams = Exam::);
            $exams =Exam::orderBy('created_at', 'desc')->paginate(20);
            return view('exam.admin_exam_list', ['exams' => $exams]);

        } elseif(Auth::user()->hasRole('teacher')){
            $subject_ids = Auth::user()->subjects()->get()->pluck('id')->toArray();
            $level_id = Auth::user()->teacher_level()->first()->id;
            $exams = Exam::whereIn('subject_id', $subject_ids)->where('level_id',$level_id)->latest()->paginate(20);
            return view('exam.index', ['exams' => $exams]);
        }
        
        if (Auth::user()->hasRole(['student'])) {

            if(Auth::user()->paid == 0)
            {
                return redirect('/home');
            }
            //$subject_ids = Auth::user()->subjects()->get()->pluck('id')->toArray();
            $level_id = Auth::user()->student_level()->first()->id;
            //$exams = Exam::whereIn('subject_id', $subject_ids)->where('level_id', $level_id)->paginate(20);
            $exams = Exam::where('level_id', $level_id)->where('active', 1)->paginate(30);
           // $exams = Exam::whereIn('subject_id', $subject_ids)->paginate(20)

//            $exam_score = ExamUserScore::where('user_id', Auth::user()->id)->
            return view('exam.student_exam_list', ['exams' => $exams]);
        }
        return back()->with('error', 'Permission Denied');
    }

    public function student_theory_list()
    {
        if(Auth::user()->hasRole(['student']))
        {
            if(Auth::user()->paid == 0)
            {
                return redirect('/home');
            }

            $level_id = Auth::user()->student_level()->first()->id;

            $theoryExams = TheoryQuestion::where('level_id', $level_id)->orderBy('created_at', 'desc')->where('active', 1)->paginate(30);//->paginate(10);//where('active', 1)->paginate(30);
            return view('exam.student_theory_exam_list', ['theoryExams' => $theoryExams]);
        }

    }

    public function take_theory_exam($ref_id)
    {
        $que = TheoryQuestion::where('ref_id', $ref_id)->first();

        $questionsList = $que->questionList()->get()->take(1);

        return view('exam.take_theory_exam', ['que' => $que, 'questionsList' => $questionsList]);
    }

    public function save_theory_answer(Request $request)
    {

        $request->validate([
            'answer_detail' => ['required', 'min:3', 'max:20000'],
        ]);

        $save = TheoryQuestionSubmission::create($request->all());
        $save->save();

        $question = TheoryQuestion::where('id', $request->question_theory_id)->first(); //Question Information

        $ref_id = $question->ref_id;
        $question_detail = TheoryQuestionDetail::where('id', $request->question_detail_id)->first()->id;
        $next = $question_detail + 1;

        return redirect('take/'.$ref_id.'/exam/theory/next/'.$next);
    }

    public function take_theory_exam_next($ref_id, $id)
    {

        $user = Auth::user();
        $que = TheoryQuestion::where('ref_id', $ref_id)->first();
        $questionsListNext = $que->questionList()->where('id', $id)->get()->take(1);
        $totalQuestions = $que->total_questions;
        $GetQuestionsAnswered = TheoryQuestionSubmission::where('question_theory_id', $que->id)->where('user_id', $user->id)->count();

        return view('exam.take_theory_exam_next', ['que' => $que, 'questionsListNext' => $questionsListNext,
            'totalQuestions' => $totalQuestions, 'GetQuestionsAnswered' => $GetQuestionsAnswered]);

    }

    public function save_theory_answer_student(Request $request)
    {
        $user = Auth::user();
        $question = TheoryQuestion::where('id', $request->question_id)->first();
        $sub_total = TheoryQuestionSubmission::where('question_theory_id', $request->question_id)->where('user_id', $user->id)->sum('total');
        $teacherID = $question->teacherInfo;
        $save = TheoryQuestionGrade::create([
            'student_id' => $user->id,
            'teacher_id' => $teacherID->id,
            'theory_question_id' => $question->id,
            'total' => $sub_total,
        ]);

        return redirect('student/home');

    }

    public function unpublish($ref_id)
    {
        $unpublish = Exam::where('ref_id', $ref_id)->first();
        $unpublish->update(['active' => 0]);
        return back();
        //return($ref_id);
    }

    //display the list of exams for admin
    public function show($slug)
    {
    }

    public function create()
    {
        $settings = Setting::where('name', 'Online Exams')->where('active', true)->first();
        if($settings == null){
            return view('user.theory.settings');
        }
        if (Auth::user()->hasRole(['admin', 'teacher', 'system'])) {
        return view('exam.create', ['edit' => 'false', 'ref_id' => '']);
        }
    }

    public function teacher_create()
    {
        return view('exam.create', ['edit' => 'false', 'ref_id' => '']);
    }

    public function edit($ref_id)
    {
        $exam = Exam::where('ref_id', $ref_id)->first();
        if ($exam == null) {
            abort('404');
        }
        return view('exam.create', ['edit' => 'true', 'ref_id' => $exam->ref_id]);
    }

    /**
     * @param ExamRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(ExamRequest $request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();
        $data['term_id'] = current_term_id();
        $data['session_id'] = current_session_id();


        if (!isset($request->is_timed) || $request->is_timed == false) {
            $data['is_timed'] = 0;
            $data['time'] = null;
        };

        if (count($request->questions) >= 1) {
            $ref_id = str_replace('-', '', Uuid::uuid4());
            $data['ref_id'] = $ref_id;
            $exam = Exam::create($data);
            $this->saveQuestions($request->questions, $exam->id);

            return response(['status' => true, 'data' => $exam->toArray(), 'msg' => 'Exam was created successfully']);
        }
        return response(['status' => false, 'data' => '', 'msg' => 'No question was sent to the server']);
    }

    /**
     * @param examRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function update(ExamRequest $request)
    {
        if (Auth::user()->hasRole(['admin', 'system', 'teacher'])) {
            $data = $request->all();
            $data['user_id'] = Auth::id();

            if (!isset($request->is_timed) || $request->is_timed == false) {
                $data['is_timed'] = 0;
                $data['time'] = null;
            };

            if (!isset($request->has_deadline) || $request->has_deadline == false) {
                $data['has_deadline'] = 0;
                $data['start'] = null;
                $data['end'] = null;
            };

            $exam = Exam::findOrFail($request->id); //using mass assignment method.
            $exam->update($data);
            if ($request->premium) {
                $exam->update(['premium' => true]);
            }
            return response(['status' => true, 'data' => $exam->toArray(), 'msg' => 'Exam was updated successfully']);
        }
        return response(['status' => false, 'data' => '', 'msg' => 'Permission Denied']);
    }

    /**
     * @param $id
     * @param bool $confirm
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function destroy($id)
    {
        $exam = Exam::where('ref_id', $id)->first();
        if ($exam == null) {
            $exam = Exam::findOrFail($id);
        }
        if (Auth::user()->hasRole(['admin', 'system', 'teacher'])) {
            $exam->scores()->delete();
            $exam->delete();
            return back()->with('success', 'Exam was deleted');
        }
        return back()->with('error', 'Exam could not be deleted! Permission denied!');
    }

    /**
     * @param $questions
     * @param $exam_id
     * @return bool
     */
    public function saveQuestions($questions, $exam_id)
    {
        $user_id = Auth::user()->id;
        foreach ($questions as $ques) {
            $image_path = '';
            if ($ques['image']) {
                $image_path = $this->UploadQuestionImage($ques['image']);
            }

            $question = Question::create(['name' => $ques['name'], 'user_id' => $user_id, 'ref_id' => Uuid::uuid4(), 'image' => $image_path]);

            if (count($ques['options']) > 0) {
                foreach ($ques['options'] as $option) {
                    QuestionOption::create(['option' => $option['option'], 'user_id' => $user_id, 'correct_option' => $option['correct_option'], 'question_id' => $question->id]);
                }
                $question->exam()->attach($exam_id);
            }
        }
        session(['question_count' => count($questions)]);
        return true;
    }

    public function startExam($ref_id)
    {
        $exam = Exam::where('ref_id', $ref_id)->first();
        if ($exam == null) {
            abort('404');
        }

        return view('exam.take_exam', ['exam' => $exam]);

    }

    public function getExamDetails($ref_id)
    {
        $exam = Exam::where('ref_id', $ref_id)->first();
        if ($exam == null) {
            abort('404');
        }

        return $this->prepareExamJsonObject($exam);
    }

    public function prepareExamJsonObject(Exam $exam)
    {
        $raw_questions = $this->getRawQuestions($exam);
        if ($raw_questions->count() == 0) {
            return response(['status' => false, 'error_type' => 'no_question']);
        }
        $myExam['exam'] = $this->getMyExamDetails($exam);
        $myExam['config'] = $this->getMyExamSettings($exam);
        $myExam['questions'] = $this->getMyExamQuestions($exam, $raw_questions);
        return response(['status' => true, 'data' => $myExam]);

    }

    private function getRawQuestions($exam)
    {
        if ($exam->random_question) {
            return $exam->questions()->inRandomOrder()->take($exam->total_question)->get();
        } else {
            return $exam->questions()->take($exam->total_question)->get();
        }
    }

    private function getMyExamSettings($exam)
    {
        return [
            'shuffleQuestions' => $exam->random_question ? true : false,
            'showPager' => false, 'allowBack' => true,
            'autoMove' => false, 'allowReview' => true,
            'time' => $this->convertexamTimeToSecond($exam->time),
            'is_timed' => $exam->is_timed,
            'total_question' => $exam->total_question,
            'attempt_allowed' => $exam->attempt_allowed,
            'score' => $exam->mark,
        ];

    }

    private function getMyExamDetails($exam)
    {
        return [
            'Id' => $exam->id,
            'name' => $exam->name,
            'description' => '',
            'ref_id' => $exam->ref_id
        ];

    }

    private function getMyExamQuestions($exam, $raw_questions)
    {
        $questions = [];
        foreach ($raw_questions as $ques) {
            $options = $this->getQuestionOptions($ques, $exam);
            $questions[] = ['Id' => $ques->id, 'Name' => $ques->name, 'Image' => Storage::exists($ques->image)?Storage::url($ques->image):null, 'Options' => $options, 'Answered' => []];
        }
        return $questions;
    }

    public function getQuestionOptions(Question $question, Exam $exam)
    {
        $options = [];
        if ($question->options->count() > 0) { //get options in question
            foreach ($question->options as $myOpt) {
                $examQuestionOption = ['Id' => $myOpt->id, 'QuestionId' => $question->id, 'Name' => $myOpt->option, 'IsAnswer' => ''];
                $options[] = $examQuestionOption; //pile options
            }
        }
        return $options;

    }

    private function validateExamRequest($request)
    {
        return Validator::make($request->all(), [ // validate  request params
            'exam_id' => 'required',
            'questions' => 'required',
            'log_id' => 'required', ///security feature to get unique exam log for each attempt
        ]);

    }

    public function logExamAccess(Request $request)
    {
        $exam_ref = $request->exam_id;
        $exam = Exam::where('ref_id', $exam_ref)->first();
        if ($exam == null) {
            abort('404');
        }
        $ref_id = str_replace('-', '', Uuid::uuid4());
        $log = ExamLog::create([
            'user_id' => Auth::user()->id, 'exam_id' => $exam->id, 'started' => Carbon::now(), 'ip' => $request->ip(), 'ref_id' => $ref_id, 'session_id'=>current_session_id()
        ]);
        if ($log) {
            return response(['status' => true, 'log_id' => $ref_id]);
        }


    }

    public function convertExamTimeToSecond($time)
    {

        if (preg_match('/[0-9][0-9]:[0-5][0-9]/', $time)) {
            $time = Carbon::createFromFormat('H:i', $time, 'GMT'); //convert
            return $time->getTimestamp(); //return time in seconds
        }
        return null;
    }

    public function submitExam(Request $request)
    {
        $validator = $this->validateExamRequest($request);
        //check if exam_id is valid
        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg' => 'Your exam did not pass our validation checks!']);
        }

        $exam = Exam::where('ref_id', $request->exam_id)->first();
        if ($exam == null) {
            return response(['status' => false, 'msg' => 'Exam not found']);
        }
        //get request parameters;
        $log_ref = $request->log_id;
        $exam_id = $exam->id;
        $time = isset($request->time) ? $request->time : null;

        //check if this exam was submitted by the client that started it earlier
        $log = ExamLog::where(['ref_id' => $log_ref, 'exam_id' => $exam_id])->first();
        if ($log == null) {
            return response(['status' => false, 'msg' => 'Exam submission did not pass our security check']);
        }
        $this->updateLogTable($log, $time);         //Update log table

        $questions = $request->questions; //Get Question Input field;
        $attempt = ExamLog::userAttempts($exam); ///Count attempt
        $user_attempt = $attempt;
        //Save answer
        $this->saveUserAnswer($exam, $log, $questions, $user_attempt);
        //Calculate Score
        return $this->gradeExam($exam, $log, $user_attempt); //grade exam
    }

    private function updateLogTable(examLog $log, $time)
    {
        $log->update([
            'time_remaining' => $time,
            'finished' => Carbon::now(),
            'additional_info' => ['ip' => request()->ip()]
        ]); ///Update log table

    }

    public function saveUserAnswer(Exam $exam, ExamLog $log, array $questions, $user_attempt)
    {
        foreach ($questions as $question) {
            if ($question['option_id'] != '') {
                $options = json_decode($question['option_id'], true);
                //save user's answers
                examUserAnswer::create(['exam_id' => $exam->id, 'question_id' => $question['question_id'], 'user_id' => Auth::id(), 'attempt' => $user_attempt, 'exam_answer_id' => $options, 'exam_answer_text' => '', 'exam_log_ref' => $log->ref_id]);
            }
        }
    }

    public function gradeExam(Exam $exam, ExamLog $log, int $user_attempt)
    {
        $user = User::find(Auth::id());
        $grade = new CalculateScore($exam, $user, $user_attempt); //Calculate score
        $user_score = new ExamUserScore(['user_id' => $user->id, 'score' => $grade->score, 'attempt' => $user_attempt, 'exam_id' => $exam->id, 'total_question_passed' => $grade->passed, 'total_question' => $exam->total_question, 'time_completed' => '', 'total_question_failed' => $grade->failed, 'exam_log_ref' => $log->ref_id, 'term_id'=>current_term_id(), 'session_id'=>current_session_id()
        ]);
        $user_score->save();

        //log exam
        $data = ['attempt' => $user_score->attempt, 'total' => $user_score->total_question, 'correct' => $user_score->total_question_passed, 'wrong' => $user_score->total_question_failed, 'mark' => $user_score->score];

        return response(['status' => true, 'msg' => 'Exam was submitted successfully', 'data' => $data]);
    }

    /**
     * @param $ref
     * @return \Illuminate\Http\JsonResponse
     */
    public function getExistingExamDataJson($ref)
    {
        $exam = Exam::where('ref_id', $ref)->first();
        $question = $exam->questions()->with('options')->get();
        return response()->json(['status' => true, 'exam' => $exam, 'question' => $question]);
    }

    public function getAvailableSubjectsAndLevels()
    {
        $levels = Level::all();
        $subjects = Subject::all();
        return response()->json(['status' => true, 'levels' => $levels, 'subjects' => $subjects]);
    }

    public function viewResults()
    {
        $scores = Auth::user()->scores()->get();
        return view('user.results', ['scores' => $scores]);
    }

    public function getExamInstruction($ref_id){
        $exam = Exam::where('ref_id', $ref_id)->first();
        if ($exam == null) {
            abort('404');
        }

        return view('exam.instruction', ['exam' => $exam]);
    }
}
