<?php

namespace App\Http\Controllers;

use App\LessonNotes;
use App\LessonUpload;
use App\Level;
use App\LoginLog;
use App\Role;
use App\Session;
use App\StudentReply;
use App\Subject;
use App\TeacherReply;
use App\Term;
use App\TheoryQuestion;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class LessonNoteController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create_note()
    {
        $user = Auth::user();
        if($user->hasRole('admin', 'system'))
        {
            $subject = Subject::all();
            $level = Level::all();
        }else {
            $subject = $user->subjects()->get();
            $level = $user->teacher_level()->get();
            $session = Session::where('active', true)->first();
            $term = Term::where('active', true)->first();
        }
        return view('user.class.create', ['subject' => $subject, 'level' => $level, 'session'=>$session, 'term'=>$term]);
    }

    public function store_note(Request $request)
    {

        if($request->hasFile('images')){

            $lesson = LessonNotes::create($request->all());
            $lesson->save();

            $allowedExtension = ['jpeg','png','jpg','gif','svg','mp3','mpeg','mp4','3gp','opus'];

           foreach ($request->file('images') as $image) {

               $mg = $image;
               $file = $image->getClientOriginalName();
               $filenameExtension = $image->getClientOriginalExtension();

               if($filenameExtension == 'jpg' || $filenameExtension == 'jpeg' || $filenameExtension == 'png' || $filenameExtension == 'gif' || $filenameExtension == 'svg') {

                   $check = in_array($filenameExtension, $allowedExtension);
                   if ($check) {
                       $filename = time() . $file;

                       $filePath = 'images/' . $filename;
                       Storage::disk('s3')->put($filePath, file_get_contents($mg), 'public');
                       $url = Storage::disk('s3')->url($filePath);

                       $uploads['name'] = "$filename";
                       $uploads['lesson_id'] = "$lesson->id";
                       $uploads['type'] = "image";
                       $uploads['url'] = "$url";
                       $upload = LessonUpload::create($uploads);
                   }
               }else{

                   return back()->with(['message_image' => 'Image format not supported, please upload, jpg, jpeg, png, gif, svg']);

               }
           }

            return redirect('preview/lesson/'.$lesson->id);

        }else{
            $lesson = LessonNotes::create($request->all());
            $lesson->save();
            return redirect('preview/lesson/'.$lesson->id);
        }
    }

    public function lesson_upload($id)
    {
        $subject = Auth::user()->subjects()->get();
        $level = Auth::user()->teacher_level()->get();
        $lesson = LessonNotes::where('id', $id)->first();

        $lesson_upload = LessonUpload::where('lesson_id', $lesson->id)->where('type', 'image')->get();
        $lesson_audio = LessonUpload::where('lesson_id', $lesson->id)->where('type', 'audio')->get();
        $lesson_video = LessonUpload::where('lesson_id', $lesson->id)->where('type', 'video')->get();

        return view('user.class.upload', ['lesson' => $lesson, 'subject' => $subject, 'level' => $level,
            'lesson_upload' => $lesson_upload, 'lesson_audio' => $lesson_audio, 'lesson_video' => $lesson_video]);
    }

    public function upload_audio(Request $request)
    {
        if($request->hasFile('audios')){

        foreach ($request->file('audios') as $audio) {
            $file = $audio->getClientOriginalName();
            $filenameExtension = $audio->getClientOriginalExtension();

            if ($filenameExtension == 'mp3' || $filenameExtension == 'opus') {
                //$audio->move(public_path('uploads'), $filename);
                $aud = $audio;

                $filename = time() . $file;

                $filePath = 'audios/' . $filename;
                Storage::disk('s3')->put($filePath, file_get_contents($aud), 'public');
                $url = Storage::disk('s3')->url($filePath);
                $uploads['name'] = "$filename";
                $uploads['lesson_id'] = "$request->lesson_id";
                $uploads['type'] = "audio";
                $uploads['title'] = "$request->title";
                $uploads['url'] = "$url";
                $uploads['description'] = "$request->description";
                LessonUpload::create($uploads);
                return back()->with(['message_audio' => 'Audio Files Uploaded Successfully']);

            }else{
                return back()->with(['error' => 'Audio format not supported, please upload,mp3,opus']);
            }
        }

        }else{
            dd('Data is too Heavy');
        }
        //dd($request);
    }

    public function upload_video(Request $request)
    {
        if($request->hasFile('videos')){

            foreach ($request->file('videos') as $video)
            {
                $file = $video->getClientOriginalName();
                $filenameExtension = $video->getClientOriginalExtension();

                if ($filenameExtension == 'mp4' || $filenameExtension == 'mwv' || $filenameExtension == '3gp' || $filenameExtension == 'ogg') {

                    //$audio->move(public_path('uploads'), $filename);
                    $vid = $video;

                    $filename = time() . $file;

                    $filePath = 'videos/' . $filename;
                    Storage::disk('s3')->put($filePath, file_get_contents($vid), 'public');
                    $url = Storage::disk('s3')->url($filePath);
                    $uploads['name'] = "$filename";
                    $uploads['lesson_id'] = "$request->lesson_id";
                    $uploads['type'] = "video";
                    $uploads['title'] = "$request->title";
                    $uploads['url'] = "$url";
                    $uploads['description'] = "$request->description";
                    LessonUpload::create($uploads);
                    return back()->with(['message_video' => 'Video File Uploaded Successfully']);
                }else{

                    return back()->with(['error' => 'Video format not supported, please upload,mp4,wmv,3gp,ogg']);
                }

            }

        }
    }

    public function store_uploads(Request $request)
    {
        $image = $request->file('file');
        $avatarName = $image->getClientOriginalName();
        $image->move(public_path('uploads'),$avatarName);

        $imageUpload = new Image();
        $imageUpload->filename = $avatarName;
        $imageUpload->save();
        return response()->json(['success'=>$avatarName]);
    }

    public function list_note()
    {
        $session = Session::where('active', true)->first();
        $term = Term::where('active', true)->first();

        if(Auth::user()->hasRole('teacher')) {
            $subject = Auth::user()->subjects()->get()->pluck('id')->toArray();
            $level = Auth::user()->teacher_level()->get()->pluck('id')->toArray();
            $notes = LessonNotes::WhereIn('subject_id', $subject)
                ->whereIn('level_id', $level)
               // ->where('session_id', $session->id)
                //->where('term_id', $term->id)
                ->latest()->paginate(20);
        }else{

            $subject = Subject::all()->pluck('id')->toArray();
            $level = Level::all()->pluck('id')->toArray();
            $notes = LessonNotes::WhereIn('subject_id', $subject)
                ->whereIn('level_id', $level)
                //->where('session_id', $session->id)
                //->where('term_id', $term->id)
                ->latest()->paginate(20);
        }

        return view('user.class.note_list', ['notes' => $notes]);
    }

    public function publish($id)
    {
        $publish = LessonNotes::where('id', $id)->first();
        $publish->update(['active'=>'1']);
        return back()->with(['message' => 'Lesson Note has been Published Successfully']);
    }

    public function unpublish($id)
    {
        $unpublish = LessonNotes::where('id', $id)->first();
        $unpublish->update(['active' => '0']);
        return back()->with(['message' => 'Lesson Note has been Unpublished Successfully']);
    }

    public function student_lesson_list()
    {
        if(Auth::user()->hasRole('student')) {
            if(Auth::user()->paid == 0)
            {
                return redirect('/home');
            }

            $session = Session::where('active', true)->first();
            $term = Term::where('active', true)->first();

            $subject = Auth::user()->subjects()->get()->pluck('id')->toArray();
            $level = Auth::user()->student_level()->get()->pluck('id')->toArray();

            $lessonNotes = LessonNotes::WhereIn('subject_id', $subject)
                ->whereIn('level_id', $level)
                //->where('session_id', $session->id)
               // ->where('term_id', $term->id)
                ->where('active', true)
                ->latest()->paginate(10);
            return view('user.class.student_lesson_list', ['lessonNotes' => $lessonNotes]);
        }
    }

    public function student_take_lesson($id)
    {
        if(Auth::user()->hasRole('student')) {
            $subject = Auth::user()->subjects()->get()->pluck('id')->toArray();
            $level = Auth::user()->student_level()->get()->pluck('id')->toArray();
            $lesson = LessonNotes::where('id', $id)->first();
            $count = $lesson->count;
            $lesson->update(['count' => $count + 1]);

            $theoryExamsCount = TheoryQuestion::where('lesson_id', $lesson->id)->count();

            $theoryExams = TheoryQuestion::where('lesson_id', $lesson->id)->get();

            $studentReplies = StudentReply::where('user_id', Auth::user()->id)->where('lesson_id', $lesson->id)->get();
            $teacherReplies = TeacherReply::where('student_id', Auth::user()->id)->where('lesson_id', $lesson->id)->get();

            $lesson_upload = LessonUpload::where('lesson_id', $lesson->id)->where('type', 'image')->get();
            $lesson_audio = LessonUpload::where('lesson_id', $lesson->id)->where('type', 'audio')->get();
            $lesson_video = LessonUpload::where('lesson_id', $lesson->id)->where('type', 'video')->get();
            return view('user.class.student_take_lesson',
                [
                    'lesson' => $lesson,
                    'lesson_upload' => $lesson_upload,
                    'lesson_audio' => $lesson_audio,
                    'lesson_video' => $lesson_video,
                    'theoryExamsCount' => $theoryExamsCount,
                    'theoryExams' => $theoryExams,
                    'teacherReplies' => $teacherReplies,
                    'studentReplies' => $studentReplies
                ]);
        }
    }

    public function edit_lesson($id)
    {
        $lesson = LessonNotes::findOrFail($id);
        $lessonUploadsAudio = LessonUpload::where('lesson_id', $lesson->id)->where('type', 'audio')->get();
        $lessonUploadsImage = LessonUpload::where('lesson_id', $lesson->id)->where('type', 'image')->get();
        $lessonUploadsVideo = LessonUpload::where('lesson_id', $lesson->id)->where('type', 'video')->get();

        $subject = Auth::user()->subjects()->get();//->pluck('id')->toArray();
        $level = Auth::user()->teacher_level()->get();//->pluck('id')->toArray();

        return view('user.class.edit_lesson',
            [
                'lesson' => $lesson,
                'lessonUploadsAudio' => $lessonUploadsAudio,
                'lessonUploadsImage' => $lessonUploadsImage,
                'lessonUploadsVideo' => $lessonUploadsVideo,
                'subject' => $subject,
                'level' => $level
            ]);
    }

    public function edit_note(Request $request)
    {
        if(Auth::user()->hasRole('teacher')) {
            $postEdit = LessonNotes::findOrFail($request->lesson_id);
            $postEdit->update(
                [
                    'lesson_note' => $request->lesson_note,
                    'title' => $request->title
                    //'level_id' => $request->level_id,
                    //'subject_id' => $request->subject_id,
                ]);

            return back()->with(['message' => 'Lesson Note has been Updated Successfully']);
        }
    }

    public function reply(Request $request)
    {
        $student = Auth::user()->id;
        $session = Session::where('active', true)->first()->id;
        $term = Term::where('active', true)->first()->id;

        if($request->hasFile('images')) {

            foreach ($request->file('images') as $image) {

                $mg = $image;
                $file = $image->getClientOriginalName();
                $filenameExtension = $image->getClientOriginalExtension();

                    if($filenameExtension == 'jpg' || $filenameExtension == 'jpeg' || $filenameExtension == 'png' || $filenameExtension == 'gif' || $filenameExtension == 'svg') {
                        $filename = time() . $file;

                        $filePath = 'images/' . $filename;
                        Storage::disk('s3')->put($filePath, file_get_contents($mg), 'public');

                        //$url = Storage::disk('s3')->get($storage);

                        $url = Storage::disk('s3')->url($filePath);

                        $student = Auth::user()->id;
                        $session = Session::where('active', true)->first()->id;
                        $term = Term::where('active', true)->first()->id;

                        $uploads['reply'] = "$request->text";
                        $uploads['lesson_id'] = "$request->lesson_id";
                        $uploads['user_id'] = "$student";
                        $uploads['teacher_id'] = "$request->teacher_id";
                        $uploads['session_id'] = "$session";
                        $uploads['term_id'] = "$term";
                        $uploads['user_id'] = "$student";
                        $uploads['url'] = "$url";
                        $upload = StudentReply::create($uploads);

                        return back()->with(['success' => 'Reply Sent Successfully']);

                    }else{
                        return back()->with(['error' => 'Image format is not supported']);
                    }
            }

        }else{


            $uploads['reply'] = "$request->text";
            $uploads['lesson_id'] = "$request->lesson_id";
            $uploads['user_id'] = "$student";
            $uploads['teacher_id'] = "$request->teacher_id";
            $uploads['session_id'] = "$session";
            $uploads['term_id'] = "$term";
            $uploads['user_id'] = "$student";
            $uploads['url'] = "";
            $upload = StudentReply::create($uploads);

            return back()->with(['success' => 'Reply Sent Successfully']);

        }
    }

    public function student_reply()
    {
        $user = Auth::user();
        $session = Session::where('active', true)->first()->id;
        $term = Term::where('active', true)->first()->id;

        $class = Level::all();
        $classID = $class->pluck('id')->toArray();

        if($user->hasRole(['admin', 'system'])){
            $replies = StudentReply::where('session_id', $session)
                ->where('term_id', $term)
                ->orderBy('id', 'desc')->get();
        }else {

            $users = User::all();
            $role = Role::where('name','teacher')->first();
            $teacherId = $role->users()->get()->pluck('id')->toArray();

            $replies = StudentReply::whereIn('teacher_id', $teacherId)
                ->where('session_id', $session)
                ->where('term_id', $term)
                ->orderBy('id', 'desc')->get();
        }
        return view('user.class.view_reply', ['replies' => $replies]);
    }

    public function teacher_reply(Request $request)
    {
        $teacher_reply = TeacherReply::create($request->all());
        $teacher_reply->save();

        return back()->with(['message' => 'Reply Posted Successfully']);
    }

    public function read_reply($id)
    {
        $view_reply = StudentReply::where('id', $id)->first();
        $view_reply->update(['status' => false]);
        return view('user.class.read_reply', ['view_reply' => $view_reply]);
    }

    public function delete_lesson($id)
    {
        $del = LessonNotes::where('id', $id)->first();
        $del->delete();

        return back()->with(['message' => 'Lesson Successfully Deleted']);
    }

    public function attendance($id)
    {
        $student = User::where('id', $id)->first();
        $class = $student->student_level()->first()->name;
        $today = Carbon::today();
        $attendance = LoginLog::where('user_id', $id)->where('type', 'Login')->orderBy('id', 'desc')->paginate(50);
        return view('user.class.attendance', ['attendance' => $attendance, 'student' => $student, 'class' => $class, 'today' => $today]);
    }

    public function class_attendance($id)
    {
        $level = Level::where('id', $id)->first();

        if($level == null){
            abort(404);
        }

        $student_ids = $level->students()->get()->pluck('id')->toArray();

        $list = LoginLog::whereIn('user_id', $student_ids)->orderBy('id', 'desc')->paginate(100);

        return view('user.class.class_attendance', ['list' => $list, 'level' => $level]);
    }

}
