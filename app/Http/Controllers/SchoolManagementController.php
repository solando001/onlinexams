<?php

namespace App\Http\Controllers;

use App\ClassPosition;
use App\Level;
use App\SchoolManagementSetting;
use App\ScoreSheet;
use App\Session;
use App\Setting;
use App\StudentScore;
use App\Subject;
use App\Term;
use App\User;
use Barryvdh\DomPDF\PDF as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class SchoolManagementController extends Controller
{
    public function settings()
    {
        $school_prop = \App\School::details();
        $settings = SchoolManagementSetting::where('school_name', $school_prop['NAME'])->get();
        $total = SchoolManagementSetting::where('school_name', $school_prop['NAME'])->sum('max_score');
        return view('user.school_management.settings', ['settings' => $settings, 'max_score' => $total]);
    }

    public function postSettings(Request $request)
    {
        $this->validate($request, [
            'max_score' => 'required|numeric',
            'type' => 'nullable|string',
            'name' => 'required|string'
        ]);
        $termID = Term::where('active', '1')->first();
        $sessionID = Session::where('active', '1')->first();

        $school_prop = \App\School::details();

        $data = $request->all();
        $data['session_id'] = $sessionID->id;
        $data['term_id'] = $termID->id;
        $data['school_name'] = $school_prop['NAME'];
        $save = SchoolManagementSetting::create($data);
        $save->save();
        return back()->with(['message' => 'Settings Created Successfully']);
    }

    public function class_list()
    {
        $settings = Setting::where('name', 'Report Management')->where('active', true)->first();
        if($settings == null){
            return view('user.theory.settings');
        }
        $level = Level::all();
        $subject = Subject::all();
        return view('user.school_management.class_list', ['level' => $level, 'subject' => $subject]);
    }

    public function viewClass($id)
    {
        $settings = Setting::where('name', 'Report Management')->where('active', true)->first();
        if($settings == null){
            return view('user.theory.settings');
        }
        $class = Level::findOrFail($id);
        $students = $class->students;
        $sessionId = Session::where('active', '1')->first()->id;
        $termId = Term::where('active', '1')->first()->id;
        $score_sheet_count = StudentScore::where('level_id', $class->id)->where('session_id', $sessionId)->where('term_id', $termId)->count();

        return view('user.school_management.view_class', ['students' => $students, 'level' => $class, 'sessionId' => $sessionId, 'termId' => $termId,
            'score_sheet_count' => $score_sheet_count]);
    }

    public function viewSubject($id)
    {
        $settings = Setting::where('name', 'Report Management')->where('active', true)->first();
        if($settings == null){
            return view('user.theory.settings');
        }
        $student = User::findOrFail($id);
        $subject = $student->subjects;
        $sessionId = Session::where('active', '1')->first();
        $termId = Term::where('active', '1')->first();
        $school_prop = \App\School::details();
        $school_name = $school_prop['NAME'];
        $current_exam_settings = SchoolManagementSetting::where('session_id', $sessionId->id)->where('term_id', $termId->id)->where('school_name', $school_prop['NAME'])->get();
        $checks = ScoreSheet::where('student_id', $student->id)->where('session_id', $sessionId->id)->where('term_id', $termId->id)->get();

        return view('user.school_management.upload_scores', [
            'subjects' => $subject,
            'student' => $student,
            'session' => $sessionId,
            'term' => $termId,
            'current_exam_settings' => $current_exam_settings,
            'scores' => $checks,
            'school_name' => $school_name
        ]);

    }

    public function load_subject(Request $request)
    {
        $level = $request->level_id;
        $subject = $request->subject_id;
       return redirect('school/management/'.$level.'/'.$subject.'/level');
    }

    public function loa_student($level, $subject)
    {
        $settings = Setting::where('name', 'Report Management')->where('active', true)->first();
        if($settings == null){
            return view('user.theory.settings');
        }
        $lev = Level::findOrFail($level);
        $user = $lev->students->load('subjects');
        $sub = Subject::findOrFail($subject);
        return view('user.school_management.list_student', ['subject' => $sub, 'students' => $user, 'level' => $lev]);
    }

    public function add_score(Request $request)
    {

        $request->validate([
            'ca1' => 'required|numeric',
            'ca2' => 'required|numeric',
            'exam' => 'required|numeric',
            'prt1' => 'numeric',
            'prt1' => 'numeric',
        ]);

        $ca1 = $request->ca1;
        $ca2 = $request->ca2;
        $exam = $request->exam;
        $prt1 = $request->prt1;
        $prt2 = $request->prt2;
        $subjects = $request->subject_id;
        $level_id = $request->level_id;
        $sessionId = Session::where('active', '1')->first()->id;
        $termId = Term::where('active', '1')->first()->id;
        ScoreSheet::create([
                'user_id' => Auth::user()->id,
                'student_id' => $request->student_id,
                'session_id' => $sessionId,
                'term_id' => $termId,
                'subject_id' => $subjects,
                'level_id' => $level_id,
                'ca1' => (isset($ca1))?$ca1: null,
                'ca2' => (isset($ca2))?$ca2: null,
                'prt1' => (isset($prt1))?$prt1: null,
                'prt2' => (isset($prt2))?$prt2: null,
                'exam' => (isset($exam))?$exam: null,
                'total' => $ca1+$ca2+$exam+$prt1+$prt2, //(isset($ca1[$key]))?$ca1[$key]: null
        ]);

        $checkStudentScore = StudentScore::where('level_id', $level_id)->where('session_id', $sessionId)->where('term_id', $termId)->where('student_id', $request->student_id)->first();//->get()->sortByDesc('total');
        $totalScore = ScoreSheet::where('student_id', $request->student_id)->where('level_id', $level_id)->where('session_id', $sessionId)->where('term_id', $termId)
            ->sum('total');

        if($checkStudentScore == null)
        {
            StudentScore::create([
                'user_id' => Auth::user()->id,
                'student_id' => $request->student_id,
                'level_id' => $level_id,
                'session_id' => $sessionId,
                'term_id' => $termId,
                'total' => $totalScore
            ]);
        }else{
            $checkStudentScore->update(['total' => $totalScore]);
        }

        return back()->with(['success' => 'Score Added Successfully']);
    }

    public function edit_score(Request $request)
    {
        
        $request->validate([
            'ca1' => 'required|numeric',
            'ca2' => 'required|numeric',
            'exam' => 'required|numeric',
            'prt1' => 'numeric',
            'prt1' => 'numeric',
        ]);

        $subjects = $request->subject_id;
        $level_id = $request->level_id;
        $student_od = $request->student_id;
        $sessionId = Session::where('active', '1')->first()->id;
        $termId = Term::where('active', '1')->first()->id;

        $find = ScoreSheet::where('student_id', $request->student_id)
            ->where('level_id', $request->level_id)
            ->where('session_id', $sessionId)
            ->where('term_id', $termId)
            ->where('subject_id', $request->subject_id)->first();
        $total = $request->ca1+$request->ca2+$request->exam+$request->prt1+$request->prt2;
        $find->update([
            'ca1' => $request->ca1,
            'ca2' => $request->ca2,
            'exam' => $request->exam,
            'prt1' => $request->prt1,
            'prt2' => $request->prt2,
            'total' => $total
        ]);
        $checkStudentScore = StudentScore::where('level_id', $level_id)->where('session_id', $sessionId)->where('term_id', $termId)->where('student_id', $request->student_id)->first();//->get()->sortByDesc('total');
        $totalScore = ScoreSheet::where('student_id', $request->student_id)->where('level_id', $level_id)->where('session_id', $sessionId)->where('term_id', $termId)
            ->sum('total');

        $checkStudentScore->update(['total' => $totalScore]);

        return back()->with(['success' => 'Score Edited Successfully']);
    }

    public function class_position_list($class)
    {
        $settings = Setting::where('name', 'Report Management')->where('active', true)->first();
        if($settings == null){
            return view('user.theory.settings');
        }
        $sessionId = Session::where('active', '1')->first()->id;
        $termId = Term::where('active', '1')->first()->id;
        $level = Level::findOrFail($class);
        $student = $level->students;
        $no_student = count($student);

        $compute = StudentScore::where('level_id', $class)->where('session_id', $sessionId)->where('term_id', $termId)->get()->sortByDesc('total');

        return view('user.school_management.compute_position', ['computes' => $compute, 'term' => $termId,
            'session' => $sessionId, 'class' => $class, 'level'=>$level, 'no_student' => $no_student]);
    }

    public function class_position(Request $request)
    {
        $students = $request->student_id;
        $postion = $request->position;
        $total = $request->total;
        ClassPosition::where('level_id', $request->level_id)->where('session_id', $request->session_id)->where('term_id', $request->term_id)->delete();
            foreach ($students as $key => $value)
            {
                ClassPosition::create([
                    'user_id' => Auth::user()->id,
                    'student_id' => $value,
                    'position' => (isset($postion[$key]))?$postion[$key]: null,
                    'total' => (isset($total[$key]))?$total[$key]: null,
                    'level_id' => $request->level_id,
                    'session_id' => $request->session_id,
                    'term_id' => $request->term_id,
                ]);
            }
        return back()->with('success', 'Position Computation Successful');
    }

    public function remark_settings()
    {
        return view('user.school_management.remark_settings');
    }

    public function remark_settings_post(Request $request)
    {
        dd($request);
    }

    public function print_preview()
    {

        $credit_notes = "";
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('user.result.result_summary', compact('credit_notes'));

        //$pdf->save(public_path($path));

//       $pdf = PDF::loadView('user.result.result_summary', array());
//       return $pdf->stream();

//        $pdf = App::make('dompdf');
//        $pdf->loadView('user.result.result_summary', array());
//        return $pdf->download('invoice.pdf');

//        $pdf = App::make('dompdf.wrapper');
//        $pdf->loadHTML('');
//        return $pdf->stream();
//        $data = "";
//        $pdf = PDF::loadView('user.result.result_summary', $data);
//        return $pdf->download('report_sheet.pdf');
            return $pdf->download('report_sheet.pdf');
    }
}
