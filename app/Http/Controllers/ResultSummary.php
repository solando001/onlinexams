<?php

namespace App\Http\Controllers;

use App\ClassPosition;
use App\Exam;
use App\ExamUserScore;
use App\Level;
use App\School;
use App\ScoreSheet;
use App\Session;
use App\Setting;
use App\StudentScore;
use App\Subject;
use App\Term;
use PDF;
use App\TheoryQuestion;
use App\TheoryQuestionSubmission;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ResultSummary extends Controller
{
    public function class_list()
    {
        $settings = Setting::where('name', 'Report Management')->where('active', true)->first();
        if($settings == null){
            return view('user.theory.settings');
        }
        $levels = Level::all();
        return view('user.result.class_list', ['levels' => $levels]);
    }

    public function student_list($id)
    {
        $settings = Setting::where('name', 'Report Management')->where('active', true)->first();
        if($settings == null){
            return view('user.theory.settings');
        }
       $level = Level::where('id', $id)->first();

       $studentList = $level->students;

        return view('user.result.student_list', ['studentList' => $studentList, 'level' => $level]);
    }

    public function view_summary($user, $level)
    {
        $settings = Setting::where('name', 'Report Management')->where('active', true)->first();
        if($settings == null){
            return view('user.theory.settings');
        }

        $users = User::where('id', $user)->first();
        $levels = Level::where('id', $level)->first();
        $school = School::details();
        $school_name = $school['NAME'];
        $school_logo = $school['LOGO'];
        $vision = $school['VISION'];
        $address = $school['ADDRESS'];
        $sessionId = Session::where('active', true)->first();
        $termId = Term::where('active', true)->first();
        $class = $users->student_level->last()->name;
        $subjects= $users->subjects;
        $checks = ScoreSheet::where('student_id', $users->id)->where('session_id', $sessionId->id)->where('term_id', $termId->id)->get();
       
        $student_in_class = $levels->students;
        $number_of_student = count($student_in_class);
        // $avg = number_format($checks->sum('total')/$number_of_student,1);

        $position = ClassPosition::where('student_id', $users->id)->where('level_id', $users->student_level->last()->id)
            ->where('session_id', $sessionId->id)->where('term_id', $termId->id)->first();
        $abbreviation = '';
        if($position)
        {
            $number = $position->position;
            $ends = array('th','st','nd','rd','th','th','th','th','th','th');
            if (($number %100) >= 11 && ($number%100) <= 13)
                $abbreviation = $number. 'th';
            else
                $abbreviation = $number. $ends[$number % 10];
        }
        return view('user.result.result_summary',
            [
                'users' => $users,
                'level' => $levels,
                'school' => $school_name,
                'logo' => $school_logo,
                'vision' => $vision,
                'address' => $address,
                'session' => $sessionId,
                'term' => $termId,
                //'total' => $total,
                'class' => $class,
                'subjects' => $subjects,
                'scores' => $checks,
                'no_of_student' => $number_of_student,
                'position' => $abbreviation
            ]);
    }

    public function check_result()
    {
        $user = Auth::user();
        $session = Session::where('active', true)->first();
        $term = Term::where('active', true)->first();
        //$subject = Subject::where('id', $id)->first();
        $level = $user->student_level()->orderBy('created_at', 'desc')->first();//get();
        $school = School::details();
        $school_name = $school['NAME'];
        $school_logo = $school['LOGO'];
        $vision = $school['VISION'];
        $address = $school['ADDRESS'];
        $scores = ScoreSheet::where('student_id', $user->id)->where('session_id', $session->id)->where('term_id', $term->id)->get();
        $total = ScoreSheet::where('student_id', $user->id)->where('session_id', $session->id)->where('term_id', $term->id)->sum('total');
        //dd($total);
        $student_in_class = $level->students;
        $number_of_student = count($student_in_class);
        $avg = number_format($total/$number_of_student,1);
        $subjects= $user->subjects;
        $class = $user->student_level->last()->name;

        $position = ClassPosition::where('student_id', $user->id)->where('level_id', $user->student_level->last()->id)
            ->where('session_id', $session->id)->where('term_id', $term->id)->first();
        $abbreviation = '';
        if($position)
        {
            $number = $position->position;
            $ends = array('th','st','nd','rd','th','th','th','th','th','th');
            if (($number %100) >= 11 && ($number%100) <= 13)
                $abbreviation = $number. 'th';
            else
                $abbreviation = $number. $ends[$number % 10];
        }

        return view('user.result.student_result', [
            'users'=>$user,
            'level' => $level,
            'session' => $session,
            'term' => $term,
            'school' => $school_name,
            'logo' => $school_logo,
            'vision' => $vision,
            'address' => $address,
            'scores' => $scores,
            'total' => $total,
            'class' => $class,
            'subjects' => $subjects,
            'no_of_student' => $number_of_student,
            'position' => $abbreviation
        ]);


    }

    public function current_result($id)
    {
        $student = Auth::user();
        $session = Session::where('active', true)->first();
        $term = Term::where('active', true)->first();
        $subject = Subject::where('id', $id)->first();
        $level = $student->student_level()->first();
        ///object results
        $examList = Exam::where('subject_id', $id)
            ->where('level_id', $level->id)
            ->where('session_id', $session->id)
            ->where('term_id', $term->id)
            ->get()->pluck('id')->toArray();

        $objScore = ExamUserScore::whereIn('exam_id', $examList)->where('user_id', $student->id)
            ->where('session_id', $session->id)
            ->where('term_id', $term->id)
            ->max('score');

        ///theory results
        $theoryExam = TheoryQuestion::where('subject_id', $id)
            ->where('level_id', $level->id)
            ->where('session_id', $session->id)
            ->where('term_id', $term->id)
            ->get()->pluck('id')->toArray();

        $therySubmissionScore = TheoryQuestionSubmission::whereIn('question_theory_id', $theoryExam)
            ->where('user_id', $student->id)->sum('score');
        $therySubmission = TheoryQuestionSubmission::whereIn('question_theory_id', $theoryExam)
            ->where('user_id', $student->id)->sum('total');

        return view('user.result.student_result', [
            'subject'=>$subject,
            'objScore' => $objScore,
            'session' => $session,
            'term' => $term,
            'therySubmissionScore' => $therySubmissionScore,
            'therySubmission' => $therySubmission
        ]);
    }
}
