<?php

namespace App\Http\Controllers;


use App\Level;
use App\Role;
use App\Setting;
use App\Subject;
use App\Traits\UserTrait;
use App\User;
use App\Price;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserRequest;



class AdminController extends Controller
{
    use UserTrait;


    public function create_student()
    {
        $settings_theory = Setting::where('name', 'Students')->where('active', true)->first();

        if($settings_theory != null) {
            $levels = Level::all();
            $subjects = Subject::all();
            return view('user.create_student', ['levels' => $levels, 'subjects' => $subjects]);
        }else{

            return view('user.theory.settings_student');
        }
    }

    /**
     * this route is used for creating a teacher in a school
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function create_teacher()
    {
        $levels = Level::all();
        $subjects = Subject::all();
        return view('admin.create_teacher', ['levels'=>$levels, 'subjects'=>$subjects]);
    }

    /**
     * List of teachers for Admin
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function list_teachers()
    {
    	$levels =Level::all();
    	$subjects = Subject::all();
    	$role = Role::where('name','teacher')->first();  //TODO better to use config in this case string teacher should be a variable
        $teachers = $role->users()->paginate(100);
     return view('admin.teacher_list', ['levels'=>$levels, 'subjects'=>$subjects, 'teachers'=>$teachers]);
    }

    /**
     Function to remove teacher information
     */
    public function remove_teacher($id)
    {
        $remove_teacher = User::find($id);
        $remove_teacher->delete();

        return back()->with(['message' => 'A Teacher Record Successfully Removed']);

        //return redirect('/teacher/list')->with('success', 'Teacher has been Removed Successfully');
    }

    /**
     * function to view edit teacher information
     */

    public function edit_teacher($id)
    {
        $edit_teacher = User::find($id);
        $levels = Level::all();
        $subjects = Subject::all();
        return view('admin.teacher_edit', ['edit_teacher' => $edit_teacher, 'levels' => $levels, 'subjects' => $subjects]);
    }


    /**
     * Function for the view of the admin create
     */

    public function create_admin_acount()
    {
        $levels = Level::all();
        $subjects = Subject::all();
        return view('admin.admin_create', ['levels' => $levels, 'subjects' => $subjects]);
    }


    /**
     * Store teachers only for admin
     * @param UserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function store_teacher(UserRequest $request)
    {
        if (Auth::user()->hasRole(['admin', 'system'])) {

            if ($request->has('role_id')) {
                $role = Role::find($request->role_id);
                $role_id = $role != null && $role->name != 'system' ? $role->id : Role::where('name', 'teacher')->first()->id;
            } else {
                $role_id = Role::where('name', 'teacher')->first()->id;
            }
            $data = [
                'email' => $request->email,
                'surname' => $request->surname,
                'username'=> 'u'.str_random(5),
                'other_names' => $request->other_names,
                'gender' => $request->gender,
                'role_id' => $role_id,
            ];
            $user = $this->saveUser($data);


            if ($user) {
                // Assign level to user
                if($request->has('level_id') && !empty($request->level_id)){
                    $user->teacher_level()->attach($request->level_id);
                }

                if($request->has('subject_ids') && !empty($request->subject_ids)){
                    $user->subjects()->attach($request->subject_ids, array('session_id' => current_session_id()));
                }

                 return back()->with(['message' => 'Record was Created Successfully']);

            }
        }
        return back()->with(['error' => 'Permission Error Occurred!']);
    }


    public function store_admin(UserRequest $request)
    {
        if (Auth::user()->hasRole(['system'])) {
                $role_id = Role::where('name', 'admin')->first()->id;
                $data = [
                'email' => $request->email,
                'surname' => $request->surname,
                'username'=> 'u'.str_random(5),
                'other_names' => $request->other_names,
                'gender' => $request->gender,
                'role_id' => $role_id,
            ];
            $user = $this->saveUser($data);


            if ($user) {
                 return back()->with(['message' => 'Admin was Created Successfully']);
            }
        }
        return back()->with(['error' => 'Permission Error Occurred!']);
    }


    /**

*   function to store student details
     */


    public function store_student(UserRequest $request)
    {
        if (Auth::user()->hasRole(['admin', 'system', 'teacher'])) {
            if ($request->has('role_id')) {
                $role = Role::find($request->role_id);
                $role_id = $role != null && $role->name != 'system' ? $role->id : Role::where('name', 'student')->first()->id;
            } else {
                $role_id = Role::where('name', 'student')->first()->id;
            }
            $data = [
                'email' => $request->email,
                'surname' => $request->surname,
                'username'=> 'u'.str_random(5),
                'other_names' => $request->other_names,
                'gender' => $request->gender,
                'role_id' => $role_id,
            ];
            $user = $this->saveUser($data);


            if ($user) {
                // Assign level to user
                if($request->has('level_id') && !empty($request->level_id)){
                    $user->student_level()->attach($request->level_id);
                }

                if($request->has('subject_ids') && !empty($request->subject_ids)){
                    $user->subjects()->attach($request->subject_ids, array('session_id' => current_session_id()));
                }

                return back()->with(['message' => 'Student Record was Created Successfully']);
            }
        }
        return back()->with(['error' => 'Permission Error Occurred!']);
    }



}
