<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\LoginLog;
use App\Session;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected function authenticated(Request $request, $user)
    {
        if ( $user->hasRole('admin') && !$this->hasCreatedSession()) {
            return redirect()->route('setup');
//        }elseif($user->hasRole('student') && $this->studentPaid()){
        }elseif($user->hasRole('student')){
            $this->LoginLogStudent();
            return redirect('/home');
//        }elseif($user->hasRole('student') && !$this->studentPaid()){
//            $this->sendOut();
//            return view('user.slack');
            //return redirect('/logout');
        }else{
            return redirect('/home');
        }
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function hasCreatedSession()
    {
        $session = Session::all();
        $count = count($session);
        if ($count) {
            return true;
        }
        return false;
    }

    public function LoginLogStudent()
    {
        $today = Carbon::today();
        $check = LoginLog::where('user_id', Auth::user()->id)->where('today', $today)->first();
            $logs = LoginLog::create([
                'user_id' => Auth::user()->id,
                'type' => 'Login',
                'description' => 'Student Login',
                'ip' => \Request::ip(),
                'today' => $today
            ]);

            if($logs){
                return true;
            }else{
                return false;
            }
    }

    public function studentPaid()
    {
        $checkPaid = User::where('id', Auth::user()->id)->first();
        if($checkPaid->paid == true)
        {
            return true;
        }else{
            return false;
        }
    }

    public function sendOut(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/');
    }


}
