<?php

namespace App\Http\Controllers;

use App\User;
use App\Level;
use App\Subject;
use App\Role;
use DB;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class TeacherController extends Controller
{
    public function student_list()
    {
        $role = Role::where('name', 'student')->first();// Get student role
        $subject_ids = Auth::user()->subjects()->get()->pluck('id')->toArray();
        //dd($subject_ids);
        $level_id[] = Auth::user()->teacher_level()->first()->id;
        //dd($level_id);
        $users = $role->users()->whereHas('subjects', function ($query) use ($subject_ids) {
            $query->whereIn('subject_id', $subject_ids);
        })->whereHas('student_level', function ($query) use ($level_id) {
            $query->whereIn('level_id', $level_id);
        })->paginate(20);

return view('user.studentlist', ['users' => $users]);
}

//Create Student Record
public function teacher_create_student()
{
    $levels = Level::all();
    $subjects = Subject::all();
    return view('user.teacher_create', ['levels' => $levels, 'subjects' => $subjects]);
}
}
