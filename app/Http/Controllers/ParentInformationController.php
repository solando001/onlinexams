<?php

namespace App\Http\Controllers;

use App\ParentInformation;
use App\Traits\SchoolSettingsTrait;
use App\User;
use Illuminate\Http\Request;

class ParentInformationController extends Controller
{
    use SchoolSettingsTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addParent($id)
    {
        $student = User::findOrFail($id);
        return view('user.parent.add_parent', ['student' => $student]);
    }

    public function storeParent(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'phone' => ['required', 'numeric', 'digits:11'],
            'email' => ['required'],
            'location' => ['required', 'string'],
            'address' => ['string'],
            'relationship' => ['required', 'string'],
        ]);

        $check = ParentInformation::where('user_id', $request->user_id)->first();
        if($check != null){
            return back()->with(['error' => 'Student already has a parent']);
        }

        $data = $request->all();
        $formated_phone = substr($request->phone,1);
        $data['phone'] = '+234'.$formated_phone;
        $addParent = ParentInformation::create($data);
        $addParent->save();

        return back()->with(['message' => 'Parent Added Successfully']);
    }

    public function editParent($id)
    {
        $student = ParentInformation::findOrFail($id);

        return view('user.parent.edit_parent', ['student' => $student]);
    }

    public function postEditParent(Request $request)
    {
        $formated_phone = substr($request->phone,1);
        $new_phone = '+234'.$formated_phone;
        $check = ParentInformation::findOrFail($request->id);
        $check->update([
            'name' => $request->name,
            'phone' => $new_phone,
            'location' => $request->location,
            'email' => $request->email,
            'address' => $request->address,
            'relationship' => $request->relationship,
        ]);

        return back()->with(['message' => 'Parent Edited Successfully']);
    }

    public function parentList()
    {
        $parents = ParentInformation::orderBy('created_at', 'desc')->paginate(20);
        return view('user.parent.list', ['parents' => $parents]);
    }
}
