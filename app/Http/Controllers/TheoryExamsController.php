<?php

namespace App\Http\Controllers;

use App\Level;
use App\Session;
use App\Setting;
use App\Subject;
use App\Term;
use App\TheoryQuestion;
use App\TheoryQuestionDetail;
use App\TheoryQuestionGrade;
use App\TheoryQuestionSubmission;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Ramsey\Uuid\Uuid;

class TheoryExamsController extends Controller
{
    public function home()
    {
        $settings_theory = Setting::where('name', 'Theory Question')->where('active', true)->first();

        if($settings_theory != null)
        {
            $user = Auth::user();
            if($user->hasRole(['admin', 'system']))
            {
                $subject = Subject::all();
                $level = Level::all();
            }else {
                $subject = $user->subjects()->get();
                $level = $user->teacher_level()->get();

            }

            return view('user.theory.home', ['subject' => $subject, 'level' => $level]);



        }else{
            return view('user.theory.settings');
        }


    }

    public function store_theory(Request $request)
    {
        $request->validate([
            'name' => ['required', 'min:3', 'max:255'],
            'total_questions' => ['required', 'numeric'],
        ]);

        //dd($request);

        $session = Session::where('active', '1')->first();
        $term = Term::where('active', '1')->first();

        $ref_id = str_replace('-', '', Uuid::uuid4());

       // $lessonId = $request->lesson_id;

          $store = TheoryQuestion::create([
              'name' => $request->name,
              'ref_id' => $ref_id,
              'total_questions' => $request->total_questions,
              'time' => $request->time,
              'user_id' => Auth::user()->id,
              'lesson_id' => $request->lesson_id,
              'level_id' => $request->level,
              'subject_id' => $request->subject,
              'session_id' => $session->id,
              'term_id' => $term->id,
              'active' => false,
          ]);
          return redirect('create/'.$store->ref_id.'/questions/option');
    }

    public function create_questions($ref_id)
    {
        $question = TheoryQuestion::where('ref_id', $ref_id)->first();

        $question_up = TheoryQuestionDetail::where('theory_question_id', $question->id)->count();
        return view('user.theory.create_question', ['question' => $question, 'question_up' => $question_up]);
    }

    public function store_questions(Request $request)
    {

        $request->validate([
            'question_detail' => ['required', 'min:3', 'max:20000'],
            'mark' => ['required', 'numeric'],
        ]);

        if ($request->hasFile('image')) {

            $file = $request->file('image');
            $name = time() . $file->getClientOriginalName();
            $filenameExtension = $file->getClientOriginalExtension();

            if ($filenameExtension == 'jpg' || $filenameExtension == 'jpeg' || $filenameExtension == 'png' || $filenameExtension == 'gif' || $filenameExtension == 'svg') {
                $filePath = 'images/' . $name;
                Storage::disk('s3')->put($filePath, file_get_contents($file), 'public');
                $url = Storage::disk('s3')->url($filePath);

                $data['theory_question_id'] = $request->theory_question_id;
                $data['question_detail'] = $request->question_detail;
                $data['mark'] = $request->mark;
                $data['url'] = $url;

                TheoryQuestionDetail::create($data);
                return back()->with(['message' => 'Question Uploaded Successfully']);
                //return back()->with(['document_success' => 'Document Uploaded Successfully']);

            } else {
                return back()->with(['error' => 'Image format not supported, upload .JPEG,.JPG, .PNG !']);
            }

        } else {

            return back()->with(['error' => 'Please upload at least one image!']);
        }
    }



    public function view_theory_questions($ref_id)
    {

            $que = TheoryQuestion::where('ref_id', $ref_id)->first();
            $questionsList = $que->questionList()->get();
            return view('user.theory.view_theory_question', ['que' => $que, 'questionsList' => $questionsList]);

    }

    public function show_theory_questions()
    {
        $session = Session::where('active', '1')->first();
        $term = Term::where('active', '1')->first();

        $user = Auth::user();
        if($user->hasRole('teacher'))
        {
            $subject_ids = Auth::user()->subjects()->get()->pluck('id')->toArray();
            $level_id = Auth::user()->teacher_level()->first()->id;
            $ExamList = TheoryQuestion::whereIn('subject_id', $subject_ids)->where('level_id', $level_id)
                ->where('session_id', $session->id)->where('term_id', $term->id)->orderBy('id', 'desc')->paginate(1000);

        }else{
            $ExamList = TheoryQuestion::where('session_id', $session->id)->where('term_id', $term->id)->orderBy('id', 'desc')->paginate(1000);
        }
        return view('user.theory.show_theory_question', ['ExamList' => $ExamList]);
    }

    public function publish_theory_questions($ref_id)
    {
        $que = TheoryQuestion::where('ref_id', $ref_id)->first();
        $que->update(['active' => true]);
        return back()->with(['message' => 'Exam Has Been Published Successfully']);
    }

    public function unpublish_theory_questions($ref_id)
    {
        $que = TheoryQuestion::where('ref_id', $ref_id)->first();
        $que->update(['active' => false]);
        return back()->with(['message' => 'Exam Has Been Unpublished Successfully']);
    }

    public function view_submission($ref_id)
    {
        $question = TheoryQuestion::where('ref_id', $ref_id)->first();
        $class= $question->questionLevel;//()->first()->id;
        $subject = $question->examSubject;
        $students = $class->students;
        return view('user.theory.submission_list',
            [
                'question' => $question,
                'students' => $students,
                'class' => $class,
                'subject' => $subject
            ]);
    }

    public function view_student_submission($ref_id, $id)
    {
        $question = TheoryQuestion::where('id', $id)->first();
        $student = User::where('id', $ref_id)->first();
        $class= $question->questionLevel;//()->first()->id;
        $subject = $question->examSubject;
        $questionsList = $question->questionList()->get();//->take(1);
       // dd($questionsList);
        return view('user.theory.mark_question',
            [
                'student' => $student,
                'class' => $class,
                'subject' => $subject,
                'question' => $question,
                'questionsList' => $questionsList
            ]);
    }

    public function grade_student(Request $request)
    {

        $request->validate([
            'score' => ['required', 'numeric'],
            //'mark' => ['required', 'numeric'],
        ]);

        if($request->mark < $request->score)
        {
            return redirect()->back()->with('alert', 'Score Should Not be more than the Obtainable Mark');
        }else {
            $submission = TheoryQuestionSubmission::where('question_detail_id', $request->question_detail_id)
                ->where('user_id', $request->user_id)->first();
            $submission->update(['score' => $request->score]);

            $grade = TheoryQuestionGrade::where('student_id', $request->user_id)->where('theory_question_id', $request->question_id)
            ->first();
            $newScore = $request->score;
            $count = $grade->grade;
            $grade->update(['grade' => $count + $newScore]);
            return redirect()->back();

        }
    }



}
