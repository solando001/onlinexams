<?php
namespace App\Http\Controllers;

use App\Level;
use App\Role;
use App\Subject;
use App\User;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index()
    {
        return view('user.index');
    }

    public function create()
    {
        $subjects = Subject::all();
        $levels = Level::all();
        $roles = Role::where('name','!=','system')->get();
        return view('user.create',['roles'=>$roles,'levels'=>$levels,'subjects'=>$subjects]);
    }

    public function edit()
    {
        return view('user.edit');
    }

    public function store(UserRequest $request)
    {
        if (Auth::user()->hasRole(['admin', 'system', 'teacher'])) {
            if ($request->has('role_id')) {
                $role = Role::find($request->role_id);
                $role_id = $role != null && $role->name != 'system' ? $role->id : Role::where('name', 'student')->first()->id;
            } else {
                $role_id = Role::where('name', 'student')->first()->id;
            }
            $data = [
                'email' => $request->email,
                'surname' => $request->surname,
                'username'=> $request->username,
                'gender'=> $request->gender,
                'other_names' => $request->other_names,
                'role_id' => $role_id,
                //'password' => $request->password
            ];
            $user = $this->saveUser($data);


            if ($user) {
                // Assign level to user
                if($request->has('level_id') && !empty($request->level_id)){
                    $user->level()->attach($request->level_id);
                }

                if($request->has('subject_ids') && !empty($request->subject_ids)){
                    $user->subjects()->attach($request->subject_ids);
                }
                //return redirect('home')->with('success', $request->name . ' was created successfully');

                 return back()->with(['message' => 'Record Created Successfully']);

            }
        }
        //return view('user.index');
        return back()->with(['error' => 'An Error Occoured While Registering!']);
    }

    public function saveUser(array $data)
    {
        $user = User::create([
            'email' => $data['email'],
            'surname' => $data['surname'],
            'other_names' => $data['other_names'],
            'gender' => $data['gender]'],
            'password' => bcrypt($data['surname']),
            'username'=> $data['username'],
            'active' => 1
        ]);
        if ($user) {
            if (isset($data['role_id'])) {
                $role = Role::find($data['role_id']);
                $role_id = $role != null && $role->name != 'system' ? $role->id : Role::where('name', 'student')->first()->id;
            } else {
                $role_id = Role::where('name', 'student')->first()->id;
            }
            $user->roles()->attach($role_id);
            return $user;
        }
        return false;
    }

    public function show()
    {
        return view('user.show');
    }

    public function update(Request $request)
    {
        return view('user.index');
    }

    public function delete()
    {
        return view('user.index');
    }
}
