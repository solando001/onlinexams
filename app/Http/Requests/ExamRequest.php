<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ExamRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'mark' => 'required|numeric',
            'is_timed' => 'boolean',
            'time' => 'required_if:is_timed,1|regex:/[0-9][0-9]:[0-5][0-9]/',
            'random_question' => 'boolean',
            'attempt_allowed' => 'numeric',
            'active' => 'boolean',
            'level_id' => 'numeric|required',
            'subject_id' => 'numeric|required',
            'total_question' => 'numeric|required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'total_question.required' => 'You must specified total number of questions for this quiz',
            'score.required_if' => 'You must specify total mark obtainable to make this quiz a gradable quiz',
            'time.required_if' => 'You must specify duration when creating a timed quiz',
            'time.reqex' => 'Quiz duration must be in this format: hh:mm'
        ];
    }
}
