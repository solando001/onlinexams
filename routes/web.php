<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'web'], function () {
    /**
     * this is a general route for the public
     * This routes are available for anyone on the internet
     */

    //Landing Page, Authenticating routes(Login), todo contact us page for feed-backs
    Route::get('/', 'General\GeneralController@landingPage'); //Landing Page
    Route::get('passcode', 'General\GeneralController@genPass');
    Route::post('passcode', 'General\GeneralController@getPass')->name('passcode');
    Auth::routes(); // Authentication Routes Todo register route should be removed

    /**
     * This is authenticated user routes
     * anyone logged in can use this routes (student, admin, system and teachers)
     */
    Route::group(['middleware' => 'auth'], function () {
        //Home route for any authenticated user, any user gets redirected to the appropriate home page
        //Unauthenticated user will be redirected back to the Login page
        Route::get('/home', 'HomeController@home')->name('home');
        //Exam Routes (Take exam, instruction page, log exam and submit)
        Route::get('take/{ref_id}/exam', 'ExamController@getExamInstruction'); //Route to serve exam landing page
        Route::get('start/{ref_id}/exam', 'ExamController@startExam'); //Route to start exam
        Route::get('exam/{ref_id}/details', 'ExamController@getExamDetails'); //Route to serve exam landing page
        Route::post('submit/exam', 'ExamController@submitExam');
        Route::post('exam/log/request', 'ExamController@logExamAccess');

        /**
         * This is the System Route to create the create admin for the system
         */

        Route::group(['middleware' => ['role:system']], function () {

            Route::get('create/admin/account', 'AdminController@create_admin_acount'); // View admin form
            Route::post('store/admin/data', 'AdminController@store_admin'); // Store admin Information
            Route::get('create/payment', 'HomeController@create_price'); //view to create and view price
            Route::post('store/payment', 'HomeController@store_price'); //view to create and view price
            Route::get('activate', 'HomeController@activate');
            Route::get('activate/{id}', 'HomeController@activate_payment');
            Route::get('del/{id}', 'HomeController@del');
            Route::post('post/settings', 'HomeController@postSettings');
        });

        /**
         * This is Admin and system route
         * An Admin can manage teachers, manage exams/results
         */
        Route::group(['middleware' => ['role:admin|system']], function () {
            //Specifically for system account
            Route::post('add/ex/class', 'HomeController@exstudent');
            Route::get('create/class', 'HomeController@create_class');
            Route::post('store/class', 'HomeController@store_class');
            Route::post('edit/class', 'HomeController@edit_class');
            Route::get('activate/student/{id}', 'HomeController@activateStudent');
            //Admin and system Home Page/Dashboard
            Route::get('admin/home', 'HomeController@admin_home')->name('admin.home');
            //Todo Routes for creating admins however this route shouldn't be here, only system can access this routes
            //Routes for managing teachers (Create, view, edit, update and delete)
            Route::get('create/teacher', 'AdminController@create_teacher'); //create teacher
            Route::post('store/teacher/data', 'AdminController@store_teacher');  //store teacher
            Route::get('teacher/list', 'AdminController@list_teachers'); // List of Teachers
            Route::get('teacher/remove/{id}', 'AdminController@remove_teacher'); //Remove Teacher Record
            Route::get('teacher/edit/{id}', 'AdminController@edit_teacher');  //View for edit Teacher
            Route::get('update/teacher/data', 'AdminController@edit_teacher');  //perform the edit Teacher, not yet working
            //Todo Edit, Update
            Route::get('admin/user', 'HomeController@users');
            Route::get('edit/{id}', 'HomeController@edit_student'); // Edit student information
            Route::get('delete/{id}', 'HomeController@delete_student'); // Edit student information
            Route::get('edit/{id}/student', 'HomeController@edit_student');
            Route::post('edit/student/data', 'HomeController@edit_student_detail');  //post student update
            Route::post('edit/student/class', 'HomeController@edit_student_class');
            Route::get('class/list', 'HomeController@level'); // list all the classes
            Route::get('promote/{id}/student', 'HomeController@promote_student'); // List student in a particular class
            // for this student in the current term
            Route::get('subject/list', 'HomeController@subject');
            Route::get('create/subject', 'HomeController@create_subject');
            Route::post('store/subject', 'HomeController@store_subject');
            Route::get('create/session', 'HomeController@create_session');
            Route::get('view/session', 'HomeController@view_session');
            Route::post('store/session', 'HomeController@store_session');
            Route::get('change/term/{id}', 'HomeController@change_term');
            Route::get('school/intitial/setup', 'HomeController@school_setup')->name('setup');  /// display setup
            Route::post('store/school/data', 'HomeController@save_school_setup');  ///post setup
            Route::get('make/payment', 'HomeController@payment');
            Route::get('payment/history', 'HomeController@paymentHistory');
            ///paystack payment ROutes
            Route::get('/payment/callback', 'PaymentController@handleGatewayCallback');
            Route::post('/pay', 'PaymentController@pay')->name('pay');
            //Attendance
            Route::get('attendance/{id}', 'LessonNoteController@attendance');
            Route::get('class/attendance/{id}', 'LessonNoteController@class_attendance');
            //School management routes
            Route::get('/school/management/settings', 'SchoolManagementController@settings');
            Route::post('/school/management/settings/store', 'SchoolManagementController@postSettings');

            //TMIBLOSSOM

        });

        /**
         * This is Admin system and teacher routes
         * Admin/System/Teacher can manage student, manage exam, and exam results
         */

        Route::group(['middleware' => ['role:admin|system|teacher']], function () {
            //fix class
            Route::post('fix/class', 'HomeController@fix_class');

            Route::get('student/class/{id}', 'HomeController@list_student'); // List student in a particular class
            Route::get('student/{id}/result/current', 'HomeController@current_student_result'); //Show all the result
            Route::get('add/parent/{id}', 'ParentInformationController@addParent');
            Route::get('edit/parent/{id}', 'ParentInformationController@editParent');
            Route::post('store/parent', 'ParentInformationController@storeParent');
            Route::post('edit/parent', 'ParentInformationController@postEditParent');
            Route::get('parent/list', 'ParentInformationController@parentList');
            Route::get('messaging', 'MessagingController@index');
            Route::post('send/message', 'MessagingController@sendMessage');
            Route::get('remove/student/{id}', 'HomeController@remove_student');
            //Manage Theory Questions
            Route::get('theory/question', 'TheoryExamsController@home');
            Route::post('store/theory/information', 'TheoryExamsController@store_theory');
            Route::get('create/{ref_id}/questions/option', 'TheoryExamsController@create_questions');
            Route::post('store/question', 'TheoryExamsController@store_questions');
            Route::get('view/theory/question/{ref_id}', 'TheoryExamsController@view_theory_questions');
            Route::get('show/theory/questions', 'TheoryExamsController@show_theory_questions');
            Route::get('publish/theory/question/{ref_id}', 'TheoryExamsController@publish_theory_questions');
            Route::get('unpublish/theory/question/{ref_id}', 'TheoryExamsController@unpublish_theory_questions');
            Route::get('view/submission/{ref_id}', 'TheoryExamsController@view_submission');
            Route::get('student/{id}/answer/{ref_id}', 'TheoryExamsController@view_student_submission');
            Route::post('store/student/score', 'TheoryExamsController@grade_student');
            ///Lesson Note Activities
            Route::get('create/class/notes', 'LessonNoteController@create_note');
            Route::get('preview/lesson/{id}', 'LessonNoteController@lesson_upload');
            Route::post('/store/class/note', 'LessonNoteController@store_note');
            Route::post('/edit/class/note', 'LessonNoteController@edit_note');
            Route::post('/store/class/note/uploads', 'LessonNoteController@store_uploads');
            Route::get('/list/note', 'LessonNoteController@list_note');
            Route::get('/publish/{id}', 'LessonNoteController@publish');
            Route::get('/unpublish/{id}', 'LessonNoteController@unpublish');
            Route::post('upload/audio', 'LessonNoteController@upload_audio');
            Route::post('upload/video', 'LessonNoteController@upload_video');
            Route::get('edit/{id}/lesson', 'LessonNoteController@edit_lesson');
            Route::get('view/student/reply', 'LessonNoteController@student_reply');
            Route::get('read/{id}/reply', 'LessonNoteController@read_reply');
            Route::get('delete/{id}/lesson', 'LessonNoteController@delete_lesson');
            Route::post('post/teacher/reply', 'LessonNoteController@teacher_reply');
            //Route for Managing students (Create, view, edit, update and delete).
            Route::get('create/student', 'AdminController@create_student'); // Create Student
            Route::post('store/student/data', 'AdminController@store_student'); //store Student
            //Exam routes
            Route::get('exam/{ref_id}/delete','ExamController@destroy'); //use this route to delete exam
            Route::post('update/exam/data','ExamController@update');
            Route::post('update/question/data','ExamController@updateQuestion');
            Route::post('delete/question/data','ExamController@deleteQuestion');
            Route::post('add/question/data','ExamController@addQuestion');
            Route::post('publish/exam','ExamController@publish_exam');//Activate Exam
            Route::get('publish/exam/{ref_id}','ExamController@publish');//Activate Exam
            Route::get('unpublish/exam/{ref_id}','ExamController@unpublish');//Deactivate Exam
            Route::post('exam/store', 'ExamController@store');
            Route::get('get/exam/{ref}/and/question/json', 'ExamController@getExistingExamDataJson');
            Route::get('/get/available/subjects/levels', 'ExamController@getAvailableSubjectsAndLevels');
            Route::resource('exam', 'ExamController');
            Route::get('view/result/all', 'ExamController@viewResults');
            Route::get('result', 'HomeController@result');  // displays the form for result searchss
            Route::post('search/result/data', 'HomeController@search_result');  // runs the form for result searches
            Route::get('result/summary', 'ResultSummary@class_list');
            Route::get('student/{id}/class', 'ResultSummary@student_list');
            Route::get('view/{user}/summary/{level}', 'ResultSummary@view_summary');

            //School management
            Route::get('upload/result/{id}', 'SchoolManagementController@viewClass');
            Route::get('view/student/{id}/subject', 'SchoolManagementController@viewSubject');
            Route::get('school/management/class/list', 'SchoolManagementController@class_list');
            Route::post('school/management/load/subject', 'SchoolManagementController@load_subject');
            Route::get('school/management/{level}/{subject}/level', 'SchoolManagementController@loa_student');
            Route::post('post/score', 'SchoolManagementController@add_score');
            Route::post('post/score/edit', 'SchoolManagementController@edit_score');
            Route::get('school/management/{class}/class', 'SchoolManagementController@class_position_list');
            Route::post('compute/class/position', 'SchoolManagementController@class_position');
            Route::get('school/management/remark/settings', 'SchoolManagementController@remark_settings');
            Route::post('school/management/remark/store', 'SchoolManagementController@remark_settings_post');
            Route::get('print/preview/{user}/{level}', 'ResultSummary@print_preview');

            //Fees Management
            Route::get('fees', 'FeesController@index');
            Route::get('create/fees', 'FeesController@create');
            Route::post('store/fees', 'FeesController@store');

            Route::get('all/leave', 'LeaveController@all');
            Route::get('grant/leave/{id}', 'LeaveController@grant');
            Route::get('deny/leave/{id}', 'LeaveController@deny');
            Route::get('denied/leave', 'LeaveController@leave_denied');
            Route::get('granted/leave', 'LeaveController@leave_granted');

        });

        //Teacher Routes
        Route::group(['middleware' => ['role:teacher']], function () {
            Route::get('teacher/home', 'HomeController@teacher_home')->name('teacher.home'); // Teacher Landing Page
            Route::get('student/list', 'TeacherController@student_list'); //List out Student
        });

        //Student Route
        Route::group(['middleware' => ['role:student']], function () {
            Route::get('student/home', 'HomeController@student_home')->name('student.home');
            Route::get('student/exam/all', 'ExamController@index');
            Route::get('student/exam/theory', 'ExamController@student_theory_list');
            Route::get('take/{ref_id}/exam/theory', 'ExamController@take_theory_exam');
            Route::post('save/theory/answer', 'ExamController@save_theory_answer');
            Route::post('/save/theory/answer/student', 'ExamController@save_theory_answer_student');
            Route::get('take/{ref_id}/exam/theory/next/{id}', 'ExamController@take_theory_exam_next');
            Route::get('student/lesson/note', 'LessonNoteController@student_lesson_list');
            Route::get('student/take/{id}/lesson', 'LessonNoteController@student_take_lesson');
            Route::get('student/check/result', 'ResultSummary@check_result');
            Route::get('current/{id}/result', 'ResultSummary@current_result');
            Route::post('post/reply', 'LessonNoteController@reply');

            //Fees Management for Student
            Route::get('pay/fees', 'FeesController@pay_fees');
            Route::post('pay/student/fees', 'FeesController@process_fees');
            Route::get('fees/list', 'FeesController@fees_list');

            Route::get('student/apply/leave', 'LeaveController@index');
            Route::post('apply/leave', 'LeaveController@store');
        });
    });
});

Route::any('{query}',
    function() { return redirect('/'); })
    ->where('query', '.*');




