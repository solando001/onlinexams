@extends('layouts.landing_page')

@section('content')
 <div class="row">
        <div class="col-sm-12">
            <div class="u-mv-large u-text-center">
                <?php
                $school_prop = \App\School::details();
                ?>
                @if($school_prop['NAME'] == "")
                <h2 class="u-mb-xsmall">Welcome to EPortal Online Exam System</h2>
                    @else
                        <h2 class="u-mb-xsmall">Welcome to {{$school_prop['NAME']}} Online Exam System</h2>
                    @endif
                <!--<p class="u-text-mute u-h6">Check out your past searches and the content you’ve browsed in. <a href="#">View last results</a></p>-->
            </div>
        </div>
    </div>

     <div class="row">
        <!--<div class="col-sm-12 col-lg-4">
            <div class="c-card u-p-medium u-text-center u-mb-medium" data-mh="landing-cards">

                <img class="u-mb-small" src="/asset/img/icon-intro1.svg" alt="iPhone icon">

                <h4 class="u-h6 u-text-bold u-mb-small">
                    Check your performance. See the results of all your active campaings.
                </h4>
                <a class="c-btn c-btn--info" href="/register.html">Student Registration</a>
            </div>
        </div>

        <div class="col-sm-12 col-lg-4">
            <div class="c-card u-p-medium u-text-center u-mb-medium" data-mh="landing-cards">

                <img class="u-mb-small" src="/asset/img/icon-intro2.svg" alt="iPhone icon">

                <h4 class="u-h6 u-text-bold u-mb-small">
                    Start console and prepare new stuff for your customers or community!
                </h4>
                <a class="c-btn c-btn--info" href="#">View Results</a>
            </div>
        </div>-->

        <div class="col-sm-12 col-lg-12">
            <div class="c-card u-p-medium u-text-center u-mb-medium" data-mh="landing-cards">
                <img class="u-mb-small" src="/asset/img/icon-intro3.svg" alt="iPhone icon">

                @if($school_prop['NAME'] == "")
                <h4 class="u-h6 u-text-bold u-mb-small">
                    Welcome to EPortal Online Exam System 
                </h4>
                @else
                    <h4 class="u-h6 u-text-bold u-mb-small">
                        Welcome to {{$school_prop['NAME']}} Online Exam System
                    </h4>
                    @endif
                <a class="c-btn c-btn--info" href="/login">Click Here To Login</a>
            </div>
        </div>
    </div>



@endsection






