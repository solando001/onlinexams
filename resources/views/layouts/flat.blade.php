<!doctype html>
<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="E-Portal">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,600" rel="stylesheet">

    <!-- Favicon -->
    <link rel="apple-touch-icon" href="">
    <link rel="shortcut icon" href="{{asset("asset/favicon.ico")}}" type="{{asset("asset/image/x-icon")}}">

    <!-- Stylesheet -->
    <link rel="stylesheet" href=" {{asset("asset/css/main.min.css")}}">
    @yield('styles')
    <script type="text/javascript">
        /*--This JavaScript method for Print command--*/
        function PrintDoc() {
            var toPrint = document.getElementById('printarea');
            var popupWin = window.open('', '_blank', 'width=300,height=100%,location=yes,left=300px');
            popupWin.document.open();
            popupWin.document.write('<html><title>..::Report Sheet Print Preview::..</title>' +
                '<link rel="stylesheet" type="text/css" href="{{asset("asset/css/main.min.css")}}" /><link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" /></head><body onload="window.print()">')
            popupWin.document.write(toPrint.innerHTML);
            popupWin.document.write('</html>');
            popupWin.document.close();
        }
        /*--This JavaScript method for Print Preview command--*/
        function PrintPreview() {
            var toPrint = document.getElementById('printarea');
            var popupWin = window.open('', '_blank', 'width=350,height=150,location=no,left=200px');
            popupWin.document.open();
            popupWin.document.write('<html><title>::Print Preview::</title>' +
                '<link rel="stylesheet" type="text/css" href="{{asset("asset/css/main.min.css")}}" media="screen"/><link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" /></head><body">')
            popupWin.document.write(toPrint.innerHTML);
            popupWin.document.write('</html>');
            popupWin.document.close();
        }
    </script>

</head>
<body class="o-page">
<!--[if lte IE 9]>
{{--<!--<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>-->--}}
<![endif]-->
@include('layouts.layout_components.sidebar.main_menu')
<!-- // .o-page__sidebar -->
<main class="o-page__content">
    @include('layouts.layout_components.header')

    <div class="container-fluid">
        <div class="row">

            @yield('content')

            {{--@include('layouts.layout_components.side_profile')--}}

        </div>

    </div><!-- // .container-fluid -->

</main><!-- // .o-page__content -->

<!-- // .o-page__content -->
<script src=" {{asset("asset/js/main.min.js")}} "></script>
@yield('scripts')
</body>
</html>