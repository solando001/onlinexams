<!doctype html>
<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="First Touch">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600" rel="stylesheet">

    <!-- Favicon -->
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="shortcut icon" href="{{asset("asset/favicon.ico")}}" type="{{asset("asset/image/x-icon")}}">

    <!-- Stylesheet -->
    <link rel="stylesheet" href=" {{asset("asset/css/main.min.css")}}">

    <style type="text/css">
        #bg-picture{
            background-image: url('');
        }
    </style>

    @yield('styles')

</head>
<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
<![endif]-->

<header class="c-navbar">
    <a class="c-navbar__brand" href="{{url('student/home')}}">
        <img src="{{asset("asset/img/logo.png")}}" alt="Dashboard's Logo">
    </a>

    <!-- Navigation items that will be collapes and toggle in small viewports -->
    <nav class="c-nav collapse" id="main-nav">
        <ul class="c-nav__list">
            <li class="c-nav__item">
                <a class="c-nav__link" href="{{url('student/exam/all')}}">OBJ Exam List</a>
            </li>
            <li class="c-nav__item">
                <a class="c-nav__link" href="{{url('/student/exam/theory')}}">Theory Exam List</a>
            </li>
            <li class="c-nav__item">
                <a class="c-nav__link" href="{{url('student/lesson/note')}}">Class Note List</a>
            </li>

            <li class="c-nav__item">
                <a class="c-nav__link" href="{{url('student/check/result')}}">Check Result</a>
            </li>
            {{--<li class="c-nav__item">--}}
                {{--<div class="c-field c-field--inline has-icon-right u-hidden-up@tablet">--}}
            {{--<span class="c-field__icon">--}}
            {{--<i class="fa fa-search"></i>--}}
            {{--</span>--}}
            {{--<li class="c-nav__item">--}}
                {{--<a class="c-nav__link" href="{{ route('logout') }}"--}}
                   {{--onclick="event.preventDefault();--}}
                           {{--document.getElementById('logout-form').submit();">--}}
                    {{--{{ __('Logout') }}--}}
                {{--</a>--}}
            {{--</li>--}}
            {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                {{--@csrf--}}
            {{--</form>--}}


            {{--<label class="u-hidden-visually" for="navbar-search-small">Seach</label>--}}
            {{--<input class="c-input" id="navbar-search-small" type="text" placeholder="Search">--}}
            {{--</div>--}}
            {{--</li>--}}
        </ul>
    </nav>
    <!-- // Navigation items  -->

    <div class="c-field has-icon-right c-navbar__search u-hidden-down@tablet u-ml-auto u-mr-small">
                <span class="c-field__icon">
                    <i class="fa fa-search"></i>
                </span>

        {{--<label class="u-hidden-visually" for="navbar-search">Search</label>--}}
        {{--<input class="c-input" id="navbar-search" type="text" placeholder="Search">--}}
    </div>


    <div class="c-dropdown dropdown">
        <a  class="c-avatar c-avatar--xsmall has-dropdown dropdown-toggle" href="#" id="dropdwonMenuAvatar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="c-avatar__img" src="{{asset("asset/img/avatar-150.jpg")}}" alt="User's Profile Picture">
        </a>

        <div class="c-dropdown__menu dropdown-menu dropdown-menu-right" aria-labelledby="dropdwonMenuAvatar">
            <a class="c-dropdown__item dropdown-item" href="{{url('student/exam/all')}}">OBJ Exam List</a>
            <a class="c-dropdown__item dropdown-item" href="{{url('/student/exam/theory')}}">Theory Exam List</a>
            <a class="c-dropdown__item dropdown-item" href="{{url('student/lesson/note')}}">Class Notes List</a>
            <a class="c-dropdown__item dropdown-item" href="{{url('student/check/result')}}">Check Result</a>



            <a class="c-dropdown__item dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </div>

    <button class="c-nav-toggle" type="button" data-toggle="collapse" data-target="#main-nav">
        <span class="c-nav-toggle__bar"></span>
        <span class="c-nav-toggle__bar"></span>
        <span class="c-nav-toggle__bar"></span>
    </button><!-- // .c-nav-toggle -->
</header>

<div class="container" id="bg-picture">
    <div class="row">
        <div class="col-sm-12">
            <div class="u-mv-large u-text-center">
                <h2 class="u-mb-xsmall">Welcome {{ Auth::user()->surname}}  {{Auth::user()->other_names }}.</h2>
                <p class="u-text-mute u-h6"> </p>
            </div>
        </div>
    </div>

    <div class="row">
        {{--<div class="col-md-5 col-xl-3 u-mb-medium u-hidden-down@tablet">--}}
            {{--<div class="c-profile-card">--}}
                {{--<div class="c-profile-card__cover">--}}
                    {{--<img src=" {{asset("asset/img/profile-card-cover.jpg")}}" alt="Adam's profile cover">--}}
                {{--</div>--}}

                {{--<div class="c-profile-card__user">--}}
                    {{--<div class="c-profile-card__avatar">--}}
                        {{--<img src="  {{asset("asset/img/avatar-150.jpg")}}" alt="Adam's image">--}}
                    {{--</div>--}}

                    {{--<h4 class="c-profile-card__name">{{ Auth::user()->surname}}  {{Auth::user()->other_names }}--}}

                    {{--</h4>--}}
                {{--</div>--}}



                {{--<ul class="c-profile-card__meta">--}}
                    {{--<li class="c-profile-card__meta-item">--}}
                        {{--<i class="fa fa-map-marker"></i>{{ Auth::user()->student_level->first()->name }}--}}
                    {{--</li>--}}

                    {{--<li class="c-profile-card__meta-item">--}}
                        {{--<i class="fa fa-bullhorn"></i>Ilorin--}}
                    {{--</li>--}}

                    {{--<li class="c-profile-card__meta-item">--}}
                        {{--<i class="fa fa-inbox"></i>11 years--}}
                    {{--</li>--}}

                    {{--<li class="c-profile-card__meta-item">--}}
                        {{--<a class="c-dropdown__item dropdown-item" href="{{ route('logout') }}"--}}
                           {{--onclick="event.preventDefault();--}}
                           {{--document.getElementById('logout-form').submit();">--}}
                            {{--{{ __('Logout') }}--}}
                        {{--</a>--}}
                        {{--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">--}}
                            {{--@csrf--}}
                        {{--</form>--}}

                    {{--</li>--}}


                {{--</ul>--}}
            </div><!--// .c-profile -->
        </div>

        <div class="col-sm-12 col-md-12 col-lg-12">
            @yield('content')
        </div>
    </div>
</div>
</div>
</div>

<!-- Main javascsript -->
<script src=" {{asset("asset/js/main.min.js")}} "></script>

@yield('scripts')
</body>
</html>