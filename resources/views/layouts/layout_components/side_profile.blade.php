@if(Auth::Check() && hasCreatedSession())
    <div class="col-xl-4">
        <div class="c-card u-p-medium u-mb-medium">
            <div class="u-text-center">
                <div class="c-avatar c-avatar--large u-mb-small u-inline-flex">
                    <img class="c-avatar__img"
                         src="{{asset("asset/img/avatar-72.png")}}"
                         alt="Adam's Face">
                </div>
                <h3 class="u-h5">{{ strtoupper(Auth::user()->surname) }}, {{ Auth::user()->other_names}}</h3>


                <?php
                    $school_prop = \App\School::details();
                ?>
                @if(Auth::Check() && Auth::User()->hasRole(['system', 'admin']))
                    <p>(Admin account)</p>
                @endif

                @if(Auth::Check() && Auth::user()->hasRole(['teacher']))
                    <p>Class Teacher of {{ Auth::user()->teacher_level->first()->name }} </p>
                    @php
                        $subjects = Auth::user()->subjects;
                    @endphp
                    <p>Subjects:
                        @foreach($subjects as $subject)
                            {{$subject->name}},
                        @endforeach
                    </p>
                @endif
            </div>

            <table class="c-table u-text-center u-pv-small u-mt-medium u-border-right-zero u-border-left-zero">


            </table>
            <div class="c-feed has-icons u-mt-medium">
                <div class="c-feed__item has-icon">
                    <i class="c-feed__item-icon u-bg-info fa fa-tumblr"></i>
                    <p>Registration Number</p>
                    <span class="c-feed__meta">{{ Auth::user()->email}}</span>
                </div>

                <div class="c-feed__item has-icon">
                    <i class="c-feed__item-icon u-bg-fancy fa fa-dropbox"></i>
                    <p>Gender</p>
                    <p class="c-feed__meta">{{Auth::user()->gender}}</p>
                </div>
                <div class="c-feed__item has-icon">
                    <i class="c-feed__item-icon u-bg-fancy fa fa-dropbox"></i>
                    <p>{{$school_prop['NAME']}} </p>
                    <p class="u-text-mute u-text-small">{{$school_prop['EMAIL']}}</p>
                    <span class="u-text-mute u-text-small">{{$school_prop['ADDRESS']}}</span>
                </div>

            </div>
        </div>
    </div>
@endif