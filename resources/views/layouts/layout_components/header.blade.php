<header class="c-navbar u-mb-medium">
    <button class="c-sidebar-toggle u-mr-small">
        <span class="c-sidebar-toggle__bar"></span>
        <span class="c-sidebar-toggle__bar"></span>
        <span class="c-sidebar-toggle__bar"></span>
    </button><!-- // .c-sidebar-toggle -->

    <?php
    $session = \App\Session::where('active', true)->first();
    $term = \App\Term::where('active', true)->first();
    ?>

    <h3 class="c-navbar__title u-mr-auto">
        Session: <span class="text-danger">{{@$session->name}} </span>  | Term: <span class="text-danger">{{@$term->name}}</span>
    </h3>

    <b>{{Auth::user()->surname }} {{Auth::user()->other_names }}</b>
    <div class="c-dropdown dropdown">
        <a  class="c-avatar c-avatar--xsmall has-dropdown dropdown-toggle" href="#" id="dropdwonMenuAvatar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

            <img class="c-avatar__img" src=" {{asset("asset/img/avatar-72.png")}}"
                 alt="User's Profile Picture">

        </a>

        <div class="c-dropdown__menu dropdown-menu dropdown-menu-right" aria-labelledby="dropdwonMenuAvatar">
            <!--<a class="c-dropdown__item dropdown-item" href="#">Edit Profile</a>
            <a class="c-dropdown__item dropdown-item" href="#">View Profile</a>
            <a class="c-dropdown__item dropdown-item" href="#">Manage Roles</a>-->
            <a class="c-dropdown__item dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>

        </div>
    </div>
</header>