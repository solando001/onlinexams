<!-- list of sidebar for teacher  -->
@if(Auth::Check() && Auth::User()->hasRole(['teacher']))
    <div class="o-page__sidebar js-page-sidebar">
        <div class="c-sidebar">
            <a class="c-sidebar__brand" href="#">
                <img class="c-sidebar__brand-img" src="{{asset("asset/img/logo.png")}}" alt="Logo"> Dashboard
            </a>

            <h4 class="c-sidebar__title">Dashboards</h4>
            <ul class="c-sidebar__list">

                <li class="c-sidebar__item">
                    <a class="c-sidebar__link" href="{{ url('/teacher/home') }}">
                        <i class="fa fa-home u-mr-xsmall"></i>Home
                    </a>
                </li>

                <li class="c-sidebar__item">
                    <a class="c-sidebar__link" href="{{ url('create/student') }}">
                        <i class="fa fa-plus u-mr-xsmall"></i>Add Student
                    </a>
                </li>

                {{--<li class="c-sidebar__item">--}}
                    {{--<a class="c-sidebar__link" href="{{ url('/exam/create') }}">--}}
                        {{--<i class="fa fa-plus u-mr-xsmall"></i>Create OBJ Exam--}}
                    {{--</a>--}}
                {{--</li>--}}

                <li class="c-sidebar__item has-submenu">
                    <a class="c-sidebar__link" data-toggle="collapse" href="#sidebar-submenu_exam-3" aria-expanded="false" aria-controls="sidebar-submenu">
                        <i class="fa fa-caret-square-o-down u-mr-xsmall"></i>School Mgt. System
                    </a>
                    <ul class="c-sidebar__submenu collapse" id="sidebar-submenu_exam-3">
                        <li><a class="c-sidebar__link" href="{{ url('school/management/class/list') }}"> Upload Results </a></li>
                        <li><a class="c-sidebar__link" href="{{ url('result/summary') }}"> Report Sheet </a></li>
                        <li><a class="c-sidebar__link" href="{{ url('result') }}"> Search Results </a></li>
                    </ul>
                </li>

                <li class="c-sidebar__item has-submenu">
                    <a class="c-sidebar__link" data-toggle="collapse" href="#sidebar-submenu_exam" aria-expanded="false" aria-controls="sidebar-submenu">
                        <i class="fa fa-caret-square-o-down u-mr-xsmall"></i>OBJ Exams
                    </a>
                    <ul class="c-sidebar__submenu collapse" id="sidebar-submenu_exam">

                        <li><a class="c-sidebar__link" href="{{ url('/exam/create') }}"> > Create Exam </a></li>
                        <li><a class="c-sidebar__link" href="{{ url('/exam') }}">  > Exam List</a></li>
{{--                        <li><a class="c-sidebar__link" href="{{ url('/view/student/Reply')}}">  > View Replies (2)</a></li>--}}

                    </ul>
                </li>



                <li class="c-sidebar__item has-submenu">
                    <a class="c-sidebar__link" data-toggle="collapse" href="#sidebar-submenu_lesson" aria-expanded="false" aria-controls="sidebar-submenu">
                        <i class="fa fa-caret-square-o-down u-mr-xsmall"></i>Lesson Notes
                    </a>
                    <ul class="c-sidebar__submenu collapse" id="sidebar-submenu_lesson">
                        <?php

                           $user = Auth::user();
                            $replies = \App\StudentReply::where('teacher_id', $user->id)->where('status', true)->count();


                            ?>
                        <li><a class="c-sidebar__link" href="{{ url('/create/class/notes')}}"> > Create Lesson </a></li>
                        <li><a class="c-sidebar__link" href="{{ url('/list/note') }}">  > View Lesson</a></li>
                        <li><a class="c-sidebar__link" href="{{ url('/view/student/reply')}}">  > View Replies ({{$replies}})</a></li>

                    </ul>
                </li>

                {{--<li class="c-sidebar__item">--}}
                    {{--<a class="c-sidebar__link" href="{{ url('/create/class/notes')}}">--}}
                        {{--<i class="fa fa-plus u-mr-xsmall"></i>Create Lesson Notes--}}
                    {{--</a>--}}
                {{--</li>--}}



                {{--<li class="c-sidebar__item">--}}
                    {{--<a class="c-sidebar__link" href="{{ url('student/list') }}">--}}
                        {{--<i class="fa fa-eye u-mr-xsmall"></i>Student List--}}
                    {{--</a>--}}
                {{--</li>--}}

               <?php
                    $teacher  = Auth::user();
                    $levels = $teacher->teacher_level;
               ?>

                <li class="c-sidebar__item has-submenu">
                    <a class="c-sidebar__link" data-toggle="collapse" href="#sidebar-submenu" aria-expanded="false" aria-controls="sidebar-submenu">
                        <i class="fa fa-caret-square-o-down u-mr-xsmall"></i>Student List
                    </a>
                    <ul class="c-sidebar__submenu collapse" id="sidebar-submenu">
                        @foreach($levels as $level)
                        <li><a class="c-sidebar__link" href="{{url('student/class/'.$level->id)}}">    {{$level->name}} Students</a></li>
                        @endforeach
                    </ul>
                </li>



                {{--<li class="c-sidebar__item">--}}
                    {{--<a class="c-sidebar__link" href="{{ url('/exam') }}">--}}
                        {{--<i class="fa fa-table u-mr-xsmall"></i>Exam List--}}
                    {{--</a>--}}
                {{--</li>--}}

                <li class="c-sidebar__item">
                    <a class="c-sidebar__link" href="{{ url('/theory/question')}}">
                        <i class="fa fa-plus u-mr-xsmall"></i>Create Theory Exam
                    </a>
                </li>

                <li class="c-sidebar__item">
                    <a class="c-sidebar__link" href="{{ url('/show/theory/questions') }}">
                        <i class="fa fa-table u-mr-xsmall"></i>Theory Exam List
                    </a>
                </li>



                {{--<li class="c-sidebar__item">--}}
                    {{--<a class="c-sidebar__link" href="{{ url('/list/note') }}">--}}
                        {{--<i class="fa fa-table u-mr-xsmall"></i>Lesson Notes--}}
                    {{--</a>--}}
                {{--</li>--}}





{{--                <li class="c-sidebar__item">--}}
{{--                    <a class="c-sidebar__link" href="{{ url('/result')}}">--}}
{{--                        <i class="fa fa-eye u-mr-xsmall"></i>Search Results--}}
{{--                    </a>--}}
{{--                </li>--}}

                {{--<li class="c-sidebar__item">--}}
                    {{--<a class="c-sidebar__link" href="{{ url('/result/summary')}}">--}}
                        {{--<i class="fa fa-eye u-mr-xsmall"></i>Result Summary--}}
                    {{--</a>--}}
                {{--</li>--}}


            </ul>

        </div><!-- // .c-sidebar -->
    </div><!-- // .o-page__sidebar -->

@endif