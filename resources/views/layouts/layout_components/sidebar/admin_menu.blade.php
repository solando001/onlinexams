@if(Auth::Check() && Auth::User()->hasRole(['system', 'admin']))
    <div class="o-page__sidebar js-page-sidebar">
        <div class="c-sidebar">
            <a class="c-sidebar__brand" href="#">
                <img class="c-sidebar__brand-img" src="{{asset("asset/img/logo.png")}}" alt="Logo"> E-Portal
            </a>

            <h4 class="c-sidebar__title">E-Portal</h4>
            <ul class="c-sidebar__list">
                @if(hasCreatedSession())
                <li class="c-sidebar__item">
                    <a class="c-sidebar__link" href="{{url('/admin/home')}}">
                        <i class="fa fa-home u-mr-xsmall"></i>Dashboard
                    </a>
                </li>

                    <li class="c-sidebar__item has-submenu">
                        <a class="c-sidebar__link" data-toggle="collapse" href="#sidebar-submenu_exam" aria-expanded="false" aria-controls="sidebar-submenu">
                            <i class="fa fa-caret-square-o-down u-mr-xsmall"></i>User Management
                        </a>
                        <ul class="c-sidebar__submenu collapse" id="sidebar-submenu_exam">

                            <li><a class="c-sidebar__link" href="{{ url('create/teacher') }}"> Add Teacher </a></li>
                            <li><a class="c-sidebar__link" href="{{ url('teacher/list') }}"> View Teachers </a></li>
                            <li><a class="c-sidebar__link" href="{{ url('create/student') }}"> Add Student </a></li>
                            <li><a class="c-sidebar__link" href="{{ url('/parent/list') }}"> Parent List </a></li>

                        </ul>
                    </li>


                    <li class="c-sidebar__item has-submenu">
                        <a class="c-sidebar__link" data-toggle="collapse" href="#sidebar-submenu_exam-2" aria-expanded="false" aria-controls="sidebar-submenu">
                            <i class="fa fa-caret-square-o-down u-mr-xsmall"></i>Theory Question Mgt.
                        </a>
                        <ul class="c-sidebar__submenu collapse" id="sidebar-submenu_exam-2">
                            <li><a class="c-sidebar__link" href="{{ url('list/note') }}"> Lesson Notes </a></li>
                            <li><a class="c-sidebar__link" href="{{ url('theory/question') }}"> Create Theory Question </a></li>
                            <li><a class="c-sidebar__link" href="{{ url('/show/theory/questions') }}"> View Theory Question </a></li>
                        </ul>
                    </li>

                    <li class="c-sidebar__item has-submenu">
                        <a class="c-sidebar__link" data-toggle="collapse" href="#sidebar-submenu_exam-3" aria-expanded="false" aria-controls="sidebar-submenu">
                            <i class="fa fa-caret-square-o-down u-mr-xsmall"></i>School Mgt. System
                        </a>
                        <ul class="c-sidebar__submenu collapse" id="sidebar-submenu_exam-3">
                            <li><a class="c-sidebar__link" href="{{ url('school/management/class/list') }}"> Upload Results </a></li>
                            <li><a class="c-sidebar__link" href="{{ url('result/summary') }}"> Report Sheet </a></li>
                            <li><a class="c-sidebar__link" href="{{ url('result') }}"> Search Results </a></li>
                        </ul>
                    </li>

{{--                    <li class="c-sidebar__item has-submenu">--}}
{{--                        <a class="c-sidebar__link" data-toggle="collapse" href="#sidebar-submenu_exam-1" aria-expanded="false" aria-controls="sidebar-submenu">--}}
{{--                            <i class="fa fa-caret-square-o-down u-mr-xsmall"></i>Fees Management--}}
{{--                        </a>--}}
{{--                        <ul class="c-sidebar__submenu collapse" id="sidebar-submenu_exam-1">--}}
{{--                            <li><a class="c-sidebar__link" href="{{ url('fees') }}"> Fees List </a></li>--}}
{{--                            <li><a class="c-sidebar__link" href="{{ url('create/fees') }}"> Create Fees </a></li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}

{{--                    <li class="c-sidebar__item has-submenu">--}}
{{--                        <a class="c-sidebar__link" data-toggle="collapse" href="#sidebar-submenu_exam-01" aria-expanded="false" aria-controls="sidebar-submenu">--}}
{{--                            <i class="fa fa-caret-square-o-down u-mr-xsmall"></i>Leave Management--}}
{{--                        </a>--}}
{{--                        <ul class="c-sidebar__submenu collapse" id="sidebar-submenu_exam-01">--}}
{{--                            <li><a class="c-sidebar__link" href="{{ url('all/leave') }}"> All Leaves </a></li>--}}
{{--                            <li><a class="c-sidebar__link" href="{{ url('granted/leave') }}"> Leaves Granted </a></li>--}}
{{--                            <li><a class="c-sidebar__link" href="{{ url('denied/leave') }}"> Leaves Denied </a></li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}



                    <li class="c-sidebar__item has-submenu">
                        <a class="c-sidebar__link" data-toggle="collapse" href="#sidebar-submenu_exam-100" aria-expanded="false" aria-controls="sidebar-submenu">
                            <i class="fa fa-caret-square-o-down u-mr-xsmall"></i>School Settings
                        </a>
                        <ul class="c-sidebar__submenu collapse" id="sidebar-submenu_exam-100">
                            <li><a class="c-sidebar__link" href="{{ url('create/subject') }}"> Add Subject </a></li>
                            <li><a class="c-sidebar__link" href="{{ url('create/class') }}"> Add Class </a></li>
                            <li><a class="c-sidebar__link" href="{{ url('create/session') }}"> Add Session </a></li>
{{--                            <li><a class="c-sidebar__link" href="{{ url('view/session') }}"> View Sessions </a></li>--}}
                            <li><a class="c-sidebar__link" href="{{ url('subject/list') }}"> View Subject </a></li>
                            <li><a class="c-sidebar__link" href="{{ url('class/list') }}"> View Classes </a></li>

                        </ul>
                    </li>



                    {{--                    <li class="c-sidebar__item">--}}
{{--                    <a class="c-sidebar__link" href="{{url('create/teacher')}}">--}}
{{--                        <i class="fa fa-plus u-mr-xsmall"></i>Create Teacher Record--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="c-sidebar__item">--}}
{{--                    <a class="c-sidebar__link" href="{{ url('/create/student') }}">--}}
{{--                        <i class="fa fa-plus u-mr-xsmall"></i>Create Student Record <!--<span class="c-badge c-badge--success c-badge--xsmall u-ml-xsmall">New</span> -->--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                    <li class="c-sidebar__item">--}}
{{--                        <a class="c-sidebar__link" href="{{ url('/create/subject')}}">--}}
{{--                            <i class="fa fa-plus u-mr-xsmall"></i>Create Subjects--}}
{{--                        </a>--}}
{{--                    </li>--}}

{{--                <li class="c-sidebar__item">--}}
{{--                    <a class="c-sidebar__link" href="{{ url('/theory/question')}}">--}}
{{--                        <i class="fa fa-plus u-mr-xsmall"></i>Create Theory Question--}}
{{--                    </a>--}}
{{--                </li>--}}

{{--                <li class="c-sidebar__item">--}}
{{--                    <a class="c-sidebar__link" href="{{ url('create/session')}}">--}}
{{--                        <i class="fa fa-plus u-mr-xsmall"></i>Create Session--}}
{{--                    </a>--}}
{{--                </li>--}}

{{--                    <li class="c-sidebar__item">--}}
{{--                        <a class="c-sidebar__link" href="{{ url('/create/class')}}">--}}
{{--                            <i class="fa fa-plus u-mr-xsmall"></i>Create Class--}}
{{--                        </a>--}}
{{--                    </li>--}}

{{--                    <li class="c-sidebar__item">--}}
{{--                        <a class="c-sidebar__link" href="{{ url('/school/management/settings')}}">--}}
{{--                            <i class="fa fa-plus u-mr-xsmall"></i>School Mgt Settings--}}
{{--                        </a>--}}
{{--                    </li>--}}

{{--                    <li class="c-sidebar__item">--}}
{{--                        <a class="c-sidebar__link" href="{{ url('/school/management/remark/settings')}}">--}}
{{--                            <i class="fa fa-plus u-mr-xsmall"></i>Remark Settings--}}
{{--                        </a>--}}
{{--                    </li>--}}

{{--                    <li class="c-sidebar__item">--}}
{{--                        <a class="c-sidebar__link" href="{{ url('school/management/class/list')}}">--}}
{{--                            <i class="fa fa-plus u-mr-xsmall"></i>Upload Student Result--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="c-sidebar__item">--}}
{{--                        <a class="c-sidebar__link" href="{{ url('/result/summary')}}">--}}
{{--                            <i class="fa fa-eye u-mr-xsmall"></i>Report Sheet--}}
{{--                        </a>--}}
{{--                    </li>--}}


                    {{--                    <li class="c-sidebar__item">--}}
{{--                        <a class="c-sidebar__link" href="{{ url('/messaging')}}">--}}
{{--                            <i class="fa fa-plus u-mr-xsmall"></i>Messaging--}}
{{--                        </a>--}}
{{--                    </li>--}}

{{--                    <li class="c-sidebar__item">--}}
{{--                        <a class="c-sidebar__link" href="{{ url('/show/theory/questions')}}">--}}
{{--                            <i class="fa fa-eye u-mr-xsmall"></i>View Theory Question--}}
{{--                        </a>--}}
{{--                    </li>--}}

{{--                    <li class="c-sidebar__item">--}}
{{--                        <a class="c-sidebar__link" href="{{ url('/list/note') }}">--}}
{{--                            <i class="fa fa-eye u-mr-xsmall"></i>Lesson Notes--}}
{{--                        </a>--}}
{{--                    </li>--}}

{{--                <li class="c-sidebar__item">--}}
{{--                    <a class="c-sidebar__link" href="{{url('teacher/list')}}">--}}
{{--                        <i class="fa fa-eye u-mr-xsmall"></i>View Teachers--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="c-sidebar__item">--}}
{{--                    <a class="c-sidebar__link" href="{{ url('/subject/list')}}">--}}
{{--                        <i class="fa fa-eye u-mr-xsmall"></i>View Subjects--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                    <li class="c-sidebar__item">--}}
{{--                        <a class="c-sidebar__link" href="{{ url('/parent/list')}}">--}}
{{--                            <i class="fa fa-eye u-mr-xsmall"></i>View Parent--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                <li class="c-sidebar__item">--}}
{{--                    <a class="c-sidebar__link" href="{{ url('/class/list')}}">--}}
{{--                        <i class="fa fa-eye u-mr-xsmall"></i>View Classes--}}
{{--                    </a>--}}
{{--                </li>--}}

{{--                    <li class="c-sidebar__item">--}}
{{--                    <a class="c-sidebar__link" href="{{ url('/result')}}">--}}
{{--                        <i class="fa fa-eye u-mr-xsmall"></i>View Results--}}
{{--                    </a>--}}
{{--                </li>--}}

{{--                    <li class="c-sidebar__item">--}}
{{--                        <a class="c-sidebar__link" href="{{ url('/attendance')}}">--}}
{{--                            <i class="fa fa-eye u-mr-xsmall"></i>View Attendance--}}
{{--                        </a>--}}
{{--                    </li>--}}


                    <li class="c-sidebar__item">
                        <a class="c-sidebar__link" href="{{ url('/make/payment')}}">
                            <i class="fa fa-eye u-mr-xsmall"></i>Make Payment
                        </a>
                    </li>
                    <li class="c-sidebar__item">
                        <a class="c-sidebar__link" href="{{ url('payment/history')}}">
                            <i class="fa fa-cog u-mr-xsmall"></i>Payment History
                        </a>
                    </li>
                @else
                <li class="c-sidebar__item">
                    <a class="c-sidebar__link" href="{{ url('school/intitial/setup')}}">
                        <i class="fa fa-cog u-mr-xsmall"></i>School Setup
                    </a>
                </li>
                @endif

                @if(Auth::Check() && Auth::User()->hasRole(['system']))
{{--                        <li class="c-sidebar__item">--}}
{{--                            <a class="c-sidebar__link" href="{{ url('create/payment')}}">--}}
{{--                                <i class="fa fa-cog u-mr-xsmall"></i>Create/View Price--}}
{{--                            </a>--}}
{{--                        </li>--}}

                        <li class="c-sidebar__item">
                            <a class="c-sidebar__link" href="{{ url('/activate')}}">
                                <i class="fa fa-cog u-mr-xsmall"></i>Activate
                            </a>
                        </li>
                    @endif
            </ul>

        </div><!-- // .c-sidebar -->


    </div><!-- // .o-page__sidebar -->
@endif