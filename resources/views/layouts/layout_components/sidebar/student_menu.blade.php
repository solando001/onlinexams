@if(Auth::Check() && Auth::User()->hasRole(['student']))
    <div class="o-page__sidebar js-page-sidebar">
        <div class="c-sidebar">
            <a class="c-sidebar__brand" href="#">
                <img class="c-sidebar__brand-img" src="{{asset("asset/img/logo.png")}}" alt="Logo"> Dashboard
            </a>

            <h4 class="c-sidebar__title">Dashboards</h4>
            <ul class="c-sidebar__list">

                <li class="c-sidebar__item">
                    <a class="c-sidebar__link" href="{{ url('/home') }}">
                        <i class="fa fa-home u-mr-xsmall"></i>Home
                    </a>
                </li>

                <li class="c-sidebar__item">
                    <a class="c-sidebar__link" href="{{ url('pay/fees') }}">
                        <i class="fa fa-money u-mr-xsmall"></i>Pay Fees
                    </a>
                    <a class="c-sidebar__link" href="{{ url('fees/list') }}">
                        <i class="fa fa-table u-mr-xsmall"></i>Fees List
                    </a>

                    <a class="c-sidebar__link" href="{{ url('student/apply/leave') }}">
                        <i class="fa fa-table u-mr-xsmall"></i>Apply for Leave
                    </a>
                </li>
            </ul>
        </div>
    </div>

@endif
