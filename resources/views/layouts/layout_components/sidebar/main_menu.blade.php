@if(Auth::check())
    @php
        $user =Auth::user();
    @endphp
    <div class="ks-navbar-horizontal ks-icons-top ks-info">
        <!--div class="container"-->
    @if($user->hasRole('teacher'))
        @include('layouts.layout_components.sidebar.teacher_menu')
    @elseif($user->hasRole('admin') || $user->hasRole('system'))
        @include('layouts.layout_components.sidebar.admin_menu')
    @elseif($user->hasRole('student'))
        @include('layouts.layout_components.sidebar.student_menu')
    @endif
    <!--/div-->
    </div>
@else
@endif