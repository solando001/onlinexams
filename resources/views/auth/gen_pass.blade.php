@extends('layouts.auth')
@section('title', 'Login Page')
@section('content')

        <div class="o-page__card">
            <div class="c-card u-mb-xsmall">
                <header class="c-card__header u-pt-large">
                    <a class="c-card__icon" href="{{ url('passcode') }}">
                        <img src="{{asset('asset/img/logo-login.svg')}}" alt="Dashboard UI Kit">
                    </a>
                    <h1 class="u-h3 u-text-center u-mb-zero">Retrieve Password</h1>

                    <center>To retrieve password for your ward, please fill the form below</center>

                    @if(session('message'))
                        <br><hr>
                        <center><h4>Your Password Is: </h4></center>
                        <center><h1>{{ session('message') }}</h1></center>
                        <hr>
                    @endif
                </header>
                
                
                <form method="POST" action="{{ route('passcode') }}" aria-label="{{ __('Login') }}" class="c-card__body">
                        @csrf

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="input1">Enter Surname </label> 
                        <input id="email" type="text" class="c-input form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" value="{{ old('surname') }}" placeholder="Surname" required autofocus>
                        @if ($errors->has('surname'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('surname') }}</strong>
                            </span>
                        @endif
                    </div>
                    <button class="c-btn c-btn--info c-btn--fullwidth" type="submit">Generate Password</button>                    
                </form>
            </div>

            <div class="o-line">
               <a class="u-text-mute u-text-small" href="{{ url('login') }}">Return to Login Page</a>
                {{-- <a class="u-text-mute u-text-small" href="forgot-password.html">Forgot Password?</a> --}}
            </div>
        </div>
        @endsection