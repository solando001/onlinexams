@extends('layouts.exam')
@section('title',$exam->name)
@section('styles')
    <style>
        .fade-transition {
            transition: opacity 0.2s ease;
        }

        .fade-enter, .fade-leave {
            opacity: 0;
        }
    </style>
@endsection
@section('content')

<div class="row">
                <div class="col-sm-12">
                    <div class="u-mv-large u-text-center">
                        <h2 class="u-mb-xsmall">  {{ $exam->name }} Exam In Progress </h2>
                       
                    </div>
                </div>
            </div>

             <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <div class="c-card u-p-medium  u-mb-medium" data-mh="landing-cards">

                        <!-- Exam Vue Component -->
                        <take-exam :ref-id="{{json_encode($exam->ref_id)}}" transition="fade" transition-mode="out-in"></take-exam>

                          </div>
                </div>
            </div>

       
@endsection
@section('scripts')
    <script src="{{asset('/js/exam/take_exam.js')}}"></script>
@endsection

