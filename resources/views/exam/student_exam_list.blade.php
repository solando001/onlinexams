@extends('layouts.student')

@section('title', 'List Of Exams For Student')

@section('content')
 <div class="col-sm-12 col-lg-9">
		<div class="c-table-responsive@tablet">
        
        <table class="c-table u-mb-large">
            <caption class="c-table__title">
                List of Exams
            </caption>
            <thead class="c-table__head c-table__head--slim">
                <tr>
                    <th class="c-table__cell c-table__cell--head">Exam Name</th>
                    <th class="c-table__cell c-table__cell--head">Time(HH/MM)</th>
                    <th class="c-table__cell c-table__cell--head">No. of Question</th>
                    <th class="c-table__cell c-table__cell--head">Actions
                       <!-- <span class="u-hidden-visually">Actions</span>-->
                    </th>
                </tr>
            </thead>

            <tbody>
            @if(Auth::user()->hasRole('student'))
            @foreach($exams as $exam)

                <?php
                $examScore = \App\ExamUserScore::where('user_id', Auth::user()->id)->where('exam_id', $exam->id)->count();
                ?>

                <tr class="c-table__row">
                    <td class="c-table__cell">
                        {{$exam->name}}                             
                    </td>

                    <td class="c-table__cell">
                        {{$exam->time}}
                    </td>

                    <td class="c-table__cell">
                        {{$exam->total_question}}
                    </td>

                    <td class="c-table__cell">
                                            <!--<a href="{{url('exam/'.$exam->ref_id.'/edit')}}" class="c-btn c-btn--warning">Edit</a>

                                             <a href="{{url('exam/'.$exam->ref_id.'/delete')}}" class="c-btn c-btn--danger"
                                               onclick="return confirm('Are you sure you want to Remove?');">Delete</a>-->



                                               @if(!$exam->active)
                                               
                                                <a href="" class="c-btn c-btn--info is-disabled" >Take Exam</a>
                                                   @endif

                                                   @if($exam->active)

                                                        @if($examScore >= 2)

                                                        <del class="u-text-danger">Exam Completed</del>
                                                        @else
                                                        <a href="{{url('take/'.$exam->ref_id.'/exam')}}" class="c-btn c-btn--success">Take Exam</a>

                                                            {{--<a href="" class="c-btn c-btn--info is-disabled" >Exam Completed</a>--}}
                                                        @endif


                                                   @endif

                                               @endforeach
                                               @else
                                               @foreach($exams as $exam)
                                               @if($exam->active)
                                               <a href="{{url('take/'.$exam->ref_id.'/exam')}}" class="c-btn c-btn--success">Take Exam</a>
                                               <!--<li>
                                                <a href="{{url('take/'.$exam->ref_id.'/exam')}}">{{$exam->name}}</a>
                                            </li>-->
                                            @endif
                    </td>
                                            
                                            
                    
                </tr>
            @endforeach

            @endif

                                            <!--<tr class="c-table__row">
                                                <td class="c-table__cell">English Language
                                                    <span class="u-block u-text-mute u-text-xsmall">
                                                        Teacher: Mrs Amoko Alice
                                                    </span>
                                                </td>
                                                <td class="c-table__cell">1hr 20min</td>
                                                <td class="c-table__cell">
                                                    <del class="u-text-danger">Completed</del>
                                                </td>
                                                <td class="c-table__cell u-text-right">
                                                    <a class="c-btn is-disabled" href="#">Start Exam</a>
                                                </td>
                                            </tr>-->
                                        </tbody>
                                    </table>

                                    {{ $exams->links() }}
                                    
                                 <!--   <div class="container">
                                        <div class="col-md-12 alert alert-info"> You do not have Users yet</div>
                                        <br>
                                    </div>-->
                                   

                                </div>
                            </div>
                            </div>

                    

@endsection