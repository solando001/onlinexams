@extends('layouts.student_flat')
@section('title', $que->name.'  in Progress')
@section('styles')
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: '#mytextarea'
        });
    </script>
@endsection

    @section('content')

        <div class="container-fluid">
{{--            <h3>{{$que->name}} ({{$questionsList->count()}})</h3>--}}
            <h3>Questions </h3>
            <div class="row">

                <div class="col-12 col-xl-12 u-p-zero">

                    <div class="row u-m-small">

                        @if($questionsList->count() > 0)
                            @foreach($questionsList as $list)
                                <div class="col-sm-12 col-md-12">
                                    <div class="c-project-card u-mb-medium">
                                        {{--<img src="{{asset("asset/img/project-card1.jpg")}}" alt="About the image">--}}

                                        <div class="c-project-card__content">
                                            <div class="c-project-card__head">
                                                <h2 class="c-project-card__title">{!! $list->question_detail !!}</h2>
                                                <br>
                                                <hr>
                                                <br>

                                                <p class="c-project-card__info">
                                                    <img src="{{$list->url}}" class="image-responsive">
                                                </p>
                                            </div>

                                            <div class="c-project-card__meta">
                                                <p>{{$list->mark}}<small class="u-block u-text-mute">Mark Obtainable</small></p>
                                                {{--<p><a href="">.</a><small class="u-block u-text-mute"> Created: {{\Carbon\Carbon::parse($list->created_at)->diffForHumans()}}</small></p>--}}
                                            </div>
                                        </div>
                                    </div>

                                    <h3>Enter your Answer in the space below</h3>

                                    {{--@if(session('message'))--}}
                                        {{--<div class="alert alert-success">--}}
                                            {{--{{ session('message') }}--}}
                                        {{--</div>--}}
                                    {{--@endif--}}

                                    @if($errors->any())
                                        <div class="text-danger">
                                            <ul>
                                                @foreach($errors->all() as $error)
                                                    <li>
                                                        {{$error}}
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif


                                    <form method="post" action="{{url('/save/theory/answer')}}">
                                        @csrf
                                        <textarea id="mytextarea" name="answer_detail">  </textarea>

                                        <br>

                                        <input type="hidden" name="question_theory_id" value="{{$que->id}}">
                                        <input type="hidden" name="question_detail_id" value="{{$list->id}}">
                                        <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                        <input type="hidden" name="total" value="{{$list->mark}}">
                                        <div class="col-md-6">
                                            <div class="c-field u-mb-small">
                                                <label class="c-field__label" for="bio"></label>
                                                <button type="submit" class="c-btn c-btn--fancy pull-left">
                                                    Submit Answer
                                                </button>
                                            </div>
                                        </div>


                                    </form>

                                </div>


                                    @endforeach

                        @else
                            <div class="col-md-12">
                                <center>
                                    <div class="alert alert-danger">
                                        <strong>Opps</strong> There are not Questions to answer at the moment
                                    </div>

                                    <a href="{{url('student/home')}}" class="c-btn c-btn--primary">
                                        Back to Home
                                    </a>
                                </center>

                            </div>

                        @endif



                    </div>

                </div>


            </div>



            <br><br>


        </div>



    @endsection