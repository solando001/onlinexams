@extends('layouts.student_flat')
@section('title', $que->name.'  in Progress')
@section('styles')
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: '#mytextarea'
        });
    </script>
@endsection

@section('content')

    <div class="container-fluid">
        {{--            <h3>{{$que->name}} ({{$questionsList->count()}})</h3>--}}
        <h3>Questions </h3>
        <div class="row">

            <div class="col-12 col-xl-12 u-p-zero">

                <div class="row u-m-small">

                    @if($questionsListNext->count() > 0)

                        @if($GetQuestionsAnswered == $totalQuestions)

                            {{--If the questions were actually completed--}}
                            <div class="col-md-12">
                                <h4 class="text-center">You have completed the exams, Click the Submit Answer Button to Submit your Answers</h4>
                                <br>
                                <div class="c-field u-mb-small">
                                    <label class="c-field__label" for="bio"></label>
                                    <center>
                                        <form method="post" action="{{url('/save/theory/answer/student')}}">
                                            @csrf

                                            <input type="hidden" name="question_id" value="{{$que->id}}">

                                            <button type="submit" class="c-btn c-btn--fancy">
                                                Submit Answer
                                            </button>

                                            {{--<button class="c-btn c-btn--warning">--}}
                                            {{--Check Answer Overview--}}
                                            {{--</button>--}}
                                        </form>
                                    </center>
                                </div>
                            </div>


                            @else

                        @foreach($questionsListNext as $list)
                            <div class="col-sm-12 col-md-12">
                                <div class="c-project-card u-mb-medium">
                                    {{--<img src="{{asset("asset/img/project-card1.jpg")}}" alt="About the image">--}}

                                    <div class="c-project-card__content">
                                        <div class="c-project-card__head">
                                            <h2 class="c-project-card__title">{!! $list->question_detail !!}</h2>
                                            <br>
                                            <p class="c-project-card__info"> </p>
                                        </div>

                                        <div class="c-project-card__meta">
                                            <p>{{$list->mark}}<small class="u-block u-text-mute">Mark Obtainable</small></p>
                                            {{--<p><a href="">.</a><small class="u-block u-text-mute"> Created: {{\Carbon\Carbon::parse($list->created_at)->diffForHumans()}}</small></p>--}}
                                        </div>
                                    </div>
                                </div>

                                <h3>Enter your Answer in the space below</h3>

                                {{--@if(session('message'))--}}
                                    {{--<div class="alert alert-success">--}}
                                        {{--{{ session('message') }}--}}
                                    {{--</div>--}}
                                {{--@endif--}}

                                @if($errors->any())
                                    <div class="text-danger">
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li>
                                                    {{$error}}
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <form method="post" action="{{url('/save/theory/answer')}}">
                                    @csrf
                                    <textarea id="mytextarea" name="answer_detail">  </textarea>

                                    <br>

                                    <input type="hidden" name="question_theory_id" value="{{$que->id}}">
                                    <input type="hidden" name="question_detail_id" value="{{$list->id}}">
                                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                    <input type="hidden" name="total" value="{{$list->mark}}">


                                    <div class="col-md-6">
                                        <div class="c-field u-mb-small">
                                            <label class="c-field__label" for="bio"></label>
                                            <button type="submit" class="c-btn c-btn--fancy pull-left">
                                                Next Question
                                            </button>
                                        </div>
                                    </div>


                                </form>

                            </div>




                        @endforeach

                        @endif






                    @else

                        <div class="col-md-12">
                            <h4 class="text-center">You have completed the exams, Click the Submit Answer Button to Submit your Answers</h4>
                            <br>
                            <div class="c-field u-mb-small">
                                <label class="c-field__label" for="bio"></label>
                                <center>
                                    <form method="post" action="{{url('/save/theory/answer/student')}}">
                                        @csrf

                                    <input type="hidden" name="question_id" value="{{$que->id}}">

                                    <button type="submit" class="c-btn c-btn--fancy">
                                   Submit Answer
                                    </button>

                                    {{--<button class="c-btn c-btn--warning">--}}
                                        {{--Check Answer Overview--}}
                                    {{--</button>--}}
                                    </form>
                                </center>
                            </div>
                        </div>

                        {{--<div class="alert alert-danger">--}}
                            {{--<strong>Opps</strong> There are not Questions At the Moment--}}
                        {{--</div>--}}

                    @endif

                </div>

            </div>

        </div>

        <br><br>

    </div>

@endsection