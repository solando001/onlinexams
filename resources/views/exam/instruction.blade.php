@extends('layouts.exam')
@section('title', 'Exam Instruction')
@section('content')

 <div class="row">
                <div class="col-sm-12">
                    <div class="u-mv-large u-text-center">
                        <h2 class="u-mb-xsmall">  Instruction for {{ $exam->name }} Exam</h2>
                       
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <div class="c-card u-p-medium  u-mb-medium" data-mh="landing-cards">

                        <!--<img class="u-mb-small" src="img/icon-intro1.svg" alt="iPhone icon">-->

                        <h3 class="u-h6 u-text-bold u-mb-small u-text-center ">
                           You have {{ $exam->time }} minutes to answer {{ $exam->total_question }} Quesions.
                        </h3>
                        <!--<div class="col-md-2"></div>
                        <div class="col-md-8">
                        <input type="checkbox" name="" > Option 1 Option 1Option 1<br>
                        <input type="checkbox" name="" > Option 2  Option 1Option 1Option 1<br>
                        <input type="checkbox" name="" > Option 3 Option 1Option 1 <br>
                        <input type="checkbox" name="" > Option 4 Option 1Option 1Option 1Option 1Option 1<br>

                        </div>
                        <div class="col-md-2"></div>-->
                        
                        <br>

                         <div class="row">

                            <div class="col-md-4"> <a href="{{ url('student/exam/all') }}" class="c-btn c-btn--secondary pull-left">Back To Exam List </a> </div>
                            <div class="col-md-4">
                           
                            </div>
                            <div class="col-md-4">  
                                <a href="{{url('start/'.$exam->ref_id.'/exam')}}" class="c-btn c-btn--info pull-right"> Continue to Test</a>
                            </div>
                           
                         </div>
                                    
                       <!-- <a class="c-btn c-btn--info pull-left" href="#">Prev </a>
                        <div class="u-text-center">
                         <a class="c-btn c-btn--info" href="#">Next </a>
                     
                          <a class="c-btn c-btn--danger pull-right" href="#">Submit</a>
                          </div> -->
                    </div>
                </div>
            </div>


  

@endsection