@extends('layouts.master_list')

@section('title', 'List Of Exams')

@section('content')

<div class="col-sm-12 col-lg-12">

    <div class="c-table-responsive@tablet">
        
        <table class="c-table u-mb-large">
            <caption class="c-table__title">
                List of Exams
            </caption>
            <thead class="c-table__head c-table__head--slim">
                <tr>
                    <th class="c-table__cell c-table__cell--head">Exam Name</th>
                    <th class="c-table__cell c-table__cell--head">Time(HH/MM)</th>
                    <th class="c-table__cell c-table__cell--head">No. of Question</th>
                    <th class="c-table__cell c-table__cell--head">Exam Class</th>
                    <th class="c-table__cell c-table__cell--head">Created</th>
                    <th class="c-table__cell c-table__cell--head">Actions
                       <!-- <span class="u-hidden-visually">Actions</span>-->
                    </th>
                </tr>
            </thead>

            <tbody>
            @if(Auth::user()->hasRole(['teacher']))
            @foreach($exams as $exam)
                <tr class="c-table__row">
                    <td class="c-table__cell">
                        {{$exam->name}}                             
                    </td>

                    <td class="c-table__cell">
                        {{$exam->time}}
                    </td>

                    <td class="c-table__cell">
                        {{$exam->total_question}}
                    </td>
                    
                     <td class="c-table__cell">
                        {{$exam->level->name}}
                    </td>
                    <td class="c-table__cell">
                        {{\Carbon\Carbon::parse($exam->created_at)->diffForHumans()}}
                    </td>



                    <td class="c-table__cell">
                       
                                            <!--<a href="{{url('take/'.$exam->ref_id.'/exam')}}" class="c-btn c-btn--success">Take Exam</a>-->
                                            
                                            <a href="{{url('exam/'.$exam->ref_id.'/edit')}}" class="c-btn c-btn--warning">Edit</a>

{{--                                             <a href="{{url('exam/'.$exam->ref_id.'/delete')}}" class="c-btn c-btn--danger"--}}
{{--                                               onclick="return confirm('Are you sure you want to Remove?');">Delete</a>--}}


                                               @if(!$exam->active)
                                               
                                                <a href="{{url('publish/exam/'.$exam->ref_id)}}" class="c-btn c-btn--info"
                                                   onclick="return confirm('Are you sure you want to Publish?');">Publish</a>
                                                   @endif


                                             
                                               @endforeach
                                               @else
                                               @foreach($exams as $exam)
                                               @if($exam->active)
                                               <!--<li>
                                                <a href="{{url('take/'.$exam->ref_id.'/exam')}}">{{$exam->name}}</a>
                                            </li>-->
                                            @endif
                    </td>
                                            
                                            
                    
                </tr>
            @endforeach

            @endif
                                        </tbody>
                                    </table>

                                    {{ $exams->links() }}
                                    

                                </div>
                            </div>






@endsection
