@extends('layouts.student')
@section('title', 'List Of Theory Exams For Student')

    @section('content')
<div class="col-sm-12 col-lg-9">
    <div class="c-table-responsive@tablet">

        <table class="c-table u-mb-large">
            <caption class="c-table__title">
                List of Theory Exams
            </caption>
            <thead class="c-table__head c-table__head--slim">
            <tr>
                <th class="c-table__cell c-table__cell--head">Exam Name</th>
                {{--<th class="c-table__cell c-table__cell--head">Time(HH/MM)</th>--}}
                <th class="c-table__cell c-table__cell--head">No. of Question</th>
                <th class="c-table__cell c-table__cell--head">Lesson Title</th>
                <th class="c-table__cell c-table__cell--head">Actions
                    <!-- <span class="u-hidden-visually">Actions</span>-->
                </th>
            </tr>
            </thead>

            <tbody>
            <?php $user = Auth::user(); ?>

            @if(Auth::user()->hasRole('student'))

                @if($theoryExams->count() > 0)
                @foreach($theoryExams as $exam)
                    <?php
                       $sub = \App\TheoryQuestionGrade::where('theory_question_id', $exam->id)->where('student_id', $user->id)->first();
                    ?>

                    <tr class="c-table__row">
                       <td class="c-table__cell">
                           {{$exam->name}}
                        <span class="u-block u-text-mute u-text-xsmall">
                          {{$exam->examSubject->name}}
                        </span>
                        </td>
                        {{--<td class="c-table__cell">1hr 20min</td>--}}
                        <td class="c-table__cell">
                            {{$exam->total_questions}}
                            {{--<del class="u-text-danger">Completed</del>--}}
                        </td>
                        <td class="c-table__cell">

                            @if($exam->lesson_id == "")
                                <span class="u-text-danger"><b>Not a Lesson Exam</b></span>
                                @else
                                {{$exam->lesson->title}}
                                {{--<span class="u-text-success"><b>A Lesson Exam</b></span>--}}
                                @endif
                            {{--<del class="u-text-danger">Completed</del>--}}
                        </td>
                         <td class="c-table__cell">

{{--                             @if($exam->lesson->active == '1' || $exam->lesson->active == "")--}}
                             @if($sub == null)

                            <a class="c-btn" href="{{url('take/'.$exam->ref_id.'/exam/theory')}}">Start Exam</a>
                                 @else

                                 <del class="u-text-danger">Exam Completed</del>
                             @endif

                                 {{--@else--}}
                             {{--<button class="c-btn c-btn--secondary disabled">Exam Not Active</button>--}}

                             {{--@endif--}}
                         </td>
                    </tr>

                @endforeach

                    @else
                    <tr>
                        <td></td>
                        <td>
                            <br>
                            <div class="col-md-12">
                                <center>
                                    <div class="alert alert-danger">
                                        <strong>Opps</strong> There are no Exams to take the moment
                                    </div>
                                </center>

                            </div>
                        </td>
                        <td></td>


                    </tr>




                    @endif
                @endif


            <!--<tr class="c-table__row">
                                                <td class="c-table__cell">English Language
                                                    <span class="u-block u-text-mute u-text-xsmall">
                                                        Teacher: Mrs Amoko Alice
                                                    </span>
                                                </td>
                                                <td class="c-table__cell">1hr 20min</td>
                                                <td class="c-table__cell">
                                                    <del class="u-text-danger">Completed</del>
                                                </td>
                                                <td class="c-table__cell u-text-right">
                                                    <a class="c-btn is-disabled" href="#">Start Exam</a>
                                                </td>
                                            </tr>-->
            </tbody>
        </table>

    {{ $theoryExams->links() }}

    <!--   <div class="container">
                                        <div class="col-md-12 alert alert-info"> You do not have Users yet</div>
                                        <br>
                                    </div>-->


    </div>
</div>
</div>



@endsection