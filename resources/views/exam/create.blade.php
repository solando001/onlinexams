@extends('layouts.master')
@section('title', 'Create Exam')
@section('styles')

@endsection

@section('content')
    <div class="col-xl-8 c-card u-p-medium u-mb-medium">
        <exam-create :edit-exam="{{$edit}}" :ref-id="{{$ref_id?json_encode($ref_id):'null'}}"></exam-create>
    </div>
@endsection
@section('scripts')
    <script src="/js/exam/create_exam.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.16/vue.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.3.4/tinymce.min.js"></script>-->
    <link href="{{asset("summernote/summernote.css")}}" rel="stylesheet">
    <script src={{asset("summernote/summernote.js")}}"></script>
@endsection
