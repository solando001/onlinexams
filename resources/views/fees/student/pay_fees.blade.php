@extends('layouts.master')
@section('title', 'EPortal - Fees')

@section('content')

    <div class="col-xl-8 c-card u-p-medium u-mb-medium">

        <h3>Create Class</h3>
        <form method="POST" action="{{ url('pay/student/fees') }}" aria-label="{{ __('Pay Fees') }}">
            @csrf

            @if(session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif


            <div class="row">
                <div class="col-lg-12">
                    <br>
                    <h5> - School Fees: {{number_format($currentFee->amount,2)}}</h5>
                    <h5> - Amount Paid: {{number_format(@$check->amount,2)}}</h5>
                    <?php @$balance = $currentFee->amount - $check->amount ?>
                    <h5> - Amount to be Paid: {{number_format($balance,2)}}</h5>

                    <hr>
                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Select Payment') }}</label>

                        <select name="payment_type" class="form-control" required>
                            <option value="">Select One</option>
                            <option value="Full Payment">Full Payment</option>
                            <option value="Part Payment">Part Payment</option>
                        </select>
                    </div>

                    <input type="hidden" name="amount" value="{{$currentFee->amount}}">
                    <input type="hidden" name="fee_id" value="{{$currentFee->id}}">

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="bio"></label>
                        <button class="c-btn c-btn--info" type="submit">Continue To Payment!
                        </button>
                    </div>


                </div>
            </div>
        </form>

        <hr>
    </div>

@endsection