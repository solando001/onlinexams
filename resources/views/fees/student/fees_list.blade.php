@extends('layouts.master')
@section('title', 'EPortal - Fees List')

@section('content')
    <div class="col-xl-8 c-card u-p-medium u-mb-medium">
        <table class="c-table u-mb-large">
            <caption class="c-table__title">
                {{Auth::user()->surname}} {{Auth::user()->other_names}} Fees List </span>
            </caption>
            @if(session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
            <thead class="c-table__head c-table__head--slim">
            <tr>
                <th class="c-table__cell c-table__cell--head">Session</th>
                <th class="c-table__cell c-table__cell--head">Term</th>
                <th class="c-table__cell c-table__cell--head">Fees</th>
                <th class="c-table__cell c-table__cell--head">Amount Paid</th>
                <th class="c-table__cell c-table__cell--head">Balance</th>
                <th class="c-table__cell c-table__cell--head">Remark</th>
                <th class="c-table__cell c-table__cell--head">
                    <span class="u-hidden-visually">Actions</span>
                </th>
            </tr>
            </thead>

            <tbody>
            @foreach($feelist as $fee)
                <tr class="c-table__row">
                    <td class="c-table__cell">{{$fee->fee->session->name}}</td>
                    <td class="c-table__cell">{{$fee->fee->term->name}}</td>
                    <td class="c-table__cell">{{number_format($fee->fee->amount,2)}}</td>
                    <td class="c-table__cell">{{number_format($fee->amount,2)}}</td>
                    <td class="c-table__cell">{{number_format($fee->balance,2)}}</td>
                    <td class="c-table__cell">{{$fee->payment_type}}</td>
                    <td class="c-table__cell u-text-right">
                        @if($fee->payment_type == "Part Payment")
                        <a class="c-btn c-btn--success c-btn--small" href="#">Complete Payment</a>
                            @else
                            <a class="c-btn c-btn--secondary c-btn--small disabled" href="#">Payment Complete</a>
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>
    </div>

@endsection
