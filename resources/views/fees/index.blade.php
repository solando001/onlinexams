@extends('layouts.master')
@section('title', 'EPortal - All Fees')

@section('content')
    <div class="col-xl-8 c-card u-p-medium u-mb-medium">

        <table class="c-table u-mb-large">
            <caption class="c-table__title">
                Fees List <span class="pull-right"><a href="{{url('create/fees')}}" class="btn btn-primary btn-sm">Create Fees</a> </span>
            </caption>
            @if(session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
            <thead class="c-table__head c-table__head--slim">
            <tr>
                <th class="c-table__cell c-table__cell--head">Session</th>
                <th class="c-table__cell c-table__cell--head">Term</th>
                <th class="c-table__cell c-table__cell--head">Amount</th>
                <th class="c-table__cell c-table__cell--head">
                    <span class="u-hidden-visually">Actions</span>
                </th>
            </tr>
            </thead>

            <tbody>
            @foreach($fees as $fee)
                <tr class="c-table__row">
                    <td class="c-table__cell">{{$fee->session->name}}</td>
                    <td class="c-table__cell">{{$fee->term->name}}</td>
                    <td class="c-table__cell">{{number_format($fee->amount,2)}}</td>
                    <td class="c-table__cell u-text-right">
                        <a class="c-btn c-btn--success c-btn--small" href="#">View Payments</a>
                    </td>
                </tr>
            @endforeach
        </table>

    </div>

    @endsection