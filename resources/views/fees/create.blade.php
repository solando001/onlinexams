@extends('layouts.master')
@section('title', 'EPortal - Create Fees')

@section('content')
    <div class="col-xl-8 c-card u-p-medium u-mb-medium">

        <h3>Create Fees</h3>
        @if(session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
        <form method="POST" action="{{ url('store/fees') }}" aria-label="{{ __('create_fees') }}">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Select Session') }}</label>
                        <select class="form-control" name="session_id" required>
                            <option value=""> Select </option>
                            @foreach($sessions as $session)
                            <option value="{{$session->id}}">{{$session->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Select Term') }}</label>
                        <select class="form-control" name="term_id" required>
                            <option value=""> Select </option>
                            @foreach($terms as $term)
                            <option value="{{$term->id}}">{{$term->name}}</option>
                                @endforeach
                        </select>
                    </div>


                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Amount') }}</label>

                        <input id="name" type="text" class="c-input form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount"
                               placeholder="Amount" value="{{ old('amount') }}" required autofocus>

                        @if ($errors->has('amount'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('amount') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="bio"></label>
                        <button class="c-btn c-btn--info" type="submit">Create Fees!
                        </button>
                    </div>

                </div>
            </div>
        </form>
    </div>

@endsection
