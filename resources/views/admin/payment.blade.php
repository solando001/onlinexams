@extends('layouts.master')
@section('title', 'EPortal Online Exam System - Make Payment')



@section('content')

    <div class="col-xl-8 c-card u-p-medium u-mb-medium">

        <h3>Make Payment</h3>
        @if(session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif

            <ul class="list-group">
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Current Session:
                    <span class="pull-right"><b>{{$current_session->name}}</b></span>
                    {{--<span class="badge badge-primary badge-pill">{{$current_session->name}}</span>--}}
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Current Term:
                    <span class="pull-right"><b>{{$current_term->name}}</b></span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Current Price:
                    <span class="pull-right"><b>&#8358; {{number_format($price)}}</b></span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Registered Students (Active):
                    <span class="pull-right"><b>{{$student->where('active', true)->count()}}</b></span>
                </li>
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    Expected Amount:
                    <span class="pull-right"><b>&#8358; {{number_format($student->count() * $price)}}</b></span>
                </li>
            </ul>

                    <?php
                        $total = $student->count() * $price;
                    ?>

                @if($payment)

                    <div class="alert alert-success">
                        <strong>Correct!!</strong> You have Successfully Make Payment for {{$current_term->name}} {{$current_session->name}} Academic Session!
                    </div>

                    @else

                <form method="POST" action="{{ route('pay') }}">

                            <input type="hidden" name="email" value="{{Auth::user()->email}}"> 

                            <input type="hidden" name="amount" value="{{$total * 100}}"> 
                            {{-- <input type="hidden" name="qty" value="{{$student->count()}}">
                            <input type="hidden" name="metadata" value="{{ json_encode($array = ['qty' => $student->where('active', true)->count(), 'price'=>$price, 'session' => $current_session->name, 'term' => $current_term->name]) }}" > 
                            <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> 
                            <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}"> 
                            {{ csrf_field() }} works only when using laravel 5.1, 5.2 --}}

                            {{-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> employ this in place of csrf_field only in laravel 5.0 --}}

                            {{-- <div class="c-field u-mb-small">
                                <label class="c-field__label" for="bio"></label>
                                <button class="c-btn c-btn--info" type="submit">Make Payment!
                                </button>
                            </div> --}}
                </form>
                @endif


    </div>

@endsection
