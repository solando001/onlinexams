@extends('layouts.master')
@section('title', 'EPortal Online Exam System- Admin Home')

@section('content')

    <div class="col-xl-12 c-card u-p-medium u-mb-medium">
        <div class="row">
            <div class="col-xl-4">
                <div class="c-graph-card" data-mh="graph-cards">
                    <div class="c-graph-card__content">
                        <h3 class="c-graph-card__title">Total Number of Students</h3>
                        {{--<p class="c-graph-card__date">Activity from 4 Jan 2017 to 10 Jan 2017</p>--}}
                        <h4 class="c-graph-card__number">{{$users_count}}</h4>
                        <p class="c-graph-card__status">As at Today</p>
                    </div>

                    <div class="c-graph-card__chart">
                        {{--<canvas id="js-chart-payout" width="300" height="14"></canvas>--}}
                        <canvas width="300" height="5"></canvas>
                    </div>
                </div>
            </div>

            <div class="col-xl-4">
                <div class="c-graph-card" data-mh="graph-cards">
                    <div class="c-graph-card__content">
                        <h3 class="c-graph-card__title">Total Number of Teachers</h3>
                        {{--<p class="c-graph-card__date">In 15 Months</p>--}}
                        <h4 class="c-graph-card__number">{{$teacher_count}}</h4>
                        <p class="c-graph-card__status">As at Today</p>
                    </div>

                    <div class="c-graph-card__chart">
                        {{--<canvas id="js-chart-earnings" width="300" height="14"></canvas>--}}
                        <canvas width="300" height="5"></canvas>
                    </div>
                </div>
            </div>

            <div class="col-xl-4">
                <div class="c-graph-card" data-mh="graph-cards">
                    <div class="c-graph-card__content">
                        <h3 class="c-graph-card__title">Total Exams Uploaded</h3>
                        {{--<p class="c-graph-card__date">In 15 Months</p>--}}
                        <h4 class="c-graph-card__number">{{$exam_count}}</h4>
                        <p class="c-graph-card__status">As at Today</p>
                    </div>

                    <div class="c-graph-card__chart">
                        {{--<canvas id="js-chart-earnings" width="300" height="14"></canvas>--}}
                        <canvas width="300" height="5"></canvas>
                    </div>
                </div>
            </div>


        </div>

    </div>
    <div class="col-xl-8 c-card u-p-medium u-mb-medium">
        <h3>Welcome {{ Auth::user()->surname }} {{ Auth::user()->other_names }}</h3>

        <!--   Previous Form for Add Teacher --->
        <div class="row">
            <div class="col-sm-12 col-lg-6">
                <div class="c-card u-p-medium u-text-center u-mb-medium" data-mh="landing-cards">
                    <img class="u-mb-small" src="/asset/img/icon-intro3.svg" alt="iPhone icon">

                    <h4 class="u-h6 u-text-bold u-mb-small">
                       <br> Admin Create Teacher Account.
                    </h4>
                    <a class="c-btn c-btn--info" href="{{ url('/create/teacher') }}">Create Teacher</a>
                </div>
            </div>

            <div class="col-sm-12 col-lg-6">
                <div class="c-card u-p-medium u-text-center u-mb-medium" data-mh="landing-cards">
                    <img class="u-mb-small" src="/asset/img/icon-intro1.svg" alt="iPhone icon">

                    <h4 class="u-h6 u-text-bold u-mb-small">
                        <br> Admin Create Student Account.
                    </h4>
                    <a class="c-btn c-btn--info" href="{{ url('/create/student') }}">Create Student</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-lg-6">
                <div class="c-card u-p-medium u-text-center u-mb-medium" data-mh="landing-cards">
                    <img class="u-mb-small" src="/asset/img/icon-intro3.svg" alt="iPhone icon">

                    <h4 class="u-h6 u-text-bold u-mb-small">
                        You can test your student by creating Objective Exams.
                    </h4>
                    <div class="u-flex u-mt-medium">
                        <a class="c-btn c-btn--info c-btn--fullwidth u-mr-xsmall" href="{{ url('/exam/create') }}">Create
                            Exams</a>
                        <a class="c-btn c-btn--secondary c-btn--fullwidth" href="{{ url('/exam') }}">See Exam List</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-lg-6">
                <div class="c-card u-p-medium u-text-center u-mb-medium" data-mh="landing-cards">
                    <img class="u-mb-small" src="/asset/img/icon-intro3.svg" alt="iPhone icon">

                    <h4 class="u-h6 u-text-bold u-mb-small">
                        You can test your student by creating Theory Exams.
                    </h4>
                    <div class="u-flex u-mt-medium">
                        <a class="c-btn c-btn--info c-btn--fullwidth u-mr-xsmall" href="{{ url('/theory/question') }}">Create
                            Theory Exams</a>
                        <a class="c-btn c-btn--secondary c-btn--fullwidth" href="{{ url('/show/theory/questions') }}">See Theory Exam List</a>
                    </div>
                </div>
            </div>

            @if(Auth::Check() && Auth::User()->hasRole(['system']))
                <div class="col-sm-12 col-lg-6">
                    <div class="c-card u-p-medium u-text-center u-mb-medium" data-mh="landing-cards">
                        <img class="u-mb-small" src="/asset/img/icon-intro1.svg" alt="iPhone icon">

                        <h4 class="u-h6 u-text-bold u-mb-small">
                             <br> System Create Admin Account.
                        </h4>
                        <a class="c-btn c-btn--info" href="{{ url('/create/admin/account') }}">Create Admin</a>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
