@extends('layouts.master')
@section('title', 'EPortal Online Exam System - Manage Settings')


@section('content')

    <div class="col-xl-8 c-card u-p-medium u-mb-medium">

        <h4>Settings Management</h4>
        <br>
        @if(session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif

        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Activation Name</th>
                <th scope="col">Amount</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            @foreach($set as $s)
            <tr>
                <th scope="row">{{$i++}}</th>
                <td>{{$s->name}}</td>
                <td>${{number_format($s->amount)}}</td>
                <td>
                    @if($s->active)
                        <div class="badge badge-pill">Active</div>
                        @else
                        <div class="badge badge-pill">Inactive</div>
                    @endif
                </td>
                <td>
                    @if($s->active)
                        <a href="{{url('/activate/'.$s->id)}}" class="c-btn c-btn--small c-btn--warning"> Deactivate</a>
                        @else
                        <a href="{{url('/activate/'.$s->id)}}" class="c-btn c-btn--small c-btn--success"> Activate</a>
                    @endif
                </td>
                <td>
                    <a href="{{url('del/'.$s->id)}}" class="c-btn c-btn--small c-btn--danger">Delete</a>
                </td>
            </tr>
                @endforeach
            </tbody>
        </table>
        {{--@foreach($set as $s)--}}
        {{--<div>--}}

            {{--<p><b>{{$s->name}}: {{$s->active}}</b>  ---}}
                {{--@if($s->active == '1')--}}
                    {{--<a href="{{url('/activate/'.$s->id)}}"> Deactivate </a>--}}
                {{--@else--}}
                    {{--<a href="{{url('/activate/'.$s->id)}}"> Activate </a>--}}
                {{--@endif--}}
            {{--</p>--}}
        {{--</div>--}}
        {{--@endforeach--}}

        <br><br><br>

        <h3>Add Settings</h3>

        <form  action="{{ url('post/settings') }}" aria-label="{{ __('Settings') }}" method="POST">
            {{csrf_field()}}
            <div class="row">
                <div class="col-lg-12">

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Settings Name') }}</label>
                        <select name="name" class="form-control">
                            <option value="Theory Question">Theory Question</option>
                            <option value="Students">Students</option>
                            <option value="Report Management">Report Management</option>
                            <option value="Online Exams">Online Computer Based Test</option>
                        </select>
                    </div>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Amount') }}</label>
                        <input id="year" type="text" class="c-input form-control" name="amount" placeholder="1000" value="{{ old('amount') }}" required>
                    </div>

                    <input type="hidden" name="active" value="0">

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="bio"></label>
                        <button class="c-btn c-btn--info" type="submit">Create Settings!
                        </button>
                    </div>


                </div>

            </div>

        </form>

        <hr>
        {{--<div>--}}
            {{--<p><b>Current Price: {{number_format($price->price)}}</b></p>--}}
            {{--<p><b>Current Price: {{$price->user->surname}}</b></p>--}}
            {{--<p><b>Number of Registered Students: {{$student->count()}}</b></p>--}}
            {{--<p><b>Expected Amount: {{$student->count() * $price->price}}</b></p>--}}
        {{--</div>--}}


    </div>

@endsection
