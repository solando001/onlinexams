@extends('layouts.master')
@section('title', 'EPortal Online Exam System- Create Admin Record')

@section('content')
    <div class="col-xl-8 c-card u-p-medium u-mb-medium">
        <h3> Create Admin's Data</h3>
        <div class="alert alert-info" role="alert">
            <strong>An Admin </strong>can create Exams, create teacher, view student data and results.
        </div>
        <form method="POST" action="{{ url('store/admin/data') }}">
            @csrf

            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            @if(session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif

            <div class="row">

                <div class="col-lg-12">
                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Surname') }}</label>

                        <input id="surname" type="text"
                               class="c-input form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}"
                               name="surname" placeholder="Adebayo" value="{{ old('surname') }}" required autofocus>

                        @if ($errors->has('surname'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('surname') }}</strong>
                    </span>
                        @endif
                    </div>


                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Other Names') }}</label>

                        <input id="other_names" type="text"
                               class="c-input form-control{{ $errors->has('other_names') ? ' is-invalid' : '' }}"
                               name="other_names" placeholder="Adebayo" value="{{ old('other_names') }}" required
                               autofocus>

                        @if ($errors->has('other_names'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('other_names') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="subject_ids">{{ __('Gender') }}</label>
                        <select id="gender"
                                class="c-input form-control{{ $errors->has('gender') ? ' is-invalid' : '' }}"
                                name="gender" value="{{ old('gender') }}" required autofocus>
                            <option value="">Select Gender</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>

                        @if ($errors->has('gender'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('gender') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="email">{{ __('Admin Registration Number') }}</label>

                        <input id="email" type="text"
                               class="c-input form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                               placeholder="09/TCH/001" value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                        @endif
                    </div>


                <!--  <div class="c-field u-mb-small">
                        <label class="c-field__label" for="subject_ids">{{ __('Assign a Class') }}</label>
                        <select id="level_id"
                                class="c-input form-control{{ $errors->has('level_id') ? ' is-invalid' : '' }}"
                                name="level_id" value="{{ old('level_id') }}" required autofocus>
                            @foreach($levels as $level)
                                <option value="{{$level->id}}">{{$level->name}}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('level_id'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('level_id') }}</strong>
                    </span>
                        @endif
                    </div>


                    <div class="c-field u-mb-small">
                        <label class="c-field__label"
                               for="subject_ids">{{ __('Assign Subject(s) (Press Ctrl + Subject to select)') }}</label>
                        <select multiple id="subject_ids" class="c-input form-control" name="subject_ids[]" required
                                autofocus>
                            @foreach($subjects as $subject)
                                <option value="{{$subject->id}}">{{$subject->name}}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('subject_ids'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('subject_ids') }}</strong>
                    </span>
                        @endif
                    </div>-->


                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="username"></label>

                        <input id="username" type="hidden"
                               class="c-input form-control{{ $errors->has('username') ? ' is-invalid' : '' }}"
                               name="username" placeholder="adebayo@gmail.com" value="{{ old('username') }}"
                               value="Teacher" required autofocus>

                        @if ($errors->has('username'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                        @endif
                    </div>
                    <input type="hidden" name="role_id" value="2">

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="bio"></label>
                        <button class="c-btn c-btn--info" type="submit">Create Admin Account!
                        </button>
                    </div>

                </div>
            </div>
        </form>
    </div>
@endsection
