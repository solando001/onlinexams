@extends('layouts.master')
@section('title', 'EPortal Online Exam System - Payment History')



@section('content')

    <div class="col-xl-8 c-card u-p-medium u-mb-medium">

        <h3>Payment History</h3>


        @foreach($paymentHistory as $payment)
        <ul class="list-group">
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Session:
                <span class="pull-right"><b> {{$payment->session}}</b></span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Term:
                <span class="pull-right"><b> {{$payment->term}}</b></span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Number of Student:
                <span class="pull-right"><b> {{$payment->qty}}</b></span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Amount Per Student:
                <span class="pull-right"><b>&#8358; {{number_format($payment->price)}}</b></span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
                Total Amount Paid:
                <span class="pull-right"><b>&#8358; {{number_format($payment->amount / 100)}}</b></span>
            </li>
            <li class="list-group-item d-flex justify-content-between align-items-center">
               When Paid:
                <span class="pull-right"><b> {{\Carbon\Carbon::parse($payment->created_at)->diffForHumans()}}</b></span>
            </li>
        </ul>

        @endforeach







        {{--<div>--}}
        {{--<p><b>Current Price: {{number_format($price->price)}}</b></p>--}}
        {{--<p><b>Current Price: {{$price->user->surname}}</b></p>--}}
        {{--<p><b>Number of Registered Students: {{$student->count()}}</b></p>--}}
        {{--<p><b>Expected Amount: {{$student->count() * $price->price}}</b></p>--}}
        {{--</div>--}}








    </div>

@endsection
