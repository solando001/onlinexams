@extends('layouts.master')
@section('title', 'EPortal Online Exam System - Create Price')


@section('content')

    <div class="col-xl-8 c-card u-p-medium u-mb-medium">

        <h3>Create Price</h3>
        @if(session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
        <form  action="{{ url('store/payment') }}" aria-label="{{ __('create_price') }}" method="POST">
            {{csrf_field()}}
            <div class="row">
                <div class="col-lg-12">

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Amount') }}</label>

                        <input id="year" type="text" class="c-input form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" name="price" placeholder="1000" value="{{ old('price') }}" required autofocus>

                        @if ($errors->has('price'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('price') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="bio"></label>
                        <button class="c-btn c-btn--info" type="submit">Create Price!
                        </button>
                    </div>


                </div>

            </div>

        </form>

        <hr>
        <div>
{{--            <p><b>Current Price: {{number_format($price->price)}}</b></p>--}}
{{--            --}}{{--<p><b>Current Price: {{$price->user->surname}}</b></p>--}}
{{--            <p><b>Number of Registered Students: {{$student->count()}}</b></p>--}}
{{--            <p><b>Expected Amount: {{$student->count() * $price->price}}</b></p>--}}
        </div>


    </div>

@endsection
