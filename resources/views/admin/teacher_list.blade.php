@extends('layouts.master')
@section('title', 'EPortal Online Exam System - List of Teachers')

@section ('content')
    <div class="col-sm-12 col-lg-8">

        <div class="c-table-responsive@tablet">
            @if($teachers->count() > 0)
                <table class="c-table u-mb-large">
                    <caption class="c-table__title">
                        Teacher's List
                    </caption>



                    <thead class="c-table__head c-table__head--slim">
                    <tr>
                        <th class="c-table__cell c-table__cell--head">Name</th>
                        <th class="c-table__cell c-table__cell--head">Reg. No.</th>
                        <th class="c-table__cell c-table__cell--head">Gender</th>
                        <th class="c-table__cell c-table__cell--head">Class Name</th>
                        <th class="c-table__cell c-table__cell--head">Login Status</th>

                        <th class="c-table__cell c-table__cell--head">
                            <span class="u-hidden-visually">Actions</span>
                        </th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($teachers as $teacher)
                        <tr class="c-table__row">
                            <td class="c-table__cell">
                                {{$teacher->surname}}
                                {{$teacher->other_names}}
                            </td>

                            <td class="c-table__cell">
                                {{$teacher->email}}
                            </td>
                            <td class="c-table__cell">
                                {{$teacher->gender}}
                            </td>
                            <td class="c-table__cell">
                                {{$teacher->teacher_level()->first()?$teacher->teacher_level()->first()->name:"no class assigned"}}
                            </td>
                            <td class="c-table__cell">
                                @if($teacher->paid == "0")
                                    <div class="badge badge-danger">Can't Login</div>
                                @else
                                    <div class="badge badge-success">Can Login</div>
                                @endif
                            </td>
                            <td class="c-table__cell u-text-right">
                                @if($teacher->paid == "0")
                                    <a class="c-btn c-btn--info c-btn--small"  href="{{ url('activate/student/'.$teacher->id)}}">Activate</a>
                                    @else
                                    <a class="c-btn c-btn--warning c-btn--small"  href="{{ url('activate/student/'.$teacher->id)}}">Deactivate</a>
                                @endif
                                   <a class="c-btn c-btn--danger c-btn--small" onclick="return confirm('Are you sure you want to remove teacher?');" href="{{ url('teacher/remove', $teacher->id) }}">Remove</a>
                             </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {{ $teachers->links() }}
            @else
                <div class="container">
                    <div class="col-md-12 alert alert-info"> You have not registered any teacher</div>
                    <br>
                </div>
            @endif

        </div>
    </div>


@endsection