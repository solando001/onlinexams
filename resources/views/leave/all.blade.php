@extends('layouts.master')
@section('title', 'EPortal Online Exam System - List of Leaves')

@section ('content')
    <div class="col-sm-12 col-lg-8">

        <div class="c-table-responsive@tablet">
            @if($leaves->count() > 0)
                @if(session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif

                <table class="c-table u-mb-large">
                    <caption class="c-table__title">
                        Leave List
                    </caption>



                    <thead class="c-table__head c-table__head--slim">
                    <tr>
                        <th class="c-table__cell c-table__cell--head">Student Name</th>
                        <th class="c-table__cell c-table__cell--head">Departure Date</th>
                        <th class="c-table__cell c-table__cell--head">Return Date</th>
                        <th class="c-table__cell c-table__cell--head">Purpose</th>
                        <th class="c-table__cell c-table__cell--head">Status</th>
{{--                        <th class="c-table__cell c-table__cell--head"></th>--}}

{{--                        <th class="c-table__cell c-table__cell--head">--}}
{{--                            <span class="u-hidden-visually">Actions</span>--}}
{{--                        </th>--}}
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($leaves as $leave)
                        <tr class="c-table__row">
                            <td class="c-table__cell">
                                {{$leave->user->surname}} {{$leave->user->other_names}}
                            </td>

                            <td class="c-table__cell">
                                {{  date('d/m/Y, h:i A', strtotime($leave->depart)) }}
{{--                                {{$leave->depart}}--}}
                            </td>

                            <td class="c-table__cell">
                                {{  date('d/m/Y, h:i A', strtotime($leave->return)) }}
{{--                                {{$leave->return}}--}}
                            </td>

                            <td class="c-table__cell">
                                {{$leave->purpose}}
                            </td>
{{--                            <td class="c-table__cell">--}}
{{--                                {{$teacher->teacher_level()->first()?$teacher->teacher_level()->first()->name:"no class assigned"}}--}}
{{--                            </td>--}}
                            <td class="c-table__cell">
{{--                                <div class="badge badge-danger">{{$leave->status}}</div>--}}
                                @if($leave->status == "pending")
                                    <a class="c-btn c-btn--success c-btn--small" onclick="return confirm('Are you sure you want to Grant the leave?');" href="{{ url('grant/leave', $leave->uuid) }}">Grant</a>
                                    <a class="c-btn c-btn--danger c-btn--small" onclick="return confirm('Are you sure you want to Grant the leave?');" href="{{ url('deny/leave', $leave->uuid) }}">Deny</a>
                                @endif
                            </td>
                            {{--                            <td class="c-table__cell u-text-right">--}}
{{--                                @if($teacher->paid == "0")--}}
{{--                                    <a class="c-btn c-btn--info c-btn--small"  href="{{ url('activate/student/'.$teacher->id)}}">Activate</a>--}}
{{--                                @else--}}
{{--                                    <a class="c-btn c-btn--warning c-btn--small"  href="{{ url('activate/student/'.$teacher->id)}}">Deactivate</a>--}}
{{--                                @endif--}}
{{--                                <a class="c-btn c-btn--success c-btn--small" onclick="return confirm('Are you sure you want to Grant the leave?');" href="{{ url('teacher/remove', $leave->id) }}">Grant</a>--}}
{{--                                <a class="c-btn c-btn--danger c-btn--small" onclick="return confirm('Are you sure you want to Grant the leave?');" href="{{ url('teacher/remove', $leave->id) }}">Deny</a>--}}
{{--                            </td>--}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {{ $leaves->links() }}
            @else
                <div class="container">
                    <div class="col-md-12 alert alert-info"> You do not have any leave application</div>
                    <br>
                </div>
            @endif

        </div>
    </div>


@endsection