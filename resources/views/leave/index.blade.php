@extends('layouts.master')
@section('title', 'EPortal - Create Fees')

@section('content')
    <div class="col-xl-8 c-card u-p-medium u-mb-medium">

        <h3>Apply for Leave</h3>
        @if(session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif

        @if(session('error'))
            <div class="alert alert-success">
                {{ session('error') }}
            </div>
        @endif


        <form method="POST" action="{{ url('apply/leave') }}" aria-label="{{ __('apply_for_leave') }}">
            @csrf
            <div class="row">
                <div class="col-lg-12">
                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Departure Date') }}</label>
                        <input id="name" type="datetime-local" class="c-input form-control{{ $errors->has('depart') ? ' is-invalid' : '' }}" name="depart"
                               placeholder="depart" value="{{ old('depart') }}" required autofocus>

                        @if ($errors->has('depart'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('depart') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Resumption Date') }}</label>
                        <input id="name" type="datetime-local" class="c-input form-control{{ $errors->has('resume') ? ' is-invalid' : '' }}" name="resume"
                               placeholder="Amount" value="{{ old('resume') }}" required>

                        @if ($errors->has('resume'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('resume') }}</strong>
                            </span>
                        @endif
                    </div>

                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Purpose') }}</label>
{{--                        <input id="name" type="datetime-local" class="c-input form-control{{ $errors->has('resume') ? ' is-invalid' : '' }}" name="resume"--}}
{{--                               placeholder="Amount" value="{{ old('resume') }}" required autofocus>--}}
                        <textarea rows="10" cols="10" name="purpose" class="form-control" required>{{ old('purpose') }}</textarea>
{{--                        @if ($errors->has('resume'))--}}
{{--                            <span class="invalid-feedback" role="alert">--}}
{{--                                <strong>{{ $errors->first('resume') }}</strong>--}}
{{--                            </span>--}}
{{--                        @endif--}}
                    </div>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="bio"></label>
                        <button class="c-btn c-btn--info" type="submit">Apply!
                        </button>
                    </div>

                </div>
            </div>
        </form>
    </div>

@endsection

