@extends('layouts.master')
@section('title', 'EPortal - Student Home')

@section('content')

    <div class="col-xl-8 c-card u-p-medium u-mb-medium">
        <h3>Welcome {{ Auth::user()->surname }} {{ Auth::user()->other_names }}</h3>

        <!--   Previous Form for Add Teacher --->
        <div class="row">
            <div class="col-sm-12 col-lg-6">
                <div class="c-card u-p-medium u-text-center u-mb-medium" data-mh="landing-cards">
                    <img class="u-mb-small" src="/asset/img/icon-intro3.svg" alt="iPhone icon">

                    <h4 class="u-h6 u-text-bold u-mb-small">
                        <br> Manage Fees.
                    </h4>
                    <a class="c-btn c-btn--info c-btn--fullwidth" href="{{ url('/fees/list') }}">Pay Fees</a>
                </div>
            </div>

            <div class="col-sm-12 col-lg-6">
                <div class="c-card u-p-medium u-text-center u-mb-medium" data-mh="landing-cards">
                    <img class="u-mb-small" src="/asset/img/icon-intro1.svg" alt="iPhone icon">

                    <h4 class="u-h6 u-text-bold u-mb-small">
                        <br> Manage Leave
                    </h4>
                    <a class="c-btn c-btn--info c-btn--fullwidth" href="{{ url('student/apply/leave') }}">Apply for Leave</a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-lg-6">
                <div class="c-card u-p-medium u-text-center u-mb-medium" data-mh="landing-cards">
                    <img class="u-mb-small" src="/asset/img/icon-intro3.svg" alt="iPhone icon">

                    <h4 class="u-h6 u-text-bold u-mb-small">
                        Class Notes
                    </h4>
                    <div class="u-flex u-mt-medium">
                        <a class="c-btn c-btn--info c-btn--fullwidth u-mr-xsmall" href="{{ url('/exam/create') }}">
                            View Notes</a>
{{--                        <a class="c-btn c-btn--secondary c-btn--fullwidth" href="{{ url('/exam') }}">See Exam List</a>--}}
                    </div>
                </div>
            </div>

            <div class="col-sm-12 col-lg-6">
                <div class="c-card u-p-medium u-text-center u-mb-medium" data-mh="landing-cards">
                    <img class="u-mb-small" src="/asset/img/icon-intro3.svg" alt="iPhone icon">

                    <h4 class="u-h6 u-text-bold u-mb-small">
                        Manage Attendance
                    </h4>
                    <div class="u-flex u-mt-medium">
                        <a class="c-btn c-btn--info c-btn--fullwidth u-mr-xsmall" href="{{ url('/theory/question') }}">
                            Mark Attendance</a>
{{--                        <a class="c-btn c-btn--secondary c-btn--fullwidth" href="{{ url('/show/theory/questions') }}">See Theory Exam List</a>--}}
                    </div>
                </div>
            </div>

        </div>
    </div>

    @endsection