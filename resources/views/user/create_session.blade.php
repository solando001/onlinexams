@extends('layouts.master')
@section('title', 'EPortal Online Exam System - Create Term')


@section('content')

<div class="col-xl-8 c-card u-p-medium u-mb-medium">
	
	<h3>Create New Session</h3>
    <div class="alert alert-info">
        This will automatically close the previous session and start a new one
    </div>

	<form method="POST" action="{{ url('store/session') }}" aria-label="{{ __('create_user') }}">
        @csrf

        @if(session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
        @endif
        @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif


        <div class="row">
            
			<div class="col-lg-12">

				<div class="c-field u-mb-small">
                    <label class="c-field__label" for="firstName">{{ __('Create Session') }}</label> 

                    <input id="year" type="text" class="c-input form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="E.G 2018/2019" value="{{ old('name') }}" required autofocus>

                    @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>

                 <div class="c-field u-mb-small">
                    <label class="c-field__label" for="bio"></label>
                    <button class="c-btn c-btn--info" type="submit">Create Session!
                    </button> 
                </div>
         
        </form>


            </div>
          <br>

            <!--<div class="col-lg-12">
                <h5>Click on the radio button to change Term</h5>
                {{-- @foreach($termlist as $term) --}}
                 {{-- <form action="/change/term/{{$term->id}}" method="POST"> --}}
                    {{-- {{  method_field('PATCH') }} --}}
                    {{-- <input type="checkbox" name="name" onchange="this.form.submit()" {{$term->completed ? 'checked' : ''}} > {{$term->name}} <br> --}}
                </form>
                {{-- @endforeach --}}
            </div>-->
            {{-- <div class="col-lg-12"> --}}
                {{-- <div class="alert alert-info">
                  <strong>Note!</strong> Selecting a new Term will automatically close the previous Term Set
                </div>

                 @if(session('messages'))
                <div class="alert alert-success">
                    {{ session('messages') }}
                </div>
                @endif --}}


                {{-- <form action="{{url('/change/term')}}" method="POST">
                @csrf
                <div class="c-field u-mb-small">
                    <label class="c-field__label" for="firstName">{{ __('Select Term') }}</label>
                    <select class="form-control" name="name" required>
                        <option value=""> Select An Option</option>
                        @foreach($termlist as $term)
                        <option value="{{$term->id}}"> {{$term->name}} </option>
                        @endforeach
                       
                    </select>
                </div>

                 <div class="c-field u-mb-small">
                    <label class="c-field__label" for="bilodjfo"></label>
                    <button class="c-btn c-btn--default" type="submit">Create Term!
                    </button> 
                </div>
                </form>
            </div> --}}
            <hr>
            <div class="col-lg-12">
            <br> 
                <blockquote>
                    Current Session:  {{$cur_session->name}} <br>
                    Current Term: {{$cur_term->name}}
                </blockquote>
                <br>
                <H3>Session</H3>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Session</th>
                        <th scope="col">Status</th>
                       {{-- <th scope="col">Action</th> --}}
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                    @foreach($sessionlist as $list)
                    <tr>
                        <th scope="row">{{$i++}}</th>
                        <td>{{$list->name}}</td>
                        <td>
                            @if($list->active)
                                <span class="text-success">Active</span>
                                @else
                                <span class="text-danger">Not Active</span>
                            @endif
                        </td>
                       
                       
                    </tr>
                        @endforeach
                    </tbody>
                </table>
                <br>
                <H3>Term</H3>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Term</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                    @foreach($termlist as $list)
                        <tr>
                            <th scope="row">{{$i++}}</th>
                            <td>{{$list->name}}</td>
                            <td>
                                @if($list->active)
                                    <span class="text-success">Active</span>
                                @else
                                    <span class="text-danger">Not Active</span>
                                @endif
                            </td>
                            <td>
                                @if($list->active)
                                    <span class="text-success">Active Term</span>
                                @else
                                    <a href="{{ url('change/term/'.$list->id) }}" class="btn btn-sm btn-info">Activate Term</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
             </div>
        </div>

	</form>

</div>

@endsection
