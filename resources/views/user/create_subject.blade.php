@extends('layouts.master')
@section('title', 'EPortal Online Exam System - Create Subject')


@section('content')

<div class="col-xl-8 c-card u-p-medium u-mb-medium">

	<h3>Create Subject</h3>
	<form method="POST" action="{{ url('store/subject') }}" aria-label="{{ __('create_user') }}">
        @csrf

        @if(session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
        @endif


		<div class="row">
			<div class="col-lg-12">
				<div class="c-field u-mb-small">
                    <label class="c-field__label" for="firstName">{{ __('Name of Subject') }}</label> 

                    <input id="name" type="text" class="c-input form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Mathematics" value="{{ old('name') }}" required autofocus>

                    @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                </div>


                <div class="c-field u-mb-small">
                    <label class="c-field__label" for="description">{{ __('Description of Subject') }}</label> 

                    <input id="description" type="text" class="c-input form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" placeholder="This is Mathematics" value="{{ old('description') }}" required autofocus>

                    @if ($errors->has('description'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                    @endif
                </div>

                <input type="hidden" name="display_name" value="">


                 <div class="c-field u-mb-small">
                    <label class="c-field__label" for="bio"></label>
                    <button class="c-btn c-btn--info" type="submit">Create Subject!
                    </button> 
                </div>


			</div>
		</div>
    </form>

</div>
@endsection