@extends('layouts.master')

@section('title', 'EPortal Online Exam System - List of Subjects')

@section ('content')
<div class="col-sm-12 col-lg-8">

	<div class="c-table-responsive@tablet">
		@if($subjects->count() > 0)
		<table class="c-table u-mb-large">
			<caption class="c-table__title">
				List of Subject
			</caption>
			<thead class="c-table__head c-table__head--slim">
				<tr>
					<th class="c-table__cell c-table__cell--head">Subject Name</th>
                    <th class="c-table__cell c-table__cell--head">Subject Detailed Description</th>
                    <th class="c-table__cell c-table__cell--head">No. of Student</th>

				</tr>
			</thead>

			<tbody>
				@foreach ($subjects as $user)
				<tr class="c-table__row">
					<td class="c-table__cell">
						{{$user->name}}                             
                    </td>

                     <td class="c-table__cell">
                        {{$user->description}}
                    </td>
					<td class="c-table__cell">
						{{count($user->students)}}
					</td>
                    
                </tr>
                @endforeach

                                            <!--<tr class="c-table__row">
                                                <td class="c-table__cell">English Language
                                                    <span class="u-block u-text-mute u-text-xsmall">
                                                        Teacher: Mrs Amoko Alice
                                                    </span>
                                                </td>
                                                <td class="c-table__cell">1hr 20min</td>
                                                <td class="c-table__cell">
                                                    <del class="u-text-danger">Completed</del>
                                                </td>
                                                <td class="c-table__cell u-text-right">
                                                    <a class="c-btn is-disabled" href="#">Start Exam</a>
                                                </td>
                                            </tr>-->
                                        </tbody>
                                    </table>

                                    {{ $subjects->links() }}
                                    @else
                                    <div class="container">
                                    	<div class="col-md-12 alert alert-info"> You do not have Subject yet</div>
                                    	<br>
                                    </div>
                                    @endif

                                </div>
                            </div>


@endsection