@extends('layouts.master_list')

@section('title', 'EPortal Online Exam System- List of Student in a Class')

    @section('content')
        <div class="col-md-12 col-lg-12 col-sm-12">
            <h4>
                List of Student in {{$class->name}} Class

{{--                @if(Auth::user()->hasRole('teacher'))--}}

{{--                @else--}}
{{--                    @if($class->name == "Ex-Students")--}}

{{--                    @else--}}

{{--                        @if(count($students) > 0)--}}

{{--                            <span class="pull-right"><a href="{{ url('/promote/'.$class->id.'/student') }}" class="c-btn c-btn--fancy">Promote All Students</a></span>--}}

{{--                        @endif--}}
{{--                    @endif--}}

{{--                @endif--}}

            </h4>

            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif
            <form action="{{ url('fix/class') }}" method="POST">
                @csrf
            <table class="table table-responsive  table-hover">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Student Name</th>
                    <th scope="col">Reg. No</th>
                    <th scope="col">No. of Login</th>
                    <th scope="col">Login Status</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; ?>
            @foreach($students as $student)
                    <?php  $login = \App\LoginLog::where('user_id', $student->id)->count(); ?>
                <tr>
                    <th scope="row"><input type="checkbox" name="studentId[]" value="{{$student->id}}"> </th>
                    <td>{{$student->surname}}, {{$student->other_names}}</td>
                    <td>{{$student->email}}</td>
                    <td>{{$login}}</td>
                    <td>
                        @if($student->paid == '0')
                            <div class="badge badge-danger">Cannot Login</div>
                        @else
                            <div class="badge badge-success">Can Login</div>
                        @endif
                    </td>
                    <td>
                        @if(Auth::user()->hasRole('teacher'))

                        @else
                            @if($student->paid == '0')
                                <a class="c-btn c-btn--info c-btn--small" href="{{ url('activate/student/'.$student->id) }}">Activate Login</a>
                            @else
                                <a class="c-btn c-btn--danger c-btn--small" href="{{ url('activate/student/'.$student->id) }}">Deactivate Login</a>
                            @endif

                                <a class="c-btn c-btn--warning c-btn--small" href="{{ url('/edit/'.$student->id.'/student') }}">Edit Info</a>
                                <a class="c-btn c-btn--secondary c-btn--small" href="{{ url('/attendance/'.$student->id) }}">View Attendance</a>
                                {{-- <a class="c-btn c-btn--fancy c-btn--small" href="{{ url('/remove/student/'.$student->id) }}"
                                   onclick="return confirm('Are you sure you want to remove this student?')">Remove Student</a> --}}
{{--                                {{$student->parent->name}}--}}
                            <?php //$student_parent = isset($student->parent) ? $student->parent : ""; ?>
{{--                            @if($student_parent != null)--}}
{{--                                    <a class="c-btn c-btn--fancy c-btn--small" href="{{ url('/edit/parent/'.$student_parent->id) }}">Edit Parent Info</a>--}}
{{--                            @else--}}
{{--                                <a class="c-btn c-btn--primary c-btn--small" href="{{ url('/add/parent/'.$student->id) }}">Add Parent Info</a>--}}
{{--                            @endif--}}

                        @endif
                    </td>
                </tr>
            @endforeach

                </tbody>
            </table>
                <div class="row">
                    <div class="col-md-2">
                        <select class="form-control" name="new_class_id" required>
                            <option value="">Select One</option>
                            @foreach($class_list as $cl)
                                <option value="{{ $cl->id }}">{{ $cl->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <input type="hidden" name="current_class" value="{{ $class->id }}">
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-primary">Promote Selected Students</button>
                    </div>
                </div>

            </form>
        </div>
    @endsection