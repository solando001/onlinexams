@extends('layouts.student')
@section('title', 'EPortal Online Exam System- Student Home')
@section('content')

            <div class="c-card u-p-medium u-text-center u-mb-medium" data-mh="landing-cards">

{{--                <div class="alert alert-info">--}}
{{--                    <strong>Attention! Virtual learning assessments holds next week  between 15th-26th, June, 2020</strong>--}}
{{--                </div>--}}

                <img class="u-mb-small" src="/asset/img/icon-intro3.svg" alt="iPhone icon">

                <h4 class="u-h6 u-text-bold u-mb-small">
                    Welcome {{ Auth::user()->surname }} {{ Auth::user()->other_names }}  to Exam Portal  <br> Click the Button below to view List of exams
                </h4>

                @if(Auth::user()->paid == '0')
                    <div class="alert alert-warning">
                        <strong>Attention! You haven't redeem your payment, please contact School Admin</strong>
                    </div>
                    @else

                <a class="c-btn c-btn--info" href="{{url('student/exam/all')}}">OBJ Exam List</a>
                <a class="c-btn c-btn--primary" href="{{url('student/exam/theory')}}">Theory Exam List</a>
                <a class="c-btn c-btn--fancy" href="{{url('student/lesson/note')}}">Class Lesson List</a>

                    @endif
            </div>
                      

               <!-- <div class="col-sm-12 col-lg-9">
                    
                                <div class="c-table-responsive@tablet">
                                    <table class="c-table u-mb-large">
                                        <caption class="c-table__title">
                                            List of Exams 
                                        </caption>
                                        <thead class="c-table__head c-table__head--slim">
                                            <tr>
                                                <th class="c-table__cell c-table__cell--head">Exam Name</th>
                                                <th class="c-table__cell c-table__cell--head">Time Allowed</th>
                                                <th class="c-table__cell c-table__cell--head">Number of Question</th>
                                                <th class="c-table__cell c-table__cell--head">
                                                    <span class="u-hidden-visually">Actions</span>
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr class="c-table__row">
                                                <td class="c-table__cell">Mathematics
                                                    <span class="u-block u-text-mute u-text-xsmall">
                                                      Teacher: Mr Morenike Amoe
                                                    </span>
                                                </td>

                                                <td class="c-table__cell">1hr 30min</td>
                                                <td class="c-table__cell">40 Questions
                                                    <span class="u-block u-text-xsmall u-text-mute">
                                                        32 Tickets remaining
                                                    </span>
                                                </td>
                                                <td class="c-table__cell u-text-right">
                                                    <a class="c-btn c-btn--success" href="#">Start Exam</a>
                                                </td>
                                            </tr>

                                            <tr class="c-table__row">
                                                <td class="c-table__cell">English Language
                                                    <span class="u-block u-text-mute u-text-xsmall">
                                                        Teacher: Mrs Amoko Alice
                                                    </span>
                                                </td>
                                                <td class="c-table__cell">1hr 20min</td>
                                                <td class="c-table__cell">
                                                    <del class="u-text-danger">Completed</del>
                                                </td>
                                                <td class="c-table__cell u-text-right">
                                                    <a class="c-btn is-disabled" href="#">Start Exam</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>-->


                
@endsection