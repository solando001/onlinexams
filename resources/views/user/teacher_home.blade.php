@extends('layouts.master')
@section('title', 'EPortal Online Exam System')

@section('content')

<div class="col-xl-8 c-card u-p-medium u-mb-medium">


    <div class="row">
     <div class="col-sm-12 col-lg-6">
            <div class="c-card u-p-medium u-text-center u-mb-medium" data-mh="landing-cards">
                <img class="u-mb-small" src="/asset/img/icon-intro3.svg" alt="iPhone icon">

                <h4 class="u-h6 u-text-bold u-mb-small">
                    Welcome to the teacher's dashboard. 
                </h4>
                <a class="c-btn c-btn--info" href="{{ url('create/student')}}">Add Student Here</a>
            </div>
        </div>

     <div class="col-sm-12 col-lg-6">
            <div class="c-card u-p-medium u-text-center u-mb-medium" data-mh="landing-cards">
                <img class="u-mb-small" src="/asset/img/icon-intro3.svg" alt="iPhone icon">

                <h4 class="u-h6 u-text-bold u-mb-small">
                    Welcome to the teacher's dashboard. 
                </h4>
                <a class="c-btn c-btn--info" href="{{ url('student/list')}}">View Student Here</a>
            </div>
        </div>
    <!--<form method="POST" action="{{ url('store/user') }}" aria-label="{{ __('create_user') }}">
        @csrf

         @if(Session::has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
        @endif
        <div class="row">

            <div class="col-lg-12">
                <div class="c-field u-mb-small">
                    <label class="c-field__label" for="firstName">{{ __('Surname') }}</label> 

                    <input id="surname" type="text" class="c-input form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" placeholder="Adebayo" value="{{ old('surname') }}" required autofocus>

                    @if ($errors->has('surname'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('surname') }}</strong>
                    </span>
                    @endif
                </div>


                <div class="c-field u-mb-small">
                    <label class="c-field__label" for="firstName">{{ __('Other Names') }}</label> 

                    <input id="other_names" type="text" class="c-input form-control{{ $errors->has('other_names') ? ' is-invalid' : '' }}" name="other_names" placeholder="Adebayo" value="{{ old('other_names') }}" required autofocus>

                    @if ($errors->has('other_names'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('other_names') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="c-field u-mb-small">
                    <label class="c-field__label" for="email">{{ __('E-Mail Address') }}</label> 

                    <input id="email" type="text" class="c-input form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="adebayo@gmail.com" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>


                <div class="c-field u-mb-small">
                    <label class="c-field__label" for="level_id">{{ __('Class') }}</label> 

                    <select id="level_id" class="c-input form-control{{ $errors->has('level_id') ? ' is-invalid' : '' }}" name="level_id" value="{{ old('level_id') }}" required autofocus>
                        @foreach($levels as $level)
                        <option value="{{$level->id}}">{{$level->name}}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('level_id'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('level_id') }}</strong>
                    </span>
                    @endif
                </div>


                <div class="c-field u-mb-small">
                    <label class="c-field__label" for="subject_ids">{{ __('Select Subject (Press Ctrl + Subject to select)') }}</label> 

                    <select multiple id="subject_ids" class="c-input form-control{{ $errors->has('subject_ids') ? ' is-invalid' : '' }}" name="subject_ids[]" value="{{ old('subject_ids') }}" required autofocus>
                        @foreach($subjects as $subject)
                        <option value="{{$subject->id}}">{{$subject->name}}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('subject_ids'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('subject_ids') }}</strong>
                    </span>
                    @endif
                </div>



                <div class="c-field u-mb-small">
                    <label class="c-field__label" for="username"></label> 

                    <input id="username" type="hidden" class="c-input form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" placeholder="adebayo@gmail.com" value="{{ old('username') }}" value="Teacher" required autofocus>

                    @if ($errors->has('username'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                    @endif
                </div>


                <input type="hidden" name="role_id" value="3">

                <div class="c-field u-mb-small">
                    <label class="c-field__label" for="bio"></label>
                    <button class="c-btn c-btn--info" type="submit">Create Teacher Account!
                    </button> 
                </div>


            </div>

        <div class="col-lg-6">
            <div class="c-field u-mb-small">
                <label class="c-field__label" for="companyName">Company Name</label>
                <input class="c-input" id="companyName" type="text" placeholder="Dashboard Ltd.">
            </div>

            <div class="c-field u-mb-small">
                <label class="c-field__label" for="website">Website</label>
                <input class="c-input" id="website" type="text" placeholder="zawiastudio.com">
            </div>  

            <label class="c-field__label" for="socialProfile">Social Profiles</label>

            <div class="c-field has-addon-left u-mb-small">
                <span class="c-field__addon u-bg-twitter">
                    <i class="fa fa-twitter u-color-white"></i>
                </span>
                <input class="c-input" id="socialProfile" type="text" placeholder="Clark">
            </div>

            <div class="c-field has-addon-left">
                <span class="c-field__addon u-bg-facebook">
                    <i class="fa fa-facebook u-color-white"></i>
                </span>
                <input class="c-input" type="text" placeholder="Clark">
            </div>

             <button class="c-btn c-btn--info c-btn--fullwidth" type="submit">Sign in to Dashboard</button> 
         </div> 
     </form>

 -->
 </div>
</div>
@endsection
