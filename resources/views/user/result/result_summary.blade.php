@extends('layouts.flat')
@section('title', 'Report Sheet')
@section('styles')
    <style>
        .c-invoice #centre{
            text-transform: capitalize;
            text-align: center;
            text-decoration: #0f0f10;
            font-size: x-large;
        }
    </style>
{{--    <script type="text/javascript">--}}
{{--        // $(document).ready(function(){--}}
{{--        //     $('.print').printPage();--}}
{{--        // });--}}

{{--        $('button').on('click',function(){--}}
{{--            print();--}}
{{--        });--}}
{{--    </script>--}}

    @endsection

    @section('content')

        <div class="container">
            <div class="row u-mb-medium u-justify-center" >
                <div class="col-lg-12" id="printarea">
                    <div class="c-invoice" >
{{--                        <button class="prt">Oka Print</button>--}}
                        @if($school == "Covenant Child School" || $school == "IVY HILL ACADEMY (SECONDARY SECTION)")
                            <table class="table">
                                <tr>
                                    <td>
                                        <img src="{{asset($logo)}}" width="150" class="pull-left">
                                    </td>
                                    <td>
                                        <h2>{{$school}}</h2>
                                        <h5>{{$address}}</h5>
                                        <h5>{{$vision}}</h5>
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th><td>{{$users->surname}} {{$users->other_names}}</td>
                                    <th>Gender</th><td>{{$users->gender}}</td>
                                </tr>
                                <tr>
                                    <th>Session</th><td>{{$session->name}}</td>
                                    <th>Term</th><td>{{$term->name}}</td>
                                </tr>
                                <tr>
                                    <th>Class</th><td>{{$level->name}}</td>
                                    <th>Admission No.</th><td>{{$users->email}}</td>
                                    {{--                                    <th>No. in class</th><td>{{$position}}</td>--}}
                                </tr>
                                </thead>
                            </table>
                            <hr>

                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Subject</th>
                                    <th scope="col">CA1</th>
                                    <th scope="col">CA2</th>
                                    <th scope="col">Exam</th>
                                    <th scope="col">Total</th>
                                    <th scope="col">Av.Score</th>
                                    <th scope="col">Grade</th>
                                    <th scope="col">Remark</th>
                                    {{-- <th scope="col">Class Highest</th> --}}
                                    {{-- <th scope="col">Class Lowest</th> --}}
                                    {{--                                    <th scope="col">Grade</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($scores as $score)
                                    <tr>
                                        <th scope="row">{{$i++}}</th>
                                        <td>{{$score->subject->name}}</td>
                                        <td>{{number_format($score->ca1,0)}}</td>
                                        <td>{{number_format($score->ca2,0)}}</td>
                                        <td>{{number_format($score->exam,0)}}</td>
                                        <td>{{number_format($totally = $score->exam+$score->ca1+$score->ca2,0)}}</td>
                                        <td>
                                            {{ \App\Helpers\ResportSummary::calculateAverageScoreForEachSubject($score->subject->id, $level->id, $term->id, $session->id) }}
                                        </td>
                                        <?php  
                                            $totally = $score->exam+$score->ca1+$score->ca2;
                                            // $grade = '';
                                            $remark = '';
                                            
                                            if($level->name == 'JSS 1 Awesome' || $level->name == 'JSS 1 Best' || $level->name == 'JSS 2' || $level->name == 'JSS 2 Best' || $level->name == 'JSS 2 Awesome'){

                                                    if(isset($score->total)){
                                                        $total = $score->total;
                                                        if ($total >= 0 && $total <= 39) {
                                                            @$grade = "F";
                                                            @$remark = "Failed";
                                                        } elseif ($total >= 40 && $total <= 49) {
                                                            @$grade = "D";
                                                            @$remark = "Pass";
                                                        } elseif ($total >= 50 && $total <= 59) {
                                                            @$grade = "C";
                                                            @$remark = "Average";
                                                        } elseif ($total >= 60 && $total <= 69) {
                                                            @$grade = "B";
                                                            @$remark = "Good";
                                                        } elseif ($total >= 70 && $total <= 79) {
                                                            @$grade = "AB";
                                                            @$remark = "Very Good";
                                                        } elseif ($total >= 80 && $total <= 100) {
                                                            @$grade = "A";
                                                            @$remark = "Excellent";
                                                        } else {
                                                            @$grade = "invalid";
                                                        }
                                                    }else{
                                                        @$grade = "-";
                                                    }
                                                }elseif($level->name == 'JSS 3'){

                                                    if(isset($score->total)){
                                                        $total = $score->total;
                                                        if ($total >= 0 && $total <= 49) {
                                                            @$grade = "F";
                                                            @$remark = "Failed";
                                                        } elseif ($total >= 50 && $total <= 59) {
                                                            @$grade = "P";
                                                            @$remark = "Passed";
                                                        } elseif ($total >= 60 && $total <= 79) {
                                                            @$grade = "C";
                                                            @$remark = "Credit";
                                                        } elseif ($total >= 80 && $total <= 100) {
                                                            @$grade = "A";
                                                            @$remark = "Excellent";
                                                        } else {
                                                            @$grade = "invalid";
                                                        }
                                                    }else{
                                                        @$grade = "-";
                                                    }

                                                }elseif($level->name == 'SS 1' || $level->name == 'SS 2' || $level->name == 'SS 3'){
                                                    if(isset($score->total)){
                                                        $total = $score->total;
                                                        if ($total >= 0 && $total <= 49) {
                                                            @$grade = "F";
                                                            @$remark = "Failed";
                                                        } elseif ($total >= 50 && $total <= 59) {
                                                            @$grade = "P";
                                                            @$remark = "Pass";
                                                        } elseif ($total >= 60 && $total <= 64) {
                                                            @$grade = "C6";
                                                            @$remark = "Average";
                                                        } elseif ($total >= 65 && $total <= 69) {
                                                            @$grade = "C5";
                                                            @$remark = "Credit";
                                                        } elseif ($total >= 70 && $total <= 74) {
                                                            @$grade = "C4";
                                                            @$remark = "Credit";
                                                        } elseif ($total >= 75 && $total <= 79) {
                                                            @$grade = "B2";
                                                            @$remark = "Good";
                                                        } elseif ($total >= 80 && $total <= 84) {
                                                            @$grade = "B1";
                                                            @$remark = "Very Good";
                                                        } elseif ($total >= 85 && $total <= 89) {
                                                            @$grade = "A2";
                                                            @$remark = "Excellent";
                                                        } elseif ($total >= 90 && $total <= 100) {
                                                            @$grade = "A1";
                                                            @$remark = "Intelligent";
                                                        } else {
                                                            @$grade = "invalid";
                                                        }
                                                    }else{
                                                        @$grade = "-";
                                                    }
                                                }
                                            ?>
                                        <td>
                                            {{ @$grade }}
                                        </td>
                                        <td>
                                            {{ $remark }}
                                        </td>
                                        {{-- <td>{{number_format($high,1)}}</td> --}}
                                        {{-- <td>{{number_format($low,1)}}</td> --}}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <hr>
                            <table class="table table-bordered">
                                <?php
                                    $total = $scores->sum('total');
                                    $ov_score =  count($scores) * 100;
                                    $stuff = $total / $ov_score;

                                    ?>
                                <thead>
                                <tr>
                                    <th>Total Mark</th><td>{{$total}} out of {{count($scores) * 100}}</td>
                                    <th>Percentage</th><td>{{number_format($total/$no_of_student,0)}}%</td>
                                </tr>
                                <tr>
                                    <th>Teacher's Remark</th><td>
                                        {{ \App\Helpers\ResportSummary::teachersRemark($stuff*100) }}
                                    </td>
                                    <th>Position in Class</th><td>
                                        @if($position == '')
                                            Nill
                                        @else
                                            {{$position}} out of {{$no_of_student}}
                                        @endif
                                    </td>
                                </tr>
                                </thead>
                            </table>
                        @else
                            <div class="row">
                                <table class="table">
                                    <tr>
                                        <td>
                                            <img src="{{asset(@$logo)}}" width="150" class="pull-left">
                                        </td>
                                        <td>
                                            <h2>{{$school}}</h2>
                                            <h5>{{$address}}</h5>
                                            <h5>{{$vision}}</h5>
                                        </td>
                                    </tr>
                                </table>
                                <br>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>Name</th><td>{{$users->surname}} {{$users->other_names}}</td>
                                    <th>Gender</th><td>{{$users->gender}}</td>
                                </tr>
                                <tr>
                                    <th>Session</th><td>{{$session->name}}</td>
                                    <th>Term</th><td>{{$term->name}}</td>
                                </tr>
                                <tr>
                                    <th>Class</th><td>{{$level->name}}</td>
                                    <th>Admission No.</th><td>{{$users->email}}</td>
{{--                                    <th>No. in class</th><td>{{$position}}</td>--}}
                                </tr>
                                </thead>
                            </table>
                            <hr>
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Subject</th>
                                    <th scope="col">CA1</th>
                                    <th scope="col">CA2</th>
                                    <th scope="col">Project</th>
                                    <th scope="col">Assign</th>
                                    <th scope="col">Exam</th>
                                    <th scope="col">Total</th>
                                    <th scope="col">Av.Score</th>
                                    <th scope="col">Grade</th>
                                    <th scope="col">Remark</th>
                                    {{-- <th scope="col">Class Highest</th> --}}
                                    {{-- <th scope="col">Class Lowest</th> --}}
{{--                                    <th scope="col">Grade</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; ?>
                                @foreach($scores as $score)
                                <tr>
                                    <th scope="row">{{$i++}}</th>
                                    <td>{{$score->subject->name}}</td>
                                    <td>{{number_format($score->ca1,0)}}</td>
                                    <td>{{number_format($score->ca2,0)}}</td>
                                    <td>{{number_format($score->prt1,0)}}</td>
                                    <td>{{number_format($score->prt2,0)}}</td>
                                    <td>{{number_format($score->exam,0)}}</td>
                                    <td>{{number_format($totally = $score->exam+$score->ca1+$score->ca2+$score->prt1+$score->prt2,0)}}</td>
                                    <td>
                                         {{ \App\Helpers\ResportSummary::calculateAverageScoreForEachSubject($score->subject->id, $level->id, $term->id, $session->id) }}
                                    </td>
                                    <?php  
                                            // $totally = $score->exam+$score->ca1+$score->ca2;
                                            $grade = '';
                                            $remark = '';
                                            
                                            if($level->name == 'JSS 1 Awesome' || $level->name == 'JSS 1 Best' || $level->name == 'JSS 2' || $level->name == 'JSS 2 Best' || $level->name == 'JSS 2 Awesome'){
                                                    if(isset($score->total)){
                                                        $total = $score->exam+$score->ca1+$score->ca2+$score->prt1+$score->prt2; //$score->total;
                                                        if ($total >= 0 && $total <= 39) {
                                                            @$grade = "F";
                                                            @$remark = "Failed";
                                                        } elseif ($total >= 40 && $total <= 49) {
                                                            @$grade = "D";
                                                            @$remark = "Pass";
                                                        } elseif ($total >= 50 && $total <= 59) {
                                                            @$grade = "C";
                                                            @$remark = "Average";
                                                        } elseif ($total >= 60 && $total <= 69) {
                                                            @$grade = "B";
                                                            @$remark = "Good";
                                                        } elseif ($total >= 70 && $total <= 79) {
                                                            @$grade = "AB";
                                                            @$remark = "Very Good";
                                                        } elseif ($total >= 80 && $total <= 100) {
                                                            @$grade = "A";
                                                            @$remark = "Excellent";
                                                        } else {
                                                            @$grade = "invalid";
                                                        }
                                                    }else{
                                                        @$grade = "-";
                                                    }
                                                }elseif($level->name == 'JSS 3'){

                                                    if(isset($score->total)){
                                                        $total =$score->exam+$score->ca1+$score->ca2+$score->prt1+$score->prt2; //$score->total;
                                                        if ($total >= 0 && $total <= 49) {
                                                            @$grade = "F";
                                                            @$remark = "Failed";
                                                        } elseif ($total >= 50 && $total <= 59) {
                                                            @$grade = "P";
                                                            @$remark = "Passed";
                                                        } elseif ($total >= 60 && $total <= 79) {
                                                            @$grade = "C";
                                                            @$remark = "Credit";
                                                        } elseif ($total >= 80 && $total <= 100) {
                                                            @$grade = "A";
                                                            @$remark = "Excellent";
                                                        } else {
                                                            @$grade = "invalid";
                                                        }
                                                    }else{
                                                        @$grade = "-";
                                                    }

                                                }elseif($level->name == 'SS 1' || $level->name == 'SS 2' || $level->name == 'SS 3'){
                                                    if(isset($score->total)){
                                                        $total = $score->total;
                                                        if ($total >= 0 && $total <= 49) {
                                                            @$grade = "F";
                                                            @$remark = "Failed";
                                                        } elseif ($total >= 50 && $total <= 59) {
                                                            @$grade = "P";
                                                            @$remark = "Pass";
                                                        } elseif ($total >= 60 && $total <= 64) {
                                                            @$grade = "C6";
                                                            @$remark = "Average";
                                                        } elseif ($total >= 65 && $total <= 69) {
                                                            @$grade = "C5";
                                                            @$remark = "Credit";
                                                        } elseif ($total >= 70 && $total <= 74) {
                                                            @$grade = "C4";
                                                            @$remark = "Credit";
                                                        } elseif ($total >= 75 && $total <= 79) {
                                                            @$grade = "B2";
                                                            @$remark = "Good";
                                                        } elseif ($total >= 80 && $total <= 84) {
                                                            @$grade = "B1";
                                                            @$remark = "Very Good";
                                                        } elseif ($total >= 85 && $total <= 89) {
                                                            @$grade = "A2";
                                                            @$remark = "Excellent";
                                                        } elseif ($total >= 90 && $total <= 100) {
                                                            @$grade = "A1";
                                                            @$remark = "Intelligent";
                                                        } else {
                                                            @$grade = "invalid";
                                                        }
                                                    }else{
                                                        @$grade = "-";
                                                    }
                                                }
                                            ?>
                                            <td>{{ @$grade }}</td>
                                            <td>{{ @$remark }}</td>
                                    {{-- <td>{{number_format($high,1)}}</td> --}}
                                    {{-- <td>{{number_format($low,1)}}</td> --}}
                                </tr>
                                    @endforeach
                                </tbody>
                            </table>
                                <hr>
                            <table class="table table-bordered">
                                <thead>
                                    <?php
                                    $total = $scores->sum('total');
                                    $ov_score =  count($scores) * 100;
                                    $stuff = $total / $ov_score;

                                    ?>
                                <tr>
                                    <th>Total Mark</th><td>{{$total}} out of {{count($scores) * 100}}</td>
                                    <th>Percentage</th><td>
{{--                                        {{number_format($total/count($scores)*100)}}--}}
                                       {{number_format($stuff*100,1)}} %</td>
                                </tr>
                                <tr>
                                    <th>Teacher's Remark</th><td>
                                        {{ \App\Helpers\ResportSummary::teachersRemark($stuff*100) }}
                                    </td>
                                    <th>Position in Class</th><td>
                                        @if($position == '')
                                            Nill
                                        @else
                                            {{$position}} out of {{$no_of_student}}
                                        @endif

                                    </td>
                                </tr>
                                </thead>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
                <div class="c-invoice__footer">
                <center><button class="btn btn-primary" onclick="PrintDoc()">Print Report Sheet</button></center>
                {{--                                    <button class="btn btn-dark" onclick="PrintPreview()">PRINT PREVIEW</button>--}}
                </div>
        </div>

        <script src="{{asset('asset/js/main.min.js')}}"></script>
{{--        <script src="{{asset('js/printPage.js')}}"></script>--}}
        @endsection

