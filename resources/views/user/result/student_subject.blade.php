@extends('layouts.student')
@section('title', 'List Of Student Subject')

    @section('content')

        <div class="col-sm-12 col-lg-9">
            <div class="c-table-responsive@tablet">

                <table class="c-table u-mb-large">
                    <caption class="c-table__title">
                        List of Subjects
                    </caption>
                    <thead class="c-table__head c-table__head--slim">
                    <tr>
                        <th class="c-table__cell c-table__cell--head">#</th>
                        <th class="c-table__cell c-table__cell--head">Subject Name</th>
                        {{--<th class="c-table__cell c-table__cell--head">Time(HH/MM)</th>--}}
                        <th class="c-table__cell c-table__cell--head">Actions
                            <!-- <span class="u-hidden-visually">Actions</span>-->
                        </th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php $user = Auth::user(); ?>

                    @if(Auth::user()->hasRole('student'))
                        <?php $i = 1; ?>
                        @foreach($subjects as $subject)
                                <tr class="c-table__row">
                                    <td class="c-table__cell">
                                        {{$i++}}
                                    </td>

                                    <td class="c-table__cell">
                                        {{$subject->name}}
                                    </td>
                                    {{--<td class="c-table__cell">1hr 20min</td>--}}
                                    <td class="c-table__cell">
                                        <a class="c-btn c-btn--small" href="{{url('current/'.$subject->id.'/result/')}}">Current Result</a>
                                    </td>
                                </tr>
                                @endforeach
                        @else
                            <tr>
                                <td></td>
                                <td>
                                    <br>
                                    <div class="col-md-12">
                                        <center>
                                            <div class="alert alert-danger">
                                                <strong>Opps</strong> There are no subjects for you
                                            </div>
                                        </center>

                                    </div>
                                </td>
                                <td></td>


                            </tr>





                    @endif


                    <!--<tr class="c-table__row">
                                                <td class="c-table__cell">English Language
                                                    <span class="u-block u-text-mute u-text-xsmall">
                                                        Teacher: Mrs Amoko Alice
                                                    </span>
                                                </td>
                                                <td class="c-table__cell">1hr 20min</td>
                                                <td class="c-table__cell">
                                                    <del class="u-text-danger">Completed</del>
                                                </td>
                                                <td class="c-table__cell u-text-right">
                                                    <a class="c-btn is-disabled" href="#">Start Exam</a>
                                                </td>
                                            </tr>-->
                    </tbody>
                </table>



            <!--   <div class="container">
                                        <div class="col-md-12 alert alert-info"> You do not have Users yet</div>
                                        <br>
                                    </div>-->


            </div>
        </div>
        </div>



    @endsection
