@extends('layouts.master')
@section('title', 'EPortal Online Exam System - Student List')


@section('content')

    <div class="col-sm-12 col-lg-8">

        <div class="c-table-responsive@tablet">
            @if($studentList->count() > 0)
                <table class="c-table u-mb-large">
                    <caption class="c-table__title">
                        List of Students in {{$level->name}} | {{$studentList->count()}}
                    </caption>
                    <thead class="c-table__head c-table__head--slim">
                    <tr>
                        <th class="c-table__cell c-table__cell--head">#</th>
                        <th class="c-table__cell c-table__cell--head">Full Name</th>
                        <th class="c-table__cell c-table__cell--head">Admssion Number</th>
                        <th class="c-table__cell c-table__cell--head">Gender</th>
                        <th class="c-table__cell c-table__cell--head">
                            <span class="">Actions</span>
                        </th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php $i = 1; ?>
                    @foreach ($studentList as $user)
                        <tr class="c-table__row">
                            <td class="c-table__cell">{{$i++}}.</td>
                            <td class="c-table__cell">
                                {{$user->surname}},   {{$user->other_names}}
                            </td>

                            <td class="c-table__cell">
                                {{$user->email}}
                            </td>

                            <td class="c-table__cell">
                                {{$user->gender}}
                            </td>
                        <td class="c-table__cell">
                            <a href="{{url('view/'.$user->id.'/summary/'.$level->id)}}" class="c-btn c-btn--secondary">View Result Summary</a>
                                {{--<span class="u-block u-text-xsmall u-text-mute">--}}
                                    {{--32 Tickets remaining--}}
                                {{--</span>--}}
                            </td>

                        </tr>
                    @endforeach

                    <!--<tr class="c-table__row">
                                                <td class="c-table__cell">English Language
                                                    <span class="u-block u-text-mute u-text-xsmall">
                                                        Teacher: Mrs Amoko Alice
                                                    </span>
                                                </td>
                                                <td class="c-table__cell">1hr 20min</td>
                                                <td class="c-table__cell">
                                                    <del class="u-text-danger">Completed</del>
                                                </td>
                                                <td class="c-table__cell u-text-right">
                                                    <a class="c-btn is-disabled" href="#">Start Exam</a>
                                                </td>
                                            </tr>-->
                    </tbody>
                </table>


            @else
                <div class="container">
                    <div class="col-md-12 alert alert-info"> You do not have any student!</div>
                    <br>
                </div>
            @endif

        </div>
    </div>



@endsection
