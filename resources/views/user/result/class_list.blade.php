@extends('layouts.master')

@section('title', 'EPortal Online Exam System- List of Classes')

@section ('content')
    <div class="col-sm-12 col-lg-8">

        <div class="c-table-responsive@tablet">
            @if($levels->count() > 0)
                <table class="c-table u-mb-large">
                    <caption class="c-table__title">
                        List of Classes
                        @if(Auth::user()->hasRole('system'))
                            <button type="button" data-toggle="modal" data-target="#exampleModal" class="c-btn c-alert--success pull-right">Add New Class</button>
                        @endif
                    </caption>
                    <thead class="c-table__head c-table__head--slim">
                    <tr>
                        <th class="c-table__cell c-table__cell--head">Class Name</th>
                        <th class="c-table__cell c-table__cell--head">Class Description</th>

                        <th class="c-table__cell c-table__cell--head">
                            <span class="u-hidden-visually">Actions</span>
                        </th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach ($levels as $level)
                        <tr class="c-table__row">
                            <td class="c-table__cell">
                                {{$level->name}}
                            </td>

                            <td class="c-table__cell">
                                {{$level->display_name}}
                            </td>


                            <td class="c-table__cell u-text-right">
                                <a class="c-btn c-btn--info" href="{{ url('/student/'.$level->id.'/class/') }}">View Student</a>
                            </td>
                        </tr>
                    @endforeach

                    <!--<tr class="c-table__row">
                                                <td class="c-table__cell">English Language
                                                    <span class="u-block u-text-mute u-text-xsmall">
                                                        Teacher: Mrs Amoko Alice
                                                    </span>
                                                </td>
                                                <td class="c-table__cell">1hr 20min</td>
                                                <td class="c-table__cell">
                                                    <del class="u-text-danger">Completed</del>
                                                </td>
                                                <td class="c-table__cell u-text-right">
                                                    <a class="c-btn is-disabled" href="#">Start Exam</a>
                                                </td>
                                            </tr>-->
                    </tbody>
                </table>

                {{--{{ $levels->links() }}--}}
            @else
                <div class="container">
                    <div class="col-md-12 alert alert-info"> You do not have Users yet</div>
                    <br>
                </div>
            @endif

        </div>
    </div>



@endsection