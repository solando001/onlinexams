@extends('layouts.master')
@section('title', 'EPortal Online Exam System - Create Term')


@section('content')

<div class="col-xl-8 c-card u-p-medium u-mb-medium">
	
	<h3>Create Term</h3>

	<form method="POST" action="{{ url('store/term') }}" aria-label="{{ __('create_user') }}">
        @csrf

        @if(session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
        @endif


        <div class="row">
			<div class="col-lg-12">

				<div class="c-field u-mb-small">
                     <label class="c-field__label" for="name">{{ __('Select Term') }}</label> 
                    <select id="name" class="c-input form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                       <option value="">Select Term</option>
                        <option value="First Term">First Term</option>
                       <option value="Second Term">Second Term</option>
                       <option value="Third Term">Third Term</option>
                    </select>

                    @if ($errors->has('gender'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('gender') }}</strong>
                    </span>
                    @endif
                </div>


				<div class="c-field u-mb-small">
                    <label class="c-field__label" for="firstName">{{ __('Create Session') }}</label> 

                    <input id="year" type="text" class="c-input form-control{{ $errors->has('year') ? ' is-invalid' : '' }}" name="year" placeholder="E.G 2018/2019" value="{{ old('year') }}" required autofocus>

                    @if ($errors->has('year'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('year') }}</strong>
                    </span>
                    @endif
                </div>

              

                 <div class="c-field u-mb-small">
                    <label class="c-field__label" for="bio"></label>
                    <button class="c-btn c-btn--info" type="submit">Create Term!
                    </button> 
                </div>


            </div>

        </div>

	</form>

</div>

@endsection
