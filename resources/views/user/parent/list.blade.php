@extends('layouts.master_list')

@section('title', 'EPortal Online Exam System- List of Parents')

@section('content')

    <div class="col-md-12 col-lg-12 col-sm-12">
        <h4> Parent List</h4>
        <table class="table table-responsive  table-hover">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Parent's Name</th>
                <th scope="col">Phone</th>
                <th scope="col">Email</th>
                <th scope="col">Student's Name</th>
                <th scope="col">Class</th>
                <th scope="col">Reg No.</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            @foreach($parents as $parent)
                <tr>
                    <th scope="row">{{$i++}}</th>
                    <td>{{$parent->name}}</td>
                    <td>{{$parent->phone}}</td>
                    <td>{{$parent->email}}</td>
                    <td>{{$parent->children->surname}} {{$parent->children->other_names}}</td>
                    <td>{{$parent->children->student_level->last()->name}}</td>
                    <td>{{$parent->children->email}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {{ $parents->links() }}

    </div>

    @endsection
