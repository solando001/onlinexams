@extends('layouts.master')
@section('title', 'EPortal Online Exam System - Create Subject')


@section('content')

    <div class="col-xl-8 c-card u-p-medium u-mb-medium">

        <h3>Add Parent for <i style="color: #9f3a38">{{$student->surname}} {{$student->other_names}} | {{$student->student_level->last()->name}}</i></h3>
        <form method="POST" action="{{ url('store/parent') }}" aria-label="{{ __('add_parent') }}">
            @csrf

            @if(session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            <div class="row">
                <div class="col-lg-12">
                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Parent Full Name') }}</label>

                        <input id="name" type="text" class="c-input form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Parent Name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Relationship') }}</label>

{{--                        <input id="name" type="text" class="c-input form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Parent Name" value="{{ old('name') }}" required autofocus>--}}

                        <select class="form-control" name="relationship" required>
                            <option value=""> Select </option>
                            <option value="Father">Father</option>
                            <option value="Mother">Mother</option>
                            <option value="Guidian">Guidian</option>
                        </select>

{{--                        @if ($errors->has(''))--}}
{{--                            <span class="invalid-feedback" role="alert">--}}
{{--                        <strong>{{ $errors->first('name') }}</strong>--}}
{{--                    </span>--}}
{{--                        @endif--}}
                    </div>



                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="description">{{ __('Phone Number') }}</label>

                        <input id="phone" type="numberic" class="c-input form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" placeholder="08078337847" value="{{ old('phone') }}" required>

                        @if ($errors->has('phone'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                        @endif
                    </div>


                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="description">{{ __('Email Address') }}</label>

                        <input id="email" type="email" class="c-input form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="ajose@gmail.com" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="description">{{ __('City/Town') }}</label>

                        <input id="location" type="text" class="c-input form-control{{ $errors->has('location') ? ' is-invalid' : '' }}" name="location" placeholder="Lagos" value="{{ old('location') }}" required>

                        @if ($errors->has('location'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('location') }}</strong>
                    </span>
                        @endif
                    </div>


                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="description">{{ __('Address') }}</label>

                        <input id="address" type="text" class="c-input form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" placeholder="Lagos" value="{{ old('address') }}">

                        @if ($errors->has('address'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                        @endif
                    </div>

                    <input type="hidden" name="user_id" value="{{$student->id}}">

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="bio"></label>
                        <button class="c-btn c-btn--info" type="submit">Add Parent!
                        </button>
                    </div>

                </div>
            </div>
        </form>

    </div>
@endsection