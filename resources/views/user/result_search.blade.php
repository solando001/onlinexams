@extends('layouts.master')
@section('title', 'EPortal Online Exam System - Search Result')

    @section('content')



        <div class="col-xl-8 c-card u-p-medium u-mb-medium">
            <h3>Search Student Result</h3>
            <div class="alert alert-info" role="alert">
                <strong>Note:</strong> You can perform filter search of result. Searches are only limited to Online Exams
            </div>
            <form method="POST" action="{{ url('search/result/data') }}">
                @csrf

                @if(session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                @if(session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif

                <div class="row">

                    <div class="col-md-4">
                            <div class="c-field u-mb-small">
                                <label class="c-field__label" for="subject_ids">{{ __('Class') }}</label>
                                <select required id="level_id" class=" form-control{{ $errors->has('level_id') ? ' is-invalid' : '' }}" name="level_id" value="{{ old('level_id') }}" >
                                    <option value="">Select Class</option>
                                    @foreach($levels as $level)
                                        <option value="{{$level->id}}">{{$level->name}}</option>
                                    @endforeach
                                </select>

                                @if ($errors->has('level_id'))
                                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('level_id') }}</strong>
                    </span>
                                @endif
                            </div>

                    </div>

                    <div class="col-md-4">

                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="subject_ids">{{ __('Select Subject') }}</label>

                            <select required id="subject_ids" class=" form-control{{ $errors->has('subject_id') ? ' is-invalid' : '' }}" name="subject_id" value="{{ old('subject_id') }}" >
                                <option value="">Select Subject</option>
                                @foreach($subjects as $subject)
                                    <option value="{{$subject->id}}">{{$subject->name}}</option>
                                @endforeach
                            </select>



                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="subject_ids">{{ __('Term') }}</label>

                            <select required id="term_id" class=" form-control{{ $errors->has('term_id') ? ' is-invalid' : '' }}" name="term_id" value="{{ old('term_id') }}" >
                                <option value="">Select Term</option>
                                @foreach($terms as $term)

                                    <option value="{{$term->id}}">{{$term->name}}</option>


                                @endforeach
                            </select>



                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="subject_ids">{{ __('Session') }}</label>

                            <select required id="session_id" class=" form-control{{ $errors->has('session_id') ? ' is-invalid' : '' }}" name="session_id" value="{{ old('session_id') }}" >
                                <option value="">Select Session</option>
                                @foreach($sessions as $session)

                                    <option value="{{$session->id}}">{{$session->name}}</option>


                                @endforeach
                            </select>



                        </div>

                    </div>


                    <div class="col-md-4">

                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="subject_ids">{{ __('Exam Type') }}</label>

                            <select required class=" form-control" name="exam_type">
                                <option value="obj">Objective</option>
                                <option value="theory">Theory</option>
                            </select>

                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="bio"></label>
                            <button class="c-btn c-btn--info" type="submit">Search Result!
                            </button>
                        </div>
                    </div>
                </div>








            </form>
        </div>

    @endsection