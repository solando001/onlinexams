@extends('layouts.master')

@section('title', 'EPortal Online Exam System- Result Page')

@section('content')

    <div class="col-sm-12 col-lg-8">

        <div class="c-table-responsive@tablet">
            @if($scores->count() > 0)
                <table class="c-table u-mb-large">
                    <caption class="c-table__title">
                        Student's Result Page
                    </caption>
                    <thead class="c-table__head c-table__head--slim">
                    <tr>
                        <th class="c-table__cell c-table__cell--head">#</th>
                        <th class="c-table__cell c-table__cell--head">Student Name</th>
                        <th class="c-table__cell c-table__cell--head">Exam Name</th>
                        <th class="c-table__cell c-table__cell--head">Class</th>
                        <th class="c-table__cell c-table__cell--head">Score</th>
                        <th class="c-table__cell c-table__cell--head">Obtainable Score</th>
                    </tr>
                    </thead>

                    <tbody>

                    <?php $i = 1; ?>
                    @foreach ($scores as $score)
                        <tr class="c-table__row">
                            <td class="c-table__cell">
                                {{$i++}}
                            </td>
                            <td class="c-table__cell">
                                {{$score->student->surname}}, {{$score->student->other_names}}
                            </td>

                            <td class="c-table__cell">
                                {{$score->gradeQuestion->name}}
                            </td>

                            <td class="c-table__cell">
                                {{$score->gradeQuestion->questionLevel()->first()->name}}
                            </td>

                            <td class="c-table__cell">


                                {{$score->grade}}
                            </td>
                            <td class="c-table__cell">
                                {{$score->total}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {{--{{ $scores->links() }}--}}
            @else
                <p>No Result available</p>
            @endif

        </div>
    </div>
@endsection