@extends('layouts.master')

@section('title', 'EPortal Online Exam System - Student Result')

@section('content')

    <div class="col-sm-12 col-lg-8">

        <div class="c-table-responsive@tablet">
            <table class="c-table u-mb-large">
                <caption class="c-table__title">
                    Result for <i>{{$student->surname}}, {{$student->other_names}}</i>
                    in {{$student->student_level->first()->name}}
                </caption>
                <thead class="c-table__head c-table__head--slim">
                <tr>
                    <th class="c-table__cell c-table__cell--head">Exam Name</th>
                    <th class="c-table__cell c-table__cell--head">Subject</th>

                    <th class="c-table__cell c-table__cell--head">Available</th>

                    <th class="c-table__cell c-table__cell--head">Score</th>

                    {{--<th class="c-table__cell c-table__cell--head">--}}
                        {{--<span class="u-hidden-visually">Actions</span>--}}
                    {{--</th>--}}
                </tr>
                </thead>

                <tbody>
                @foreach($results as $result)
                    <tr class="c-table__row">
                        <td class="c-table__cell">{{$result->exam->name}}</td>
                        <td class="c-table__cell">{{$result->exam->quizSubject->name}}</td>
                        <td class="c-table__cell">{{$result->exam->mark}}</td>
                        <td class="c-table__cell">
                            {{$result->score}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <!--<a href="" class="btn btn-primary btn-small">Print</a>-->
        </div>
    </div>

@endsection