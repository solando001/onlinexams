@extends('layouts.master')
@section('title', 'EPortal Online Exam System')
@section('content')

    <!-- HEADER OF THE MOST OF THE PART THAT IS CONCEERNB  -->
 
<div class="col-sm-12 col-lg-8">
                    
<div class="c-table-responsive@tablet">
    @if($users->count() > 0)
       <table class="c-table u-mb-large">
                                        <caption class="c-table__title">
                                            List of Teachers 
                                        </caption>
                                        <thead class="c-table__head c-table__head--slim">
                                            <tr>
                                                <th class="c-table__cell c-table__cell--head">Name</th>
                                                <th class="c-table__cell c-table__cell--head">Email</th>
                                                <th class="c-table__cell c-table__cell--head">Class Taught</th>
                                                <th class="c-table__cell c-table__cell--head">
                                                    <span class="u-hidden-visually">Actions</span>
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                             @foreach ($users as $user)
                                            <tr class="c-table__row">
                                                <td class="c-table__cell">{{$user->surname}} {{$user->other_names}}
                                                   <!-- <span class="u-block u-text-mute u-text-xsmall">
                                                      Teacher: Mr Morenike Amoe
                                                    </span> -->
                                                </td>

                                                <td class="c-table__cell">{{$user->email}}</td>
                                                <td class="c-table__cell">{{$user->email}}
                                                    <!--<span class="u-block u-text-xsmall u-text-mute">
                                                        32 Tickets remaining
                                                    </span>-->
                                                </td>
                                                <td class="c-table__cell u-text-right">
                                                    <a class="c-btn c-btn--success" href="#">View Detail</a>
                                                </td>
                                            </tr>
                                            @endforeach

                                            <!--<tr class="c-table__row">
                                                <td class="c-table__cell">English Language
                                                    <span class="u-block u-text-mute u-text-xsmall">
                                                        Teacher: Mrs Amoko Alice
                                                    </span>
                                                </td>
                                                <td class="c-table__cell">1hr 20min</td>
                                                <td class="c-table__cell">
                                                    <del class="u-text-danger">Completed</del>
                                                </td>
                                                <td class="c-table__cell u-text-right">
                                                    <a class="c-btn is-disabled" href="#">Start Exam</a>
                                                </td>
                                            </tr>-->
                                        </tbody>
                                    </table>
 @else
                                        <div class="container">
                                            <div class="col-md-12 alert alert-info"> You do not have Users yet</div>
                                            <br>
                                        </div>
                                    @endif

                                </div>
                            </div>


    
   
         
@endsection