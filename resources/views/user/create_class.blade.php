@extends('layouts.master')
@section('title', 'EPortal Online Exam System - Create Class')


@section('content')

    <div class="col-xl-8 c-card u-p-medium u-mb-medium">

        <h3>Create Class</h3>
        <form method="POST" action="{{ url('store/class') }}" aria-label="{{ __('create_user') }}">
            @csrf

            @if(session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif


            <div class="row">
                <div class="col-lg-12">
                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Name of Class') }}</label>

                        <input id="name" type="text" class="c-input form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                               placeholder="PRI 1" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                        @endif
                    </div>


                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="description">{{ __('Description of Class') }}</label>

                        <input id="description" type="text" class="c-input form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="display_name"
                               placeholder="Primary School" value="{{ old('display_name') }}" required autofocus>

                        @if ($errors->has('description'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="bio"></label>
                        <button class="c-btn c-btn--info" type="submit">Create Class!
                        </button>
                    </div>


                </div>
            </div>
        </form>

<hr>
        <table class="table">
            <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Class Name</th>
                <th scope="col">Class Description</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            @foreach($classes as $class)
            <tr>
                <th scope="row">{{$i++}}</th>
                <td>{{$class->name}}</td>
                <td>{{$class->display_name}}</td>
                <td><div type="button" class="btn btn-warning" data-toggle="modal" data-target="#exampleModalCenter_{{$class->id}}">Edit Class</div> </td>
            </tr>



            <div class="modal fade" id="exampleModalCenter_{{$class->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Edit {{$class->name}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="{{ url('edit/class') }}" aria-label="{{ __('create_user') }}">
                                @csrf
                            <div class="c-field u-mb-small">
                                <label class="c-field__label" for="firstName">{{ __('Name of Class') }}</label>

                                <input id="name" type="text" class="c-input form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                                       placeholder="PRI 1" value="{{$class->name}}">
                            </div>

                                <div class="c-field u-mb-small">
                                    <label class="c-field__label" for="firstName">{{ __('Class Description') }}</label>

                                    <input id="name" type="text" class="c-input form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="display_name"
                                           placeholder="PRI 1" value="{{$class->display_name}}">
                                </div>
                                <input type="hidden" name="id" value="{{$class->id}}">

                                <div class="c-field u-mb-small">
                                    <label class="c-field__label" for="bio"></label>
                                    <button class="c-btn c-btn--info" type="submit">Update Class!
                                    </button>
                                </div>

                            </form>


                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                        </div>
                    </div>
                </div>
            </div>
                @endforeach
            </tbody>
        </table>


    </div>
@endsection