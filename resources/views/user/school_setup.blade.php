@extends('layouts.master')
@section('title', 'E-Portal Online Exam System - School Setup')


    @section('content')
        <div class="col-xl-8 c-card u-p-medium u-mb-medium">
            <h3>School Setup</h3>
            <form method="POST" action="{{ url('store/school/data') }}">
                @csrf

                @if(session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                @if(session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif
                <div class="row">

                    <div class="col-lg-12">
                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="firstName">{{ __('Shool Name') }}</label>

                            <input id="name" type="text" class="c-input form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                   name="name" placeholder="Ivy Hill College" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                            @endif
                        </div>


                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="firstName">{{ __('School Email') }}</label>

                            <input id="email" type="email" class="c-input form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                   name="email" placeholder="ivyhillcollege@gmail.com" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                            @endif
                        </div>


                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="firstName">{{ __('Phone Number') }}</label>

                            <input id="phone" type="text" class="c-input form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                   name="phone" placeholder="08098998798" value="{{ old('phone') }}" required autofocus>

                            @if ($errors->has('phone'))
                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                            @endif
                        </div>


                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="firstName">{{ __('School Address') }}</label>

                            <input id="address" type="text" class="c-input form-control{{ $errors->has('address') ? ' is-invalid' : '' }}"
                                   name="address" placeholder="N0 1, Asuwas Way, Ikej Lagos" value="{{ old('address') }}" required autofocus>

                            @if ($errors->has('address'))
                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('address') }}</strong>
                    </span>
                            @endif
                        </div>


                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="school_session">{{ __('Session Name') }}</label>

                            <input id="school_session" type="text" class="c-input form-control{{ $errors->has('school_session') ? ' is-invalid' : '' }}"
                                   name="school_session" placeholder=" E.G 2018/2019 " value="{{ old('school_session') }}" required autofocus>

                            @if ($errors->has('school_session'))
                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('school_session') }}</strong>
                    </span>
                            @endif
                        </div>


                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="term">{{ __('Select Term') }}</label>
                            <select id="term" class="c-input form-control{{ $errors->has('term') ? ' is-invalid' : '' }}" name="term" value="{{ old('term') }}" required autofocus>
                                <option value="1">First Term</option>
                                <option value="2">Second Term</option>
                                <option value="3">Third Term</option>
                            </select>

                            @if ($errors->has('term'))
                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('term') }}</strong>
                    </span>
                            @endif
                        </div>
                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="bio"></label>
                            <button class="c-btn c-btn--info" type="submit">SAVE SETUP!
                            </button>
                        </div>


                    </div>
                </div>
            </form>
        </div>


        @endsection
