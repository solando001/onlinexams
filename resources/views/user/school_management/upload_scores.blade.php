@extends('layouts.master_list')

@section('title', 'EPortal Online Exam System - List of Subjects')
@section('styles')
    {{--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>--}}
    {{--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>--}}
@endsection


@section('content')
    <div class="col-md-12 col-lg-12 col-sm-12">
        <h4>Student List of Subjects for <b style="color: #9f3a38"><i>{{$student->surname}} {{$student->other_names}} | {{$student->student_level->last()->name}}</i></b></h4>
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if($school_name == "Covenant Child School" || $school_name == "IVY HILL ACADEMY (SECONDARY SECTION)")
        <table class="table table-responsive  table-hover">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Subject</th>
                <th scope="col">CA1</th>
                <th scope="col">CA2</th>
                <th scope="col">Exam</th>
                <th scope="col">Total</th>
                <th scope="col">Grade</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            @foreach($subjects as $st)
                <?php
                $score = $student->score()
                    ->where('session_id', $session->id)
                    ->where('term_id', $term->id)
                    ->where('level_id', $student->student_level->last()->id)
                    ->where('subject_id', $st->id)->first();
                ?>
                <tr>
                <td>{{$i++}}</td>
                <td>{{$st->name}}</td>
                <td>{{(isset($score->ca1))?$score->ca1: '0'}}</td>
                <td>{{(isset($score->ca2))?$score->ca2: '0'}}</td>
                <td>{{(isset($score->exam))?$score->exam: '0'}}</td>
                <td>{{(isset($score->total))?$score->total: '0'}}</td>
                <td>
                    <?php
                    $grade = '';
                    $remark = '';
                    if($student->student_level->last()->name == 'JSS 1 Best' || $student->student_level->last()->name == 'JSS 1 Awesome'  || $student->student_level->last()->name == 'JSS 2'){
                        if(isset($score->total)){
                            $total = $score->total;
                            if ($total >= 0 && $total <= 39) {
                                @$grade = "F";
                            } elseif ($total >= 40 && $total <= 49) {
                                @$grade = "D";
                            } elseif ($total >= 50 && $total <= 59) {
                                @$grade = "C";
                            } elseif ($total >= 60 && $total <= 69) {
                                @$grade = "B";
                            } elseif ($total >= 70 && $total <= 79) {
                                @$grade = "AB";
                            } elseif ($total >= 80 && $total <= 100) {
                                @$grade = "A";
                            } else {
                                @$grade = "invalid";
                            }
                        }else{
                            $grade = "-";
                        }
                    }elseif($student->student_level->last()->name == 'JSS 3'){

                        if(isset($score->total)){
                            $total = $score->total;
                            if ($total >= 0 && $total <= 49) {
                                @$grade = "F";
                            } elseif ($total >= 50 && $total <= 59) {
                                @$grade = "P";
                            } elseif ($total >= 60 && $total <= 79) {
                                @$grade = "C";
                            } elseif ($total >= 80 && $total <= 100) {
                                @$grade = "A";
                            } else {
                                @$grade = "invalid";
                            }
                        }else{
                            $grade = "-";
                        }

                    }elseif($student->student_level->last()->name == 'SS 1' || $student->student_level->last()->name == 'SS 2'){
                        if(isset($score->total)){
                            $total = $score->total;
                            if ($total >= 0 && $total <= 49) {
                                @$grade = "F";
                            } elseif ($total >= 50 && $total <= 59) {
                                @$grade = "P";
                            } elseif ($total >= 60 && $total <= 64) {
                                @$grade = "C6";
                            } elseif ($total >= 65 && $total <= 69) {
                                @$grade = "C5";
                            } elseif ($total >= 70 && $total <= 74) {
                                @$grade = "C4";
                            } elseif ($total >= 75 && $total <= 79) {
                                @$grade = "B2";
                            } elseif ($total >= 80 && $total <= 84) {
                                @$grade = "B1";
                            } elseif ($total >= 85 && $total <= 89) {
                                @$grade = "A2";
                            } elseif ($total >= 90 && $total <= 100) {
                                @$grade = "A1";
                            } else {
                                @$grade = "invalid";
                            }
                        }else{
                            $grade = "-";
                        }
                    }
                    ?>
                    {{$grade}}
                </td>
                <td>

                    @if(isset($score->total))
                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#exampleModal_edit_{{$st->id}}">
                            Edit Score
                        </button>
                    @else
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal_{{$st->id}}">
                            Upload Score
                        </button>
                    @endif
                </td>
            </tr>

            <div class="modal fade" id="exampleModal_{{$st->id}}" data-backdrop="static" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <br><br><br><br><br><br><br><br><br><br><br><br>
                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add Score for {{$st->name}} </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="{{url('post/score')}}" method="POST" name="myForm" >
                            @csrf
                        <div class="modal-body">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">CA1</label>
                                    <input type="number" class="form-control" name="ca1" min="1" max="20" >
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">CA2</label>
                                    <input type="number" class="form-control" name="ca2" min="1" max="20">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Exam</label>
                                    <input type="number" class="form-control" name="exam" min="1" max="60">
                                </div>
                        </div>

                            <input type="hidden" name="subject_id" value="{{$st->id}}">
                            <input type="hidden" name="student_id" value="{{$student->id}}">
                            <input type="hidden" name="level_id" value="{{$student->student_level->last()->id}}">

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save Score</button>
                        </div>

                        </form>
                    </div>
                </div>
            </div>

                <div class="modal fade" id="exampleModal_edit_{{$st->id}}" data-backdrop="static" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <br><br><br><br><br><br><br><br><br><br><br><br>
                    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Edit Score {{$st->name}} </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <form action="{{url('post/score/edit')}}" method="POST" name="myForm" >
                                @csrf
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">CA1</label>
                                        <input type="number" class="form-control" name="ca1" value="{{(isset($score->ca1))?$score->ca1: '0'}}" min="1" max="20">
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">CA2</label>
                                        <input type="number" class="form-control" value="{{(isset($score->ca2))?$score->ca2: '0'}}" name="ca2" min="1" max="20">
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">Exam</label>
                                        <input type="number" class="form-control" name="exam" value="{{(isset($score->exam))?$score->exam: '0'}}" min="1" max="60">
                                    </div>
                                </div>

                                <input type="hidden" name="subject_id" value="{{$st->id}}">
                                <input type="hidden" name="student_id" value="{{$student->id}}">
                                <input type="hidden" name="level_id" value="{{$student->student_level->last()->id}}">

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Edit Score</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>

            @endforeach
            </tbody>
            <tbody>
                <tr>
                    <td></td>
                    <td> <a href="{{url('upload/result/'.$student->student_level->last()->id)}}" class="btn btn-default">Back to Student List</a></td>
                </tr>
            </tbody>
        </table>
            @else
{{--     SECOND VIBE       --}}
            <table class="table table-responsive  table-hover">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Subject</th>
                    <th scope="col">CA1</th>
                    <th scope="col">CA2</th>
                    <th scope="col">Project</th>
                    <th scope="col">Assignment</th>
                    <th scope="col">Exam</th>
                    <th scope="col">Total</th>
                    <th scope="col">Grade</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; ?>
                @foreach($subjects as $st)
                    <?php
                    $score = $student->score()
                        ->where('session_id', $session->id)
                        ->where('term_id', $term->id)
                        ->where('level_id', $student->student_level->last()->id)
                        ->where('subject_id', $st->id)->first();
                    ?>
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$st->name}}</td>
                        <td>{{(isset($score->ca1))?$score->ca1: '0'}}</td>
                        <td>{{(isset($score->ca2))?$score->ca2: '0'}}</td>
                        <td>{{(isset($score->prt1))?$score->prt1: '0'}}</td>
                        <td>{{(isset($score->prt2))?$score->prt2: '0'}}</td>
                        <td>{{(isset($score->exam))?$score->exam: '0'}}</td>
                        <td>{{(isset($score->total))?$score->total: '0'}}</td>
                        <td>
                            <?php
                    $grade = '';
                    $remark = '';
                    if($student->student_level->last()->name == 'JSS 1 Best' || $student->student_level->last()->name == 'JSS 1 Awesome'  || $student->student_level->last()->name == 'JSS 2'){
                        if(isset($score->total)){
                            $total = $score->total;
                            if ($total >= 0 && $total <= 39) {
                                @$grade = "F";
                            } elseif ($total >= 40 && $total <= 49) {
                                @$grade = "D";
                            } elseif ($total >= 50 && $total <= 59) {
                                @$grade = "C";
                            } elseif ($total >= 60 && $total <= 69) {
                                @$grade = "B";
                            } elseif ($total >= 70 && $total <= 79) {
                                @$grade = "AB";
                            } elseif ($total >= 80 && $total <= 100) {
                                @$grade = "A";
                            } else {
                                @$grade = "invalid";
                            }
                        }else{
                            $grade = "-";
                        }
                    }elseif($student->student_level->last()->name == 'JSS 3'){

                        if(isset($score->total)){
                            $total = $score->total;
                            if ($total >= 0 && $total < 49) {
                                @$grade = "F";
                            } elseif ($total >= 50 && $total <= 59) {
                                @$grade = "P";
                            } elseif ($total >= 60 && $total <= 79) {
                                @$grade = "C";
                            } elseif ($total >= 80 && $total <= 100) {
                                @$grade = "A";
                            } else {
                                @$grade = "invalid";
                            }
                        }else{
                            $grade = "-";
                        }

                    }elseif($student->student_level->last()->name == 'SS 1' || $student->student_level->last()->name == 'SS 2'){
                        if(isset($score->total)){
                            $total = $score->total;
                            if ($total >= 0 && $total <= 49) {
                                @$grade = "F";
                            } elseif ($total >= 50 && $total <= 59) {
                                @$grade = "P";
                            } elseif ($total >= 60 && $total <= 64) {
                                @$grade = "C6";
                            } elseif ($total >= 65 && $total <= 69) {
                                @$grade = "C5";
                            } elseif ($total >= 70 && $total <= 74) {
                                @$grade = "C4";
                            } elseif ($total >= 75 && $total <= 79) {
                                @$grade = "B2";
                            } elseif ($total >= 80 && $total <= 84) {
                                @$grade = "B1";
                            } elseif ($total >= 85 && $total <= 89) {
                                @$grade = "A2";
                            } elseif ($total >= 90 && $total <= 100) {
                                @$grade = "A1";
                            } else {
                                @$grade = "invalid";
                            }
                        }else{
                            $grade = "-";
                        }
                    }
                    ?>
                            {{$grade}}
                        </td>
                        <td>

                            @if(isset($score->total))
                                <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#exampleModal_edit_{{$st->id}}">
                                    Edit Score
                                </button>
                            @else
                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#exampleModal_{{$st->id}}">
                                    Upload Score
                                </button>
                            @endif
                        </td>
                    </tr>

                    <div class="modal fade" id="exampleModal_{{$st->id}}" data-backdrop="static" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <br><br><br><br><br><br><br><br><br><br><br><br>
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Add Score for {{$st->name}} </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{url('post/score')}}" method="POST" name="myForm" >
                                    @csrf
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">CA1</label>
                                            <input type="text" class="form-control" name="ca1" min="1" max="10">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">CA2</label>
                                            <input type="text" class="form-control" name="ca2" min="1" max="10">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Practical</label>
                                            <input type="text" class="form-control" name="prt1" min="1" max="10">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Assignment</label>
                                            <input type="text" class="form-control" name="prt2" min="1" max="10">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Exam</label>
                                            <input type="text" class="form-control" name="exam" min="1" max="60">
                                        </div>
                                    </div>

                                    <input type="hidden" name="subject_id" value="{{$st->id}}">
                                    <input type="hidden" name="student_id" value="{{$student->id}}">
                                    <input type="hidden" name="level_id" value="{{$student->student_level->last()->id}}">

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save Score</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="exampleModal_edit_{{$st->id}}" data-backdrop="static" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <br><br><br><br><br><br><br><br><br><br><br><br>
                        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit Score {{$st->name}} </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form action="{{url('post/score/edit')}}" method="POST" name="myForm" >
                                    @csrf
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">CA1</label>
                                            <input type="text" class="form-control" name="ca1" value="{{(isset($score->ca1))?$score->ca1: '0'}}" min="1" max="10" >
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">CA2</label>
                                            <input type="text" class="form-control" value="{{(isset($score->ca2))?$score->ca2: '0'}}" name="ca2" min="1" max="10">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Practical/Project</label>
                                            <input type="text" class="form-control" name="prt1" value="{{(isset($score->prt1))?$score->prt1: '0'}}" min="1" max="10">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Assignment</label>
                                            <input type="text" class="form-control" name="prt2" value="{{(isset($score->prt2))?$score->prt2: '0'}}" min="1" max="10">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Exam</label>
                                            <input type="text" class="form-control" name="exam" value="{{(isset($score->exam))?$score->exam: '0'}}" min="1" max="60">
                                        </div>
                                    </div>

                                    <input type="hidden" name="subject_id" value="{{$st->id}}">
                                    <input type="hidden" name="student_id" value="{{$student->id}}">
                                    <input type="hidden" name="level_id" value="{{$student->student_level->last()->id}}">

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Edit Score</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
                </tbody>
                <tbody>
                <tr>
                    <td></td>
                    <td> <a href="{{url('upload/result/'.$student->student_level->last()->id)}}" class="btn btn-default">Back to Student List</a></td>
                </tr>
                </tbody>
            </table>



        @endif

    </div>

@endsection