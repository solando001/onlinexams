@extends('layouts.master')
@section('title', 'EPortal Online Exam System - Student List')


@section('content')

    <div class="col-sm-12 col-lg-8">

        <div class="c-table-responsive@tablet">
            @if($level->count() > 0)
                <table class="c-table u-mb-large">
                    <caption class="c-table__title">
                        List of Classes
                    </caption>
                    <thead class="c-table__head c-table__head--slim">
                    <tr>
                        <th class="c-table__cell c-table__cell--head">Class Code</th>
                        <th class="c-table__cell c-table__cell--head">Class Name</th>
                        <th class="c-table__cell c-table__cell--head">
                            <span class="u-hidden-visually">Actions</span>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($level as $lev)
                        <tr class="c-table__row">
                            <td class="c-table__cell">
                                {{$lev->name}}
                            </td>
                            <td class="c-table__cell">
                                {{$lev->display_name}}
                            </td>
                            <td class="c-table__cell">
                                <a href="{{url('upload/result/'.$lev->id)}}" class="btn btn-primary">Upload Scores</a>
                            <a href="{{url('school/management/'.$lev->id.'/class')}}" class="btn btn-warning">Compute Class Position</a>
{{--                            <a href="{{url('#')}}" class="btn btn-primary">Compute Subject Position</a>--}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

{{--                {{ $level->links() }}--}}
            @else
                <div class="container">
                    <div class="col-md-12 alert alert-info"> There are no classes!</div>
                    <br>
                </div>
            @endif

        </div>
    </div>

@endsection
