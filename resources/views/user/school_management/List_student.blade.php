@extends('layouts.master_list')

@section('title', 'EPortal Online Exam System- List of Subjects')
@section('styles')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
@endsection
@section('content')

    <div class="col-md-12 col-lg-12 col-sm-12">
        <h4>{{$level->name}} <b style="color: #9f3a38"><i>{{$subject->name}}</i></b></h4>
        <table class="table table-responsive  table-hover">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Reg</th>
                <th scope="col">class</th>
            </tr>
            </thead>

            <tbody>
            <?php $i = 1; ?>
            @foreach($students  as $student)

                <tr>
                    <th scope="col">{{$i++}}</th>
                    <th scope="col">{{$student->surname}} {{$student->other_names}}</th>
                    <th scope="col">{{$student->email}}</th>
{{--                    <th scope="col">{{$student->subjects}}</th>--}}
                    <th scope="col">  </th>
                </tr>
                @endforeach
            </tbody>


        </table>


@endsection