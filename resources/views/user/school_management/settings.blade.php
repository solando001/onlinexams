@extends('layouts.master')
@section('title', 'EPortal Online Exam System - Create Subject')


@section('content')

    <div class="col-xl-8 c-card u-p-medium u-mb-medium">

        <h3>School Management Settings</h3>

        <div class="alert alert-info">
            <strong>Note</strong> This settings are based on the current Session and Term
        </div>

        <form method="POST" action="{{ url('school/management/settings/store') }}" aria-label="{{ __('store_settings') }}">
            @csrf

            @if(session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            <div class="row">
                <div class="col-lg-12">

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Select Type') }}</label>
                        <select class="form-control" name="type" required>
                            <option value=""> Please Select </option>
                            <option value="CA">Continuous Assessment</option>
                            <option value="Practical">Practical</option>
                            <option value="Assignment">Assignment</option>
                            <option value="Exam">Examination</option>
                        </select>
                    </div>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Name') }}</label>

                        <input id="name" type="text" class="c-input form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="E.g CA 1, CA 2,  Practical, Assignment EXAM" value="{{ old('name') }}" required>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                        @endif
                    </div>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="description">{{ __('Max Score') }}</label>

                        <input id="max_score" type="numberic" class="c-input form-control{{ $errors->has('max_score') ? ' is-invalid' : '' }}" name="max_score" placeholder="10" value="{{ old('max_Score') }}" required>

                        @if ($errors->has('max_score'))
                            <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('max_Score') }}</strong>
                    </span>
                        @endif
                    </div>

                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="bio"></label>
                        <button class="c-btn c-btn--info" type="submit">Create Settings!
                        </button>
                    </div>

                </div>
            </div>
        </form>


        <div class="row">
            <div class="col-lg-12">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
{{--                        <th scope="col">CA Type</th>--}}
                        <th scope="col">Name</th>
                        <th scope="col">Max Score</th>
                        <th scope="col">Session</th>
                        <th scope="col">Term</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 1;
                    $cummulative_total = "";
                    ?>
                    @foreach($settings as $setting)
                    <tr>
                        <th scope="row">{{$i++}}</th>
                        <td>{{$setting->type}}</td>
                        <td>{{$setting->name}}</td>
                        <td>{{$setting->max_score}}</td>
                        <td>{{$setting->session->name}}</td>
                        <td>{{$setting->term->name}}</td>
                        <td>
                            <a href="" class="btn btn-info btn-xs">Edit</a>
                            <a href="" class="btn btn-danger btn-xs">Remove</a>
                        </td>
                    </tr>
                    @endforeach

                    <tr>
                        <th scope="row">##</th>
                        <td ><b>Grand Total</b></td>
                        <td> {{$max_score}} </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>

            </div>
@endsection