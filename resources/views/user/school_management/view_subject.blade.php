@extends('layouts.master_list')

@section('title', 'EPortal Online Exam System- List of Subjects')
@section('styles')
{{--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>--}}
{{--    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>--}}
    @endsection

@section('script')
    <script>
        function myFunction() {
            var ca1, ca2,exam;

            // Get the value of the input field with id="numb"
            ca1 = document.getElementById("ca1").value;
            ca2 = document.getElementById("ca2").value;
            exam = document.getElementById("exam").value;

            // If x is Not a Number or less than one or greater than 10
            if (ca1  > 20) {
                alert( "CA 1 Cannot be more than 20 marks!");
                document.getElementById("ca1").backgroundColor = "#ff6666";
                return false;
            }

            if (ca2  > 20) {
                alert( "CA 2 Cannot be more than 20 marks!");
                document.getElementById("ca1").backgroundColor = "#ff6666";
                return false;
            }

            if (exam  > 60) {
                alert( "Exam Cannot be more than 60 marks!");
                document.getElementById("ca1").backgroundColor = "#ff6666";
                return false;
            }

            return true;
            // } else {
            //     text = "Input OK";
            // }
            // document.getElementById("demo").innerHTML = text;
        }

    </script>
    @endsection
@section('content')

    <div class="col-md-12 col-lg-12 col-sm-12">
        <h4>Student List of Subjects for <b style="color: #9f3a38"><i>{{$student->surname}} {{$student->other_names}} | {{$student->student_level->last()->name}}</i></b></h4>

        <form action="{{url('post/score')}}" method="POST" name="myForm" >
{{--            onsubmit = "return(myFunction());">--}}
            @csrf
            @if($school_name == "Covenant Child School" || $school_name == "IVY HILL ACADEMY (SECONDARY SECTION)")
               <!-- Covenant Child Part  -->
            <table class="table table-responsive  table-hover">
                <thead class="thead-dark">

                </thead>
                @if(count($scores) > 0)
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Subject</th>
                        <th scope="col">CA1</th>
                        <th scope="col">CA2</th>
                        <th scope="col">Exam</th>
                        <th scope="col">Total</th>
                        <th scope="col">Grade</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1; ?>
                    @foreach($scores as $check)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$check->subject->name}}</td>
                        <td>{{$check->ca1}}</td>
                        <td>{{$check->ca2}}</td>
                        <td>{{$check->exam}}</td>
                        <td>{{$total = $check->exam+$check->ca1+$check->ca2}}</td>
                        <td>
                            <?php
                            if($total >= 70){
                                $grade = "A";
                            }elseif ($total >= 60){
                                $grade = "B";
                            }elseif ($total >= 50){
                                $grade = "C";
                            }elseif ($total >= 40){
                                $grade = "D";
                            }elseif ($total >= 30){
                                $grade = "E";
                            }else{
                                $grade = "F";
                            }
                            ?>
                            {{$grade}}
                        </td>

                        <?php
                        $level = $student->student_level->last()->id;
                        $no = \App\Subject::findOrFail($check->subject->id);
                        $c1 = \App\ScoreSheet::where('subject_id', $check->subject->id)->where('level_id', $level)
                            ->where('session_id', $session->id)->where('term_id', $term->id)->where('student_id', $student->id)->sum('ca1');
                        $c2 = \App\ScoreSheet::where('subject_id', $check->subject->id)->where('level_id', $level)
                            ->where('session_id', $session->id)->where('term_id', $term->id)->where('student_id', $student->id)->sum('ca2');
//                        $pr1 = \App\ScoreSheet::where('subject_id', $check->subject->id)->where('level_id', $level)
//                            ->where('session_id', $session->id)->where('term_id', $term->id)->where('student_id', $student->id)->sum('prt1');
//                        $pr2 = \App\ScoreSheet::where('subject_id', $check->subject->id)->where('level_id', $level)
//                            ->where('session_id', $session->id)->where('term_id', $term->id)->where('student_id', $student->id)->sum('prt2');
                        $exam = \App\ScoreSheet::where('subject_id', $check->subject->id)->where('level_id', $level)
                            ->where('session_id', $session->id)->where('term_id', $term->id)->where('student_id', $student->id)->sum('exam');
                        $kol = $c1+$c2+$exam;

                        $total= \App\ScoreSheet::where('subject_id', $check->subject->id)->where('level_id', $level)
                            ->where('session_id', $session->id)->where('term_id', $term->id)->where('student_id', $student->id)->first();
                        $total->update(['total' => $kol]);
                        ?>
                    </tr>
                        @endforeach

                    <?php
                    $toty = \App\ScoreSheet::where('level_id', $level)
                        ->where('session_id', $session->id)->where('term_id', $term->id)->where('student_id', $student->id)->sum('total');
                    $tot = \App\StudentScore::where('level_id', $level)
                        ->where('session_id', $session->id)->where('term_id', $term->id)->where('student_id', $student->id)->first();
                    if($tot == null)
                    {
                        \App\StudentScore::create([
                            'user_id' => Auth::user()->id,
                            'student_id' => $student->id,
                            'level_id' => $student->student_level->last()->id,
                            'session_id' => $session->id,
                            'term_id' => $term->id,
                            'total' => $toty
                        ]);
                    }else{
                        $tot->update(['total' => $toty]);
                    }
                    ?>
                    </tbody>
                    <tbody>
                    <tr>
                        <td></td>
                        <td> <a href="{{url('upload/result/'.$level)}}" class="btn btn-default"  >Back to Student List</a></td>
                    </tr>
                    </tbody>


                @else
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Subject</th>
                        <th scope="col">CA1</th>
                        <th scope="col">CA2</th>
                        <th scope="col">Exam</th>
                    </tr>
                <tbody>
                <?php $i = 1; ?>
                @foreach($subjects as $st)

                    <tr>
                        <td scope="col">{{$i++}}</td>
                        <td>{{$st->name}}</td>
                        <input type="hidden" name="subject_id[]" value="{{$st->id}}">
                        <input type="hidden" name="student_id" value="{{$student->id}}">
                        <input type="hidden" name="level_id" value="{{$student->student_level->last()->id}}">
                        <td>
                            <select name="ca1[]" style="width: 70px">
                                @for($x=1; $x<=20; $x++)
                                <option value="{{$x}}">{{$x}}</option>
                                    @endfor
                            </select>
                        </td>
                        <td>
                            <select name="ca2[]" style="width: 70px">
                                @for($x=1; $x<=20; $x++)
                                    <option value="{{$x}}">{{$x}}</option>
                                @endfor
                            </select>
                        </td>
                        <td>
                            <select name="exam[]" style="width: 70px">
                                @for($x=1; $x<=60; $x++)
                                    <option value="{{$x}}">{{$x}}</option>
                                @endfor
                            </select>
                        </td>
                    </tr>
                @endforeach

                    </tbody>
                    <tbody>
                    <tr>
                        <td></td>
                        <td> <button class="btn btn-success" type="submit" >Submit</button></td>
                    </tr>
                    </tbody>

                @endif

            </table>
                @else
                <!-- TMIBlossom Academy Part  -->
            <table class="table table-responsive  table-hover">
                    <thead class="thead-dark">

                    </thead>
                    @if(count($scores) > 0)
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Subject</th>
                            <th scope="col">CA1</th>
                            <th scope="col">CA2</th>
                            <th scope="col">Prac/Proj</th>
                            <th scope="col">Assignment</th>
                            <th scope="col">Exam</th>
                            <th scope="col">Total</th>
                            <th scope="col">Grade</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $i = 1; ?>
                        @foreach($scores as $check)

                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$check->subject->name}}</td>
                                <td>{{$check->ca1}}</td>
                                <td>{{$check->ca2}}</td>
                                <td>{{$check->prt1}}</td>
                                <td>{{$check->prt2}}</td>
                                <td>{{$check->exam}}</td>
                                <td>{{$total = $check->exam+$check->ca1+$check->ca2+$check->prt1+$check->prt2}}</td>
                                <td>
                                    <?php
                                        if($total <= 39){
                                            $grade = "F";
                                        }elseif ($total <= 49){
                                            $grade = "E";
                                        }elseif ($total <= 59){
                                            $grade = "C";
                                        }elseif ($total <= 69){
                                            $grade = "B";
                                        }elseif ($total <= 79){
                                            $grade = "AB";
                                        }else{
                                            $grade = "A";
                                        }
//
                                    ?>
                                    {{$grade}}
                                </td>

                                <?php
                                $level = $student->student_level->last()->id;
                                $no = \App\Subject::findOrFail($check->subject->id);
                                $c1 = \App\ScoreSheet::where('subject_id', $check->subject->id)->where('level_id', $level)
                                    ->where('session_id', $session->id)->where('term_id', $term->id)->where('student_id', $student->id)->sum('ca1');
                                $c2 = \App\ScoreSheet::where('subject_id', $check->subject->id)->where('level_id', $level)
                                    ->where('session_id', $session->id)->where('term_id', $term->id)->where('student_id', $student->id)->sum('ca2');
                                $pr1 = \App\ScoreSheet::where('subject_id', $check->subject->id)->where('level_id', $level)
                                    ->where('session_id', $session->id)->where('term_id', $term->id)->where('student_id', $student->id)->sum('prt1');
                                $pr2 = \App\ScoreSheet::where('subject_id', $check->subject->id)->where('level_id', $level)
                                    ->where('session_id', $session->id)->where('term_id', $term->id)->where('student_id', $student->id)->sum('prt2');
                                $exam = \App\ScoreSheet::where('subject_id', $check->subject->id)->where('level_id', $level)
                                    ->where('session_id', $session->id)->where('term_id', $term->id)->where('student_id', $student->id)->sum('exam');
                                $kol = $c1+$c2+$pr1+$pr2+$exam;

                                $total= \App\ScoreSheet::where('subject_id', $check->subject->id)->where('level_id', $level)
                                    ->where('session_id', $session->id)->where('term_id', $term->id)->where('student_id', $student->id)->first();
                                $total->update(['total' => $kol]);
                                ?>
                            </tr>
                        @endforeach

                        <?php
                        $toty = \App\ScoreSheet::where('level_id', $level)
                            ->where('session_id', $session->id)->where('term_id', $term->id)->where('student_id', $student->id)->sum('total');
                        $tot = \App\StudentScore::where('level_id', $level)
                            ->where('session_id', $session->id)->where('term_id', $term->id)->where('student_id', $student->id)->first();


                        if($tot == null)
                        {
                            \App\StudentScore::create([
                                'user_id' => Auth::user()->id,
                                'student_id' => $student->id,
                                'level_id' => $student->student_level->last()->id,
                                'session_id' => $session->id,
                                'term_id' => $term->id,
                                'total' => $toty,
                                //'average' => $avg,
                            ]);
                        }else{
                            $tot->update(['total' => $toty]);
                        }
                        ?>

                        </tbody>

                    <tbody>
                    <tr>
                        <td></td>
                        <td> <a href="{{url('upload/result/'.$level)}}" class="btn btn-default">Back to Student List</a></td>
                    </tr>
                    </tbody>


                @else
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Subject</th>
                            <th scope="col">CA1</th>
                            <th scope="col">CA2</th>
                            <th scope="col">Prac/Proj</th>
                            <th scope="col">Assignment</th>
                            <th scope="col">Exam</th>
                        </tr>
                        <tbody>
                        <?php $i = 1; ?>
                        @foreach($subjects as $st)

                            <tr>
                                <td scope="col">{{$i++}}</td>
                                <td>{{$st->name}}</td>
                                <input type="hidden" name="subject_id[]" value="{{$st->id}}">
                                <input type="hidden" name="student_id" value="{{$student->id}}">
                                <input type="hidden" name="level_id" value="{{$student->student_level->last()->id}}">
                                <td>
                                    <select name="ca1[]" style="width: 70px">
                                        @for($x=1; $x<=10; $x++)
                                            <option value="{{$x}}">{{$x}}</option>
                                        @endfor
                                    </select>
                                </td>
                                <td>
                                    <select name="ca2[]" style="width: 70px">
                                        @for($x=1; $x<=10; $x++)
                                            <option value="{{$x}}">{{$x}}</option>
                                        @endfor
                                    </select>
                                </td>
                                <td>
                                    <select name="prt1[]" style="width: 70px">
                                        @for($x=1; $x<=10; $x++)
                                            <option value="{{$x}}">{{$x}}</option>
                                        @endfor
                                    </select>
                                </td>
                                <td>
                                    <select name="prt2[]" style="width: 70px">
                                        @for($x=1; $x<=10; $x++)
                                            <option value="{{$x}}">{{$x}}</option>
                                        @endfor
                                    </select>
                                </td>
                                <td>
                                    <select name="exam[]" style="width: 70px">
                                        @for($x=1; $x<=60; $x++)
                                            <option value="{{$x}}">{{$x}}</option>
                                        @endfor
                                    </select>
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                        <tbody>
                        <tr>
                            <td></td>
                            <td> <button class="btn btn-success" type="submit" >Submit</button></td>
                        </tr>
                        </tbody>

                    @endif

                </table>
            @endif
        </form>
    </div>
@endsection