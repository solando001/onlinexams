@extends('layouts.master')
@section('title', 'EPortal Online Exam System - Create Remarks Settings')

@section('content')
    <div class="col-xl-8 c-card u-p-medium u-mb-medium">

        <h3>School Management Comment and remark Settings</h3>

{{--        <div class="alert alert-info">--}}
{{--            <strong>Note</strong> This settings are based on the current Session and Term--}}
{{--        </div>--}}

        <form method="POST" action="{{ url('school/management/remark/store') }}" aria-label="{{ __('store_settings') }}">
            @csrf

            @if(session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            <div class="row">
                <div class="col-lg-12">

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Select Type') }}</label>
                        <select class="form-control" name="type" required>
                            <option value=""> Please Select </option>
                            <option value="CA">Continuous Assessment</option>
                            <option value="Practical">Practical</option>
                            <option value="Assignment">Assignment</option>
                            <option value="Exam">Examination</option>
                        </select>
                    </div>

                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="bio"></label>
                        <button class="c-btn c-btn--info" type="submit">Create Settings!
                        </button>
                    </div>

                </div>
            </div>
        </form>

    </div>


@endsection