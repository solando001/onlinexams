@extends('layouts.master_list')
@section('title', 'EPortal Online Exam System- List of Students')
@section('content')

    <div class="col-md-12 col-lg-12 col-sm-12">
        <h4>Student in {{$level->name}} </h4>

        <table class="table table-responsive  table-hover">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Student Name</th>
                <th scope="col">Reg. No</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1; ?>
            @foreach($students as $st)
                <?php
                $lop = \App\ScoreSheet::where('level_id', $level->id)->where('student_id', $st->id)->where('session_id', $sessionId)->where('term_id', $termId)->first();
                ?>
            <tr>
                <td scope="col">{{$i++}}</td>
                <td>{{$st->surname}} {{$st->other_names}}</td>
                <td>{{$st->email}}</td>
                <td><a href="{{url('view/student/'.$st->id.'/subject')}}" class="btn btn-primary">Upload Scores</a> </td>
{{--                @if($lop == null)--}}
{{--                <td><a href="{{url('view/student/'.$st->id.'/subject')}}" class="btn btn-primary">Upload Scores</a> </td>--}}
{{--                    @else--}}
{{--                    <td><a href="{{url('view/student/'.$st->id.'/subject')}}" class="btn btn-default">Scores Uploaded</a> </td>--}}
{{--                @endif--}}
            </tr>
            @endforeach
            </tbody>

        </table>

        @if(count($students) == $score_sheet_count)
        <a href="{{url('school/management/'.$level->id.'/class')}}" class="btn btn-warning">Compute Class Position</a>
        @endif
    </div>

@endsection