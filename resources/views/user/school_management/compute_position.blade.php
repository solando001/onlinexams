@extends('layouts.master')
@section('title', 'EPortal Online Exam System - Compute Position')


@section('content')

    <div class="col-sm-12 col-lg-8">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <div class="c-table-responsive@tablet">

            @if($computes->count() > 0)
                <form action="{{url('compute/class/position')}}" method="POST">
                    @csrf
                <table class="c-table u-mb-large">
                    <caption class="c-table__title">
                        Compute Position for {{$level->name}}
                    </caption>
                    <thead class="c-table__head c-table__head--slim">
                    <tr>
                        <th class="c-table__cell c-table__cell--head">Name</th>
                        <th class="c-table__cell c-table__cell--head">Total Score</th>
                        <th class="c-table__cell c-table__cell--head">Position</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $i=1; ?>
                    @foreach ($computes as $lev)
                        <tr class="c-table__row">
                            <td class="c-table__cell">
                                {{$lev->student->surname}}
                            </td>
                            <td class="c-table__cell">
                                {{$lev->total}}
                            </td>
                            <td class="c-table__cell">
                                <input name="position[]" value="{{$i++}}" size="1" type="text" readonly>
                            </td>
                        </tr>
                        <input name="student_id[]" value="{{$lev->student->id}}" type="hidden">
                        <input name="total[]" value="{{$lev->total}}" type="hidden">
                        <input name="level_id" value="{{$class}}" type="hidden">
                        <input name="session_id" value="{{$session}}" type="hidden">
                        <input name="term_id" value="{{$term}}" type="hidden">
                    @endforeach
                    </tbody>
                </table>
                    @if(count($computes) > 0 || $no_student > 0)
                        @if(count($computes) ==$no_student)
                        <button class="btn btn-success" type="submit">Compute Position</button>
                            @else
                            <button class="btn btn-danger" disabled>Student Not yet completed</button>
                        @endif
                    @endif
                </form>
            @else
                <div class="container">
                    <div class="col-md-12 alert alert-info"> There are no student available!</div>
                    <br>
                </div>
            @endif

        </div>
    </div>

@endsection
