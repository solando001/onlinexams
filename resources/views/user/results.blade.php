@extends('layouts.master')

@section('title', 'EPortal Online Exam System- Result Page')

@section('content')

    <div class="col-sm-12 col-lg-8">

        <div class="c-table-responsive@tablet">
            @if($scores->count() > 0)
                <table class="c-table u-mb-large">
                    <caption class="c-table__title">
                        Student's Result Page
                    </caption>
                    <thead class="c-table__head c-table__head--slim">
                    <tr>
                        <th class="c-table__cell c-table__cell--head">#</th>
                        <th class="c-table__cell c-table__cell--head">Student Name</th>
                        <th class="c-table__cell c-table__cell--head">Exam Name</th>
                        <th class="c-table__cell c-table__cell--head">Class</th>
                        <th class="c-table__cell c-table__cell--head">Score</th>
                        <th class="c-table__cell c-table__cell--head">Mark Obtainable</th>
                    </tr>
                    </thead>

                    <tbody>

                    <?php $i = 1; ?>
                        @foreach ($scores as $score)
                        <tr class="c-table__row">
                            <td class="c-table__cell">
                                {{$i++}}
                            </td>
                            <td class="c-table__cell">
                                {{$score->user->surname}}, {{$score->user->other_names}}
                            </td>

                            <td class="c-table__cell">
                                {{$score->exam->name}}
                            </td>

                            <td class="c-table__cell">
                                {{$score->exam->level()->first()->name}}
                            </td>

                            <td class="c-table__cell">
                                {{$score->score}}
                            </td>
                            <td class="c-table__cell">
                                {{$score->exam->mark}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

                {{--{{ $scores->links() }}--}}
@else
            <p>No Result available</p>
            @endif

        </div>
    </div>
@endsection