@extends('layouts.master')
@section('title', 'EPortal Online Exam System - Create Subject')
@section('script')

    <script>
        $('#test').on('change', function() {
             alert( this.value ); // or $(this).val()
            // if(this.value == "Parent") {
            //     $('#parent').show();
            //     $('#passport').hide();
            // } else {
            //     $('#parent').hide();
            //     $('#passport').show();
            // }
        });
    </script>

@endsection

@section('content')

    <div class="col-xl-8 c-card u-p-medium u-mb-medium">

        <h3>Broadcast SMS Messaging</h3>
        <form method="POST" action="{{ url('send/message') }}" aria-label="{{ __('messaging') }}">
            @csrf

            @if(session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
            @endif

            <div class="row">
                <div class="col-lg-12">
                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Select Recipient') }}</label>

                        {{--                        <input id="name" type="text" class="c-input form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Parent Name" value="{{ old('name') }}" required autofocus>--}}

                        <select class="form-control" id="test" name="recipient" required>
                            <option value=""> Select </option>
                            <option value="Parent">All Parent </option>
                            <option value="Teachers">All Teachers</option>
                        </select>
                    </div>


                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="description">{{ __('Phone Number') }}</label>
                        <textarea class="form-control{{ $errors->has('message') ? ' is-invalid' : '' }}"  name="message" placeholder="Type you message" rows="5" required></textarea>
{{--                        <input id="phone" type="numberic" class="c-input form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" placeholder="08078337847" value="{{ old('phone') }}" required>--}}

                        @if ($errors->has('message'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('message') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="bio"></label>
                        <button class="c-btn c-btn--info" type="submit">Send!
                        </button>
                    </div>

                </div>
            </div>
        </form>

    </div>
@endsection