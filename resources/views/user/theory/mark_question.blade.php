@extends('layouts.flat')
@section('title', 'Mark Questions - '.$question->name)

@section('styles')
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
@endsection
@section('content')

    <div class="container-fluid">
        {{--            <h3>{{$que->name}} ({{$questionsList->count()}})</h3>--}}
        <h3>  </h3>
        <div class="row">
            <div class="row u-m-small">
                <div class="col-md-12">
                    @if(session('alert'))
                        <div class="alert alert-danger">
                            <strong>{{ session('alert') }}</strong>
                        </div>
                    @endif
                </div>

                <div class="col-sm-12 col-md-12">
                    Student Name: <b>{{$student->surname}}, {{$student->other_names}}</b><br>
                    Subject Name: <b>{{$subject->name}}</b> <br>
                    Class Name:  <b>{{$class->name}}</b> <br>
                    Assessment Name: <b>{{$question->name}}</b>
                </div>
            </div>
            <div class="col-12 col-xl-12 u-p-zero">

                <div class="row u-m-small">
                    <?php $i = 1;  ?>
                    @if(isset($questionsList) && $questionsList->count() > 0)
                        @foreach($questionsList as $list)
                            <div class="col-sm-12 col-md-12">
                                <div class="c-project-card u-mb-medium">
                                    {{--<img src="{{asset("asset/img/project-card1.jpg")}}" alt="About the image">--}}

                                    <div class="c-project-card__content">


                                        <div class="c-project-card__head">
                                            <div>
                                                <div class="badge-info">Qusetion - {{$i++}}</div>
                                            </div>
                                            <hr>
                                            <br>
                                            <h2 class="c-project-card__title">{!! $list->question_detail !!}</h2>
                                            <br>

                                            <hr>

                                            <br>

                                            <h4>Student Answer</h4>

                                            <?php

                                                $submission = \App\TheoryQuestionSubmission::where('question_detail_id', $list->id)
                                                    ->where('user_id', $student->id)->first();

                                            ?>

                                            @if($submission  == null)
                                           <div class="alert alert-info">
                                               <strong>Opps</strong> Student Did not answer this question
                                           </div>
                                                @else
                                                <p class="c-project-card__info">
                                                    {!! $submission ->answer_detail !!}
                                                </p>
                                            @endif
                                        </div>

                                        <div class="c-project-card__meta">
                                            <p>{{$list->mark}}<small class="u-block u-text-mute">Mark Obtainable</small></p>
                                            @if($submission  == null)
                                                <del class="text-danger">
                                                    No Answer to Grade
                                                </del>
                                                @else


                                            @if($submission->score == "0")
                                            <p>
                                                <button type="button" data-toggle="modal" data-target="#myGrade_{{$list->id}}" class="c-btn  btn-sm c-btn--fancy pull-left">
                                                    Grade Answer
                                                </button>
                                            </p>
                                                @else
                                                    <p> <span class="u-text-success"><strong>{{$submission->score}}</strong></span><small class="u-block u-text-mute">Mark Obtained</small></p>
                                                    {{--<span class="u-text-success"><b>Score:</b></span>--}}
                                                @endif
                                            @endif
                                            {{--<small class="u-block u-text-mute"> Created: {{\Carbon\Carbon::parse($list->created_at)->diffForHumans()}}</small></p>--}}
                                        </div>
                                    </div>
                                </div>





                            </div>

                                <div id="myGrade_{{$list->id}}" class="modal fade" role="dialog" data-backdrop="static">

                                    <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">

                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">Answer</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <br><br><br>
                                                <h4>Student Answer</h4>

                                                @if($submission  == null)
                                                    <div class="alert alert-info">
                                                        <strong>Opps</strong> Student Did not answer this question
                                                    </div>
                                                @else
                                                    <p class="c-project-card__info">
                                                        {!! $submission ->answer_detail !!}
                                                    </p>
                                                @endif
                                                {{--<p>--}}
                                                    {{--{!! $submission ->answer_detail !!}--}}
                                                {{--</p>--}}
                                                <hr>
                                                <form action="{{url('store/student/score')}}" method="POST">
                                                    @csrf
                                                    <p> Max Score Obtainable: {{$list->mark}} </p>
                                                    <br>
                                                    <div class="c-field u-mb-small">
                                                        <label class="c-field__label" for="firstName">{{ __('Score') }} </label>

                                                        <input id="year" type="text" class="c-input form-control"
                                                               name="score" placeholder="5" value="{{ old('score') }}" required autofocus>
                                                    </div>

                                                    <input type="hidden" name="question_detail_id" value="{{$list->id}}">
                                                    <input type="hidden" name="user_id" value="{{$student->id}}">
                                                    <input type="hidden" name="mark" value="{{$list->mark}}">
                                                    <input type="hidden" name="question_id" value="{{$question->id}}">


                                                    <div class="c-field u-mb-small">
                                                        <label class="c-field__label" for="bio"></label>
                                                        <button class="c-btn c-btn--info" type="submit">Submit Score!
                                                        </button>
                                                    </div>
                                                </form>




                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            @endforeach

                            <div class="col-md-12">
                                <center>
                                    <a href="{{url('view/submission/'.$question->ref_id)}}" class="c-btn c-btn--primary">
                                        Back to Student Submission List
                                    </a>
                                </center>

                            </div>
                    @else
                        <div class="col-md-12">
                            <center>
                                <div class="alert alert-danger">
                                    <strong>Opps</strong> There are no submission
                                </div>

                                <a href="{{url('view/submission/'.$question->ref_id)}}" class="c-btn c-btn--primary">
                                    Back to Home
                                </a>
                            </center>

                        </div>
                        <br><br>

                    @endif



                </div>

            </div>


        </div>


    </div>





@endsection