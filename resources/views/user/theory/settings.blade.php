@extends('layouts.flat')

@section('title', 'Create Questions for')


    @section('content')

        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="c-card u-p-medium">
                        <div class="row u-justify-center">
                            <div class="col-md-7">
                                <a class="u-mt-large u-text-center">
                                    <h4 class="u-mb-xsmall"> You have Not Subscribed for this Feature </h4>
                                    <p class="u-text-mute u-mb-large"> Please Contact the Admin at - Email:dominahltech@gmail.com or Call 08137331282</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        @endsection