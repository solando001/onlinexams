@extends('layouts.master')
@section('title', 'Upload Theory Question Information')

    @section('content')

        <div class="col-xl-8 c-card u-p-medium u-mb-medium">
            <h3>Create Theory Exam Information</h3>
            <form method="POST" action="{{ url('store/theory/information') }}" aria-label="{{ __('create_user') }}">
                @csrf

                @if(session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif

                @if($errors->any())
                    <div class="text-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>
                                    {{$error}}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">

                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="firstName">{{ __('Title of Exam') }}</label>

                            <input id="year" type="text" class="c-input form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                   name="name" placeholder="Health Science" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="name">{{ __('Select Class') }}</label>
                            <select id="name" class="c-input form-control" name="level" required>
                                <option value="">Select Class</option>
                                @foreach($level as $lev)
                                <option value="{{$lev->id}}">{{$lev->name}}</option>
                                @endforeach
                            </select>

                            @if ($errors->has('level'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('level') }}</strong>
                                </span>
                            @endif
                        </div>


                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="name">{{ __('Select Subject') }}</label>
                            <select id="name" class="c-input form-control" name="subject" required>
                                <option value="">Select Term</option>
                                @foreach($subject as $sub)
                                <option value="{{$sub->id}}">{{$sub->name}}</option>
                                    @endforeach
                            </select>

                            @if ($errors->has('subject'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('subject') }}</strong>
                                </span>
                            @endif
                        </div>


                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="firstName">{{ __('Total Questions Allowed') }}</label>

                            <input id="year" type="text" class="c-input form-control{{ $errors->has('total_questions') ? ' is-invalid' : '' }}"
                                   name="total_questions" placeholder="E.G 3" value="{{ old('total_questions') }}" required>

                            @if ($errors->has('total_questions'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('total_questions') }}</strong>
                            </span>
                            @endif
                        </div>


                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="firstName">{{ __('Theory Exam Deadline') }}</label>

                            <input id="year" type="date" class="c-input form-control{{ $errors->has('time') ? ' is-invalid' : '' }}"
                                   name="time" placeholder="E.G 2018/2019" value="{{ old('time') }}" required>

                            @if ($errors->has('time'))
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('time') }}</strong>
                            </span>
                            @endif
                        </div>


                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="bio"></label>
                            <button class="c-btn c-btn--info" type="submit">Create Exam!
                            </button>
                        </div>


                    </div>

                </div>

            </form>

        </div>

        @endsection

