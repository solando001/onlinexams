@extends('layouts.flat')

@section('title', 'List of Theory Questions')


    @section('content')

        <div class="col-sm-12 col-lg-12">

            <div class="c-table-responsive@tablet">

                    <table class="c-table u-mb-large">
                        <caption class="c-table__title">
                            List of Theory Question
                        </caption>
                        @if(session('message'))
                            <div class="alert alert-success">
                                {{ session('message') }}
                            </div>
                        @endif

                        @if($ExamList->count() > 0)
                        <thead class="c-table__head c-table__head--slim">


                        <tr>
                            <th class="c-table__cell c-table__cell--head">Name</th>
                            <th class="c-table__cell c-table__cell--head">Lesson Title </th>
                            <th class="c-table__cell c-table__cell--head">Total Question</th>
                            <th class="c-table__cell c-table__cell--head">Subject</th>
                            <th class="c-table__cell c-table__cell--head">Class</th>
                            @if(Auth::user()->hasRole(['system', 'admin']))
                            <th class="c-table__cell c-table__cell--head">Teacher's Name</th>
                            @endif
                            <th class="c-table__cell c-table__cell--head">
                                <span class="">Actions</span>
                            </th>
                        </tr>
                        @foreach($ExamList as $list)

                        <tr class="c-table__row">
                            <td class="c-table__cell">
                                {{$list->name}}
                            </td>
                            <td class="c-table__cell">
                                @if($list->lesson == null)
                                    <strike>No Lesson Attached</strike>
                                    @else
                                    <a href="{{url('preview/lesson/'.$list->lesson->id)}}" class="text-capitalize">
                                        <div class="badge-info"><b>{{$list->lesson->title}}</b> </div>
                                    </a>
                                    @endif
                            </td>
                            <td class="c-table__cell">
                                {{$list->total_questions}}
                            </td>
                            <td class="c-table__cell">
                                {{$list->examSubject->name}}
                            </td>
                            <td class="c-table__cell">
                                {{$list->questionLevel->name}}
                            </td>
                            @if(Auth::user()->hasRole(['system', 'admin']))
                                <td class="c-table__cell">
                                    {{$list->teacherInfo->surname}}, {{$list->teacherInfo->other_names}}
                                </td>
                                @endif
                            <td class="c-table__cell u-text-left">

                                @if($list->active)
                                <a class="c-btn c-btn--fancy c-btn--small" href="{{url('unpublish/theory/question/'.$list->ref_id)}}">Unplubish</a>
                                @else
                                    <a class="c-btn c-btn--warning c-btn--small" href="{{url('publish/theory/question/'.$list->ref_id)}}">Publish</a>
                                    @endif

                                <a class="c-btn btn-primary c-btn--small" href="{{url('view/submission/'.$list->ref_id)}}">View Submissions</a>

                                {{--<a class="c-btn c-btn--primary" href="{{url('edit/theory/question/'.$list->ref_id)}}">Edit</a>--}}
                                <a class="c-btn c-btn--success c-btn--small" href="{{url('view/theory/question/'.$list->ref_id)}}">View Detail</a>
                            </td>
                        </tr>
                        @endforeach

                        </thead>

                        @else

                            <tr>
                                <td>
                                    <div class="alert alert-danger">
                                        <strong>Opps!</strong> There's no Exam available at the moment
                                    </div>
                                </td>
                                <td></td>
                                <td></td>

                            </tr>



                        @endif
                    </table>

                {{$ExamList->links()}}
            </div>
        </div>

    @endsection