@extends('layouts.master')

@section('title', ' Students in '.$class->name)

@section('content')
    <div class="col-sm-12 col-lg-8">

        <div class="c-table-responsive@tablet">
            <table class="c-table u-mb-large">
                <caption class="c-table__title">
                    Student in <u>{{$class->name}}</u> Class offering <u>{{$subject->name}}</u> | {{$question->name}}

                    @if($class->name == "Ex-Students")

                    @else

                        {{--@if(count($students) > 0)--}}

                            {{--<span class="pull-right"><a href="{{ url('/promote/'.$class->id.'/student') }}" class="c-btn c-btn--fancy">Promote All Students</a></span>--}}

                        {{--@endif--}}

                    @endif
                </caption>

                <thead class="c-table__head c-table__head--slim">
                <tr>
                    <th class="c-table__cell c-table__cell--head">#</th>
                    <th class="c-table__cell c-table__cell--head">Student Name</th>
                    <th class="c-table__cell c-table__cell--head">Reg. Number</th>
                    <th class="c-table__cell c-table__cell--head">
                        <span class="u-hidden-visually">Actions</span>
                    </th>
                </tr>
                </thead>

                @if(session('messages'))
                    <div class="alert alert-success">
                        {{ session('messages') }}
                    </div>
                @endif

                @if(session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                <tbody>
                <?php $i = 1; ?>
                @foreach($students as $student)
                    <tr class="c-table__row">
                        <td class="c-table__cell">{{$i++}}</td>
                        <td class="c-table__cell">{{$student->surname}}, {{$student->other_names}}</td>
                        <td class="c-table__cell">{{$student->email}}</td>
                        <td class="c-table__cell u-text-right">
                            {{--<a class="c-btn c-btn--warning" href="{{ url('/promote/'.$student->id.'/student') }}">Promote Student</a>--}}
                            {{--<a class="c-btn c-btn--danger c-btn--small" onclick="return confirm('Selected Student will be deleted. Are you sure you want to continue?');" href="{{ url('/delete/'.$student->id) }}">Delete Student's Details</a>--}}

                            <a class="c-btn c-btn--info c-btn--small" href="{{ url('/student/'.$student->id.'/answer/'.$question->id) }}">
                                View Submission
                            </a>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>


        </div>
    </div>

@endsection