@extends('layouts.flat')

@section('title', 'Create Questions')

    @section('content')

        <div class="container-fluid">
            <h3>{{$que->name}} ({{$questionsList->count()}})</h3>
            <div class="row">

                <div class="col-12 col-xl-12 u-p-zero">

                    <div class="row u-m-small">

                        @if($questionsList->count() > 0)
                                @foreach($questionsList as $list)
                                <div class="col-sm-12 col-md-12">
                                    <div class="c-project-card u-mb-medium">
                                        {{--<img src="{{asset("asset/img/project-card1.jpg")}}" alt="About the image">--}}

                                        <div class="c-project-card__content">
                                            <div class="c-project-card__head">
                                                <h2 class="c-project-card__title">{!! $list->question_detail !!}</h2>
                                                <br>
                                                <hr>
                                                <br>

                                                <p class="c-project-card__info">
                                                <img src="{{$list->url}}" class="image-responsive">
                                                </p>
                                            </div>

                                            <div class="c-project-card__meta">
                                                <p>{{$list->mark}}<small class="u-block u-text-mute">Mark Obtainable</small></p>
                                                <p><a href="">.</a><small class="u-block u-text-mute"> Created: {{\Carbon\Carbon::parse($list->created_at)->diffForHumans()}}</small></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    @endforeach

                            @else

                            <div class="alert alert-danger">
                                <strong>Opps</strong> There are not Questions At the Moment
                            </div>

                        @endif



                    </div>

                </div>


            </div>


            <div class="col-md-6">
                <div class="c-field u-mb-small">
                    <label class="c-field__label" for="bio"></label>
                    <a href="{{url('create/'.$que->ref_id.'/questions/option')}}" class="c-btn c-btn--fancy pull-left">
                        + Add More Questions
                    </a>
                </div>
            </div>

            <br><br>


        </div>


        @endsection