
@extends('layouts.master')
@section('title', 'Create Questions for'. $question->name)

    @section('styles')
        <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
        <script>
            tinymce.init({
                selector: '#mytextarea'
            });
        </script>
        @endsection

        @section('content')

            <div class="col-xl-8 c-card u-p-medium u-mb-medium">
                <h3>Create Questions</h3>
                <h5 class="text-danger">Total Questions Uploaded - {{$question_up}}</h5>
                <br>
                @if(session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif

                @if(session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                @if($errors->any())
                    <div class="text-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>
                                    {{$error}}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form method="post" action="{{url('/store/question')}}" enctype="multipart/form-data">
                    @csrf
                    <textarea id="mytextarea" name="question_detail">  </textarea>
                    <br>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Image File (Optional)') }}</label>

                        <input id="year" type="file" class="c-input form-control" name="image">

                        @if ($errors->has('image'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>
                    <br>

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Obtainable Mark') }}</label>

                        <input id="year" type="text" class="c-input form-control{{ $errors->has('mark') ? ' is-invalid' : '' }}"
                               name="mark" placeholder="20" value="{{ old('mark') }}" required>

                        @if ($errors->has('mark'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('mark') }}</strong>
                            </span>
                        @endif
                    </div>

                    <input type="hidden" name="theory_question_id" value="{{$question->id}}">


                    <div class="row">
                        <div class="col-md-6">
                            <div class="c-field u-mb-small">
                                <label class="c-field__label" for="bio"></label>
                                <button class="c-btn c-btn--info" type="submit">Save Question
                                </button>
                            </div>
                        </div>

                        @if($question_up > 0)
                            <div class="col-md-6">
                                <div class="c-field u-mb-small">
                                    <label class="c-field__label" for="bio"></label>
                                    <a href="{{url('view/theory/question/'.$question->ref_id)}}" class="c-btn c-btn--fancy pull-right">
                                        View Questions
                                    </a>
                                </div>
                            </div>
                        @endif
                    </div>

                </form>
            </div>

        @endsection
