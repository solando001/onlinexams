@extends('layouts.master')
@section('title', 'EPortal Online Exam System - Student List')


@section('content')

<div class="col-sm-12 col-lg-8">

	<div class="c-table-responsive@tablet">
		@if($users->count() > 0)
		<table class="c-table u-mb-large">
			<caption class="c-table__title">
				List of Students 
			</caption>
			<thead class="c-table__head c-table__head--slim">
				<tr>
					<th class="c-table__cell c-table__cell--head">Surame</th>
					<th class="c-table__cell c-table__cell--head">Other Names</th>
					<th class="c-table__cell c-table__cell--head">Admssion Number</th>
                    <th class="c-table__cell c-table__cell--head">Gender</th>
					<th class="c-table__cell c-table__cell--head">
						<span class="u-hidden-visually">Actions</span>
					</th>
				</tr>
			</thead>

			<tbody>
				@foreach ($users as $user)
				<tr class="c-table__row">
					<td class="c-table__cell">
						{{$user->surname}}                             
                    </td>

                    <td class="c-table__cell">
                    	{{$user->other_names}}
                    </td>

                    <td class="c-table__cell">
                        {{$user->email}}
                    </td>

                     <td class="c-table__cell">
                        {{$user->gender}}
                    </td>
                                              <!--<td class="c-table__cell">{{$user->email}}
                                                    <span class="u-block u-text-xsmall u-text-mute">
                                                        32 Tickets remaining
                                                    </span>
                                                </td>-->
                    
                </tr>
            @endforeach

                                            <!--<tr class="c-table__row">
                                                <td class="c-table__cell">English Language
                                                    <span class="u-block u-text-mute u-text-xsmall">
                                                        Teacher: Mrs Amoko Alice
                                                    </span>
                                                </td>
                                                <td class="c-table__cell">1hr 20min</td>
                                                <td class="c-table__cell">
                                                    <del class="u-text-danger">Completed</del>
                                                </td>
                                                <td class="c-table__cell u-text-right">
                                                    <a class="c-btn is-disabled" href="#">Start Exam</a>
                                                </td>
                                            </tr>-->
                                        </tbody>
                                    </table>

                                    {{ $users->links() }}
                                    @else
                                    <div class="container">
                                    	<div class="col-md-12 alert alert-info"> You do not have any student!</div>
                                    	<br>
                                    </div>
                                    @endif

                                </div>
                            </div>



                            @endsection
