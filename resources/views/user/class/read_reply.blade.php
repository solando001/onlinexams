@extends('layouts.flat')
@section('title', 'Read Replies')
@section('styles')
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">--}}
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">

    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: '#mytextarea'
        });
    </script>


@endsection

@section('content')
    <div class="container-fluid">

        <div class="row">
            <div class="col-xl-12">

            <article class="c-stage">
                <div class="c-stage__header o-media u-justify-start">
                    <div class="c-stage__icon o-media__img">
                        <i class="fa fa-check"></i>
                    </div>
                    <div class="c-stage__header-title o-media__body">
                        <h6 class="u-mb-zero">{{$view_reply->student->surname}}, {{$view_reply->student->other_names}}</h6>
                        <p class="u-text-xsmall u-text-mute">{{$view_reply->lesson->title}}  |  Subject: {{$view_reply->lesson->subject->name}} |  Class: {{$view_reply->lesson->level->name}}</p>
                    </div>
                </div>

                <div class="c-stage__panel u-p-medium">

                    {{--<p class="u-text-mute u-text-uppercase u-text-small u-mb-xsmall">Description</p>--}}
                    <p class="u-mb-medium">
                        {!! $view_reply->reply !!}
                    </p>
                    <br>
                    <br>
                    <p>

                    @if($view_reply->url == "")

                        @else
                    <div class="thumbnail">
                        <a href="{{$view_reply->url}}">
                            <img src="{{$view_reply->url}}" alt="Lights" style="width:100%">
                            <div class="caption">
                                {{--<p>Lorem ipsum...</p>--}}
                            </div>
                        </a>
                    </div>
                    @endif
                    </p>
                    <br>


                </div><!-- // .c-stage__panel -->

                <div class="c-stage__panel u-p-medium">
                    <p class="u-text-mute u-text-uppercase u-text-small u-mb-xsmall">Send Reply</p>

                    @if(session('message'))
                        <div class="alert alert-success">
                            {{ session('message') }}
                        </div>
                    @endif

                    <form action="{{url('post/teacher/reply')}}" method="POST">
                        @csrf
                        <p class="u-mb-medium">
                            <textarea id="mytextarea" name="text" cols="10" required>  </textarea>
                        </p>

                        <input type="hidden" name="student_id" value="{{$view_reply->user_id}}">
                        <input type="hidden" name="teacher_id" value="{{$view_reply->teacher_id}}">
                        <input type="hidden" name="lesson_id" value="{{$view_reply->lesson_id}}">
                        <input type="hidden" name="reply_id" value="{{$view_reply->id}}">

                        <p>
                            <a href="{{url('view/student/reply')}}" class="c-btn c-btn--info c-btn--small pull-right">Back</a>
                            <button type="submit"  class="c-btn c-btn--secondary c-btn--small pull-left">Send Reply</button>
                            <br>
                        </p>

                    </form>


                </div>
            </article><!-- // .c-stage -->
            </div>
        </div>
    </div>

    @endsection