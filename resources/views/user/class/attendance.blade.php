@extends('layouts.flat')
@section('title', 'Attendance List ')

@section('content')

    <div class="col-sm-12 col-lg-12">

        <div class="c-table-responsive@tablet">
            <table class="c-table u-mb-large">
                <caption class="c-table__title">
                    Attendance for {{$student->surname}} {{$student->other_names}}
                </caption>

                <thead class="c-table__head c-table__head--slim">
                <tr>
                    <th class="c-table__cell c-table__cell--head">#</th>
                    <th class="c-table__cell c-table__cell--head">Student Name</th>
                    <th class="c-table__cell c-table__cell--head">Class</th>
                    <th class="c-table__cell c-table__cell--head">Day Attended</th>
                </tr>
                </thead>
                @if(session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif

                {{--@if(session('error'))--}}
                {{--<div class="alert alert-danger">--}}
                {{--{{ session('error') }}--}}
                {{--</div>--}}
                {{--@endif--}}

                <tbody>
                <?php $i = 1; ?>
                @foreach($attendance as $note)

                    <?php
                    $tot = $today;

                    ?>
                    <tr class="c-table__row">
                        <td class="c-table__cell">{{$i++}}</td>
                        <td class="c-table__cell">{{$note->student->surname}} {{$note->student->other_names}}</td>
                        <td class="c-table__cell">{{$class}}</td>
                        <td class="c-table__cell">
                            <?php $note->timestamps = false; ?>
                            {{$note->created_at = date("l, dS F, Y")}}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{$attendance->links()}}


        </div>
    </div>

@endsection