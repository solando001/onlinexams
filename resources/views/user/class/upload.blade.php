@extends('layouts.flat')
@section('title', 'Preview Lesson Note')

@section('styles')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.0/min/dropzone.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.0/dropzone.js"></script>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <h4 class="text-center">

                </h4>

                <div class="alert alert-info">
                    <li>
                        <strong>Subject:</strong> {{$lesson->subject->name}}
                    </li>
                    <li>
                        <strong>Class:</strong>  {{$lesson->level->name}}
                    </li>
                    <li>
                        <strong>Teacher's Name:</strong> {{$lesson->user->surname}} {{$lesson->user->other_names}}
                    </li>
                </div>
            </div>



            <div class="col-12 col-xl-12 u-p-zero">
                <div class="row u-m-small">
                    <div class="col-sm-12 col-md-12">
                        <div class="c-project-card u-mb-medium">
                            {{--<img src="{{asset("asset/img/project-card1.jpg")}}" alt="About the image">--}}

                            @if(session('message_video'))
                                <div class="alert alert-success">
                                    {{ session('message_video') }}
                                </div>
                            @endif

                            @if(session('message_audio'))
                                <div class="alert alert-success">
                                    {{ session('message_audio') }}
                                </div>
                            @endif

                            @if(session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                            @endif

                            <div class="c-project-card__content">

                                <div class="c-project-card__head">
                                    <h2>{{$lesson->title}}</h2>
                                    <hr>
                                    <h2 class="c-project-card__title">{!! $lesson->lesson_note !!}</h2>
                                    <br>
                                    <p class="c-project-card__info"> </p>
                                </div>

                                <div class="c-project-card__meta">
                                    <p>
                                        <button type="button" data-toggle="modal" data-target="#myModal"
                                                class="c-btn  btn-sm c-btn--fancy pull-left">Add Assessment
                                        </button>
                                    </p>
                                    {{--<p>{{$lesson->subject->name}}<small class="u-block u-text-mute">Subject</small></p>--}}
                                    {{--<p><a href="">.</a><small class="u-block u-text-mute"> Created: {{\Carbon\Carbon::parse($lesson->created_at)->diffForHumans()}}</small></p>--}}


                                    <p>
                                        <button type="button" data-toggle="modal" data-target="#myVideo"
                                                class="c-btn  btn-sm c-btn--secondary">Upload Video Files
                                        </button>


                                        <button type="button" data-toggle="modal" data-target="#myAudio"
                                                class="c-btn  btn-sm c-btn--info ">Upload Audio Files
                                        </button>
                                            {{--Created: {{\Carbon\Carbon::parse($lesson->created_at)->diffForHumans()}}--}}
                                        </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <br>


                @if(isset($lesson_audio) && count($lesson_audio) > 0)
                <div class="container-fluid">
                    <div class="row">
                        <?php $i = 1 ?>

                       @foreach($lesson_audio as $audio)
                                <div class="col-sm-6 col-lg-6 col-xl-3">
                                    <div class="c-project-card u-mb-medium">
                                        <audio controls>
            {{--                                <source src="{{asset('uploads/splinter.mp3')}}" type="audio/ogg">--}}
                                            <source src="{{$audio->url}}" type="audio/mpeg">
                                            <code>Your browser does not support the audio player, please use Chrome, Opera or FireFox brower.</code>
                                        </audio>
                                    </div>
                                    <div class="c-project-card__content">
                                        <div class="c-project-card__head">
                                            <h4 class="c-project-card__title">{{$audio->title}}</h4>
                                            {{--<p class="c-project-card__info">Kinfolk  |  Last Update: 21 Dec 2016</p>--}}
                                        </div>
                                    </div>
                        </div>
                            @endforeach
                    </div>
                </div>
                @else
                    <div class="col-md-12">
                        <div class="alert alert-info">
                            <strong>Opps!</strong> No Audio File Yet
                        </div>
                    </div>

                    @endif
                <br>

                <br>



                @if(isset($lesson_video) && count($lesson_video) > 0)
                    <div class="container-fluid">
                        <div class="row">
                            <?php $i = 1 ?>

                            @foreach($lesson_video as $video)

                                    <div class="col-sm-6 col-lg-6 col-xl-3">
                                        <div class="c-project-card u-mb-medium">

                                    <video width="420" height="400" controls >
                                        <source src="{{$video->url}}" type="video/mp4">
                                        {{--<source src="movie.ogg" type="video/ogg">--}}
                                        <code>Your browser does not support the video tag, please use Chrome, Opera or FireFox brower.</code>

                                    </video>

                                            <div class="c-project-card__content">
                                                <div class="c-project-card__head">
                                                    <h4 class="c-project-card__title">{{$video->title}}</h4>
                                                    {{--<p class="c-project-card__info">Kinfolk  |  Last Update: 21 Dec 2016</p>--}}
                                                </div>
                                            </div>

                                        </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                @else
                    <div class="col-md-12">
                        <div class="alert alert-info">
                            <strong>Opps!</strong> No Video File Yet
                        </div>
                    </div>

                @endif
                <br>

                <br>



                <div class="container-fluid">
                    <div class="row">

                        @foreach($lesson_upload as $upload)
                        <div class="col-sm-6 col-lg-6 col-xl-3">

                            <div class="c-project-card u-mb-medium">
                                <img class="c-project-card__img" src="{{$upload->url}}" alt="About the image">
                                <div class="c-project-card__content">
                                    <div class="c-project-card__head">
                                        <h4 class="c-project-card__title">Images</h4>
                                        {{--<p class="c-project-card__info">Kinfolk  |  Last Update: 21 Dec 2016</p>--}}
                                    </div>
                                    {{--<div class="c-project-card__meta">--}}
                                        {{--<p>4,870 USD--}}
                                            {{--<small class="u-block u-text-mute">Budget / Salary</small>--}}
                                        {{--</p>--}}
                                        {{--<p>Early Dec 2017--}}
                                            {{--<small class="u-block u-text-danger">10 Days Remaining</small>--}}
                                        {{--</p>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            </div>

                        </div>
                    </div>
                            @endforeach
                </div>

                </div>


        </div>
    </div>



        {{--Video file upload modal--}}
        <div id="myVideo" class="modal fade" role="dialog" data-backdrop="static">

            <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Upload Video File</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">

                        <div class="col-md-12">

                            <form action="{{url('upload/video')}}" method="POST" enctype="multipart/form-data">
                                @csrf

                                <div class="c-field u-mb-small">
                                    <label class="c-field__label" for="firstName">{{ __('Title of Media') }}</label>

                                    <input id="year" type="text" class="c-input form-control"
                                           name="title" placeholder="Twinkle Twinkle Little Star" value="{{ old('title') }}" required autofocus>
                                </div>

                                <div class="c-field u-mb-small">
                                    <label class="c-field__label" for="firstName">{{ __('Description of Media') }}</label>

                                    <textarea class="form-control" name="description" required></textarea>
                                </div>


                                <div class="input-group hdtuto control-group lst increment" >

                                    <input type="file" name="videos[]" class="myfrm form-control">

                                    {{--<div class="input-group-btn">--}}

                                    {{--<button class="btn btn-success" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>--}}

                                    {{--</div>--}}

                                </div>

                                <div class="clone hide">

                                    <div class="hdtuto control-group lst input-group" style="margin-top:10px">

                                        <input type="file" name="videos[]" class="myfrm form-control">

                                        <div class="input-group-btn">

                                            <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>

                                        </div>

                                    </div>

                                </div>
                                <br>

                                <input type="hidden" name="lesson_id" value="{{$lesson->id}}">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="c-field u-mb-small">
                                            <label class="c-field__label" for="bio"></label>
                                            <button class="c-btn c-btn--info" type="submit">Upload Video Files
                                            </button>
                                        </div>
                                    </div>

                                </div>

                            </form>

                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        {{--Audio file upload modal--}}
        <div id="myAudio" class="modal fade" role="dialog" data-backdrop="static">

            <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Upload Audio Files</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">

                        <div class="col-md-12">

                            <form action="{{url('upload/audio')}}" method="POST" enctype="multipart/form-data">
                                @csrf

                                <div class="c-field u-mb-small">
                                    <label class="c-field__label" for="firstName">{{ __('Title of Media') }}</label>

                                    <input id="year" type="text" class="c-input form-control"
                                           name="title" placeholder="Twinkle Twinkle Little Star" value="{{ old('title') }}" required autofocus>
                                </div>

                                <div class="c-field u-mb-small">
                                    <label class="c-field__label" for="firstName">{{ __('Description of Media') }}</label>

                                    <textarea class="form-control" name="description" required></textarea>
                                </div>


                        <div class="input-group hdtuto control-group lst increment" >

                            <input type="file" name="audios[]" class="myfrm form-control">

                            {{--<div class="input-group-btn">--}}

                                {{--<button class="btn btn-success" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>--}}

                            {{--</div>--}}

                        </div>

                        <div class="clone hide">

                            <div class="hdtuto control-group lst input-group" style="margin-top:10px">

                                <input type="file" name="audios[]" class="myfrm form-control">

                                <div class="input-group-btn">

                                    <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>

                                </div>

                            </div>

                        </div>
                                <br>

                                <input type="hidden" name="lesson_id" value="{{$lesson->id}}">

                        <div class="row">
                                    <div class="col-md-6">
                                        <div class="c-field u-mb-small">
                                            <label class="c-field__label" for="bio"></label>
                                            <button class="c-btn c-btn--info" type="submit">Upload Audio Files
                                            </button>
                                        </div>
                                    </div>

                        </div>

                            </form>

                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

{{--Assignment Modal--}}

    <div id="myModal" class="modal fade" role="dialog" data-backdrop="static">

            <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Create Assessment</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <form method="POST" action="{{ url('store/theory/information') }}" aria-label="{{ __('create_user') }}">
                        @csrf

                        @if(session('message'))
                            <div class="alert alert-success">
                                {{ session('message') }}
                            </div>
                        @endif

                        @if($errors->any())
                            <div class="text-danger">
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>
                                            {{$error}}
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Title of Exam') }}</label>

                        <input id="year" type="text" class="c-input form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                               name="name" placeholder="Health Science" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                        @endif
                    </div>

                        <div class="c-field u-mb-small">
                            <label class="c-field__label" for="name">{{ __('Class') }}</label>
                            <input id="year" type="text" class="c-input form-control"
                                   value="{{ $lesson->level->name }}" required disabled>

                            {{--<select id="name" class="c-input form-control" name="subject" required>--}}
                            {{--<option value="">Select Term</option>                            --}}
                            {{--@foreach($subject as $sub)--}}
                            {{--<option value="{{$sub->id}}">{{$sub->name}}</option>--}}
                            {{--@endforeach--}}
                            {{--</select>--}}

                            <input type="hidden" name="level" value="{{ $lesson->level->id }}">
                        </div>


                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="name">{{ __('Subject') }}</label>
                        <input id="year" type="text" class="c-input form-control"
                                value="{{ $lesson->subject->name }}" required disabled>

                        {{--<select id="name" class="c-input form-control" name="subject" required>--}}
                            {{--<option value="">Select Term</option>                            --}}
                            {{--@foreach($subject as $sub)--}}
                                {{--<option value="{{$sub->id}}">{{$sub->name}}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}

                       <input type="hidden" name="subject" value="{{ $lesson->subject->id }}">
                    </div>


                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Total Questions Allowed') }}</label>

                        <input id="year" type="text" class="c-input form-control{{ $errors->has('total_questions') ? ' is-invalid' : '' }}"
                               name="total_questions" placeholder="E.G 3" value="{{ old('total_questions') }}" required>

                        @if ($errors->has('total_questions'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('total_questions') }}</strong>
                            </span>
                        @endif
                    </div>


                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Theory Exam Deadline') }}</label>

                        <input id="year" type="date" class="c-input form-control{{ $errors->has('time') ? ' is-invalid' : '' }}"
                               name="time" placeholder="E.G 2018/2019" value="{{ old('time') }}" required>

                        @if ($errors->has('time'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('time') }}</strong>
                            </span>
                        @endif
                    </div>

                        <input type="hidden" name="lesson_id" value="{{$lesson->id}}">


                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="bio"></label>
                        <button class="c-btn c-btn--info" type="submit">Create Assessment!
                        </button>
                    </div>

                    </form>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>




        <script type="text/javascript">

            $(document).ready(function() {

                $(".btn-success").click(function(){

                    var lsthmtl = $(".clone").html();

                    $(".increment").after(lsthmtl);

                });

                $("body").on("click",".btn-danger",function(){

                    $(this).parents(".hdtuto control-group lst").remove();

                });

            });

        </script>


@endsection