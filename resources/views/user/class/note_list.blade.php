@extends('layouts.flat')
@section('title', 'Lesson Notes ')

@section('content')

    <div class="col-sm-12 col-lg-12">

        <div class="c-table-responsive@tablet">
            <table class="c-table u-mb-large">
                <caption class="c-table__title">
                    List of Notes
                </caption>

                <thead class="c-table__head c-table__head--slim">
                <tr>
                    <th class="c-table__cell c-table__cell--head">#</th>
                    <th class="c-table__cell c-table__cell--head">Title</th>
                    <th class="c-table__cell c-table__cell--head">Subject</th>
                    <th class="c-table__cell c-table__cell--head">Class</th>
                    <th class="c-table__cell c-table__cell--head">Views</th>
                    @if(Auth::user()->hasRole(['system', 'admin']))
                        <th class="c-table__cell c-table__cell--head">Teacher's Name</th>
                    @endif
                    <th class="c-table__cell c-table__cell--head">
                        <span class="">Actions</span>
                    </th>
                </tr>
                </thead>

                @if(session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif

                {{--@if(session('error'))--}}
                    {{--<div class="alert alert-danger">--}}
                        {{--{{ session('error') }}--}}
                    {{--</div>--}}
                {{--@endif--}}

                <tbody>
                <?php $i = 1; ?>
                @foreach($notes as $note)
                    <tr class="c-table__row">
                        <td class="c-table__cell">{{$i++}}</td>
                        <td class="c-table__cell">{{$note->title}}</td>
                        <td class="c-table__cell">{{$note->subject->name}}</td>
                        <td class="c-table__cell">{{$note->level->name}}</td>
                        <td class="c-table__cell">{{$note->count}}</td>
                        @if(Auth::user()->hasRole(['system', 'admin']))
                            <td class="c-table__cell">{{$note->user->surname}}, {{$note->user->other_names}}</td>
                        @endif

                        <td class="c-table__cell u-text-left">
                            @if($note->active)
                            <a class="c-btn c-btn--fancy c-btn--small" href="{{url('/unpublish/'.$note->id)}}">UnPublish</a>
                            @else
                                <a class="c-btn c-btn--primary c-btn--small" href="{{url('/publish/'.$note->id)}}">Publish</a>
                            @endif
                            {{--<a class="c-btn c-btn--danger c-btn--small" onclick="return confirm('Selected Student will be deleted. Are you sure you want to continue?');" href="{{ url('/delete/'.$student->id) }}">Delete Student's Details</a>--}}

                            <a class="c-btn c-btn--info c-btn--small" href="{{ url('preview/lesson/'.$note->id) }}">
                                View Lesson Note
                            </a>

                                <a class="c-btn c-btn--secondary c-btn--small" href="{{ url('edit/'.$note->id.'/lesson/') }}">
                                    Edit Lesson Note
                                </a>

                                <a class="c-btn c-btn--danger c-btn--small" onclick="return confirm('Are you sure you want to delete this Lesson Note');"  href="{{ url('delete/'.$note->id.'/lesson') }}">
                                    Delete Lesson Note
                                </a>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$notes->links()}}


        </div>
    </div>

@endsection