@extends('layouts.flat')
@section('title', 'Class Attendance List ')


@section('content')

    <div class="col-sm-12 col-lg-12">

        <div class="c-table-responsive@tablet">
            {{--<table class="c-table u-mb-large" id="example">--}}
            <table id="datatables" class="c-table u-mb-large">
                <caption class="c-table__title">
                    Attendance for {{$level->name}}
                </caption>

                <thead class="c-table__head c-table__head--slim">
                <tr>
                    <th class="c-table__cell c-table__cell--head">#</th>
                    <th class="c-table__cell c-table__cell--head">Student Name</th>
                    {{--<th class="c-table__cell c-table__cell--head">Class</th>--}}
                    <th class="c-table__cell c-table__cell--head">When / Date </th>
                </tr>
                </thead>
                @if(session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif

                {{--@if(session('error'))--}}
                {{--<div class="alert alert-danger">--}}
                {{--{{ session('error') }}--}}
                {{--</div>--}}
                {{--@endif--}}

                <tbody>
                <?php $i = 1; ?>
                @foreach($list as $note)

                    <?php
//                    $tot = $today;

                    ?>
                    <tr class="c-table__row">
                        <td class="c-table__cell">{{$i++}}</td>
                        <td class="c-table__cell">{{$note->student->surname}} {{$note->student->other_names}}</td>
{{--                        <td class="c-table__cell">{{$level->name}}</td>--}}
                        <td class="c-table__cell">
                            <p class="text-danger">{{\Carbon\Carbon::parse($note->created_at)->diffForHumans()}}</p>
                            -  <p>{{Carbon\Carbon::parse($note->created_at)->toRfc7231String()}}</p>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{$list->links()}}


        </div>
    </div>


@endsection

