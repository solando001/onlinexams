@extends('layouts.student_flat')

@section('title', 'Student Lesson Notes')

@section('styles')
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">--}}
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
    <link rel="stylesheet" href="{{asset('asset/fluid-gallery.css')}}">
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: '#mytextarea'
        });
    </script>
@endsection

    @section('content')
        <div class="container-fluid">
            {{--            <h3>{{$que->name}} ({{$questionsList->count()}})</h3>--}}
            {{--<h3>Questions </h3>--}}
            <div class="row">

                <div class="col-12 col-xl-12 u-p-zero">

                    <div class="row u-m-small">

                        <div class="col-sm-12 col-md-12">
                            <div class="c-project-card u-mb-medium">
                                {{--<img src="{{asset("asset/img/project-card1.jpg")}}" alt="About the image">--}}

                                @if(session('success'))
                                    <div class="alert alert-success">
                                        {{ session('success') }}
                                    </div>
                                @endif

                                <div class="c-project-card__content">

                                    <div class="c-project-card__head">
                                        <h3>{{$lesson->title}}</h3><br>
                                        <hr>
                                        <br>
                                        <h2 class="c-project-card__title">{!! $lesson->lesson_note !!}</h2>
                                        <br>
                                        <p class="c-project-card__info"> </p>
                                    </div>

                                    <div class="c-project-card__meta">
                                        @if($theoryExamsCount > 0)
                                        <p>
                                            <button type="button" data-toggle="modal" data-target="#myModal"
                                                    class="c-btn  btn-sm c-btn--fancy pull-left">Take Assessment
                                            </button>

                                        </p>
                                        @else
                                            <p>.<small class="u-block u-text-mute">No Assessment Available</small></p>
                                            @endif
                                        {{--<p> []  <small class="u-block u-text-mute">Mark Obtainable</small></p>--}}
                                        <p><a class="c-btn  btn-sm c-btn--success" href="{{url('student/lesson/note')}}">Back To Lesson List</a>
                                            {{--<small class="u-block u-text-mute"> Created: {{\Carbon\Carbon::parse($list->created_at)->diffForHumans()}}</small>--}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

            <hr>


            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12">

                        <ul class="c-tabs__list nav nav-tabs" id="myTab" role="tablist">
                            {{--<li><a class="c-tabs__link " id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Activity</a></li>--}}

                            <li><a class="c-tabs__link active" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Image Files ({{$lesson_upload->count()}}) </a></li>

                            <li><a class="c-tabs__link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Audio Files ({{$lesson_audio->count()}})</a></li>

                            <li><a class="c-tabs__link" id="nav-customer-tab" data-toggle="tab" href="#nav-customer" role="tab" aria-controls="nav-customer" aria-selected="false">Video Files ({{$lesson_video->count()}})</a></li>
                        </ul>

                        <div class="c-tabs__content tab-content u-mb-large" id="nav-tabContent">
                            <div class="c-tabs__pane active u-pb-medium" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <center><h4></h4></center>
                                @if(isset($lesson_upload) && count($lesson_upload) > 0)
                                <div class="container gallery-container">

                                    <h3>Image Gallery</h3>

                                    {{--<p class="page-description text-center">Fluid Layout With Overlay Effect</p>--}}

                                    <div class="tz-gallery">

                                        <div class="row">
                                            @foreach($lesson_upload as $upload)
                                            <div class="col-sm-12 col-md-4">
                                                <a class="lightbox" href="{{$upload->url}}">
                                                    <img src="{{$upload->url}}" alt="Bridge">
                                                </a>
                                            </div>
                                                @endforeach
                                        </div>
                                    </div>
                                </div>
                                    @else

                                    <div class="alert alert-info">
                                        <strong>Opps!</strong>There are no Image File for this Lesson
                                    </div>
                                @endif

                            </div>

                            <div class="c-tabs__pane u-pb-medium" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                <center><h4>Audio Files</h4></center>
                                @if(isset($lesson_audio) && count($lesson_audio) > 0)
                                    <div class="container-fluid">
                                        <div class="row">
                                            <?php $i = 1 ?>

                                            @foreach($lesson_audio as $audio)
                                                <div class="col-sm-6 col-lg-6 col-xl-3">
                                                    <div class="c-project-card u-mb-medium">
                                                        <audio controls>
                                                            {{--                                <source src="{{asset('uploads/splinter.mp3')}}" type="audio/ogg">--}}
                                                            <source src="{{$audio->url}}" type="audio/mpeg">
                                                            <code>Your browser does not support the audio player, please use Chrome, Opera or FireFox brower.</code>
                                                        </audio>
                                                    </div>
                                                    <div class="c-project-card__content">
                                                        <div class="c-project-card__head">
                                                            <h4 class="c-project-card__title">{{$audio->title}}</h4>
                                                            {{--<p class="c-project-card__info">Kinfolk  |  Last Update: 21 Dec 2016</p>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @else

                                        <div class="alert alert-info">
                                            <strong>Opps!</strong>There are no Audio File for this Lesson
                                        </div>


                                @endif


                            </div>

                            <div class="c-tabs__pane u-pb-medium" id="nav-customer" role="tabpanel" aria-labelledby="nav-customer-tab">
                                <center><h4>Video Files</h4></center>
                                @if(isset($lesson_video) && count($lesson_video) > 0)
                                    <div class="container-fluid">
                                        <div class="row">
                                            <?php $i = 1 ?>

                                            @foreach($lesson_video as $video)

                                                <div class="col-sm-12 col-lg-12 col-xl-12">
                                                    <div class="c-project-card u-mb-medium">

                                                        <center><video class="col-sm-12 col-lg-12 col-xl-12" controls >
                                                            <source src="{{$video->url}}" type="video/mp4">
                                                            {{--<source src="movie.ogg" type="video/ogg">--}}
                                                            <code>Your browser does not support the video tag, please use Chrome, Opera or FireFox brower.</code>

                                                        </video>
                                                        </center>

                                                        <div class="c-project-card__content">
                                                            <div class="c-project-card__head">
                                                                <center><h3 class="c-project-card__title">{{$video->title}}</h3></center>
                                                                {{--<p class="c-project-card__info">Kinfolk  |  Last Update: 21 Dec 2016</p>--}}
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @else
                                        <div class="alert alert-info">
                                            <strong>Opps!</strong> There are no Video File For this Lesson
                                        </div>
                                @endif


                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-lg-12 col-sm-12 col-md-12 u-p-zero">
                            <div class="c-chat">
                                <div class="c-chat__body">
                                    @foreach($studentReplies as $student)
                                    <div class="c-chat__message o-media">
                                        <div class="o-media__img u-mr-small">
                                            <div class="c-avatar c-avatar--small">
                                                <div class="c-avatar__img">
                                                    <img src="{{asset('asset/img/avatar1-72.jpg')}}" alt="Adam's Face">
                                                </div>
                                            </div>
                                        </div><!-- // .o-media__img -->

                                        <div class="o-media__body">
                                            <h4 class="c-chat__message-author">{{$student->student->surname}} {{$student->student->other_names}} | Student</h4>
                                            <span class="c-chat__message-time">{{\Carbon\Carbon::parse($student->created_at)->diffForHumans()}}</span>
                                            <p class="c-chat__message-content">
                                               {!! $student->reply !!}
                                            </p>
                                        </div><!-- // .o-media__body -->
                                    </div>
                                        @endforeach

                                        @foreach($teacherReplies as $teacher)
                                            <div class="c-chat__message o-media">
                                                <div class="o-media__img u-mr-small">
                                                    <div class="c-avatar c-avatar--small">
                                                        <div class="c-avatar__img">
                                                            <img src="{{asset('asset/img/avatar1-72.jpg')}}" alt="Adam's Face">
                                                        </div>
                                                    </div>
                                                </div><!-- // .o-media__img -->

                                                <div class="o-media__body">
                                                    <h4 class="c-chat__message-author">{{$teacher->teacher->surname}}  {{$teacher->teacher->other_names}}| Teacher</h4>
                                                    <span class="c-chat__message-time">{{\Carbon\Carbon::parse($teacher->created_at)->diffForHumans()}}</span>
                                                    <p class="c-chat__message-content">
                                                      {!! $teacher->text !!}
                                                    </p>
                                                </div><!-- // .o-media__body -->
                                            </div>
                                        @endforeach
                                </div>
                            </div>
                        </div>

                        {{--<div class="col-lg-8 u-p-zero">--}}
                            {{--<div class="c-chat">--}}
                                {{--<div class="c-chat__body">--}}

                                    {{--@foreach($studentReplies as $student)--}}

                                    {{--<div class="c-chat__message o-media">--}}
                                        {{--<div class="o-media__img u-mr-small">--}}
                                            {{--<div class="c-avatar c-avatar--small">--}}
                                                {{--<div class="c-avatar__img">--}}
                                                    {{--<img src="{{asset('asset/img/avatar1-72.jpg')}}" alt="Adam's Face">--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</div><!-- // .o-media__img -->--}}

                                        {{--<div class="o-media__body">--}}
                                            {{--<h4 class="c-chat__message-author">{{$student->student->surname}}</h4>--}}
                                            {{--<span class="c-chat__message-time">8:31AM</span>--}}
                                            {{--<p class="c-chat__message-content">--}}
                                                {{--{!! $student->reply !!}--}}
                                            {{--</p>--}}
                                        {{--</div><!-- // .o-media__body -->--}}
                                    {{--</div>--}}
                                        {{--@endforeach--}}



                                {{--</div><!-- // .c-chat__body -->--}}

                                {{--<form class="c-chat__composer">--}}
                                    {{--<div class="c-field has-addon-left">--}}
                                        {{--<button class="c-field__addon"><i class="fa fa-plus"></i></button>--}}
                                        {{--<input class="c-input" type="text" placeholder="Clark">--}}
                                    {{--</div>--}}
                                {{--</form><!-- // .c-chat__composer -->--}}

                            {{--</div>--}}
                        {{--</div><!-- // .col-sm-8 -->--}}
                    </div>


                </div>


                <center>
                    <button class="c-btn c-btn--info" data-toggle="modal" data-target="#myReply">Send Reply To Teacher</button>
                </center>

                <br><br>

                <div id="myReply" class="modal fade" role="dialog" data-backdrop="static">

                    <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <br> <br> <br> <br> <br>
                                <h4 class="modal-title">Reply Teacher</h4>
                                {{--<button type="button" class="close" data-dismiss="modal">&times;</button>--}}
                            </div>
                            <div class="modal-body">

                                <form action="{{url('post/reply')}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <label>Your Reply Here:</label>
                                    <textarea id="mytextarea" name="text" cols="10" required>  </textarea>

                                    <br>

                                    <label class="c-field__label" for="firstName">{{ __('Upload Image (Optional)') }}</label>
                                    <div class="input-group hdtuto control-group lst increment" >


                                        <input type="file" name="images[]" class="myfrm form-control">

                                        <div class="input-group-btn">
                                            {{--<button class="btn btn-success" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>--}}

                                        </div>

                                    </div>

                                    <input type="hidden" name="lesson_id" value="{{$lesson->id}}">
                                    <input type="hidden" name="teacher_id" value="{{$lesson->user_id}}">

                                    <br>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="c-field u-mb-small">
                                                <label class="c-field__label" for="bio"></label>
                                                <button class="c-btn c-btn--secondary" type="submit">Send Reply
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </form>


                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>



            <div id="myModal" class="modal fade" role="dialog" data-backdrop="static">

                <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <br><br>
                            <h4 class="modal-title">Assessment List</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">

                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Title</th>
                                        <th scope="col">Total Question</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 1; ?>
                                    @foreach($theoryExams as $exam)
                                        <?php
                                        $sub = \App\TheoryQuestionGrade::where('theory_question_id', $exam->id)->where('student_id', Auth::user()->id)->first();
                                        ?>
                                    <tr>
                                        <th scope="row">{{$i++}}</th>
                                        <td>{{$exam->name}}</td>
                                        <td>{{$exam->total_questions}}</td>
                                        <td>
                                            @if($sub == null)
                                            <a href="{{url('take/'.$exam->ref_id.'/exam/theory')}}" class="btn btn-primary"> Take Assessment</a>
                                                @else
                                                <a href="#" class="btn btn-primary disabled"> Assessment Completed</a>
                                            @endif
                                        </td>
                                    </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>









                <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
                <script>
                    baguetteBox.run('.tz-gallery');
                </script>


        @endsection