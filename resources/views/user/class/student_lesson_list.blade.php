@extends('layouts.student')
@section('title', 'Student Lesson Note List')

    @section('content')

        <div class="col-sm-12 col-lg-9">
            <div class="c-table-responsive@tablet">

                <table class="c-table u-mb-large">
                    <caption class="c-table__title">
                        List of Lesson Available
                    </caption>
                    <thead class="c-table__head c-table__head--slim">
                    <tr>
                        <th class="c-table__cell c-table__cell--head">Lesson Name</th>
                        <th class="c-table__cell c-table__cell--head">Subject</th>
                        {{--<th class="c-table__cell c-table__cell--head">No. of Question</th>--}}
                        <th class="c-table__cell c-table__cell--head">Actions
                            <!-- <span class="u-hidden-visually">Actions</span>-->
                        </th>
                    </tr>
                    </thead>

                    <tbody>

                    @if(isset($lessonNotes) && count($lessonNotes) > 0)
                        @foreach($lessonNotes as $note)
                    <tr class="c-table__row">
                        <td class="c-table__cell">
                           {{$note->title}}
                            <span class="u-block u-text-mute u-text-xsmall">

                            </span>
                        </td>
                        <td class="c-table__cell">
                            {{$note->subject->name}}
                        </td>
                        {{--<td class="c-table__cell">--}}
                            {{--Name--}}
                        {{--</td>--}}
                        <td class="c-table__cell">
                            <a class="c-btn c-btn--info btn-sm" href="{{url('student/take/'.$note->id.'/lesson')}}">Start Lesson</a>
                        </td>
                    </tr>
                    @endforeach



                        @else
                        <tr>
                            <td></td>
                            <td>
                                <br>
                                <div class="col-md-12">
                                    <center>
                                        <div class="alert alert-info">
                                            <strong>Opps</strong> No Lesson Available at the Moment
                                        </div>
                                    </center>

                                </div>
                            </td>
                            <td></td>


                        </tr>


                    @endif

                    </tbody>
                </table>

                {{$lessonNotes->links()}}
            </div>
        </div>


        @endsection