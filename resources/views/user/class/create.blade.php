@extends('layouts.flat')
@section('title', 'Create Lesson Note')

@section('styles')
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: '#mytextarea'

        });
    </script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.0/min/dropzone.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.0/dropzone.js"></script>

@endsection

@section('content')

    <div class="col-xl-12 c-card u-p-medium u-mb-medium">
        <h3>Create Lesson Note</h3>
        {{--<h5 class="text-danger">Total Questions Uploaded </h5>--}}
        <br>
        @if(session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif

        @if(session('message_image'))
            <div class="alert alert-danger">
                {{ session('message_image') }}
            </div>
        @endif

        @if($errors->any())
            <div class="text-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>
                            {{$error}}
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="post" action="{{url('/store/class/note')}}" enctype="multipart/form-data">
            @csrf

            <div class="row">
                <div class="col-md-4">
            <div class="c-field u-mb-small">
                <label class="c-field__label" for="firstName">{{ __('Title of Lesson Note') }}</label>

                <input id="year" type="text" class="c-input form-control{{ $errors->has('mark') ? ' is-invalid' : '' }}"
                       name="title" placeholder="Properties of Matter" value="{{ old('title') }}" required>

                @if ($errors->has('title'))
                    <span class="invalid-feedback" role="alert">
                       <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
            </div>
                </div>

                <div class="col-md-4">
                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Select Subject') }}</label>

                      <select class="form-control" name="subject_id">
                          @foreach($subject as $sub)
                          <option value="{{$sub->id}}">{{$sub->name}}</option>
                              @endforeach
                      </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Select Class') }}</label>

                        <select class="form-control" name="level_id">
                            @foreach($level as $lev)
                            <option value="{{$lev->id}}">{{$lev->name}}</option>
                                @endforeach
                        </select>
                    </div>
                </div>

            </div>

            <textarea id="mytextarea" name="lesson_note" cols="10" required>  </textarea>

            <br>
            <label class="c-field__label" for="firstName">{{ __('Upload Images') }}</label>
            <div class="input-group hdtuto control-group lst increment" >


                <input type="file" name="images[]" class="myfrm form-control">

                <div class="input-group-btn">

                    <button class="btn btn-success" type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>

                </div>

            </div>

            <div class="clone hide">

                <div class="hdtuto control-group lst input-group" style="margin-top:10px">

                    <input type="file" name="images[]" class="myfrm form-control">

                    <div class="input-group-btn">

                        <button class="btn btn-danger" type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>

                    </div>

                </div>

            </div>


            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
            <input type="hidden" name="session_id" value="{{$session->id}}">
            <input type="hidden" name="term_id" value="{{$term->id}}">

            <br>
            <div class="row">
                <div class="col-md-6">
                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="bio"></label>
                        <button class="c-btn c-btn--info" type="submit">Create Lesson Notes
                        </button>
                    </div>
                </div>

            </div>
        </form>
    </div>



    <script type="text/javascript">

        $(document).ready(function() {

            $(".btn-success").click(function(){

                var lsthmtl = $(".clone").html();

                $(".increment").after(lsthmtl);

            });

            $("body").on("click",".btn-danger",function(){

                $(this).parents(".hdtuto control-group lst").remove();

            });

        });

    </script>

@endsection