@extends('layouts.flat')
@section('title', 'Edit Lesson Notes ')


@section('styles')
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: '#mytextarea'

        });
    </script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.0/min/dropzone.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.0/dropzone.js"></script>

@endsection

@section('content')
    <div class="col-xl-12 c-card u-p-medium u-mb-medium">
        <h3>Edit Lesson Note</h3>
        {{--<h5 class="text-danger">Total Questions Uploaded </h5>--}}
        <br>
        @if(session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif

        @if($errors->any())
            <div class="text-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>
                            {{$error}}
                        </li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="post" action="{{url('/edit/class/note')}}" enctype="multipart/form-data">
            @csrf

            <div class="row">
                <div class="col-md-4">
                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Title of Lesson Note') }}</label>
                        <input id="year" type="text" class="c-input form-control" name="title" value="{{$lesson->title}}">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Select Subject') }}</label>

                        <select class="form-control" name="subject_id">
                            <option value="">{{$lesson->subject->name}}</option>
                            <option>------</option>
                            @foreach($subject as $sub)
                                <option value="{{$sub->id}}">{{$sub->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="firstName">{{ __('Select Class') }}</label>

                        <select class="form-control" name="level_id">
                            <option value="">{{$lesson->level->name}}</option>
                            <option>-----</option>
                            @foreach($level as $lev)
                                <option value="{{$lev->id}}">{{$lev->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

            </div>

            <textarea id="mytextarea" name="lesson_note" cols="10" required>
            {{$lesson->lesson_note}}
            </textarea>

            <br>


            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
            <input type="hidden" name="lesson_id" value="{{$lesson->id}}">
            {{--<input type="hidden" name="session_id" value="{{$session->id}}">--}}
            {{--<input type="hidden" name="term_id" value="{{$term->id}}">--}}

            <br>
            <div class="row">
                <div class="col-md-6">
                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="bio"></label>
                        <button class="c-btn c-btn--info" type="submit">Edit Lesson Notes
                        </button>
                    </div>
                </div>

            </div>
        </form>
    </div>


@endsection