@extends('layouts.master')
@section('title', 'EPortal Online Exam System- Create Student Record')

@section('content')

<div class="col-xl-8 c-card u-p-medium u-mb-medium">
    <h3>Edit Student Record</h3>
    <div class="alert alert-info" role="alert">
        <strong>As an Admin </strong> you can edit students details and change their Subjects
    </div>
        <form method="POST" action="{{ url('edit/student/data') }}">
            @csrf

             @if(session('error'))
        <div class="alert alert-danger">
            {{ session('error') }}
        </div>
        @endif

        @if(session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
        @endif
        <div class="row">

            <div class="col-lg-12">
                <div class="c-field u-mb-small">
                    <label class="c-field__label" for="firstName">{{ __('Surname') }}</label>
                    <input id="surname" type="text" class="c-input form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname"
                           value="{{$student->surname}}" required>
                </div>


                <div class="c-field u-mb-small">
                    <label class="c-field__label" for="firstName">{{ __('Other Names') }}</label>

                    <input id="other_names" type="text" class="c-input form-control{{ $errors->has('other_names') ? ' is-invalid' : '' }}" name="other_names"
                           value="{{$student->other_names}}" required >

                </div>

                  <div class="c-field u-mb-small">
                     <label class="c-field__label" for="subject_ids">{{ __('Gender') }}</label> 
                    <select id="gender" class="c-input form-control{{ $errors->has('gender') ? ' is-invalid' : '' }}" name="gender" required >
                        <option value="{{$student->gender}}">{{$student->gender}}</option>
                        <option>--------</option>
                        <option value="Male">Male</option>
                       <option value="Female">Female</option>
                    </select>
                </div>

                <div class="c-field u-mb-small">
                    <label class="c-field__label" for="subject_ids">{{ __('Reg No.') }}</label>
                    <input id="other_names" type="text" class="c-input form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                           value="{{$student->email}}" required >

                </div>

                {{--<div class="c-field u-mb-small">--}}
                     {{--<label class="c-field__label" for="subject_ids">{{ __('Class') }}</label> --}}
                    {{--<select id="level_id" class="c-input form-control{{ $errors->has('level_id') ? ' is-invalid' : '' }}" name="level_id">--}}
                        {{--@foreach($cur_student_level as $curLevel)--}}
                        {{--<option value="{{$curLevel->id}}">{{$curLevel->name}}</option>--}}
                        {{--@endforeach--}}
                        {{--<option>--------</option>--}}
                        {{--@foreach($levels as $level)--}}
                        {{--<option value="{{$level->id}}">{{$level->name}}</option>--}}
                        {{--@endforeach--}}
                    {{--</select>--}}
                {{--</div>--}}

                {{-- <div class="c-field u-mb-small">
                    <label class="c-field__label" for="subject_ids">{{ __('Registered Subjects') }} ({{count($reg_subject)}})</label>
                    @foreach($reg_subject as $curSubject)
                    <li>
                       {{$curSubject->name}}
                    </li>
                    @endforeach
                </div>
                <hr>
                <br> --}}


                <div class="c-field u-mb-small">
                     <label class="c-field__label" for="subject_ids">{{ __('Select New Subjects (Press Ctrl + Subject to select)') }}</label>
                    <select multiple id="subject_ids" class="c-input form-control" name="subject_ids[]">
                        @foreach($unreg_subject as $subject)
                        <option value="{{$subject->id}}">{{$subject->name}}</option>
                        @endforeach
                    </select>

                    @if ($errors->has('subject_ids'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('subject_ids') }}</strong>
                    </span>
                    @endif
                </div>

                <input type="hidden" name="role_id" value="3">
                <input type="hidden" name="student_id" value="{{$student->id}}">

                <div class="c-field u-mb-small">
                    <label class="c-field__label" for="bio"></label>
                    <button class="c-btn c-btn--info" type="submit">Update Student Record!
                    </button> 
                </div>


            </div>
        </div>
        </form>


        <hr>
        @if($reg_subject->count() > 0)
            <h4>Registered Subject</h4>
            <form method="POST" action="{{ url('edit/student/class') }}">
                @csrf
            <div class="c-field u-mb-small">
                {{-- <label class="c-field__label" for="subject_ids">{{ __('Registered Subjects') }} ({{count($reg_subject)}})</label> --}}
                <div class="alert alert-info">Any subject you select will be removed</div>
                @foreach($reg_subject as $curSubject)
                <li>
                <input type="checkbox" name="subject[]" value="{{$curSubject->id}}"> {{$curSubject->name}}
                </li>
                @endforeach
            </div>
            <input type="hidden" name="role_id" value="3">
            <input type="hidden" name="student_id" value="{{$student->id}}">

            <div class="c-field u-mb-small">
                <label class="c-field__label" for="bio"></label>
                <button class="c-btn c-btn--primary" type="submit" >Remove Subjects!
                </button> 
            </div>
            </form>
        @endif

    </div>
@endsection