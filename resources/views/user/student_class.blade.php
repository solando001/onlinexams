@extends('layouts.master')

@section('title', 'EPortal Online Exam System- List of Classes')

@section ('content')
<div class="col-sm-12 col-lg-8">

	<div class="c-table-responsive@tablet">
		@if($levels->count() > 0)
		<table class="c-table u-mb-large">
			<caption class="c-table__title">
				List of Classes
				@if(Auth::user()->hasRole('system'))
				<button type="button" data-toggle="modal" data-target="#exampleModal" class="c-btn c-alert--success pull-right">Add New Class</button>
					@endif
			</caption>
			<thead class="c-table__head c-table__head--slim">
				<tr>
					<th class="c-table__cell c-table__cell--head">Class Name</th>
					<th class="c-table__cell c-table__cell--head">Class Description</th>
					<th class="c-table__cell c-table__cell--head">No. of Student</th>

					<th class="c-table__cell c-table__cell--head">
						<span class="u-hidden-visually">Actions</span>
					</th>
				</tr>
			</thead>

			<tbody>
				@foreach ($levels as $user)
				<tr class="c-table__row">
					<td class="c-table__cell">
						{{$user->name}}                             
                    </td>

                    <td class="c-table__cell">
                    	{{$user->display_name}}
                    </td>

					<td class="c-table__cell">
						{{count($user->students)}}
					</td>


					<td class="c-table__cell u-text-right">
                      	<a class="c-btn c-btn--info" href="{{ url('/student/class/'.$user->id) }}">View Student</a>
                      	<a class="c-btn c-btn--fancy" href="{{ url('class/attendance/'.$user->id) }}">Class Attendance</a>
{{--                      	<a class="c-btn c-btn--primary" href="{{ url('upload/result/'.$user->id) }}">Upload Results</a>--}}
                    </td>
                </tr>
                @endforeach

                                            <!--<tr class="c-table__row">
                                                <td class="c-table__cell">English Language
                                                    <span class="u-block u-text-mute u-text-xsmall">
                                                        Teacher: Mrs Amoko Alice
                                                    </span>
                                                </td>
                                                <td class="c-table__cell">1hr 20min</td>
                                                <td class="c-table__cell">
                                                    <del class="u-text-danger">Completed</del>
                                                </td>
                                                <td class="c-table__cell u-text-right">
                                                    <a class="c-btn is-disabled" href="#">Start Exam</a>
                                                </td>
                                            </tr>-->
                                        </tbody>
                                    </table>

                                    {{ $levels->links() }}
                                    @else
                                    <div class="container">
                                    	<div class="col-md-12 alert alert-info"> You do not have Users yet</div>
                                    	<br>
                                    </div>
                                    @endif

                                </div>
                            </div>



						<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class=" modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Create Class</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times</span>
										</button>
									</div>
									<form action="{{url('add/ex/class')}}" method="post">
										{{csrf_field()}}
									<div class="modal-body">

											<div class="col-lg-12">
												<div class="c-field u-mb-small">
													<label class="c-field__label" for="firstName">{{ __('Create Class ') }}</label>

													<input id="year" type="text" class="c-input form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="E.G Ex-Student" value="{{ old('name') }}" required autofocus>

													@if ($errors->has('name'))
														<span class="invalid-feedback" role="alert">
															<strong>{{ $errors->first('name') }}</strong>
														</span>
													@endif
												</div>
											</div>

											<div class="modal-footer">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button type="submit" class="btn btn-primary">Save changes</button>
											</div>

									</div>

									</form>

								</div>
							</div>
						</div>


@endsection