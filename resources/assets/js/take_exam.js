/**
 * First we will import all of this page's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import  Vue from 'vue';
import  takeExam from './components/Exam/Take/mainComponent.vue';
import store from './components/Exam/Take/store.js';
import axios from 'axios';
import VeeValidate from 'vee-validate';
import swal from 'sweetalert';
//import VueRouter from "vue-router";
//import VueResource from "vue-resource";


const config = {
    errorBagName: 'errors', // change if property conflicts
    fieldsBagName: 'fields',
    delay: 0,
    locale: 'en',
    dictionary: null,
    strict: true,
    classes: false,
    classNames: {
        touched: 'touched', // the control has been blurred
        untouched: 'untouched', // the control hasn't been blurred
        valid: 'valid', // model is valid
        invalid: 'invalid', // model is invalid
        pristine: 'pristine', // control has not been interacted with
        dirty: 'dirty' // control has been interacted with
    },
    events: 'input|blur',
    inject: true,
    validity: false,
    aria: true
};

Vue.use(VeeValidate, config);
//Vue.use(VueRouter);
//Vue.use(VueResource);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

axios.defaults.headers.common['X-CSRF-TOKEN']= document.querySelector('meta[name="csrf-token"]').getAttribute('content'); //add token;

new Vue({
    el: 'take-exam',
    components:{takeExam},
    store: store,
});