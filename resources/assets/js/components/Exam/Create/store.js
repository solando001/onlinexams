import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import getters from "./getters.js";
Vue.use(Vuex);

const state = {
    time:{
        HH:"00",
        mm:"05"
    },
    exam: {
        name: null,
        mark:100,
        is_timed:true,
        time:"00:05",
        random_question:true,
        total_question:1,
        attempt_allowed:1000,
        active:false,
        level_id: null,
        subject_id:null,
    },
    levels:[],
    subjects:[],
    questions:[
        {
            name:null,
            image: null,
            options:[
                {
                    option:null,
                    correct_option: false,
                }
            ],
        }
    ]
};
const mutations = {
    updateExam(state, data){
        state.exam = data.exam;
        let a = data.exam.time.split(':');
        state.time.HH = a[0];
        state.time.mm = a[1];
    },
    postUpdateExam(state, data){
        Object.assign(state.exam, data);
    },
    updateQuestions(state, data){
        state.questions = data.question;
    },
    updateSubjectLevel(state, data){
        state.subjects = data.subjects;
        state.levels = data.levels;
    },
    activateExam(state){
        state.exam.active = true;
    }
};
const actions = {
    getAvailableSubjectsAndLevels(context){
        return new Promise((resolve, reject) => {

            axios.get('/get/available/subjects/levels')
                .then(function (response) {
                    let server_response = response.data;
                    if (server_response.status == true) {
                        resolve(server_response);
                        context.commit('updateSubjectLevel',server_response);
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        })
    },

    getExistingExamAndQuestions(context, ref_id){
        return new Promise((resolve, reject) => {

            axios.get('/get/exam/'+ref_id+'/and/question/json')
                .then(function (response) {
                    let server_response = response.data;
                    if (server_response.status == true) {
                        //console.log(server_response);
                        resolve(server_response);
                        context.commit('updateExam', server_response);
                        context.commit('updateQuestions', server_response);
                        //context.commit('updateSubscription',server_response);
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        })
    },

    saveExamData(context, edit){
        return new Promise((resolve, reject) => {
            let data = context.state.exam;
            data.questions = context.state.questions;
            console.log(data);
            let url = '/exam/store';
            if (edit) {
              url = '/update/exam/data';
            }
            axios.post(url, data)
                .then(function (response) {
                    let server_response = response.data;
                    //if (server_response.status == true) {
                        resolve(server_response);
                    //}
                })
                .catch(function (error) {
                    resolve(error);
                    console.log(error);
                });
        })
    },

    updateStepOne(context, edit){
        return new Promise((resolve, reject) => {
            let data = context.state.exam;
              let url = '/update/exam/data';
            axios.post(url, data)
                .then(function (response) {
                    let server_response = response.data;
                    //if (server_response.status == true) {
                        resolve(server_response);
                    //}
                })
                .catch(function (error) {
                    resolve(error);
                    console.log(error);
                });
        })
    },

    updateQuestion(context, question){
        return new Promise((resolve, reject) => {
              let url = '/update/question/data';
            axios.post(url, question)
                .then(function (response) {
                    let server_response = response.data;
                    if (server_response.status == true) {
                        resolve(server_response);
                    }
                })
                .catch(function (error) {
                    resolve(error);
                    console.log(error);
                });
        })
    },
    deleteQuestion(context, question){
        return new Promise((resolve, reject) => {
              let url = '/delete/question/data';
            axios.post(url, question)
                .then(function (response) {
                    let server_response = response.data;
                    if (server_response.status == true) {
                        resolve(server_response);
                    }
                })
                .catch(function (error) {
                    resolve(error);
                    console.log(error);
                });
        })
    },
    createNewQuestion(context, question){
        return new Promise((resolve, reject) => {
              let url = '/add/question/data';
            axios.post(url, question)
                .then(function (response) {
                    let server_response = response.data;
                    if (server_response.status == true) {
                        resolve(server_response);
                    }
                })
                .catch(function (error) {
                    resolve(error);
                    console.log(error);
                });
        })
    },

    publishExam(context, exam){
        return new Promise((resolve, reject) => {
              let url = '/publish/exam';
            axios.post(url, exam)
                .then(function (response) {
                    let server_response = response.data;
                    if (server_response.status == true) {
                        resolve(server_response);
                    }
                })
                .catch(function (error) {
                    resolve(error);
                    console.log(error);
                });
        })
    },
};

export default new Vuex.Store({
    state,
    mutations,
    actions,
    getters
})
