import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import getters from "./getters.js";
Vue.use(Vuex);

const state = {
    exam: {},
    questions: [],
    config: {},
    log_id: null,
    result:{}
};
const mutations = {
    updateExamDetails(state, data){
        state.exam = data.exam;
        state.questions = data.questions;
        state.config = data.config;
    },
    updateLog(state, data){
        state.log_id = data.log_id;
    },
    updateResult(state, data){
        state.result = data;
    },
};
const actions = {
    getExam(context, ref_id){
        return new Promise((resolve, reject) => {

            axios.get('/exam/'+ref_id+'/details')
                .then(function (response) {
                    let server_response = response.data;
                    //console.log(server_response);
                    if (server_response.status == true) {
                        resolve(server_response);
                        context.commit('updateExamDetails', server_response.data);
                    }
                })
                .catch(function (error) {
                    console.log(error);
                });
        })
    },

    examIsLoaded(context, ref_id){
        return new Promise((resolve, reject) => {
            let url = '/exam/log/request';
            axios.post(url, {exam_id: ref_id})
                .then(function (response) {
                    let server_response = response.data;
                    if (server_response.status === true) {
                        resolve(server_response);
                        context.commit('updateLog', server_response);
                    }
                })
                .catch(function (error) {
                    resolve(error);
                    console.log(error);
                });
        })
    },

    submitExam(context, params){
        return new Promise((resolve, reject) => {
            params.log_id = state.log_id;
            let url = '/submit/exam';
            axios.post(url, params)
                .then(function (response) {
                    let server_response = response.data;
                    resolve(server_response);
                    context.commit('updateResult', server_response.data);

                })
                .catch(function (error) {
                    resolve(error);
                    console.log(error);
                    //this.msg = response.msg;
                    //this.changeComponent('error-response-component');
                });
        })
    },
};

export default new Vuex.Store({
    state,
    mutations,
    actions,
    getters
})
