<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTheoryQuestionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theory_question_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('theory_question_id')->unsigned();
            $table->text('question_detail');
            $table->string('mark');
            $table->timestamps();

            $table->foreign('theory_question_id')->references('id')->on('theory_questions')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('theory_question_details');
    }
}
