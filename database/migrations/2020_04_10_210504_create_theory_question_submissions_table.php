<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTheoryQuestionSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theory_question_submissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('question_theory_id')->unsigned();
            $table->integer('question_detail_id')->unsigned();
            $table->text('answer_detail');
            $table->integer('total');
            $table->integer('score');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')->Update('cascade');

            $table->foreign('question_theory_id')->references('id')->on('theory_questions')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('question_detail_id')->references('id')->on('theory_question_details')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('theory_question_submissions');
    }
}
