<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTheoryQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('theory_questions', function (Blueprint $table) {
            $table->increments('id');

            $table->string('ref_id');
            $table->string('name');
            $table->string('total_questions');
            $table->string('time');
            $table->integer('user_id')->unsigned();
            $table->integer('lesson_id')->unsigned()->nullable();
            $table->integer('level_id')->unsigned();
            $table->integer('subject_id')->unsigned();
            $table->integer('session_id')->unsigned();
            $table->integer('term_id')->unsigned();
            $table->boolean('active')->default(false);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('lesson_id')->references('id')->on('lesson_notes')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('level_id')->references('id')->on('levels')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('subject_id')->references('id')->on('subjects')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('session_id')->references('id')->on('sessions')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('term_id')->references('id')->on('term')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('theory_questions');
    }
}
