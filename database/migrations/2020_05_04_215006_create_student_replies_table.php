<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_replies', function (Blueprint $table) {
            $table->increments('id');
            $table->text('reply', 10000);
            $table->integer('lesson_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('teacher_id')->unsigned();
            $table->integer('session_id')->unsigned();
            $table->integer('term_id')->unsigned();
            $table->string('url');
            $table->boolean('status')->default(true);
            $table->timestamps();

            $table->foreign('lesson_id')->references('id')->on('lesson_notes')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('teacher_id')->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('session_id')->references('id')->on('sessions')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('term_id')->references('id')->on('term')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_replies');
    }
}
