<?php

use Illuminate\Database\Seeder;

class TermTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public $term;
    public function __construct()
    {
        //

        $this->term = [
        		'FIRST TERM' =>
        		[
        				'name' => 'First Term',
        				'active' => '1'
        		],

        		'SECOND TERM' =>
        		[
        				'name' => 'Second Term',
        				'active' => '0'
        		],

        		'THIRD TERM' =>
        		[
        				'name' => 'Third Term',
        				'active' => '0'
        		],
        ];

    }

    public function run()

    {
    	///create all term
        foreach ($this->term as $k => $v) {
            DB::table('term')->insert($v);
    }
}
}
