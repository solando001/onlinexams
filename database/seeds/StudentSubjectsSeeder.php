<?php

use Illuminate\Database\Seeder;

class StudentSubjectsSeeder extends Seeder
{
    public $subjects;

    public function __construct()
    {
        $this->subjects = [
            'ENG' =>
                [
                    'name' => 'English',
                    'display_name' => 'English Language',
                    'description' => 'This is English Language'
                ],
            'MAT' =>
                [
                    'name' => 'Mathematics',
                    'display_name' => 'Mathematics',
                    'description' => 'This is Mathematics'
                ],
        ];
    }

    public function run()
    {
        ///create all subjects
        foreach ($this->subjects as $k => $v) {
            DB::table('subjects')->insert($v);
        }
    }
}