<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public $settings;
    public function __construct()
    {
        //

        $this->settings = [
            'THEORY' =>
                [
                    'name' => 'Theory Question',
                    'amount' => '500',
                    'active' => '0'
                ],

            'STUDENT' =>
                [
                    'name' => 'Students',
                    'amount' => '500',
                    'active' => '1'
                ],
        ];

    }

    public function run()

    {
        ///create all term
        foreach ($this->settings as $k => $v) {
            DB::table('settings')->insert($v);
        }
    }
}
