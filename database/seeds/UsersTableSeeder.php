<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public $system_role;
    public $admin_role;

    public function __construct()
    {

        $this->system_role = App\Role::where('name', 'system')->first();
    }

    public function run()
    {

        $user = \App\User::create([
            'surname' => 'System',
            'other_names' => 'system',
            'username'=>'system',
            'email' => 'system/de/987',
            'password' => bcrypt('W3LisP42P4zGfLisP42P4zGf'),
            'active' => 1
        ]);

        DB::table('role_user')->insert([
            'user_id' => $user->id,
            'role_id' => $this->system_role->id,
        ]);

        $user = \App\User::create([
            'surname' => 'E-poral',
            'other_names' => 'System',
            'username'=>'system',
            'email' => 'system@gmail.com',
            'password' => bcrypt('Solomon001'),
            'active' => 1
        ]);

        DB::table('role_user')->insert([
            'user_id' => $user->id,
            'role_id' => $this->system_role->id,
        ]);

    }
}
