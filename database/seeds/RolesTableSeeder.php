<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class RolesTableSeeder extends Seeder

{
    /**
     * Run the database seeds.
     *
     * @return void
     *
     */
    public $roles;

    public function __construct()
    {
        $this->roles = [
            'system' =>
                [
                    'name' => 'system',
                    'display_name' => 'System role',
                    'description' => 'This is a system role, He manages the platform'
                ],
            'admin' =>
                [
                    'name' => 'admin',
                    'display_name' => 'Administrator Role',
                    'description' => 'This is an administrator. He has access and can alter/delete anything on the system'
                ],
            'student' =>
                [
                    'name' => 'student',
                    'display_name' => 'Student Role',
                    'description' => 'This is a student role.'
                ],
            'teacher' =>
                [
                    'name' => 'teacher',
                    'display_name' => 'Teacher Role',
                    'description' => 'This is a teacher role.'
                ],
        ];
    }

    public function run()
    {
        ///create all roles
        foreach ($this->roles as $k => $v) {
            DB::table('roles')->insert($v);
        }


    }
}
